# 镜像基于java:8
# FROM java:8
FROM anapsix/alpine-java:8_server-jre_unlimited

RUN mkdir -p /yth2
#创建项目日志存放的文件夹
RUN mkdir -p /yth2/logs
# 匿名挂载目录
# VOLUME /home/yth2
WORKDIR /yth2
# ADD *.jar yth2svr.jar
# 将jar包添加到容器中 /app/目录并更名为xxx.jar
ADD ./target/classes/config ./config
ADD ./target/lib ./lib
ADD ./target/yth2svr-0.0.1-SNAPSHOT.jar ./yth2svr.jar
EXPOSE 8888
#ENTRYPOINT [ "java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/yth2svr.jar" ]
# 运行jar包命令 "nohup" "&" 可省略
# ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/home/yth2/yth2svr.jar", "--spring.profiles.active=dev", "--server.port=8888"]
ENTRYPOINT ["java","-Dfile.encoding=UTF-8","-jar","yth2svr.jar", "--server.port=8888"]

# CMD sleep 20;java -jar -Dfile.encoding=UTF-8 yth2svr.jar >nohup.out 2>&1 &


