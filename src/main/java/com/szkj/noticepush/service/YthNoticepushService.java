package com.szkj.noticepush.service;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.application.entity.YthApplicationProMine;
import com.szkj.auth.service.AuthService;
import com.szkj.common.util.UpdateUtil;
import com.szkj.noticepush.entity.CtpUserHistoryMessage;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.reposi.CUHMReposi;
import com.szkj.noticepush.reposi.YthNoticepushReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Yang
 * @since 2022-04-22 15:30:00
 */
@Slf4j
@Service
public class YthNoticepushService {
    @Autowired
    private YthNoticepushReposi reposi;
    @Autowired
    private CUHMReposi cuhmReposi;
    @Autowired
    private AuthService authService;

    @Transactional(rollbackFor = Exception.class)
    public YthNoticePush append(YthNoticePush bean,String token) throws Exception {
        try {
            JSONObject juserInfo =authService.getLoginInfo(token);
            bean.setCreateBy(juserInfo.get("userId").toString())
                    .setUpdateBy(juserInfo.get("userId").toString())
                    .setCreateName(juserInfo.get("userName").toString())
                    .setUpdateName(juserInfo.get("userName").toString());
            reposi.save(bean);
            return bean;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }


    @Transactional(rollbackFor = Exception.class)
    public String modify(JSONObject body, int userId) throws Exception {
        try {
            String ids = (String) body.get("id");
            String pushstatus = (String) body.get("pushStatus");

            String[] id = null;String[] pushStatus = null;
            if (ids != null && ids != "") {
                if (ids.contains(",")) {
                    id = ids.split(",");
                } else {
                    id=new String[]{ids};
                }
            }
            if (pushstatus != null && pushstatus != "") {
                if (pushstatus.contains(",")) {
                    pushStatus = pushstatus.split(",");
                } else {
                    pushStatus=new String[]{pushstatus};
                }
            }
            if(pushStatus.length==id.length){
                for (int i = 0; i < id.length; i++) {
                    YthNoticePush oldbean=reposi.findById(Long.parseLong(id[i])).get();
                    YthNoticePush bean =  new YthNoticePush();
                    bean.setId(Long.parseLong(id[i]));
                    bean.setPushStatus(Integer.parseInt(pushStatus[i]));
                    Calendar calendar = Calendar.getInstance();
                    long time = calendar.getTimeInMillis();
                    bean.setUpdateBy(userId+"").setUpdateTime(time);
                    UpdateUtil.copyNullProperties(bean,oldbean);  //修改的字段覆盖原对象
                    reposi.save(oldbean);
                }
                return "success";
            }else{
                return "传参的id和状态数据不匹配";
            }

        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }


    @Transactional(rollbackFor = Exception.class)
    public String cuhmModify(JSONObject body) throws Exception {
        try {
            String ids = (String) body.get("id");
            String pushstatus = (String) body.get("pushStatus");

            String[] id = null;String[] pushStatus = null;
            if (ids != null && ids != "") {
                if (ids.contains(",")) {
                    id = ids.split(",");
                } else {
                    id=new String[]{ids};
                }
            }
            if (pushstatus != null && pushstatus != "") {
                if (pushstatus.contains(",")) {
                    pushStatus = pushstatus.split(",");
                } else {
                    pushStatus=new String[]{pushstatus};
                }
            }
            if(pushStatus.length==id.length){
                for (int i = 0; i < id.length; i++) {
                    CtpUserHistoryMessage oldbean=cuhmReposi.findById(Long.parseLong(id[i])).get();
                    CtpUserHistoryMessage bean =  new CtpUserHistoryMessage();
                    bean.setId(Long.parseLong(id[i]));
                    bean.setPushStatus(Integer.parseInt(pushStatus[i]));
                    UpdateUtil.copyNullProperties(bean,oldbean);  //修改的字段覆盖原对象
                    cuhmReposi.save(oldbean);
                }
                return "success";
            }else{
                return "传参的id和状态数据不匹配";
            }

        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

}
