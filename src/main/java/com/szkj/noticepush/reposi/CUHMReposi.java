package com.szkj.noticepush.reposi;

import com.szkj.noticepush.entity.CtpUserHistoryMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CUHMReposi extends JpaRepository<CtpUserHistoryMessage, Long>, JpaSpecificationExecutor<CtpUserHistoryMessage> {
}
