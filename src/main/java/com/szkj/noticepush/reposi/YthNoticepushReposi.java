package com.szkj.noticepush.reposi;


import com.szkj.noticepush.entity.YthNoticePush;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * 持久层
 *
 * @author yang
 * @since 2022-04-20 15:14:24
 */
public interface YthNoticepushReposi extends JpaRepository<YthNoticePush, Long>, JpaSpecificationExecutor<YthNoticePush> {

    @Query(value = "select * from yth_notice_push where push_status=0 and push_user=?1", nativeQuery = true)
    List<YthNoticePush> getNotPush(@Param("pushUserId") Integer pushUserId);
}

