package com.szkj.noticepush.controller;


import cn.hutool.json.JSONObject;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.noticepush.service.YthNoticepushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * 控制层
 *
 * @author yang
 * @since 2022-04-20 15:39:28
 */
@Slf4j
@RestController
@RequestMapping("/noticepush")
@CrossOrigin("*")
public class YthNoticepushController {
    @Autowired
    private YthNoticepushService service;
    @Autowired
    private YthOperationLogService logservice;
    @Autowired
    private AuthService authService;

    @PostMapping("/change_push_status")
    public String modify(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId = authService.getUserInfo(_token).getInt("userId");
        try {
            String back_result = service.modify(body, userId);
            String result =null;
            if("success".equals(back_result)){
                result = RespUtil.success("成功", back_result);
            }else{
                result = RespUtil.error("失败", back_result);
            }

            //操作日志
            logservice.append(userId, "update", "noticepush", "change_push_status", body.toString(), result, "", userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "update", "noticepush", "change_push_status", body.toString(), "", e.toString(), userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }

    @PostMapping("/change_cuhm_push_status")
    public String cuhmModify(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId = authService.getUserInfo(_token).getInt("userId");
        try {
            String back_result = service.cuhmModify(body);
            String result =null;
            if("success".equals(back_result)){
                result = RespUtil.success("成功", back_result);
            }else{
                result = RespUtil.error("失败", back_result);
            }

            //操作日志
            logservice.append(userId, "update", "noticepush", "change_cuhm_push_status", body.toString(), result, "", userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "update", "noticepush", "change_cuhm_push_status", body.toString(), "", e.toString(), userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }
}

