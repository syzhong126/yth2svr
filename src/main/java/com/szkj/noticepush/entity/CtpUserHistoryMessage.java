package com.szkj.noticepush.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年07月12日 9:59
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "ctp_user_history_message")
public class CtpUserHistoryMessage {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "reference_id")
    private Long referenceId;
    @Column(name = "sender_id")
    private Long senderId;
    @Column(name = "receiver_id")
    private Long receiverId;
    @Column(name = "message_category")
    private Integer messageCategory;
    @Column(name = "message_type")
    private Byte messageType;
    @Column(name = "message_content")
    private String messageContent;
    @Column(name = "creation_date")
    private Timestamp creationDate;
    @Column(name = "link_type")
    private String linkType;
    @Column(name = "link_param_0")
    private String linkParam0;
    @Column(name = "link_param_1")
    private String linkParam1;
    @Column(name = "link_param_2")
    private String linkParam2;
    @Column(name = "link_param_3")
    private String linkParam3;
    @Column(name = "link_param_4")
    private String linkParam4;
    @Column(name = "link_param_5")
    private String linkParam5;
    @Column(name = "link_param_6")
    private String linkParam6;
    @Column(name = "link_param_7")
    private String linkParam7;
    @Column(name = "link_param_8")
    private String linkParam8;
    @Column(name = "link_param_9")
    private String linkParam9;
    @Column(name = "open_type")
    private Byte openType;
    @Column(name = "is_read")
    private Byte isRead;
    @Column(name = "important_level")
    private int importantLevel;
    @Column(name = "IS_AT")
    private Short isAt;
    @Column(name = "IS_REPLY")
    private Short isReply;
    @Column(name = "IS_TRACK")
    private Short isTrack;
    @Column(name = "IS_TEMPLATE")
    private Short isTemplate;
    @Column(name = "TEMPLATE_ID")
    private Long templateId;
    @Column(name = "POP_PARAMS")
    private String popParams;
    @Column(name = "POP_TYPE")
    private Short popType;
    @Column(name = "SOURCE_TYPE")
    private Short sourceType;
    @Column(name = "push_status")
    private Integer pushStatus;
}
