package com.szkj.noticepush.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年04月22日 10:49
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_notice_push")
public class YthNoticePush {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "push_type")
    private Integer pushType;
    @Column(name = "content")
    private String content;
    @Column(name = "push_time", nullable = false)
    private Long pushTime;
    @Column(name = "push_status")
    private Integer pushStatus;
    @Column(name = "push_user")
    private Integer pushUser;
    @Column(name = "link")
    private String link;
    @Column(name = "title")
    private String title;
    @Column(name = "from_system")
    private String fromSystem;
    @Column(name = "moudle_detail")
    private String moudleDetail;
    @Column(name = "moudle_type")
    private String moudleType;
    @Column(name = "msg_type")
    private Integer msgType;
    @Column(name = "del_flag")
    private Integer delFlag;
    /**
     * 创建人
     */
    @Column(name = "create_by", nullable = false)
    private String createBy;

    @Column(name = "create_name", nullable = false)
    private String createName;
    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime;

    /**
     * 修改人
     */
    @Column(name = "update_by", nullable = false)
    private String updateBy;

    @Column(name = "update_name", nullable = false)
    private String updateName;
    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Long updateTime;
}
