package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeDownloadReq;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface YthKnowledgeDownloadReqReposi extends CrudRepository<YthKnowledgeDownloadReq, Long>, JpaSpecificationExecutor<YthKnowledgeDownloadReq> {

    @Query(value = "select * from yth_knowledge_download_req where knowledge_id = ?1 and create_by = ?2", nativeQuery = true)
    YthKnowledgeDownloadReq findByKnowledgeIdAndCreateBy(Long knowledgeId, int createBy);
}