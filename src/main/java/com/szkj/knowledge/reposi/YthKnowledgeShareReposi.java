package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeShare;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface YthKnowledgeShareReposi extends CrudRepository<YthKnowledgeShare, Long>, JpaSpecificationExecutor<YthKnowledgeShare> {

}