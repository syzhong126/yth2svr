package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeDownloadRec;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface YthKnowledgeDownloadRecReposi extends CrudRepository<YthKnowledgeDownloadRec, Long>, JpaSpecificationExecutor<YthKnowledgeDownloadRec> {

}