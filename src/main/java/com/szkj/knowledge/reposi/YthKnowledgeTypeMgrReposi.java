package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeTypeMgr;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface YthKnowledgeTypeMgrReposi extends CrudRepository<YthKnowledgeTypeMgr, Integer>, JpaSpecificationExecutor<YthKnowledgeTypeMgr> {

    @Query(value = "select * from yth_knowledge_type_mgr where user_id = ?1", nativeQuery = true)
    List<YthKnowledgeTypeMgr> findByUserId(int userId);
}