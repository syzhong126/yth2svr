package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeStatRankDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface YthKnowledgeStatRankDetailReposi extends JpaRepository<YthKnowledgeStatRankDetail, Integer>, JpaSpecificationExecutor<YthKnowledgeStatRankDetail> {

}