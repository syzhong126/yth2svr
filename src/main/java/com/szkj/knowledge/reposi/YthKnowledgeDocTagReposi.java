package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeDocTag;
import com.szkj.knowledge.entity.YthKnowledgeTag;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface YthKnowledgeDocTagReposi extends CrudRepository<YthKnowledgeDocTag, Long>, JpaSpecificationExecutor<YthKnowledgeDocTag> {

    @Transactional
    void deleteByKnowledgeIdAndTagId(Long knowledgeId, Long tagId);

    @Transactional
    void deleteByKnowledgeId(Long knowledgeId);

    List<YthKnowledgeDocTag> findByTagId(Long tagId);
}