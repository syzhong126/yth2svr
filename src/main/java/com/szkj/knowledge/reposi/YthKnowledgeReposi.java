package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledge;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface YthKnowledgeReposi extends CrudRepository<YthKnowledge, Long>, JpaSpecificationExecutor<YthKnowledge> {

    @Query(value = "select * from yth_knowledge where is_del = 0 and path_code like ?1% and id = ?2", nativeQuery = true)
    YthKnowledge findByPathCodePrefixAndKnowledgeId(String pathCodePrefix, Long knowledgeId);

    @Query(value = "select * from yth_knowledge where is_del = 0 and path_code like ?1%", nativeQuery = true)
    List<YthKnowledge> findByPathCodePrefix(String pathCodePrefix);

    @Query(value = "select * from yth_knowledge where is_del = 0 and path_code like ?1% and id != ?2", nativeQuery = true)
    List<YthKnowledge> findChildrenByPathCodePrefix(String pathCodePrefix, Long knowledgeId);

    @Query(value = "select * from yth_knowledge where path_code like ?1%", nativeQuery = true)
    List<YthKnowledge> findAllByPathCodePrefix(String pathCodePrefix);

    @Query(value = "select * from yth_knowledge where is_del = 0 and parent_id = ?1 order by path_code desc limit 1", nativeQuery = true)
    YthKnowledge findByParentIdAndMaxPathCode(Long knowledgeId);

    @Query(value = "select * from yth_knowledge where is_del = 0 and parent_id = ?1", nativeQuery = true)
    List<YthKnowledge> findByParentId(Long knowledgeId);

    @Query(value = "select * from yth_knowledge where is_del = 0 and parent_id = ?1 and type = ?2", nativeQuery = true)
    List<YthKnowledge> findByParentIdAndType(Long knowledgeId, int type);

    @Query(value = "select * from yth_knowledge where is_del = 0 and is_issued = 2 and type = 1", nativeQuery = true)
    List<YthKnowledge> findAllIssuedDoc();

    @Query(value = "select * from yth_knowledge where is_del = 0 and is_issued = 2 and type = 1 order by ext_3 desc limit ?1", nativeQuery = true)
    List<YthKnowledge> findTopDocs(int topNum);

    @Query(value = "select * from yth_knowledge where id = ?1", nativeQuery = true)
    YthKnowledge getParentNameByParentId(Long knowledgeId);

    @Query(value = "select * from yth_knowledge where path_code = ?1", nativeQuery = true)
    YthKnowledge findByPathCode(String pathCode);
}