package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeTag;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface YthKnowledgeTagReposi extends CrudRepository<YthKnowledgeTag, Long>, JpaSpecificationExecutor<YthKnowledgeTag> {

    @Query(value = "select * from yth_knowledge_tag where tag=?1", nativeQuery = true)
    YthKnowledgeTag finByTag(String tag);

    @Query(value = "select * from yth_knowledge_tag where has_taboo = 0", nativeQuery = true)
    List<YthKnowledgeTag> findAllUnckeckedTag();

    @Query(value = "select * from yth_knowledge_tag where has_taboo <> 0", nativeQuery = true)
    List<YthKnowledgeTag> findAllCkeckedTag();

    @Query(value = "SELECT a.* " +
            "FROM yth_knowledge_tag a " +
            "INNER JOIN yth_knowledge_doc_tag b ON a.tag_id = b.tag_id " +
            "INNER JOIN yth_knowledge c ON b.knowledge_id = c.id " +
            "WHERE a.is_approved = 1 AND c.is_del = 0 AND c.is_issued = 2 " +
            "GROUP BY a.tag_id " +
            "ORDER BY COUNT(c.id) DESC " +
            "LIMIT ?1", nativeQuery = true)
    List<YthKnowledgeTag> findTopRelevancy(int topNum);

    @Query(value = "SELECT a.* " +
            "FROM yth_knowledge_tag a " +
            "INNER JOIN yth_knowledge_doc_tag b on a.tag_id = b.tag_id " +
            "WHERE b.knowledge_id = ?1", nativeQuery = true)
    List<YthKnowledgeTag> findByKnowledgeId(long knowledgeId);
}