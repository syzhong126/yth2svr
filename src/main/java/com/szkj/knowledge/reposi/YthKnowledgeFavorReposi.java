package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeFavor;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface YthKnowledgeFavorReposi extends CrudRepository<YthKnowledgeFavor, Long>, JpaSpecificationExecutor<YthKnowledgeFavor> {

    @Query(value = "select * from yth_knowledge_favor where knowledge_id = ?1 and create_by = ?2", nativeQuery = true)
    YthKnowledgeFavor findByKnowledgeIdAndCreateBy(Long knowledgeId, int createBy);
}