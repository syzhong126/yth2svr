package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeShareScope;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface YthKnowledgeShareScopeReposi extends CrudRepository<YthKnowledgeShareScope, Long>, JpaSpecificationExecutor<YthKnowledgeShareScope> {

    /**
     * 根据createBy进行删除
     * @param knowledgeId
     */
    @Transactional
    void deleteByKnowledgeId(Long knowledgeId);

    /**
     * 可判断普通用户是否有查看类别目录的权限，无权限则返回null，有权限则返回关联记录。
     * @param knowledgeId
     * @param unitId
     * @param deptId
     * @param postId
     * @param userId
     */
    @Query(value = "select * " +
                   "from yth_knowledge_share_scope " +
                   "where knowledge_id = ?1 and type = 0 and can_read = 1 " +
                   "and ((scope_type = 1 and id = ?2) or (scope_type = 2 and id = ?3) or (scope_type = 3 and id = ?4) or (scope_type = 4 and id = ?5))", nativeQuery = true)
    YthKnowledgeShareScope findByDirIdAndCanReadAndUserInfo(Long knowledgeId, int unitId, int deptId, int postId, int userId);

    /**
     * 可判断普通用户是否有写入指定的类别目录的权限，无权限则返回null，有权限则返回关联记录。
     * @param knowledgeId 指定的类别目录ID
     * @param unitId 用户所属公司ID
     * @param deptId 用户所属部门ID
     * @param postId 用户所属岗位ID
     * @param userId 用户ID
     */
    @Query(value = "select * " +
                   "from yth_knowledge_share_scope " +
                   "where knowledge_id = ?1 and type = 0 and can_write = 1 " +
                   "and ((scope_type = 1 and id = ?2) or (scope_type = 2 and id = ?3) or (scope_type = 3 and id = ?4) or (scope_type = 4 and id = ?5))", nativeQuery = true)
    YthKnowledgeShareScope findByDirIdAndCanWriteAndUserInfo(Long knowledgeId, int unitId, int deptId, int postId, int userId);

    @Query(value = "select * from yth_knowledge_share_scope where knowledge_id = ?1", nativeQuery = true)
    List<YthKnowledgeShareScope> findByKnowledgeId(Long knowledgeId);

}