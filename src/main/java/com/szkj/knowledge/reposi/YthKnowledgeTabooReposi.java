package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeTaboo;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface YthKnowledgeTabooReposi extends CrudRepository<YthKnowledgeTaboo, Long>, JpaSpecificationExecutor<YthKnowledgeTaboo> {

}