package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeStatCloud;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface YthKnowledgeStatCloudReposi extends CrudRepository<YthKnowledgeStatCloud, Integer>, JpaSpecificationExecutor<YthKnowledgeStatCloud> {

    @Query(value = "select * from yth_knowledge_stat_cloud order by create_time desc limit 1", nativeQuery = true)
    YthKnowledgeStatCloud findNewest();
}