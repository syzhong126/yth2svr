package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeBrowse;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface YthKnowledgeBrowseReposi extends CrudRepository<YthKnowledgeBrowse, Long>, JpaSpecificationExecutor<YthKnowledgeBrowse> {

    @Query(value = "select * from yth_knowledge_browse where knowledge_id = ?1 and create_by = ?2", nativeQuery = true)
    YthKnowledgeBrowse findByKnowledgeIdAndCreateBy(Long knowledgeId, int createBy);
}