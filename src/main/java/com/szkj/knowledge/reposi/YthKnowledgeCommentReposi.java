package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgeComment;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface YthKnowledgeCommentReposi extends CrudRepository<YthKnowledgeComment, Long>, JpaSpecificationExecutor<YthKnowledgeComment> {

    @Query(value = "select * from yth_knowledge_comment where has_taboo = 0", nativeQuery = true)
    List<YthKnowledgeComment> findAllUnckeckedComment();

    @Query(value = "select * from yth_knowledge_comment where has_taboo <> 0", nativeQuery = true)
    List<YthKnowledgeComment> findAllCkeckedComment();
}