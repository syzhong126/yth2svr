package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthKnowledgePraise;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface YthKnowledgePraiseReposi extends CrudRepository<YthKnowledgePraise, Long>, JpaSpecificationExecutor<YthKnowledgePraise> {

    @Query(value = "select * from yth_knowledge_praise where knowledge_id = ?1 and create_by = ?2", nativeQuery = true)
    YthKnowledgePraise findByKnowledgeIdAndCreateBy(Long knowledgeId, int createBy);

    void deleteByKnowledgeIdAndCreateBy(Long knowledgeId, int createBy);
}