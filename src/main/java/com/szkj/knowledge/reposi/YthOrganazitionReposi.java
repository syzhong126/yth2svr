package com.szkj.knowledge.reposi;

import com.szkj.knowledge.entity.YthOrganazition;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface YthOrganazitionReposi extends CrudRepository<YthOrganazition, Integer>, JpaSpecificationExecutor<YthOrganazition> {

    @Query(value = "SELECT a.* " +
            "FROM yth_organazition a " +
            "WHERE a.is_del = 0 AND a.org_id IN ( " +
            "  SELECT c.parent_id " +
            "  FROM yth_user a " +
            "  JOIN yth_user_org b ON a.user_id = b.user_id " +
            "  JOIN yth_organazition c ON b.org_id2 = c.org_id AND c.is_del = 0 " +
            "  WHERE a.is_del = 0 " +
            ") " +
            "UNION " +
            "SELECT DISTINCT c.* " +
            "FROM yth_user a " +
            "JOIN yth_user_org b ON a.user_id = b.user_id " +
            "JOIN yth_organazition c ON b.org_id2 = c.org_id AND c.is_del = 0 " +
            "WHERE a.is_del = 0", nativeQuery = true)
    List<YthOrganazition> findAllUnitAndDept();

    @Query(value = "SELECT a.* " +
            "FROM yth_organazition a " +
            "WHERE a.org_id = ?1 AND a.is_del = 0 " +
            "UNION " +
            "SELECT a.* " +
            "FROM yth_organazition a " +
            "JOIN yth_organazition b ON a.parent_id = b.org_id AND b.org_type = 2 AND b.is_del = 0 " +
            "WHERE b.org_id = ?1 AND a.is_del = 0", nativeQuery = true)
    List<YthOrganazition> findUnitAndDept(int unitId);
}