package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 热门文档排行榜数据明细表
 */
@Data
@Entity
@Table(name = "yth_knowledge_stat_rank_detail")
public class YthKnowledgeStatRankDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 明细ID
     */
    @Id
    @Column(name = "detail_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer detailId;

    /**
     * 排行榜ID
     */
    @Column(name = "rank_id", nullable = false)
    private Integer rankId;

    /**
     * 知识文档ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 排名序号
     */
    @Column(name = "order_no", nullable = false)
    private Integer orderNo = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

}
