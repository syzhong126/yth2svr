package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文档下载记录表
 */
@Data
@Entity
@Table(name = "yth_knowledge_download_rec")
public class YthKnowledgeDownloadRec implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 下载记录ID
     */
    @Id
    @Column(name = "rec_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long recId;

    /**
     * 下载申请ID
     */
    @Column(name = "req_id", nullable = false)
    private Long reqId;

    /**
     * 文档ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

}
