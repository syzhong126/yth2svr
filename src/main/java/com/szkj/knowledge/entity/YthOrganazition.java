package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "yth_organazition")
public class YthOrganazition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "org_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orgId;

    @Column(name = "org_name")
    private String orgName;

    @Column(name = "org_level")
    private Integer orgLevel;

    @Column(name = "org_code")
    private String orgCode;

    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 1 公司，2 部门
     */
    @Column(name = "org_type")
    private Integer orgType;

    /**
     * 简称
     */
    @Column(name = "short_name")
    private String shortName;

    /**
     * 外部ID，如数据来源主键ID
     */
    @Column(name = "ext_id")
    private String extId;

    /**
     * 删除标记0否1是
     */
    @Column(name = "is_del", nullable = false)
    private Boolean del = Boolean.FALSE;

    /**
     * 对应的hr编码
     */
    @Column(name = "new_hr_id", nullable = false)
    private String newHrId = "";

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "is_leaf")
    private Boolean leaf = Boolean.TRUE;

}
