package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 标签表
 */
@Data
@Entity
@Table(name = "yth_knowledge_tag")
public class YthKnowledgeTag implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标签ID
     */
    @Id
    @Column(name = "tag_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long tagId;

    /**
     * 标签
     */
    @Column(name = "tag", nullable = false)
    private String tag = "";

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

    /**
     * 包含禁词标识 0:不包含 1:包含
     */
    @Column(name = "has_taboo", nullable = false)
    private Integer hasTaboo = 0;

    /**
     * 检查禁词时间
     */
    @Column(name = "checked_time", nullable = false)
    private Long checkedTime = 0L;

    /**
     * 审核状态标识 0:未审核 1:审核通过 2:审核不通过
     */
    @Column(name = "is_approved", nullable = false)
    private Integer isApproved = 0;

    /**
     * 一体化平台用户ID
     */
    @Column(name = "approved_by", nullable = false)
    private Integer approvedBy = 0;

    /**
     * 审核时间
     */
    @Column(name = "approved_time", nullable = false)
    private Long approvedTime = 0L;

}
