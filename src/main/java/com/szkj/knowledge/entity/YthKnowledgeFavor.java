package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文档收藏记录表
 */
@Data
@Entity
@Table(name = "yth_knowledge_favor")
public class YthKnowledgeFavor implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 收藏记录ID
     */
    @Id
    @Column(name = "favor_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long favorId;

    /**
     * 文档ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

}
