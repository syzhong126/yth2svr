package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 禁词表
 */
@Data
@Entity
@Table(name = "yth_knowledge_taboo")
public class YthKnowledgeTaboo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 禁词ID
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long tabooId;

    /**
     * 禁词
     */
    @Column(name = "taboo", nullable = false)
    private String taboo = "";

    /**
     * 一体化平台用户姓名
     */
    @Column(name = "create_name", nullable = false)
    private String createName = "";

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private String createBy = "";

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

    /**
     * 一体化平台用户姓名
     */
    @Column(name = "update_name", nullable = false)
    private String updateName = "";

    /**
     * 一体化平台用户ID
     */
    @Column(name = "update_by", nullable = false)
    private String updateBy = "";

    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Long updateTime = 0L;

}
