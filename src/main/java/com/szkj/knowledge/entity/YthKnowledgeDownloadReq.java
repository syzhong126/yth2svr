package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文档下载申请表
 */
@Data
@Entity
@Table(name = "yth_knowledge_download_req")
public class YthKnowledgeDownloadReq implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 下载申请ID
     */
    @Id
    @Column(name = "req_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long reqId;

    /**
     * 文档ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

    /**
     * 审核状态标识 0:未审核 1:审核通过 2:审核不通过
     */
    @Column(name = "is_approved", nullable = false)
    private Integer isApproved = 0;

    /**
     * 一体化平台用户ID
     */
    @Column(name = "approved_by", nullable = false)
    private Integer approvedBy = 0;

    /**
     * 审核时间
     */
    @Column(name = "approved_time", nullable = false)
    private Long approvedTime = 0L;

}
