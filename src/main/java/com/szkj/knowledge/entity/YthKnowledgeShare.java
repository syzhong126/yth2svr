package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文档分享记录表
 */
@Data
@Entity
@Table(name = "yth_knowledge_share")
public class YthKnowledgeShare implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文档分享记录ID
     */
    @Id
    @Column(name = "share_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long shareId;

    /**
     * 文档ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 分享方式 0:推荐到精华库 1:推荐给用户
     */
    @Column(name = "share_type", nullable = false)
    private Integer shareType = 0;

    /**
     * 分享的用户 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

}
