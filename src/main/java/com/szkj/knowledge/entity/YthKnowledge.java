package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 知识类别目录和知识文档
 */
@Data
@Entity
@Table(name = "yth_knowledge")
public class YthKnowledge implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 知识ID
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long knowledgeId;

    /**
     * 类型 0:类别 1:文档
     */
    @Column(name = "type", nullable = false)
    private Integer type = 0;

    /**
     * 父ID
     */
    @Column(name = "parent_id", nullable = false)
    private Long parentId = 0L;

    /**
     * 层级
     */
    @Column(name = "level", nullable = false)
    private Integer level = 0;

    /**
     * 层级路径编码，每层用3位数字表示
     */
    @Column(name = "path_code", nullable = false)
    private String pathCode = "";

    /**
     * 知识名称
     */
    @Column(name = "name", nullable = false)
    private String name = "";

    /**
     * 文档作者
     */
    @Column(name = "author", nullable = false)
    private String author = "";

    /**
     * 文档大小 单位:B
     */
    @Column(name = "doc_size", nullable = false)
    private BigDecimal docSize = BigDecimal.ZERO;

    /**
     * 文档大小
     */
    @Column(name = "size_name", nullable = false)
    private String docSizeName = "";

    /**
     * 文档存储桶
     */
    @Column(name = "bucket", nullable = false)
    private String bucket = "";

    /**
     * 文档存储名
     */
    @Column(name = "file_name", nullable = false)
    private String fileName = "";

    /**
     * 文档存储路径
     */
    @Column(name = "save_path", nullable = false)
    private String savePath = "";

    /**
     * 文档下载地址
     */
    @Column(name = "download_url", nullable = false)
    private String downloadUrl = "";

    /**
     * 文档可下载标识 0:不可下载 1:可下载
     */
    @Column(name = "download", nullable = false)
    private Integer download = 0;

    /**
     * 文档下载须审核标识 0:不须审核 1:须审核
     */
    @Column(name = "download_check", nullable = false)
    private Integer downloadCheck = 0;

    /**
     * 文档阅读量
     */
    @Column(name = "browse_num", nullable = false)
    private Integer browseNum = 0;

    /**
     * 文档下载量
     */
    @Column(name = "download_num", nullable = false)
    private Integer downloadNum = 0;

    /**
     * 文档收藏量
     */
    @Column(name = "favor_num", nullable = false)
    private Integer favorNum = 0;

    /**
     * 文档点赞量
     */
    @Column(name = "praise_num", nullable = false)
    private Integer praiseNum = 0;

    /**
     * 文档分享量
     */
    @Column(name = "share_num", nullable = false)
    private Integer shareNum = 0;

    /**
     * 文档推荐到精华库的量
     */
    @Column(name = "essence_num", nullable = false)
    private Integer essenceNum = 0;

    /**
     * 文档评论量
     */
    @Column(name = "comment_num", nullable = false)
    private Integer commentNum = 0;

    /**
     * 文档发布状态标识 0:草稿 1:待审核发布 2:已发布 3:不予发布
     */
    @Column(name = "is_issued", nullable = false)
    private Integer isIssued = 0;

    /**
     * 文档精华标识 0:普通 1:精华
     */
    @Column(name = "is_essence", nullable = false)
    private Integer isEssence = 0;

    /**
     * 重点关注标识 0:普通 1:重点关注
     */
    @Column(name = "is_focus", nullable = false)
    private Integer isFocus = 0;

/**
 * 最新发布标识 0:普通 1:最新发布
 */
    @Column(name = "is_new", nullable = false)
    private Integer isNew = 0;

    /**
     * 删除标识 0:正常 1:已删除
     */
    @Column(name = "is_del", nullable = false)
    private Integer isDel = 0;

    /**
     * 二级类别目录对应的一体化平台公司ID
     */
    @Column(name = "ext_1", nullable = false)
    private String ext1 = "";

    /**
     * 已审核的标签字段，多个用,分隔。
     */
    @Column(name = "ext_2", nullable = false)
    private String ext2 = "";

    /**
     * 扩展字段3
     */
    @Column(name = "ext_3", nullable = false)
    private String ext3 = "";

    /**
     * 扩展字段4
     */
    @Column(name = "ext_4", nullable = false)
    private String ext4 = "";

    /**
     * 扩展字段5
     */
    @Column(name = "ext_5", nullable = false)
    private String ext5 = "";

    /**
     * 扩展字段6
     */
    @Column(name = "ext_6", nullable = false)
    private String ext6 = "";

    /**
     * 扩展字段7
     */
    @Column(name = "ext_7", nullable = false)
    private String ext7 = "";

    /**
     * 扩展字段8
     */
    @Column(name = "ext_8", nullable = false)
    private String ext8 = "";

    /**
     * 扩展字段9
     */
    @Column(name = "ext_9", nullable = false)
    private String ext9 = "";

    /**
     * 扩展字段10
     */
    @Column(name = "ext_10", nullable = false)
    private String ext10 = "";

    /**
     * 一体化平台用户姓名
     */
    @Column(name = "create_name", nullable = false)
    private String createName = "";

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private String createBy = "";

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

    /**
     * 一体化平台用户姓名
     */
    @Column(name = "update_name", nullable = false)
    private String updateName = "";

    /**
     * 一体化平台用户ID
     */
    @Column(name = "update_by", nullable = false)
    private String updateBy = "";

    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Long updateTime = 0L;

    /**
     * 是否包含子类别 0:否 1:是
     */
    @Column(name = "has_child", nullable = false)
    private String hasChild = "";
}
