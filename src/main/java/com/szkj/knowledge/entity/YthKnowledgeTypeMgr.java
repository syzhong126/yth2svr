package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 二级类别与文档管理员关联表
 */
@Data
@Entity
@Table(name = "yth_knowledge_type_mgr")
public class YthKnowledgeTypeMgr implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户角色关联ID
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer mapId;

    /**
     * 二级类别ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 一体化平台用户ID
     */
    @Column(name = "user_id", nullable = false)
    private Integer userId = 0;

    /**
     * 一体化平台用户姓名
     */
    @Column(name = "create_name", nullable = false)
    private String createName = "";

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private String createBy = "";

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

}
