package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文档标签云图数据表
 */
@Data
@Entity
@Table(name = "yth_knowledge_stat_cloud")
public class YthKnowledgeStatCloud implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 云图ID
     */
    @Id
    @Column(name = "cloud_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer cloudId;

    /**
     * 云图数据
     */
    @Column(name = "data_txt", nullable = false)
    private String dataTxt;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

}
