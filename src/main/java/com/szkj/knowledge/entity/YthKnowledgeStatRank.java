package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 热门文档排行榜数据表
 */
@Data
@Entity
@Table(name = "yth_knowledge_stat_rank")
public class YthKnowledgeStatRank implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 排行榜ID
     */
    @Id
    @Column(name = "rank_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rankId;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

}
