package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文档与标签关联表
 */
@Data
@Entity
@Table(name = "yth_knowledge_doc_tag")
public class YthKnowledgeDocTag implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文档标签关联ID
     */
    @Id
    @Column(name = "map_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long mapId;

    /**
     * 文档ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 标签ID
     */
    @Column(name = "tag_id", nullable = false)
    private Long tagId = 0L;

    /**
     * 是否创建标签 0:否 1:是
     */
    @Column(name = "is_create_tag", nullable = false)
    private Boolean createTag = Boolean.FALSE;

}
