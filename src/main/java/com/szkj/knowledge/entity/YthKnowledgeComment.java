package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "yth_knowledge_comment")
public class YthKnowledgeComment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 评论记录ID
     */
    @Id
    @Column(name = "comment_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commentId;

    /**
     * 文档ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 评论
     */
    @Column(name = "comment", nullable = false)
    private String comment = "";

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

    /**
     * 含禁词标识 0:未检查 1:不包含 2:包含
     */
    @Column(name = "has_taboo", nullable = false)
    private Integer hasTaboo = 0;

    /**
     * 检查禁词时间
     */
    @Column(name = "checked_time", nullable = false)
    private Long checkedTime = 0L;

    /**
     * 审核状态标识 0:未审核 1:审核通过 2:审核不通过
     */
    @Column(name = "is_approved", nullable = false)
    private Integer isApproved = 0;

    /**
     * 一体化平台用户ID
     */
    @Column(name = "approved_by", nullable = false)
    private Integer approvedBy = 0;

    /**
     * 审核时间
     */
    @Column(name = "approved_time", nullable = false)
    private Long approvedTime = 0L;

}
