package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 三级及三级以下的类别和文档与共享对象的关联表
 */
@Data
@Entity
@Table(name = "yth_knowledge_share_scope")
public class YthKnowledgeShareScope implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 类别共享单位关联ID
     */
    @Id
    @Column(name = "map_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long mapId;

    /**
     * 知识ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 类型 0:类别 1:文档
     */
    @Column(name = "type", nullable = false)
    private Integer type = 0;

    /**
     * 范围类型 1:公司 2:部门 3:岗位 4:用户
     */
    @Column(name = "scope_type", nullable = false)
    private Integer scopeType = 0;

    /**
     * 一体化平台单位ID
     */
    @Column(name = "id", nullable = false)
    private Integer id = 0;

    /**
     * 文档分享记录ID
     */
    @Column(name = "share_id", nullable = false)
    private Long shareId = 0L;

    /**
     * 类别可查看标识 0:不可查看 1:可查看
     */
    @Column(name = "can_read", nullable = false)
    private Integer canRead = 0;

    /**
     * 类别可写入标识 0:不可写入 1:可写入
     */
    @Column(name = "can_write", nullable = false)
    private Integer canWrite = 0;

}
