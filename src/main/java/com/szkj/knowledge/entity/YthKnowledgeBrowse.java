package com.szkj.knowledge.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文档阅读记录表
 */
@Data
@Entity
@Table(name = "yth_knowledge_browse")
public class YthKnowledgeBrowse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 浏览记录ID
     */
    @Id
    @Column(name = "browse_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long browseId;

    /**
     * 文档ID
     */
    @Column(name = "knowledge_id", nullable = false)
    private Long knowledgeId = 0L;

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

}
