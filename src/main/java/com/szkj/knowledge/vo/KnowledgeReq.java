package com.szkj.knowledge.vo;

import com.szkj.knowledge.entity.YthKnowledgeShareScope;
import com.szkj.knowledge.entity.YthKnowledgeTag;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class KnowledgeReq {
    Long knowledgeId;
    int type;
    Long parentId;
    String name;
    String author;
    BigDecimal docSize;
    String docSizeName;
    String bucket;
    String fileName;
    String downloadUrl;
    int download;
    int downloadCheck;
    int isIssued;
    List<YthKnowledgeShareScope> shareScope;
    List<AddDocTag> addTags;
    List<YthKnowledgeTag> delTags;
}
