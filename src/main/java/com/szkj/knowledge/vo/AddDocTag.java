package com.szkj.knowledge.vo;

import lombok.Data;

@Data
public class AddDocTag {
    Long tagId;
    String tag;
    int isCreate;
}
