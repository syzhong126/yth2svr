package com.szkj.knowledge.vo;

import lombok.Data;

@Data
public class KnowledgeShareScopeResp {
    int scopeType;
    int id;
    String name;
    int canRead;
    int canWrite;
}
