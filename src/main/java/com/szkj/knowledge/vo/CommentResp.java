package com.szkj.knowledge.vo;

import lombok.Data;

@Data
public class CommentResp {
    Long commentId;
    Long knowledgeId;
    String name;
    String comment;
    String createBy;
    int createTime;
    int hasTaboo;
    int checkedTime;
    int isApproved;
    String approvedBy;
    int approvedTime;
}
