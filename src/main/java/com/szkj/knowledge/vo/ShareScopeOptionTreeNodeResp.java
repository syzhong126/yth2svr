package com.szkj.knowledge.vo;

import lombok.Data;

@Data
public class ShareScopeOptionTreeNodeResp {
    Integer id;
    String name;
    Integer parentId;
    Integer scopeType;
    Integer canReadEnable;
    Integer canWriteEnable;
}
