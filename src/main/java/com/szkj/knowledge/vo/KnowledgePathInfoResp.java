package com.szkj.knowledge.vo;

import lombok.Data;

@Data
public class KnowledgePathInfoResp {
    Long knowledgeId;
    int level;
    String name;
}
