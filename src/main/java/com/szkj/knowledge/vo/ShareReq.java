package com.szkj.knowledge.vo;

import com.szkj.knowledge.entity.YthKnowledgeShareScope;
import lombok.Data;

import java.util.List;

@Data
public class ShareReq {
    Long knowledgeId;
    int shareType;
    List<YthKnowledgeShareScope> shareScope;
}
