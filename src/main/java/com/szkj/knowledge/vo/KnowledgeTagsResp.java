package com.szkj.knowledge.vo;

import lombok.Data;

@Data
public class KnowledgeTagsResp {
    Long tagId;
    String tag;
    int hasTaboo;
    int isApproved;
}
