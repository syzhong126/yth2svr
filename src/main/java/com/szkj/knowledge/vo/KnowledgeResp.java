package com.szkj.knowledge.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class KnowledgeResp {
    Long knowledgeId;
    int type;
    Long parentId;
    String parentName;
    int level;
    String pathCode;
    String name;
    String author;
    BigDecimal docSize;
    String docSizeName;
    String bucket;
    String fileName;
    String downloadUrl;
    int download;
    int downloadCheck;
    int browseNum;
    int downloadNum;
    int favorNum;
    int praiseNum;
    int shareNum;
    int commentNum;
    int isIssued;
    int isEssence;
    String createBy;
    Long createTime;
    String updateBy;
    Long updateTime;
    String ext1;
    String ext2;
    String ext3;
    String ext4;
    String ext5;
    String ext6;
    String ext7;
    String ext8;
    String ext9;
    String ext10;
    int hasChildren;
    int browsed;
    int favored;
    int praised;
    List<KnowledgeShareScopeResp> shareScope;
    List<KnowledgeTagsResp> tags;
}
