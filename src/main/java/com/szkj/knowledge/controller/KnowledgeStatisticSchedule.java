package com.szkj.knowledge.controller;

import com.szkj.knowledge.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class KnowledgeStatisticSchedule {
    @Autowired
    private StatisticService statisticService;

    @Scheduled(fixedRateString = "${docRankStatisticInterval}")
    public void docRankStatistic() {
        statisticService.docRankStatistic();
    }

    @Scheduled(fixedRateString = "${docTagCloudStatisticInterval}")
    public void docTagCloudStatistic() {
        statisticService.docTagCloudStatistic();
    }

}
