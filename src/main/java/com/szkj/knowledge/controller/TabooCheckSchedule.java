package com.szkj.knowledge.controller;

import com.szkj.knowledge.service.CommentService;
import com.szkj.knowledge.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TabooCheckSchedule {
    @Autowired
    private CommentService commentService;
    @Autowired
    private TagService tagService;

    @Scheduled(fixedRateString = "${uncheckedCommentCheckInterval}")
    public void checkUncheckedComment() {
        commentService.checkUncheckedComment();
    }

    @Scheduled(fixedRateString = "${checkedCommentCheckInterval}")
    public void checkCheckedComment() {
        commentService.checkCheckedComment();
    }

    @Scheduled(fixedRateString = "${uncheckedTagCheckInterval}")
    public void checkUncheckedTag() {
        tagService.checkUncheckedTag();
    }

    @Scheduled(fixedRateString = "${checkedTagCheckInterval}")
    public void checkCheckedTag() {
        tagService.checkCheckedTag();
    }
}
