package com.szkj.knowledge.controller;

import cn.hutool.json.JSONArray;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.SQLService;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.*;
import com.szkj.knowledge.vo.KnowledgeReq;
import com.szkj.knowledge.vo.ShareReq;
import com.szkj.knowledge.service.*;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import cn.hutool.json.JSONObject;

import javax.servlet.http.HttpServletRequest;

@Component
@RestController
@CrossOrigin("*")
@RequestMapping(value="/knowledge")
@Slf4j
public class KnowledgeCtrl {
    @Autowired
    private KnowledgeService knowledgeService;
    @Autowired
    private IssueService issueService;
    @Autowired
    private BrowseService browseService;
    @Autowired
    private DownloadService downloadService;
    @Autowired
    private ShareService shareService;
    @Autowired
    private FavorService favorService;
    @Autowired
    private PraiseService praiseService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private TagService tagService;
    @Autowired
    private AuthService authService;
    @Autowired
    private SQLService sqlService;
    @Autowired
    private StatisticService statisticService;

    @RequestMapping(method = {RequestMethod.POST})
    public String addKnowledge(HttpServletRequest request, @RequestBody KnowledgeReq req) {
        String token = request.getHeader("token");
        try {
            return knowledgeService.addKnowledge(req, authService.getLoginInfo(token));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(method = {RequestMethod.PUT})
    public String updateKnowledge(HttpServletRequest request, @RequestBody KnowledgeReq req) {
        String token = request.getHeader("token");
        try {
            return knowledgeService.updateKnowledge(req, authService.getLoginInfo(token));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(method = {RequestMethod.DELETE})
    public String delKnowledge(HttpServletRequest request, @RequestBody KnowledgeReq req) {
        String token = request.getHeader("token");
        try {
            return knowledgeService.delKnowledge(req, authService.getLoginInfo(token));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/issued", method = {RequestMethod.PUT})
    public String issued(HttpServletRequest request, @RequestBody YthKnowledge knowledge) {
        String token = request.getHeader("token");
        try {
            return issueService.issued(knowledge.getKnowledgeId(), knowledge.getIsIssued(),
                    token, authService.getLoginInfo(token));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/browsed", method = {RequestMethod.POST})
    public String addBrowse(HttpServletRequest request, @RequestBody YthKnowledgeBrowse browse) {
        String token = request.getHeader("token");
        try {
            return browseService.add(browse.getKnowledgeId(), authService.getLoginInfo(token).getInt("userId"));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/download", method = {RequestMethod.POST})
    public String download(HttpServletRequest request, @RequestBody YthKnowledgeDownloadReq req) {
        String token = request.getHeader("token");
        try {
            return downloadService.download(req.getKnowledgeId(), authService.getLoginInfo(token));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/download/approved", method = {RequestMethod.PUT})
    public String downloadReqApproved(HttpServletRequest request, @RequestBody YthKnowledgeDownloadReq req) {
        String token = request.getHeader("token");
        try {
            return downloadService.approved(req.getReqId(), req.getIsApproved(), authService.getLoginInfo(token).getInt("userId"), token);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/share", method = {RequestMethod.POST})
    public String share(HttpServletRequest request, @RequestBody ShareReq shareReq) {
        String token = request.getHeader("token");
        try {
            return shareService.add(shareReq, authService.getLoginInfo(token).getInt("userId"), token);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/favor", method = {RequestMethod.POST})
    public String addFavor(HttpServletRequest request, @RequestBody YthKnowledgeFavor favor) {
        String token = request.getHeader("token");
        try {
            return favorService.add(favor.getKnowledgeId(), authService.getLoginInfo(token).getInt("userId"));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/favor", method = {RequestMethod.DELETE})
    public String delFavor(HttpServletRequest request, @RequestBody YthKnowledgeFavor favor) {
        String token = request.getHeader("token");
        try {
            return favorService.del(favor.getKnowledgeId(), authService.getLoginInfo(token).getInt("userId"));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/praise", method = {RequestMethod.POST})
    public String addPraise(HttpServletRequest request, @RequestBody YthKnowledgePraise praise) {
        String token = request.getHeader("token");
        try {
            return praiseService.add(praise.getKnowledgeId(), authService.getLoginInfo(token).getInt("userId"));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/praise", method = {RequestMethod.DELETE})
    public String delPraise(HttpServletRequest request, @RequestBody YthKnowledgePraise praise) {
        String token = request.getHeader("token");
        try {
            return praiseService.del(praise.getKnowledgeId(), authService.getLoginInfo(token).getInt("userId"));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/comment", method = {RequestMethod.POST})
    public String addComment(HttpServletRequest request, @RequestBody YthKnowledgeComment comment) {
        String token = request.getHeader("token");
        try {
            return commentService.add(comment, authService.getLoginInfo(token));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/comment/check", method = {RequestMethod.PUT})
    public String checkComment(HttpServletRequest request, @RequestBody YthKnowledgeComment comment) {
        String token = request.getHeader("token");
        try {
            commentService.checkComment(comment.getCommentId());
            return RespUtil.success("成功", new JSONObject());
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/comment/approved", method = {RequestMethod.PUT})
    public String commentApproved(HttpServletRequest request, @RequestBody YthKnowledgeComment comment) {
        String token = request.getHeader("token");
        try {
            return commentService.approved(comment.getCommentId(), comment.getIsApproved(), token, authService.getLoginInfo(token));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/comment", method = {RequestMethod.DELETE})
    public String delComment(HttpServletRequest request, @RequestBody YthKnowledgeComment comment) {
        String token = request.getHeader("token");
        try {
            return commentService.del(comment.getCommentId(), authService.getLoginInfo(token));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/tag/check", method = {RequestMethod.PUT})
    public String checkTag(HttpServletRequest request, @RequestBody YthKnowledgeTag tag) {
        String token = request.getHeader("token");
        try {
            tagService.checkTag(tag.getTagId());
            return RespUtil.success("成功", new JSONObject());
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/tag/approved", method = {RequestMethod.PUT})
    public String updateTag(HttpServletRequest request, @RequestBody YthKnowledgeTag tag) {
        String token = request.getHeader("token");
        try {
            return tagService.approved(tag.getTagId(), tag.getIsApproved()
                    , authService.getLoginInfo(token).getInt("userId"), token);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="", method = {RequestMethod.GET})
    public String getKnowledgeInfo(HttpServletRequest request, @RequestParam Long knowledgeId) {
        String token = request.getHeader("token");
        try {
            return knowledgeService.getKnowledgeInfo(knowledgeId, authService.getLoginInfo(token));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/list", method = {RequestMethod.POST})
    public String getKnowledgeList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetKnowledgeListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/sharetome/list", method = {RequestMethod.POST})
    public String getShareToMeList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        JSONObject newBody;
        try {
            newBody = knowledgeService.resetShareToMeListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/sharetoothers/list", method = {RequestMethod.POST})
    public String getShareToOthersList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        JSONObject newBody;
        try {
            newBody = knowledgeService.resetShareToOthersListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/favor/list", method = {RequestMethod.POST})
    public String getFavorList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetMyFavorListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/mydoc/list", method = {RequestMethod.POST})
    public String getMyDocList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetMyDocListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/download/req/list", method = {RequestMethod.POST})
    public String getDownloadReqList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetDownloadReqApproveListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/download/myreq/list", method = {RequestMethod.POST})
    public String getDownloadMyReqList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetDownloadMyReqApproveListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/download/rec/list", method = {RequestMethod.POST})
    public String getDownloadRecList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetDownloadRecListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/download/myrec/list", method = {RequestMethod.POST})
    public String getDownloadMyRecList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetDownloadMyRecListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/issue/list", method = {RequestMethod.POST})
    public String getIssueApproveList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetIssueApproveListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/comment", method = {RequestMethod.GET})
    public String getCommentList(HttpServletRequest request, @RequestParam Long commentId) {
        String token = request.getHeader("token");
        try {
            return commentService.getCommentInfo(commentId, authService.getLoginInfo(token).getInt("userId"));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/comment/list", method = {RequestMethod.POST})
    public String getCommentApproveList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetCommentApproveListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/mycomment/list", method = {RequestMethod.POST})
    public String getMyCommentApproveList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetMyCommentApproveListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/tag/list", method = {RequestMethod.POST})
    public String getTagApproveList(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.resetTagApproveListQueryBody(authService.getLoginInfo(token), body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/share/scope/list", method = {RequestMethod.GET})
    public String getShareScopeList(HttpServletRequest request, @RequestParam Long knowledgeId
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = knowledgeService.setShareScopeListQueryBody(authService.getLoginInfo(token), knowledgeId);
            JSONArray data = sqlService.queryList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/stat/rank", method = {RequestMethod.POST})
    public String getRank(@ModelAttribute("authUserInfo") AuthUserInfoModel userInfo, @RequestBody JSONObject body) {
        try {
            JSONObject data = sqlService.queryPageList(userInfo, body);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/stat/cloud", method = {RequestMethod.GET})
    public String getCloud() {
        JSONArray result = statisticService.getCloud();
        return RespUtil.success("成功", result);
    }

    @RequestMapping(value="/type/share/scope/option", method = {RequestMethod.GET})
    public String getDirShareScopeOption(@ModelAttribute("authUserInfo") AuthUserInfoModel userInfo, @RequestParam Long knowledgeId) {
        try {
            return shareService.getDirShareScopeOptionTree(userInfo, knowledgeId);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/doc/share/scope/option", method = {RequestMethod.POST})
    public String getDocShareScopeOption(@ModelAttribute("authUserInfo") AuthUserInfoModel userInfo, @RequestBody JSONObject body) {
        try {
            return shareService.getDocShareScopeOptionTree(userInfo, body);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/correlate/list", method = {RequestMethod.GET})
    public String getCorrelateList(HttpServletRequest request, @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo, @RequestParam Long knowledgeId) {
        String token = request.getHeader("token");
        try {
            return knowledgeService.getCorrelateList(authService.getLoginInfo(token), userInfo, knowledgeId);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/path", method = {RequestMethod.GET})
    public String getAncestors(@RequestParam Long knowledgeId) {
        try {
            return knowledgeService.getPathInfo(knowledgeId);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }
}
