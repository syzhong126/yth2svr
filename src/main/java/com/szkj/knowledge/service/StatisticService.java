package com.szkj.knowledge.service;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.szkj.knowledge.entity.*;
import com.szkj.knowledge.reposi.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticService {
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgeStatRankReposi ythKnowledgeStatRankReposi;
    @Autowired
    private YthKnowledgeStatRankDetailReposi ythKnowledgeStatRankDetailReposi;
    @Autowired
    private YthKnowledgeTagReposi ythKnowledgeTagReposi;
    @Autowired
    private YthKnowledgeStatCloudReposi ythKnowledgeStatCloudReposi;

    public void docRankStatistic() {
        List<YthKnowledge> docList = ythKnowledgeReposi.findAllIssuedDoc();
        for (YthKnowledge doc : docList) {
            // 热度 = 阅读数 + 收藏数 + 点赞数 + 分享数 + 评论数
            doc.setExt3(String.valueOf(doc.getBrowseNum() + doc.getFavorNum() + doc.getPraiseNum() + doc.getShareNum() + doc.getCommentNum()));
            ythKnowledgeReposi.save(doc);
        }

        YthKnowledgeStatRank ythKnowledgeStatRank = new YthKnowledgeStatRank();
        ythKnowledgeStatRank.setCreateTime(System.currentTimeMillis());
        YthKnowledgeStatRank savedYthKnowledgeStatRank = ythKnowledgeStatRankReposi.save(ythKnowledgeStatRank);

        docList = ythKnowledgeReposi.findTopDocs(10);
        for (int i = 0; i < docList.size(); i++) {
            YthKnowledgeStatRankDetail ythKnowledgeStatRankDetail = new YthKnowledgeStatRankDetail();
            ythKnowledgeStatRankDetail.setRankId(savedYthKnowledgeStatRank.getRankId());
            ythKnowledgeStatRankDetail.setKnowledgeId(docList.get(i).getKnowledgeId());
            ythKnowledgeStatRankDetail.setOrderNo(i + 1);
            ythKnowledgeStatRankDetailReposi.save(ythKnowledgeStatRankDetail);
        }
    }

    public void docTagCloudStatistic() {
        List<YthKnowledgeTag> tagList = ythKnowledgeTagReposi.findTopRelevancy(10);
        if (tagList.size() > 0) {
            JSONArray data = new JSONArray();
            for (int i = 0; i < tagList.size(); i++) {
                YthKnowledgeTag tag = tagList.get(i);
                JSONObject item = new JSONObject();
                item.set("tagId", tag.getTagId());
                item.set("tag", tag.getTag());
                item.set("order", i + 1);
                data.set(item);
            }
            YthKnowledgeStatCloud ythKnowledgeStatCloud = new YthKnowledgeStatCloud();
            ythKnowledgeStatCloud.setDataTxt(data.toStringPretty());
            ythKnowledgeStatCloud.setCreateTime(System.currentTimeMillis());
            ythKnowledgeStatCloudReposi.save(ythKnowledgeStatCloud);
        }
    }

    public JSONArray getCloud() {
        YthKnowledgeStatCloud ythKnowledgeStatCloud = ythKnowledgeStatCloudReposi.findNewest();
        return new JSONArray(ythKnowledgeStatCloud.getDataTxt());
    }
}
