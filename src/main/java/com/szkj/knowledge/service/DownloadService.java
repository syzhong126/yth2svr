package com.szkj.knowledge.service;

import cn.hutool.json.JSONObject;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.YthKnowledge;
import com.szkj.knowledge.entity.YthKnowledgeDownloadRec;
import com.szkj.knowledge.entity.YthKnowledgeDownloadReq;
import com.szkj.knowledge.reposi.YthKnowledgeDownloadRecReposi;
import com.szkj.knowledge.reposi.YthKnowledgeDownloadReqReposi;
import com.szkj.knowledge.reposi.YthKnowledgeReposi;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.service.YthNoticepushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class DownloadService {
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgeDownloadReqReposi ythKnowledgeDownloadReqReposi;
    @Autowired
    private YthKnowledgeDownloadRecReposi ythKnowledgeDownloadRecReposi;
    @Autowired
    private YthNoticepushService ythNoticepushService;
    @Autowired
    private YthOperationLogService logservice;
    @Autowired
    private AuthorizeService authorizeService;

    private final int DOC_CAN_DOWNLOAD = 1;
    private final int DOC_DOWNLOAD_NEED_CHECK = 1;
    private final int USER_CAN_DOWNLOAD = 1;

    private void knowledgeDownloadNumPlusOne(YthKnowledge ythKnowledge) {
        int downloadNum = ythKnowledge.getDownloadNum() + 1;
        ythKnowledge.setDownloadNum(downloadNum);
        ythKnowledgeReposi.save(ythKnowledge);
    }

    @Transactional(rollbackFor = Exception.class)
    public String download(Long knowledgeId, JSONObject userInfo) throws Exception {
        try {
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(knowledgeId);
            if (ythKnowledge.isPresent()) {
                if (!authorizeService.canReadDir(userInfo, ythKnowledge.get().getParentId())) {
                    String result = RespUtil.error("没有操作权限。", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                            "/knowledge/download", "{\"knowledgeId\":" + knowledgeId + "}", result, "", userInfo.getInt("userId"));
                    return result;
                }

                if (DOC_CAN_DOWNLOAD == ythKnowledge.get().getDownload()) {
                    if (DOC_DOWNLOAD_NEED_CHECK != ythKnowledge.get().getDownloadCheck()) {
                        // 增加下载量
                        knowledgeDownloadNumPlusOne(ythKnowledge.get());

                        // 增加下载记录
                        YthKnowledgeDownloadRec ythKnowledgeDownloadRec = new YthKnowledgeDownloadRec();
                        ythKnowledgeDownloadRec.setKnowledgeId(knowledgeId);
                        ythKnowledgeDownloadRec.setReqId(0L);
                        ythKnowledgeDownloadRec.setCreateBy(userInfo.getInt("userId"));
                        ythKnowledgeDownloadRec.setCreateTime(System.currentTimeMillis());
                        ythKnowledgeDownloadRecReposi.save(ythKnowledgeDownloadRec);

                        Map<String, Object> data = new HashMap<String, Object>();
                        data.put("downloadUrl", ythKnowledge.get().getDownloadUrl());
                        String result = RespUtil.success("成功", data);
                        logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                                "/knowledge/download", "{\"knowledgeId\":" + knowledgeId + "}", result, "", userInfo.getInt("userId"));
                        return result;
                    } else {
                        YthKnowledgeDownloadReq ythKnowledgeDownloadReq = ythKnowledgeDownloadReqReposi.findByKnowledgeIdAndCreateBy(knowledgeId, userInfo.getInt("userId"));
                        if (null != ythKnowledgeDownloadReq) {
                            if (USER_CAN_DOWNLOAD == ythKnowledgeDownloadReq.getIsApproved()) {
                                // 增加下载量
                                knowledgeDownloadNumPlusOne(ythKnowledge.get());

                                // 增加下载记录
                                YthKnowledgeDownloadRec ythKnowledgeDownloadRec = new YthKnowledgeDownloadRec();
                                ythKnowledgeDownloadRec.setKnowledgeId(knowledgeId);
                                ythKnowledgeDownloadRec.setReqId(ythKnowledgeDownloadReq.getReqId());
                                ythKnowledgeDownloadRec.setCreateBy(userInfo.getInt("userId"));
                                ythKnowledgeDownloadRec.setCreateTime(System.currentTimeMillis());
                                ythKnowledgeDownloadRecReposi.save(ythKnowledgeDownloadRec);

                                Map<String, Object> data = new HashMap<String, Object>();
                                data.put("downloadUrl", ythKnowledge.get().getDownloadUrl());
                                String result = RespUtil.success("成功", data);
                                logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                                        "/knowledge/download", "{\"knowledgeId\":" + knowledgeId + "}", result, "", userInfo.getInt("userId"));
                                return result;
                            } else {
                                String result = RespUtil.error("下载申请未审核通过", new JSONObject());
                                logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                                        "/knowledge/download", "{\"knowledgeId\":" + knowledgeId + "}", result, "", userInfo.getInt("userId"));
                                return result;
                            }
                        } else {
                            ythKnowledgeDownloadReq = new YthKnowledgeDownloadReq();
                            ythKnowledgeDownloadReq.setKnowledgeId(knowledgeId);
                            ythKnowledgeDownloadReq.setCreateBy(userInfo.getInt("userId"));
                            ythKnowledgeDownloadReq.setCreateTime(System.currentTimeMillis());
                            ythKnowledgeDownloadReqReposi.save(ythKnowledgeDownloadReq);
                            String result = RespUtil.error("下载须审核。已创建下载申请，须审核通过后再下载。", new JSONObject());
                            logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                                    "/knowledge/download", "{\"knowledgeId\":" + knowledgeId + "}", result, "", userInfo.getInt("userId"));
                            return result;
                        }
                    }
                } else {
                    String result = RespUtil.error("文档不可下载", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                            "/knowledge/download", "{\"knowledgeId\":" + knowledgeId + "}", result, "", userInfo.getInt("userId"));
                    return result;
                }
            } else {
                String result = RespUtil.error("文档不存在", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                        "/knowledge/download", "{\"knowledgeId\":" + knowledgeId + "}", result, "", userInfo.getInt("userId"));
                return result;
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String approved(Long reqId, int isApproved, int userId, String token) throws Exception {
        try {
            Optional<YthKnowledgeDownloadReq> req = ythKnowledgeDownloadReqReposi.findById(reqId);
            if (req.isPresent()) {
                req.get().setIsApproved(isApproved);
                req.get().setApprovedBy(userId);
                req.get().setApprovedTime(System.currentTimeMillis());
                ythKnowledgeDownloadReqReposi.save(req.get());

                // 添加推送消息
                if (1 == isApproved) {
                    YthNoticePush ythNoticePush = new YthNoticePush();
                    ythNoticePush.setPushUser(req.get().getCreateBy())
                            .setPushType(2).setPushStatus(0).setPushTime(System.currentTimeMillis())
                            .setFromSystem("yth").setMoudleDetail("knowledge")
                            .setCreateTime(System.currentTimeMillis())
                            .setUpdateTime(System.currentTimeMillis())
                            .setTitle("文档下载")
                            .setContent("您的文档下载申请已审核通过。文档："
                                    + ythKnowledgeReposi.findById(req.get().getKnowledgeId())
                                    .orElse(new YthKnowledge()).getName() + "。")
                            .setMoudleType("2");
                    ythNoticepushService.append(ythNoticePush, token);
                }
                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userId, "update", "knowledge", "/download/approved"
                        , "{\"reqId\":" + reqId + "\"isApproved\":" + isApproved + "}", result, "", userId);
                return result;
            }
            String result = RespUtil.error("下载申请不存在", new JSONObject());
            logservice.append(userId, "update", "knowledge", "/download/approved"
                    , "{\"reqId\":" + reqId + "\"isApproved\":" + isApproved + "}", result, "", userId);
            return result;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

}
