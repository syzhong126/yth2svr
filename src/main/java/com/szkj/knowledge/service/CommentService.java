package com.szkj.knowledge.service;

import cn.hutool.json.JSONObject;
import com.szkj.auth.entity.User;
import com.szkj.auth.reposi.UserReposi;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.YthKnowledge;
import com.szkj.knowledge.entity.YthKnowledgeComment;
import com.szkj.knowledge.entity.YthKnowledgeTaboo;
import com.szkj.knowledge.reposi.YthKnowledgeTabooReposi;
import com.szkj.knowledge.vo.CommentResp;
import com.szkj.knowledge.reposi.YthKnowledgeCommentReposi;
import com.szkj.knowledge.reposi.YthKnowledgeReposi;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.service.YthNoticepushService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {
    @Autowired
    private UserReposi ythUserReposi;
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgeCommentReposi ythKnowledgeCommentReposi;
    @Autowired
    private YthKnowledgeTabooReposi ythKnowledgeTabooReposi;
    @Autowired
    private YthNoticepushService ythNoticepushService;
    @Autowired
    private YthOperationLogService logservice;
    @Autowired
    private AuthorizeService authorizeService;

    private void knowledgeCommentNumPlusOne(YthKnowledge ythKnowledge) {
        int commentNum = ythKnowledge.getCommentNum() + 1;
        ythKnowledge.setCommentNum(commentNum);
        ythKnowledgeReposi.save(ythKnowledge);
    }

    private void knowledgeCommentNumMinusOne(YthKnowledge ythKnowledge) {
        int commentNum = Math.max((ythKnowledge.getCommentNum() - 1), 0);
        ythKnowledge.setCommentNum(commentNum);
        ythKnowledgeReposi.save(ythKnowledge);
    }

    @Transactional(rollbackFor = Exception.class)
    public String add(YthKnowledgeComment comment, JSONObject userInfo) throws Exception {
        try {
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(comment.getKnowledgeId());
            if (ythKnowledge.isPresent()) {
                String userRole = authorizeService.getUserRole(userInfo);
                if (!authorizeService.canReadDir(userInfo, ythKnowledge.get().getParentId())
                        || !(authorizeService.ROLE_APP_MGR.equals(userRole) || authorizeService.ROLE_MGR.equals(userRole)
                        || ythKnowledge.get().getCreateBy().equals(String.valueOf(userInfo.getInt("userId"))))) {
                    String result = RespUtil.error("没有操作权限。", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                            "/knowledge/comment", "{\"knowledgeId\":" + comment.getKnowledgeId() + ",\"comment\":" + comment.getComment() + "}", result, "", userInfo.getInt("userId"));
                    return result;
                }

                // 增加评论量
                knowledgeCommentNumPlusOne(ythKnowledge.get());

                comment.setCreateBy(userInfo.getInt("userId"));
                comment.setCreateTime(System.currentTimeMillis());
                ythKnowledgeCommentReposi.save(comment);
                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                        "/knowledge/comment", "{\"knowledgeId\":" + comment.getKnowledgeId() + ",\"comment\":" + comment.getComment() + "}", result, "", userInfo.getInt("userId"));
                return result;
            }
            String result = RespUtil.error("文档不存在", new JSONObject());
            logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                    "/knowledge/comment", "{\"knowledgeId\":" + comment.getKnowledgeId() + ",\"comment\":" + comment.getComment() + "}", result, "", userInfo.getInt("userId"));
            return result;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String approved(Long commentId, int isApproved, String token, JSONObject userInfo) throws Exception {
        try {
            Optional<YthKnowledgeComment> ythKnowledgeComment = ythKnowledgeCommentReposi.findById(commentId);
            if (ythKnowledgeComment.isPresent()) {
                Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(ythKnowledgeComment.get().getKnowledgeId());
                if (ythKnowledge.isPresent()) {
                    String userRole = authorizeService.getUserRole(userInfo);
                    if (!authorizeService.canWriteDir(userInfo, ythKnowledge.get().getParentId())
                            || !(authorizeService.ROLE_APP_MGR.equals(userRole) || authorizeService.ROLE_MGR.equals(userRole))) {
                        String result = RespUtil.error("没有操作权限。", new JSONObject());
                        logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                                "/knowledge/comment", "{\"commentId\":" + commentId + ",\"isApproved\":" + isApproved + "}", result, "", userInfo.getInt("userId"));
                        return result;
                    }

                    // 判断状态变化
                    if (ythKnowledgeComment.get().getIsApproved() == isApproved) {
                        String result = RespUtil.error("当前发布状态：" + isApproved + " 原发布状态：" + ythKnowledgeComment.get().getIsApproved() + "。", new JSONObject());
                        logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                                "/knowledge/comment", "{\"commentId\":" + commentId + ",\"isApproved\":" + isApproved + "}", result, "", userInfo.getInt("userId"));
                        return result;
                    }

                    ythKnowledgeComment.get().setIsApproved(isApproved);
                    ythKnowledgeComment.get().setApprovedBy(userInfo.getInt("userId"));
                    ythKnowledgeComment.get().setApprovedTime(System.currentTimeMillis());
                    ythKnowledgeCommentReposi.save(ythKnowledgeComment.get());

                    // 添加推送消息
                    if (1 == isApproved) {
                        YthNoticePush ythNoticePush = new YthNoticePush();
                        ythNoticePush.setPushUser(ythKnowledgeComment.get().getCreateBy())
                                .setPushType(2).setPushStatus(0).setPushTime(System.currentTimeMillis())
                                .setFromSystem("yth").setMoudleDetail("knowledge")
                                .setCreateTime(System.currentTimeMillis())
                                .setUpdateTime(System.currentTimeMillis())
                                .setTitle("文档评论")
                                .setContent("您对文档发表的评论已审核通过。评论："
                                        + ythKnowledgeComment.get().getComment().substring(0, (Math.min(ythKnowledgeComment.get().getComment().length(), 10))) + (ythKnowledgeComment.get().getComment().length() > 10 ? "……" : ""))
                                .setMoudleType("1");
                        ythNoticepushService.append(ythNoticePush, token);
                    }
                    String result = RespUtil.success("成功", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                            "/knowledge/comment", "{\"commentId\":" + commentId + ",\"isApproved\":" + isApproved + "}", result, "", userInfo.getInt("userId"));
                    return result;
                }
                String result = RespUtil.error("文档不存在", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                        "/knowledge/comment", "{\"commentId\":" + commentId + ",\"isApproved\":" + isApproved + "}", result, "", userInfo.getInt("userId"));
                return result;
            }
            String result = RespUtil.error("评论不存在", new JSONObject());
            logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                    "/knowledge/comment", "{\"commentId\":" + commentId + ",\"isApproved\":" + isApproved + "}", result, "", userInfo.getInt("userId"));
            return result;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String del(Long commentId, JSONObject userInfo) throws Exception {
        try {
            Optional<YthKnowledgeComment> ythKnowledgeComment = ythKnowledgeCommentReposi.findById(commentId);
            if (ythKnowledgeComment.isPresent()) {
                Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(ythKnowledgeComment.get().getKnowledgeId());
                if (ythKnowledge.isPresent()) {
                    String userRole = authorizeService.getUserRole(userInfo);
                    if (!authorizeService.canReadDir(userInfo, ythKnowledge.get().getParentId())
                            || !(authorizeService.ROLE_APP_MGR.equals(userRole) || authorizeService.ROLE_MGR.equals(userRole)
                            || ythKnowledgeComment.get().getCreateBy().equals(userInfo.getInt("userId")))) {
                        String result = RespUtil.error("没有操作权限。", new JSONObject());
                        logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                                "/knowledge/comment", "{\"commentId\":" + commentId + "}", result, "", userInfo.getInt("userId"));
                        return result;
                    }

                    // 减少评论量
                    knowledgeCommentNumMinusOne(ythKnowledge.get());

                    ythKnowledgeCommentReposi.delete(ythKnowledgeComment.get());

                    String result = RespUtil.success("成功", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                            "/knowledge/comment", "{\"commentId\":" + commentId + "}", result, "", userInfo.getInt("userId"));
                    return result;
                }
                String result = RespUtil.error("文档不存在", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                        "/knowledge/comment", "{\"commentId\":" + commentId + "}", result, "", userInfo.getInt("userId"));
                return result;
            }
            String result = RespUtil.error("评论不存在", new JSONObject());
            logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                    "/knowledge/comment", "{\"commentId\":" + commentId + "}", result, "", userInfo.getInt("userId"));
            return result;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public String getCommentInfo(Long commentId, int userId) {
        Optional<YthKnowledgeComment> ythKnowledgeComment = ythKnowledgeCommentReposi.findById(commentId);
        if (ythKnowledgeComment.isPresent()) {
            if (1 != ythKnowledgeComment.get().getIsApproved() && userId != ythKnowledgeComment.get().getCreateBy()) {
                return RespUtil.error("没有权限", new JSONObject());
            }
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(commentId);
            if (ythKnowledge.isPresent()) {
                CommentResp commentResp = new CommentResp();
                BeanUtils.copyProperties(ythKnowledgeComment.get(), commentResp);
                commentResp.setName(ythKnowledge.get().getName());
                commentResp.setCreateBy(convertUserIdToUserName(ythKnowledgeComment.get().getCreateBy()));
                commentResp.setApprovedBy(convertUserIdToUserName(ythKnowledgeComment.get().getApprovedBy()));
                return RespUtil.success("成功", new JSONObject());
            }
            return RespUtil.error("文档不存在", new JSONObject());
        }
        return RespUtil.error("评论不存在", new JSONObject());
    }

    private String convertUserIdToUserName(int userId) {
        Optional<User> ythUser = ythUserReposi.findById(userId);
        if (ythUser.isPresent()) {
            return ythUser.get().getUserName();
        }
        return null;
    }

    private void checkCommentsIfContainTaboo(List<YthKnowledgeComment> commentList) {
        List<YthKnowledgeTaboo> tabooList = (List<YthKnowledgeTaboo>) ythKnowledgeTabooReposi.findAll();
        for (int i = 0; i < commentList.size(); i++) {
            boolean hasTaboo = false;
            for (int j = 0; j < tabooList.size(); j++) {
                if (commentList.get(i).getComment().contains(tabooList.get(j).getTaboo())) {
                    hasTaboo = true;
                }
            }
            if (hasTaboo) {
                commentList.get(i).setHasTaboo(2);
                commentList.get(i).setIsApproved(0);
            } else {
                commentList.get(i).setHasTaboo(1);
            }
            commentList.get(i).setCheckedTime(System.currentTimeMillis());
            ythKnowledgeCommentReposi.save(commentList.get(i));
        }
    }

    public void checkComment(Long commentId) {
        boolean hasTaboo = false;
        Optional<YthKnowledgeComment> ythKnowledgeComment = ythKnowledgeCommentReposi.findById(commentId);
        if (ythKnowledgeComment.isPresent()) {
            List<YthKnowledgeTaboo> tabooList = (List<YthKnowledgeTaboo>) ythKnowledgeTabooReposi.findAll();
            for (int i = 0; i < tabooList.size(); i++) {
                if (ythKnowledgeComment.get().getComment().contains(tabooList.get(i).getTaboo())) {
                    hasTaboo = true;
                }
            }
            if (hasTaboo) {
                ythKnowledgeComment.get().setHasTaboo(2);
                ythKnowledgeComment.get().setIsApproved(0);
            } else {
                ythKnowledgeComment.get().setHasTaboo(1);
            }
            ythKnowledgeComment.get().setCheckedTime(System.currentTimeMillis());
            ythKnowledgeCommentReposi.save(ythKnowledgeComment.get());
        }
    }

    public void checkUncheckedComment() {
        List<YthKnowledgeComment> commentList = ythKnowledgeCommentReposi.findAllUnckeckedComment();
        checkCommentsIfContainTaboo(commentList);
    }

    public void checkCheckedComment() {
        List<YthKnowledgeComment> commentList = ythKnowledgeCommentReposi.findAllCkeckedComment();
        checkCommentsIfContainTaboo(commentList);
    }

}
