package com.szkj.knowledge.service;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.szkj.auth.entity.User;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.reposi.UserReposi;
import com.szkj.common.service.SQLService;
import com.szkj.common.util.JsonUtil;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.YthKnowledge;
import com.szkj.knowledge.entity.YthKnowledgeShare;
import com.szkj.knowledge.entity.YthKnowledgeShareScope;
import com.szkj.knowledge.entity.YthOrganazition;
import com.szkj.knowledge.reposi.*;
import com.szkj.knowledge.vo.ShareReq;
import com.szkj.knowledge.vo.ShareScopeOptionTreeNodeResp;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.service.YthNoticepushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShareService {
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgeShareReposi ythKnowledgeShareReposi;
    @Autowired
    private YthKnowledgeShareScopeReposi ythKnowledgeShareScopeReposi;
    @Autowired
    private YthOrganazitionReposi ythOrganazitionReposi;
    @Autowired
    private UserReposi ythUserReposi;
    @Autowired
    private SQLService sqlService;
    @Autowired
    private YthNoticepushService ythNoticepushService;

    private void knowledgeShareNumPlusOne(YthKnowledge ythKnowledge) {
        int shareNum = ythKnowledge.getShareNum() + 1;
        ythKnowledge.setShareNum(shareNum);
        ythKnowledgeReposi.save(ythKnowledge);
    }

    private void knowledgeEssenceNumPlusOne(YthKnowledge ythKnowledge) {
        int essenceNum = ythKnowledge.getEssenceNum() + 1;
        ythKnowledge.setEssenceNum(essenceNum);
        ythKnowledge.setIsEssence(isEssence(ythKnowledge));
        ythKnowledgeReposi.save(ythKnowledge);
    }

    private int isEssence(YthKnowledge ythKnowledge) {
        int threshold = 1000;
        return ythKnowledge.getEssenceNum() >= threshold ? 1 : 0;
    }

    @Transactional(rollbackFor = Exception.class)
    public String add(ShareReq shareReq, int userId, String token) throws Exception {
        try {
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(shareReq.getKnowledgeId());
            if (ythKnowledge.isPresent()) {
                // 增加分享量
                knowledgeShareNumPlusOne(ythKnowledge.get());

                int oldIsEssence = ythKnowledge.get().getIsEssence();

                // 增加推荐精华量
                knowledgeEssenceNumPlusOne(ythKnowledge.get());

                // 添加推送消息
                if (0 == oldIsEssence && 1 == ythKnowledge.get().getIsEssence()) {
                    YthNoticePush ythNoticePush = new YthNoticePush();
                    ythNoticePush.setPushUser(Integer.parseInt(ythKnowledge.get().getCreateBy()))
                            .setPushType(2).setPushStatus(0).setPushTime(System.currentTimeMillis())
                            .setFromSystem("yth").setMoudleDetail("knowledge")
                            .setCreateTime(System.currentTimeMillis())
                            .setUpdateTime(System.currentTimeMillis())
                            .setTitle("精华库")
                            .setContent("您上传的文档已入选精华库。文档：" + ythKnowledge.get().getName() + "。")
                            .setMoudleType("4");
                    ythNoticepushService.append(ythNoticePush, token);
                }

                YthKnowledgeShare ythKnowledgeShare = new YthKnowledgeShare();
                ythKnowledgeShare.setKnowledgeId(shareReq.getKnowledgeId());
                ythKnowledgeShare.setShareType(shareReq.getShareType());
                ythKnowledgeShare.setCreateBy(userId);
                ythKnowledgeShare.setCreateTime(System.currentTimeMillis());
                YthKnowledgeShare savedShare = ythKnowledgeShareReposi.save(ythKnowledgeShare);
                for (int i = 0; i < shareReq.getShareScope().size(); i++) {
                    YthKnowledgeShareScope shareScope = shareReq.getShareScope().get(i);
                    shareScope.setShareId(savedShare.getShareId());
                    shareScope.setKnowledgeId(shareReq.getKnowledgeId());
                    shareScope.setType(1);
                    ythKnowledgeShareScopeReposi.save(shareScope);

                    // 添加推送消息
                    YthNoticePush ythNoticePush = new YthNoticePush();
                    ythNoticePush.setPushUser(shareScope.getId())
                            .setPushType(2).setPushStatus(0).setPushTime(System.currentTimeMillis())
                            .setFromSystem("yth").setMoudleDetail("knowledge")
                            .setCreateTime(System.currentTimeMillis())
                            .setUpdateTime(System.currentTimeMillis())
                            .setTitle("文档分享")
                            .setContent(ythUserReposi.findById(userId).orElse(new User()).getUserName()
                                    + "与您分享了文档：" + ythKnowledge.get().getName() + "。")
                            .setMoudleType("0");
                    ythNoticepushService.append(ythNoticePush, token);
                }
                return RespUtil.success("成功", new JSONObject());
            }
            return RespUtil.error("文档不存在", new JSONObject());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public String getDirShareScopeOptionTree(AuthUserInfoModel userInfo, Long knowledgeId) throws Exception {
        JSONObject respData = new JSONObject();

        Optional<YthKnowledge> ythKnowledgeOptional = ythKnowledgeReposi.findById(knowledgeId);
        if (ythKnowledgeOptional.isPresent()) {
            if (ythKnowledgeOptional.get().getType() == 0) {
                if (ythKnowledgeOptional.get().getLevel() < 2) {
                    List<ShareScopeOptionTreeNodeResp> shareScopeOptionTreeNodeRespList = new ArrayList<>();
                    List<YthOrganazition> ythOrganazitionList = ythOrganazitionReposi.findAllUnitAndDept();
                    for (YthOrganazition ythOrganazition : ythOrganazitionList) {
                        ShareScopeOptionTreeNodeResp shareScopeOptionTreeNodeResp = new ShareScopeOptionTreeNodeResp();
                        shareScopeOptionTreeNodeResp.setId(ythOrganazition.getOrgId());
                        if (null == ythOrganazition.getShortName() || ythOrganazition.getShortName().equals("")) {
                            shareScopeOptionTreeNodeResp.setName(ythOrganazition.getOrgName());
                        } else {
                            shareScopeOptionTreeNodeResp.setName(ythOrganazition.getShortName());
                        }
                        if (2 != ythOrganazition.getOrgType()) {
                            shareScopeOptionTreeNodeResp.setParentId(0);
                        } else {
                            shareScopeOptionTreeNodeResp.setParentId(ythOrganazition.getParentId());
                        }
                        if (0 == ythOrganazition.getOrgType()) {
                            shareScopeOptionTreeNodeResp.setScopeType(1);
                        } else {
                            shareScopeOptionTreeNodeResp.setScopeType(ythOrganazition.getOrgType());
                        }
                        shareScopeOptionTreeNodeResp.setCanReadEnable(1);
                        shareScopeOptionTreeNodeResp.setCanWriteEnable(1);
                        shareScopeOptionTreeNodeRespList.add(shareScopeOptionTreeNodeResp);
                        JsonUtil.resetJsonKeyValue(respData, "shareScopeOptionTreeNode", shareScopeOptionTreeNodeRespList);
                    }
                } else if (ythKnowledgeOptional.get().getLevel() == 2) {
                    List<ShareScopeOptionTreeNodeResp> shareScopeOptionTreeNodeRespList = new ArrayList<>();
                    List<YthOrganazition> ythOrganazitionList = ythOrganazitionReposi.findUnitAndDept(Integer.parseInt(ythKnowledgeOptional.get().getExt1()));
                    for (YthOrganazition ythOrganazition : ythOrganazitionList) {
                        ShareScopeOptionTreeNodeResp shareScopeOptionTreeNodeResp = new ShareScopeOptionTreeNodeResp();
                        shareScopeOptionTreeNodeResp.setId(ythOrganazition.getOrgId());
                        if (null == ythOrganazition.getShortName() || ythOrganazition.getShortName().equals("")) {
                            shareScopeOptionTreeNodeResp.setName(ythOrganazition.getOrgName());
                        } else {
                            shareScopeOptionTreeNodeResp.setName(ythOrganazition.getShortName());
                        }
                        if (2 != ythOrganazition.getOrgType()) {
                            shareScopeOptionTreeNodeResp.setParentId(0);
                        } else {
                            shareScopeOptionTreeNodeResp.setParentId(ythOrganazition.getParentId());
                        }
                        if (0 == ythOrganazition.getOrgType()) {
                            shareScopeOptionTreeNodeResp.setScopeType(1);
                        } else {
                            shareScopeOptionTreeNodeResp.setScopeType(ythOrganazition.getOrgType());
                        }
                        shareScopeOptionTreeNodeResp.setCanReadEnable(1);
                        shareScopeOptionTreeNodeResp.setCanWriteEnable(1);
                        shareScopeOptionTreeNodeRespList.add(shareScopeOptionTreeNodeResp);
                        JsonUtil.resetJsonKeyValue(respData, "shareScopeOptionTreeNode", shareScopeOptionTreeNodeRespList);
                    }
                } else {
                    JSONObject body = new JSONObject();
                    JsonUtil.resetJsonKeyValue(body, "sqlId", 1123);
                    JsonUtil.resetJsonKeyValue(body, "params2", "{knowledgeId:" + knowledgeId + "}");
                    JsonUtil.resetJsonKeyValue(body, "pagenum", 1);
                    JsonUtil.resetJsonKeyValue(body, "pagerows", 5000);
                    JsonUtil.resetJsonKeyValue(respData, "shareScopeOptionTreeNode",
                            sqlService.queryPageList(userInfo, body).getByPath("data", JSONArray.class));
                }
            } else {
                return RespUtil.error("类型错误，当前的类型是文档。", new JSONObject());
            }
        }

        return RespUtil.success("成功", respData);
    }

    public String getDocShareScopeOptionTree(AuthUserInfoModel userInfo, JSONObject body) throws Exception {
        JSONObject respData = new JSONObject();

        JsonUtil.resetJsonKeyValue(body, "sqlId", 1124);
        JsonUtil.resetJsonKeyValue(respData, "shareScopeOption",
                sqlService.queryPageList(userInfo, body).getByPath("data", JSONArray.class));

        return RespUtil.success("成功", respData);
    }
}
