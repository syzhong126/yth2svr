package com.szkj.knowledge.service;

import cn.hutool.json.JSONObject;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.YthKnowledge;
import com.szkj.knowledge.entity.YthKnowledgePraise;
import com.szkj.knowledge.reposi.YthKnowledgePraiseReposi;
import com.szkj.knowledge.reposi.YthKnowledgeReposi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PraiseService {
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgePraiseReposi ythKnowledgePraiseReposi;
    @Autowired
    private YthOperationLogService logservice;

    private void knowledgePraiseNumPlusOne(YthKnowledge ythKnowledge) {
        int praiseNum = ythKnowledge.getPraiseNum() + 1;
        ythKnowledge.setPraiseNum(praiseNum);
        ythKnowledgeReposi.save(ythKnowledge);
    }

    private void knowledgePraiseNumMinusOne(YthKnowledge ythKnowledge) {
        int praiseNum = Math.max((ythKnowledge.getPraiseNum() - 1), 0);
        ythKnowledge.setPraiseNum(praiseNum);
        ythKnowledgeReposi.save(ythKnowledge);
    }

    @Transactional(rollbackFor = Exception.class)
    public String add(Long knowledgeId, int userId) throws Exception {
        try {
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(knowledgeId);
            if (ythKnowledge.isPresent()) {
                // 增加点赞量
                knowledgePraiseNumPlusOne(ythKnowledge.get());

                YthKnowledgePraise ythKnowledgePraise = ythKnowledgePraiseReposi.findByKnowledgeIdAndCreateBy(knowledgeId, userId);
                if (null == ythKnowledgePraise) {
                    ythKnowledgePraise = new YthKnowledgePraise();
                    ythKnowledgePraise.setKnowledgeId(knowledgeId);
                    ythKnowledgePraise.setCreateBy(userId);
                    ythKnowledgePraise.setCreateTime(System.currentTimeMillis());
                    ythKnowledgePraiseReposi.save(ythKnowledgePraise);
                }
                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userId, "insert", "knowledge",
                        "/knowledge/praise", "knowledgeId=" + knowledgeId, result, "", userId);
                return result;
            }
            String result = RespUtil.error("文档不存在", new JSONObject());
            logservice.append(userId, "insert", "knowledge",
                    "/knowledge/praise", "knowledgeId=" + knowledgeId, result, "", userId);
            return result;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String del(Long knowledgeId, int userId) throws Exception {
        try {
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(knowledgeId);
            if (ythKnowledge.isPresent()) {
                // 减少点赞量
                knowledgePraiseNumMinusOne(ythKnowledge.get());

                YthKnowledgePraise ythKnowledgePraise = ythKnowledgePraiseReposi.findByKnowledgeIdAndCreateBy(knowledgeId, userId);
                if (null != ythKnowledgePraise) {
                    ythKnowledgePraiseReposi.deleteById(ythKnowledgePraise.getPraiseId());
                }
                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userId, "delete", "knowledge",
                        "/knowledge/praise", "knowledgeId=" + knowledgeId, result, "", userId);
                return result;
            }
            String result = RespUtil.error("文档不存在", new JSONObject());
            logservice.append(userId, "delete", "knowledge",
                    "/knowledge/praise", "knowledgeId=" + knowledgeId, result, "", userId);
            return result;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
