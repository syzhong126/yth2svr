package com.szkj.knowledge.service;

import cn.hutool.json.JSONObject;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.YthKnowledge;
import com.szkj.knowledge.entity.YthKnowledgeFavor;
import com.szkj.knowledge.reposi.YthKnowledgeFavorReposi;
import com.szkj.knowledge.reposi.YthKnowledgeReposi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class FavorService {
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgeFavorReposi ythKnowledgeFavorReposi;
    @Autowired
    private YthOperationLogService logservice;

    private void knowledgeFavorNumPlusOne(YthKnowledge ythKnowledge) {
        int favorNum = ythKnowledge.getFavorNum() + 1;
        ythKnowledge.setFavorNum(favorNum);
        ythKnowledgeReposi.save(ythKnowledge);
    }

    private void knowledgeFavorNumMinusOne(YthKnowledge ythKnowledge) {
        int favorNum = Math.max((ythKnowledge.getFavorNum() - 1), 0);
        ythKnowledge.setFavorNum(favorNum);
        ythKnowledgeReposi.save(ythKnowledge);
    }

    @Transactional(rollbackFor = Exception.class)
    public String add(Long knowledgeId, int userId) throws Exception {
        try {
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(knowledgeId);
            if (ythKnowledge.isPresent()) {
                // 增加收藏量
                knowledgeFavorNumPlusOne(ythKnowledge.get());

                YthKnowledgeFavor ythKnowledgeFavor = ythKnowledgeFavorReposi.findByKnowledgeIdAndCreateBy(knowledgeId, userId);
                if (null == ythKnowledgeFavor) {
                    ythKnowledgeFavor = new YthKnowledgeFavor();
                    ythKnowledgeFavor.setKnowledgeId(knowledgeId);
                    ythKnowledgeFavor.setCreateBy(userId);
                    ythKnowledgeFavor.setCreateTime(System.currentTimeMillis());
                    ythKnowledgeFavorReposi.save(ythKnowledgeFavor);
                }
                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userId, "insert", "knowledge",
                        "/knowledge/favor", "knowledgeId=" + knowledgeId, result, "", userId);
                return result;
            }
            String result = RespUtil.error("文档不存在", new JSONObject());
            logservice.append(userId, "insert", "knowledge",
                    "/knowledge/favor", "knowledgeId=" + knowledgeId, result, "", userId);
            return result;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String del(Long knowledgeId, int userId) throws Exception {
        try {
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(knowledgeId);
            if (ythKnowledge.isPresent()) {
                // 减少收藏量
                knowledgeFavorNumMinusOne(ythKnowledge.get());

                YthKnowledgeFavor ythKnowledgeFavor = ythKnowledgeFavorReposi.findByKnowledgeIdAndCreateBy(knowledgeId, userId);
                if (null != ythKnowledgeFavor) {
                    ythKnowledgeFavorReposi.deleteById(ythKnowledgeFavor.getFavorId());
                }
                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userId, "delete", "knowledge",
                        "/knowledge/favor", "knowledgeId=" + knowledgeId, result, "", userId);
                return result;
            }
            String result = RespUtil.error("文档不存在", new JSONObject());
            logservice.append(userId, "delete", "knowledge",
                    "/knowledge/favor", "knowledgeId=" + knowledgeId, result, "", userId);
            return result;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
