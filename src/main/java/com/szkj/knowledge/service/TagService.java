package com.szkj.knowledge.service;

import cn.hutool.json.JSONObject;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.*;
import com.szkj.knowledge.reposi.YthKnowledgeDocTagReposi;
import com.szkj.knowledge.reposi.YthKnowledgeReposi;
import com.szkj.knowledge.reposi.YthKnowledgeTabooReposi;
import com.szkj.knowledge.reposi.YthKnowledgeTagReposi;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.service.YthNoticepushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TagService {
    @Autowired
    private YthKnowledgeTagReposi ythKnowledgeTagReposi;
    @Autowired
    private YthKnowledgeDocTagReposi ythKnowledgeDocTagReposi;
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgeTabooReposi ythKnowledgeTabooReposi;
    @Autowired
    private YthNoticepushService ythNoticepushService;

    @Transactional(rollbackFor = Exception.class)
    public String approved(Long tagId, int isApproved, int userId, String token) throws Exception {
        try {
            Optional<YthKnowledgeTag> ythKnowledgeTag = ythKnowledgeTagReposi.findById(tagId);
            if (ythKnowledgeTag.isPresent()) {
                ythKnowledgeTag.get().setIsApproved(isApproved);
                ythKnowledgeTag.get().setApprovedBy(userId);
                ythKnowledgeTag.get().setApprovedTime(System.currentTimeMillis());
                ythKnowledgeTagReposi.save(ythKnowledgeTag.get());

                // 更新关联文档的ext_2字段，该字段为审核通过的标签，多个标签用“,”分隔
                List<YthKnowledgeDocTag> ythKnowledgeDocTagList = ythKnowledgeDocTagReposi.findByTagId(tagId);
                for (int i = 0; i < ythKnowledgeDocTagList.size(); i++) {
                    Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(ythKnowledgeDocTagList.get(i).getKnowledgeId());
                    if (ythKnowledge.isPresent()) {
                        if (1 == isApproved) {
                            ythKnowledge.get().setExt2(ythKnowledge.get().getExt2().concat("," + ythKnowledgeTag.get().getTag()));
                        } else {
                            ythKnowledge.get().setExt2(ythKnowledge.get().getExt2().replaceAll("," + ythKnowledgeTag.get().getTag() + "|" + ythKnowledgeTag.get().getTag() + ",", ""));
                        }
                        ythKnowledgeReposi.save(ythKnowledge.get());
                    }
                }

                // 添加推送消息
                if (1 == isApproved) {
                    YthNoticePush ythNoticePush = new YthNoticePush();
                    ythNoticePush.setPushUser(ythKnowledgeTag.get().getCreateBy())
                            .setPushType(2).setPushStatus(0).setPushTime(System.currentTimeMillis())
                            .setFromSystem("yth").setMoudleDetail("knowledge")
                            .setCreateTime(System.currentTimeMillis())
                            .setUpdateTime(System.currentTimeMillis())
                            .setTitle("文档标签")
                            .setContent("您创建的标签已审核通过。标签：" + ythKnowledgeTag.get().getTag() + "。")
                            .setMoudleType("5");
                    ythNoticepushService.append(ythNoticePush, token);
                }

                return RespUtil.success("成功", new JSONObject());
            }
            return RespUtil.error("评论不存在", new JSONObject());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void checkTagsIfContainTaboo(List<YthKnowledgeTag> tagList) {
        List<YthKnowledgeTaboo> tabooList = (List<YthKnowledgeTaboo>) ythKnowledgeTabooReposi.findAll();
        for (int i = 0; i < tagList.size(); i ++) {
            boolean hasTaboo = false;
            for (int j = 0; j < tabooList.size(); j ++) {
                if (tagList.get(i).getTag().contains(tabooList.get(j).getTaboo())) {
                    hasTaboo = true;
                }
            }
            if (hasTaboo) {
                tagList.get(i).setHasTaboo(2);
                tagList.get(i).setIsApproved(0);
            } else {
                tagList.get(i).setHasTaboo(1);
            }
            tagList.get(i).setCheckedTime(System.currentTimeMillis());
            ythKnowledgeTagReposi.save(tagList.get(i));
        }
    }

    public void checkTag(long tagId) {
        boolean hasTaboo = false;
        Optional<YthKnowledgeTag> ythKnowledgeTag = ythKnowledgeTagReposi.findById(tagId);
        if (ythKnowledgeTag.isPresent()) {
            List<YthKnowledgeTaboo> tabooList = (List<YthKnowledgeTaboo>) ythKnowledgeTabooReposi.findAll();
            for (int i = 0; i < tabooList.size(); i++) {
                if (ythKnowledgeTag.get().getTag().contains(tabooList.get(i).getTaboo())) {
                    hasTaboo = true;
                }
            }
            if (hasTaboo) {
                ythKnowledgeTag.get().setHasTaboo(2);
                ythKnowledgeTag.get().setIsApproved(0);
            } else {
                ythKnowledgeTag.get().setHasTaboo(1);
            }
            ythKnowledgeTag.get().setCheckedTime(System.currentTimeMillis());
            ythKnowledgeTagReposi.save(ythKnowledgeTag.get());
        }
    }

    public void checkUncheckedTag() {
        List<YthKnowledgeTag> tagList = ythKnowledgeTagReposi.findAllUnckeckedTag();
        checkTagsIfContainTaboo(tagList);
    }

    public void checkCheckedTag() {
        List<YthKnowledgeTag> tagList = ythKnowledgeTagReposi.findAllCkeckedTag();
        checkTagsIfContainTaboo(tagList);
    }
}
