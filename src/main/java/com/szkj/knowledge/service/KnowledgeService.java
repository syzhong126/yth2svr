package com.szkj.knowledge.service;

import cn.hutool.json.JSONArray;
import com.szkj.auth.entity.User;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.reposi.UserReposi;
import com.szkj.common.service.SQLService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.JsonUtil;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.*;
import com.szkj.knowledge.vo.*;
import com.szkj.knowledge.reposi.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import cn.hutool.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class KnowledgeService {
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgeShareScopeReposi ythKnowledgeShareScopeReposi;
    @Autowired
    private YthKnowledgeDocTagReposi ythKnowledgeDocTagReposi;
    @Autowired
    private YthKnowledgeTagReposi ythKnowledgeTagReposi;
    @Autowired
    private SQLService sqlService;
    @Autowired
    private YthKnowledgeBrowseReposi ythKnowledgeBrowseReposi;
    @Autowired
    private YthKnowledgeFavorReposi ythKnowledgeFavorReposi;
    @Autowired
    private YthKnowledgePraiseReposi ythKnowledgePraiseReposi;
    @Autowired
    private YthOrganazitionReposi ythOrganazitionReposi;
    @Autowired
    private UserReposi ythUserReposi;
    @Autowired
    private AuthorizeService authorizeService;
    @Autowired
    private YthOperationLogService logservice;

    private final int KNOWLEDGE_TYPE_DIR = 0;
    private final int KNOWLEDGE_TYPE_DOC = 1;

    private final int TAG_OP_CREATE = 1;

    private void addKnowledgeShareScope(YthKnowledge ythKnowledge, List<YthKnowledgeShareScope> shareScope) {
        for (int i = 0; i < shareScope.size(); i++) {
            YthKnowledgeShareScope scope = shareScope.get(i);
            YthKnowledgeShareScope ythKnowledgeShareScope = new YthKnowledgeShareScope();
            ythKnowledgeShareScope.setKnowledgeId(ythKnowledge.getKnowledgeId());
            ythKnowledgeShareScope.setType(ythKnowledge.getType());
            ythKnowledgeShareScope.setCanRead(scope.getCanRead());
            ythKnowledgeShareScope.setCanWrite(scope.getCanWrite());
            ythKnowledgeShareScope.setScopeType(scope.getScopeType());
            ythKnowledgeShareScope.setId(scope.getId());
            ythKnowledgeShareScopeReposi.save(ythKnowledgeShareScope);
        }
    }

    private void updateKnowledgeShareScope(YthKnowledge ythKnowledge, List<YthKnowledgeShareScope> shareScope) {
        ythKnowledgeShareScopeReposi.deleteByKnowledgeId(ythKnowledge.getKnowledgeId());
        addKnowledgeShareScope(ythKnowledge, shareScope);
    }

    private void addDocTags(YthKnowledge ythKnowledge, List<AddDocTag> addTags, int userId) {
        for (int i = 0; i < addTags.size(); i++) {
            AddDocTag tag = addTags.get(i);
            Long tagId;
            boolean isCreateTag = false;
            if (TAG_OP_CREATE == tag.getIsCreate()) {
                // 要判重，因为标签未审核通过时查询不到，导致用户再次创建。
                YthKnowledgeTag ythKnowledgeTag = ythKnowledgeTagReposi.finByTag(tag.getTag());
                if (null == ythKnowledgeTag) {
                    ythKnowledgeTag = new YthKnowledgeTag();
                    ythKnowledgeTag.setTag(tag.getTag());
                    ythKnowledgeTag.setCreateBy(userId);
                    ythKnowledgeTag.setCreateTime(System.currentTimeMillis());
                    YthKnowledgeTag savedYthKnowledgeTag = ythKnowledgeTagReposi.save(ythKnowledgeTag);
                    tagId = savedYthKnowledgeTag.getTagId();
                } else {
                    tagId = ythKnowledgeTag.getTagId();
                }
                isCreateTag = true;
            } else {
                tagId = tag.getTagId();
            }
            YthKnowledgeDocTag ythKnowledgeDocTag = new YthKnowledgeDocTag();
            ythKnowledgeDocTag.setKnowledgeId(ythKnowledge.getKnowledgeId());
            ythKnowledgeDocTag.setTagId(tagId);
            ythKnowledgeDocTag.setCreateTag(isCreateTag);
            ythKnowledgeDocTagReposi.save(ythKnowledgeDocTag);
        }
    }

    private void delDocTags(YthKnowledge ythKnowledge, List<YthKnowledgeTag> delTags) {
        for (int i = 0; i < delTags.size(); i++) {
            YthKnowledgeTag tag = delTags.get(i);
            ythKnowledgeDocTagReposi.deleteByKnowledgeIdAndTagId(ythKnowledge.getKnowledgeId(), tag.getTagId());
        }
    }

    private void setKnowledgeLevelInfo(YthKnowledge ythKnowledge) {
        Optional<YthKnowledge> parent = ythKnowledgeReposi.findById(ythKnowledge.getParentId());
        if (parent.isPresent()) {
            ythKnowledge.setLevel(parent.get().getLevel() + 1);
            List<YthKnowledge> brotherList = ythKnowledgeReposi.findByParentId(ythKnowledge.getParentId());
            if (!brotherList.isEmpty()) {
                YthKnowledge brother = ythKnowledgeReposi.findByParentIdAndMaxPathCode(ythKnowledge.getParentId());
                ythKnowledge.setPathCode(parent.get().getPathCode() + String.format("%04d", Integer.parseInt(brother.getPathCode().substring(brother.getPathCode().length() - 4)) + 1));
            } else {
                ythKnowledge.setPathCode(parent.get().getPathCode() + "0001");
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String addKnowledge(KnowledgeReq req, JSONObject userInfo) throws Exception {

        try {
            // 检查当前用户是否有权限在所选的父类别下新建知识
            if (!authorizeService.canWriteDir(userInfo, req.getParentId())) {
                String result = RespUtil.error("没有操作权限。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            }

            YthKnowledge ythKnowledge = new YthKnowledge();
            // 类型
            ythKnowledge.setType(req.getType());
            // 父类别
            ythKnowledge.setParentId(req.getParentId());
            // 层级信息
            setKnowledgeLevelInfo(ythKnowledge);
            // 名称
            ythKnowledge.setName(req.getName());
            // 创建人
            ythKnowledge.setCreateName(userInfo.getStr("userName"));
            ythKnowledge.setCreateBy(String.valueOf(userInfo.getInt("userId")));
            // 创建时间
            ythKnowledge.setCreateTime(System.currentTimeMillis());
            // 修改人信息
            ythKnowledge.setUpdateName(userInfo.getStr("userName"));
            ythKnowledge.setUpdateBy(String.valueOf(userInfo.getInt("userId")));
            // 修改时间
            ythKnowledge.setUpdateTime(System.currentTimeMillis());
            // 是否包含子类别
            ythKnowledge.setHasChild("0");

            if (KNOWLEDGE_TYPE_DIR == req.getType()) {
                // 检查用户是否有新增类别目录的权限
                String userRole = authorizeService.getUserRole(userInfo);
                if (!authorizeService.ROLE_APP_MGR.equals(userRole) && !authorizeService.ROLE_MGR.equals(userRole)) {
                    String result = RespUtil.error("没有操作权限。", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                            "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                    return result;
                }

                // 新增知识类别记录
                YthKnowledge savedYthKnowledge = ythKnowledgeReposi.save(ythKnowledge);

                // 所选的父类别是否包含子类别
                Optional<YthKnowledge> parent = ythKnowledgeReposi.findById(ythKnowledge.getParentId());
                if (parent.isPresent()) {
                    parent.get().setHasChild("1");
                    ythKnowledgeReposi.save(parent.get());
                }

                // 新增知识类别的共享属性
                addKnowledgeShareScope(savedYthKnowledge, req.getShareScope());

                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            } else if (KNOWLEDGE_TYPE_DOC == req.getType()) {

                // 知识文档的作者
                ythKnowledge.setAuthor(req.getAuthor());
                // 知识文档的大小
                ythKnowledge.setDocSize(req.getDocSize());
                // 知识文档的大小
                ythKnowledge.setDocSizeName(req.getDocSizeName());
                // 知识文档的存储桶
                ythKnowledge.setBucket(req.getBucket());
                // 知识文档的存储名
                ythKnowledge.setFileName(req.getFileName());
                // 知识文档的下载地址
                ythKnowledge.setDownloadUrl(req.getDownloadUrl());
                // 知识文档的可下载标识
                ythKnowledge.setDownload(req.getDownload());
                // 知识文档的下载需审核标识
                ythKnowledge.setDownloadCheck(req.getDownloadCheck());
                // 知识文档的发布状态标识
                ythKnowledge.setIsIssued(req.getIsIssued());

                // 新增知识文档记录
                YthKnowledge savedYthKnowledge = ythKnowledgeReposi.save(ythKnowledge);

                // 新增文档标签
                addDocTags(savedYthKnowledge, req.getAddTags(), userInfo.getInt("userId"));

                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            } else {
                log.error("未定义的type值：" + req.getType());
                String result = RespUtil.error("知识类型未定义。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "insert", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String updateKnowledge(KnowledgeReq req, JSONObject userInfo) throws Exception {

        try {
            // 检查当前用户是否有权限在所选的父类别下新建知识
            if (!authorizeService.canWriteDir(userInfo, req.getParentId())) {
                String result = RespUtil.error("没有操作权限。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            }

            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(req.getKnowledgeId());
            if (!ythKnowledge.isPresent()) {
                String result = RespUtil.error("当前知识文档/类别不存在，无法更新。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            }

            if (req.getType() != ythKnowledge.get().getType()) {
                String result = RespUtil.error("知识文档/类别的类型不可修改。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            }

            // 名称
            ythKnowledge.get().setName(req.getName());
            // 修改人信息
            ythKnowledge.get().setUpdateName(userInfo.getStr("userName"));
            ythKnowledge.get().setUpdateBy(String.valueOf(userInfo.getInt("userId")));
            // 修改时间
            ythKnowledge.get().setUpdateTime(System.currentTimeMillis());

            if (KNOWLEDGE_TYPE_DIR == req.getType()) {
                // 检查当前用户是否有权限修改当前知识类别
                String userRole = authorizeService.getUserRole(userInfo);
                if (!authorizeService.ROLE_APP_MGR.equals(userRole)
                        && !(authorizeService.ROLE_MGR.equals(userRole) && (ythKnowledge.get().getCreateBy().equals(String.valueOf(userInfo.getInt("userId")))))) {
                    String result = RespUtil.error("没有操作权限。", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                            "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                    return result;
                }

                // 更新所属父类别
                if (!req.getParentId().equals(ythKnowledge.get().getParentId())) {

                    Long oldParentId = ythKnowledge.get().getParentId();
                    Long newParentId = req.getParentId();
                    ythKnowledge.get().setParentId(newParentId);

                    // 所有子类别/文档的层级和路径编码都需要更新
                    String oldPathCodePrefix = ythKnowledge.get().getPathCode();
                    int oldLevel = ythKnowledge.get().getLevel();
                    setKnowledgeLevelInfo(ythKnowledge.get());
                    String newPathCodePrefix = ythKnowledge.get().getPathCode();
                    int newLevel = ythKnowledge.get().getLevel();
                    int levelDiff = newLevel - oldLevel;

                    List<YthKnowledge> knowledgeList = ythKnowledgeReposi.findAllByPathCodePrefix(oldPathCodePrefix);
                    for (int i = 0; i < knowledgeList.size(); i++) {
                        // 当前知识类别，不做任何处理
                        if (knowledgeList.get(i).getPathCode().equals(oldPathCodePrefix)) {
                            continue;
                        }

                        // 子类别/文档层级
                        knowledgeList.get(i).setLevel(knowledgeList.get(i).getLevel() + levelDiff);

                        // 子类别/文档路径编码
                        int length = knowledgeList.get(i).getPathCode().length();
                        String tailCode = knowledgeList.get(i).getPathCode().substring(oldPathCodePrefix.length(), length);
                        knowledgeList.get(i).setPathCode(newPathCodePrefix + tailCode);

                        ythKnowledgeReposi.save(knowledgeList.get(i));
                    }

                    // 所属父类别是否包含子类别
                    List<YthKnowledge> brotherDirlist = ythKnowledgeReposi.findByParentIdAndType(oldParentId, KNOWLEDGE_TYPE_DIR);
                    if (0 >= brotherDirlist.size()) {
                        Optional<YthKnowledge> parent = ythKnowledgeReposi.findById(ythKnowledge.get().getParentId());
                        if (parent.isPresent()) {
                            parent.get().setHasChild("0");
                            ythKnowledgeReposi.save(parent.get());
                        }
                    }
                }

                // 更新知识类别记录
                YthKnowledge savedYthKnowledge = ythKnowledgeReposi.save(ythKnowledge.get());

                // 修改知识类别的共享属性
                updateKnowledgeShareScope(savedYthKnowledge, req.getShareScope());

                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            } else if (KNOWLEDGE_TYPE_DOC == req.getType()) {
                // 检查当前用户是否有权限修改当前知识文档
                String userRole = authorizeService.getUserRole(userInfo);
                if (!authorizeService.ROLE_APP_MGR.equals(userRole)
                        && !authorizeService.ROLE_MGR.equals(userRole) && !ythKnowledge.get().getCreateBy().equals(String.valueOf(userInfo.getInt("userId")))) {
                    String result = RespUtil.error("没有操作权限。", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                            "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                    return result;
                }

                // 更新所属父类别
                if (!req.getParentId().equals(ythKnowledge.get().getParentId())) {
                    // 当前知识文档的层级相关信息
                    ythKnowledge.get().setParentId(req.getParentId());
                    setKnowledgeLevelInfo(ythKnowledge.get());
                }

                // 知识文档的作者
                ythKnowledge.get().setAuthor(req.getAuthor());
                // 知识文档的大小
                ythKnowledge.get().setDocSize(req.getDocSize());
                // 知识文档的下载地址
                ythKnowledge.get().setDocSizeName(req.getDocSizeName());
                // 知识文档的下载地址
                ythKnowledge.get().setBucket(req.getBucket());
                // 知识文档的下载地址
                ythKnowledge.get().setFileName(req.getFileName());
                // 知识文档的下载地址
                ythKnowledge.get().setDownloadUrl(req.getDownloadUrl());
                // 知识文档的可下载标识
                ythKnowledge.get().setDownload(req.getDownload());
                // 知识文档的下载需审核标识
                ythKnowledge.get().setDownloadCheck(req.getDownloadCheck());
                // 知识文档的发布状态标识
                ythKnowledge.get().setIsIssued(req.getIsIssued());

                // 更新知识文档记录
                YthKnowledge savedYthKnowledge = ythKnowledgeReposi.save(ythKnowledge.get());

                // 新增文档标签
                addDocTags(savedYthKnowledge, req.getAddTags(), userInfo.getInt("userId"));
                // 删除文档标签
                delDocTags(savedYthKnowledge, req.getDelTags());

                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            } else {
                log.error("未定义的type值：" + req.getType());
                String result = RespUtil.error("知识类型未定义。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String delKnowledge(KnowledgeReq req, JSONObject userInfo) throws Exception {
        try {
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(req.getKnowledgeId());
            if (!ythKnowledge.isPresent()) {
                String result = RespUtil.error("当前知识文档/类别不存在，无法更新。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            }

            // 检查当前用户是否有权限在所选的父类别下删除知识
            if (!authorizeService.canWriteDir(userInfo, ythKnowledge.get().getParentId())) {
                String result = RespUtil.error("没有操作权限。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                        "/knowledge",  req.toString(), result, "",  userInfo.getInt("userId"));
                return result;
            }

            if (KNOWLEDGE_TYPE_DIR == ythKnowledge.get().getType()) {
                // 检查当前用户是否有权限删除当前知识类别
                String userRole = authorizeService.getUserRole(userInfo);

                if (!authorizeService.ROLE_APP_MGR.equals(userRole)
                        && !(authorizeService.ROLE_MGR.equals(userRole) && ythKnowledge.get().getCreateBy().equals(String.valueOf(userInfo.getInt("userId"))))) {
                    String result = RespUtil.error("没有操作权限。", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                            "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                    return result;
                }

                List<YthKnowledge> childrenList = ythKnowledgeReposi.findChildrenByPathCodePrefix(ythKnowledge.get().getPathCode(), ythKnowledge.get().getKnowledgeId());
                if (childrenList.size() > 0) {
                    String result = RespUtil.error("当前类别非空，不可删除。", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                            "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                    return result;
                }

                ythKnowledge.get().setIsDel(1);
                // 所属父类别是否包含子类别
                List<YthKnowledge> brotherDirlist = ythKnowledgeReposi.findByParentIdAndType(ythKnowledge.get().getParentId(), KNOWLEDGE_TYPE_DIR);
                if (0 >= brotherDirlist.size()) {
                    Optional<YthKnowledge> parent = ythKnowledgeReposi.findById(ythKnowledge.get().getParentId());
                    if (parent.isPresent()) {
                        parent.get().setHasChild("0");
                        ythKnowledgeReposi.save(parent.get());
                    }
                }

                ythKnowledgeReposi.save(ythKnowledge.get());

                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            } else if (KNOWLEDGE_TYPE_DOC == req.getType()) {
                // 检查当前用户是否有权限删除当前知识文档
                String userRole = authorizeService.getUserRole(userInfo);
                if (!authorizeService.ROLE_APP_MGR.equals(userRole) && !authorizeService.ROLE_MGR.equals(userRole)
                        && (authorizeService.ROLE_USER.equals(userRole) && !ythKnowledge.get().getCreateBy().equals(String.valueOf(userInfo.getInt("userId"))))) {
                    String result = RespUtil.error("没有操作权限。", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                            "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                    return result;
                }

                ythKnowledge.get().setIsDel(1);
                ythKnowledgeReposi.save(ythKnowledge.get());

                String result = RespUtil.success("成功", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            } else {
                log.error("未定义的type值：" + req.getType());
                String result = RespUtil.error("知识类型未定义。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "delete", "knowledge",
                        "/knowledge", req.toString(), result, "", userInfo.getInt("userId"));
                return result;
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public String getKnowledgeInfo(Long knowledgeId, JSONObject userInfo) {
        Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(knowledgeId);
        if (ythKnowledge.isPresent()) {
            if (KNOWLEDGE_TYPE_DIR == ythKnowledge.get().getType()) {
                if (authorizeService.canReadDir(userInfo, ythKnowledge.get().getParentId())) {
                    KnowledgeResp knowledgeResp = new KnowledgeResp();
                    BeanUtils.copyProperties(ythKnowledge.get(), knowledgeResp);

                    YthKnowledge parent = ythKnowledgeReposi.getParentNameByParentId(ythKnowledge.get().getParentId());
                    knowledgeResp.setParentName(null != parent ? parent.getName() : "");

                    List<YthKnowledge> ythKnowledgeList = ythKnowledgeReposi.findByParentIdAndType(ythKnowledge.get().getKnowledgeId(), KNOWLEDGE_TYPE_DIR);
                    if (null != ythKnowledgeList) {
                        knowledgeResp.setHasChildren(1);
                    } else {
                        knowledgeResp.setHasChildren(0);
                    }

                    knowledgeResp.setBrowsed(0);
                    knowledgeResp.setFavored(0);
                    knowledgeResp.setPraised(0);

                    knowledgeResp.setCreateBy(ythKnowledge.get().getCreateName());
                    knowledgeResp.setUpdateBy(ythKnowledge.get().getUpdateName());

                    List<YthKnowledgeShareScope> ythKnowledgeShareScopeList = ythKnowledgeShareScopeReposi.findByKnowledgeId(knowledgeId);
                    List<KnowledgeShareScopeResp> shareScopeList = new ArrayList<>();
                    for (int i = 0; i < ythKnowledgeShareScopeList.size(); i++) {
                        KnowledgeShareScopeResp knowledgeShareScopeResp = new KnowledgeShareScopeResp();
                        BeanUtils.copyProperties(ythKnowledgeShareScopeList.get(i), knowledgeShareScopeResp);
                        if (4 != knowledgeShareScopeResp.getScopeType()) {
                            Optional<YthOrganazition> ythOrganazitionOptional = ythOrganazitionReposi.findById(knowledgeShareScopeResp.getId());
                            ythOrganazitionOptional.ifPresent(ythOrganazition -> knowledgeShareScopeResp.setName(StringUtils.isBlank(ythOrganazition.getShortName()) ? ythOrganazition.getOrgName() : ythOrganazition.getShortName()));
                        } else {
                            Optional<User> userOptional = ythUserReposi.findById(knowledgeShareScopeResp.getId());
                            userOptional.ifPresent(user -> knowledgeShareScopeResp.setName(user.getUserName()));
                        }
                        shareScopeList.add(knowledgeShareScopeResp);
                    }
                    knowledgeResp.setShareScope(shareScopeList);
                    return RespUtil.success("成功", knowledgeResp);
                }
                return RespUtil.error("没有权限", new JSONObject());
            } else {
                if (authorizeService.canReadDir(userInfo, ythKnowledge.get().getParentId())) {
                    KnowledgeResp knowledgeResp = new KnowledgeResp();
                    BeanUtils.copyProperties(ythKnowledge.get(), knowledgeResp);

                    YthKnowledge parent = ythKnowledgeReposi.getParentNameByParentId(ythKnowledge.get().getParentId());
                    knowledgeResp.setParentName(null != parent ? parent.getName() : "");

                    knowledgeResp.setHasChildren(0);

                    YthKnowledgeBrowse ythKnowledgeBrowse = ythKnowledgeBrowseReposi.findByKnowledgeIdAndCreateBy(
                            ythKnowledge.get().getKnowledgeId()
                            , userInfo.getInt("userId")
                    );
                    if (null != ythKnowledgeBrowse) {
                        knowledgeResp.setBrowsed(1);
                    } else {
                        knowledgeResp.setBrowsed(0);
                    }

                    YthKnowledgeFavor ythKnowledgeFavor = ythKnowledgeFavorReposi.findByKnowledgeIdAndCreateBy(
                            ythKnowledge.get().getKnowledgeId()
                            , userInfo.getInt("userId")
                    );
                    if (null != ythKnowledgeFavor) {
                        knowledgeResp.setFavored(1);
                    } else {
                        knowledgeResp.setFavored(0);
                    }

                    YthKnowledgePraise ythKnowledgePraised = ythKnowledgePraiseReposi.findByKnowledgeIdAndCreateBy(
                            ythKnowledge.get().getKnowledgeId()
                            , userInfo.getInt("userId")
                    );
                    if (null != ythKnowledgePraised) {
                        knowledgeResp.setPraised(1);
                    } else {
                        knowledgeResp.setPraised(0);
                    }

                    knowledgeResp.setCreateBy(ythKnowledge.get().getCreateName());
                    knowledgeResp.setUpdateBy(ythKnowledge.get().getUpdateName());

                    List<YthKnowledgeTag> ythKnowledgeTagsList = ythKnowledgeTagReposi.findByKnowledgeId(knowledgeId);
                    List<KnowledgeTagsResp> tagsRespList = new ArrayList<>();
                    for (int i = 0; i < ythKnowledgeTagsList.size(); i++) {
                        KnowledgeTagsResp knowledgeTagsResp = new KnowledgeTagsResp();
                        BeanUtils.copyProperties(ythKnowledgeTagsList.get(i), knowledgeTagsResp);
                        tagsRespList.add(knowledgeTagsResp);
                    }
                    knowledgeResp.setTags(tagsRespList);
                    return RespUtil.success("成功", knowledgeResp);
                }
                return RespUtil.error("没有权限", new JSONObject());
            }
        }
        return RespUtil.error("知识不存在", new JSONObject());
    }

    public JSONObject resetKnowledgeListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {

        String userRole = authorizeService.getUserRole(userInfo);

        if (authorizeService.ROLE_USER.equals(userRole)) {
            JSONObject userDefaultOrgInfo = authorizeService.findUserDefaultOrgInfo(userInfo);
            if (userDefaultOrgInfo != null) {
                int unitId = userDefaultOrgInfo.getInt("orgId");
                int deptId = userDefaultOrgInfo.getInt("deptId");
                int postId = userDefaultOrgInfo.getInt("postId");
                int userId = userInfo.getInt("userId");
                JsonUtil.resetJsonKeyValue(body, "sqlId", 1101);
                JsonUtil.resetJsonKeyValue(body, "params2", "{unitId:" + unitId + ", deptId:" + deptId + ", postId:" + postId + ", userId:" + userId + "}");
            } else {
                throw new Exception("没有权限");
            }
        } else if (authorizeService.ROLE_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1102);
            JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        } else if (authorizeService.ROLE_APP_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1103);
            JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        } else if (authorizeService.ROLE_NONE.equals(userRole)) {
            throw new Exception("没有权限");
        }

        return body;
    }

    public JSONObject resetShareToOthersListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {
        String userRole = authorizeService.getUserRole(userInfo);
        if (authorizeService.ROLE_NONE.equals(userRole)) {
            throw new Exception("没有权限");
        }

        JsonUtil.resetJsonKeyValue(body, "sqlId", 1104);
        JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        return body;
    }

    public JSONObject resetShareToMeListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {
        String userRole = authorizeService.getUserRole(userInfo);
        if (authorizeService.ROLE_NONE.equals(userRole)) {
            throw new Exception("没有权限");
        }

        JsonUtil.resetJsonKeyValue(body, "sqlId", 1105);
        JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        return body;
    }

    public JSONObject resetMyFavorListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {
        String userRole = authorizeService.getUserRole(userInfo);
        if (authorizeService.ROLE_NONE.equals(userRole)) {
            throw new Exception("没有权限");
        }

        JsonUtil.resetJsonKeyValue(body, "sqlId", 1106);
        JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        return body;
    }

    public JSONObject resetMyDocListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {
        String userRole = authorizeService.getUserRole(userInfo);
        if (authorizeService.ROLE_NONE.equals(userRole)) {
            throw new Exception("没有权限");
        }

        JsonUtil.resetJsonKeyValue(body, "sqlId", 1107);
        JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        return body;
    }

    public JSONObject resetDownloadReqApproveListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {

        String userRole = authorizeService.getUserRole(userInfo);

        if (authorizeService.ROLE_USER.equals(userRole)) {
            JSONObject userDefaultOrgInfo = authorizeService.findUserDefaultOrgInfo(userInfo);
            if (userDefaultOrgInfo != null) {
                int unitId = userDefaultOrgInfo.getInt("orgId");
                int deptId = userDefaultOrgInfo.getInt("deptId");
                int postId = userDefaultOrgInfo.getInt("postId");
                int userId = userInfo.getInt("userId");
                JsonUtil.resetJsonKeyValue(body, "sqlId", 1108);
                JsonUtil.resetJsonKeyValue(body, "params2", "{unitId:" + unitId + ", deptId:" + deptId + ", postId:" + postId + ", userId:" + userId + "}");
            } else {
                throw new Exception("没有权限");
            }
        } else if (authorizeService.ROLE_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1109);
            JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        } else if (authorizeService.ROLE_APP_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1110);
            JsonUtil.resetJsonKeyValue(body, "params2", "");
        } else {
            throw new Exception("没有权限");
        }

        return body;
    }

    public JSONObject resetDownloadMyReqApproveListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {
        JSONObject userDefaultOrgInfo = authorizeService.findUserDefaultOrgInfo(userInfo);
        if (userDefaultOrgInfo != null) {
            int unitId = userDefaultOrgInfo.getInt("orgId");
            int deptId = userDefaultOrgInfo.getInt("deptId");
            int postId = userDefaultOrgInfo.getInt("postId");
            int userId = userInfo.getInt("userId");
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1108);
            JsonUtil.resetJsonKeyValue(body, "params2", "{unitId:" + unitId + ", deptId:" + deptId + ", postId:" + postId + ", userId:" + userId + "}");
        } else {
            throw new Exception("没有权限");
        }
        return body;
    }

    public JSONObject resetDownloadRecListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {

        String userRole = authorizeService.getUserRole(userInfo);

        if (authorizeService.ROLE_USER.equals(userRole)) {
            JSONObject userDefaultOrgInfo = authorizeService.findUserDefaultOrgInfo(userInfo);
            if (userDefaultOrgInfo != null) {
                int unitId = userDefaultOrgInfo.getInt("orgId");
                int deptId = userDefaultOrgInfo.getInt("deptId");
                int postId = userDefaultOrgInfo.getInt("postId");
                int userId = userInfo.getInt("userId");
                JsonUtil.resetJsonKeyValue(body, "sqlId", 1111);
                JsonUtil.resetJsonKeyValue(body, "params2", "{unitId:" + unitId + ", deptId:" + deptId + ", postId:" + postId + ", userId:" + userId + "}");
            } else {
                throw new Exception("没有权限");
            }
        } else if (authorizeService.ROLE_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1112);
            JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        } else if (authorizeService.ROLE_APP_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1113);
            JsonUtil.resetJsonKeyValue(body, "params2", "");
        } else {
            throw new Exception("没有权限");
        }

        return body;
    }

    public JSONObject resetDownloadMyRecListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {
        JSONObject userDefaultOrgInfo = authorizeService.findUserDefaultOrgInfo(userInfo);
        if (userDefaultOrgInfo != null) {
            int unitId = userDefaultOrgInfo.getInt("orgId");
            int deptId = userDefaultOrgInfo.getInt("deptId");
            int postId = userDefaultOrgInfo.getInt("postId");
            int userId = userInfo.getInt("userId");
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1111);
            JsonUtil.resetJsonKeyValue(body, "params2", "{unitId:" + unitId + ", deptId:" + deptId + ", postId:" + postId + ", userId:" + userId + "}");
        } else {
            throw new Exception("没有权限");
        }
        return body;
    }

    public JSONObject resetIssueApproveListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {

        String userRole = authorizeService.getUserRole(userInfo);

        if (authorizeService.ROLE_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1114);
            JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        } else if (authorizeService.ROLE_APP_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1115);
            JsonUtil.resetJsonKeyValue(body, "params2", "");
        } else {
            throw new Exception("没有权限");
        }

        return body;
    }

    public JSONObject resetCommentApproveListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {

        String userRole = authorizeService.getUserRole(userInfo);

        if (authorizeService.ROLE_USER.equals(userRole)) {
            JSONObject userDefaultOrgInfo = authorizeService.findUserDefaultOrgInfo(userInfo);
            if (userDefaultOrgInfo != null) {
                int unitId = userDefaultOrgInfo.getInt("orgId");
                int deptId = userDefaultOrgInfo.getInt("deptId");
                int postId = userDefaultOrgInfo.getInt("postId");
                int userId = userInfo.getInt("userId");
                JsonUtil.resetJsonKeyValue(body, "sqlId", 1116);
                JsonUtil.resetJsonKeyValue(body, "params2", "{unitId:" + unitId + ", deptId:" + deptId + ", postId:" + postId + ", userId:" + userId + "}");
            } else {
                throw new Exception("没有权限");
            }
        } else if (authorizeService.ROLE_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1117);
            JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        } else if (authorizeService.ROLE_APP_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1118);
            JsonUtil.resetJsonKeyValue(body, "params2", "");
        } else {
            throw new Exception("没有权限");
        }

        return body;
    }

    public JSONObject resetMyCommentApproveListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {
        JSONObject userDefaultOrgInfo = authorizeService.findUserDefaultOrgInfo(userInfo);
        if (userDefaultOrgInfo != null) {
            int unitId = userDefaultOrgInfo.getInt("orgId");
            int deptId = userDefaultOrgInfo.getInt("deptId");
            int postId = userDefaultOrgInfo.getInt("postId");
            int userId = userInfo.getInt("userId");
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1116);
            JsonUtil.resetJsonKeyValue(body, "params2", "{unitId:" + unitId + ", deptId:" + deptId + ", postId:" + postId + ", userId:" + userId + "}");
        } else {
            throw new Exception("没有权限");
        }
        return body;
    }

    public JSONObject resetTagApproveListQueryBody(JSONObject userInfo, JSONObject body) throws Exception {

        String userRole = authorizeService.getUserRole(userInfo);

        if (authorizeService.ROLE_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1119);
            JsonUtil.resetJsonKeyValue(body, "params2", "{userId:" + userInfo.getInt("userId") + "}");
        } else if (authorizeService.ROLE_APP_MGR.equals(userRole)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1120);
            JsonUtil.resetJsonKeyValue(body, "params2", "");
        } else {
            throw new Exception("没有权限");
        }

        return body;
    }

    public JSONObject setShareScopeListQueryBody(JSONObject userInfo, Long knowledgeId) throws Exception {

        JSONObject body = new JSONObject();

        if (authorizeService.canReadDir(userInfo, knowledgeId)) {
            JsonUtil.resetJsonKeyValue(body, "sqlId", 1121);
            JsonUtil.resetJsonKeyValue(body, "params2", "{knowledgeId:" + knowledgeId + "}");
        } else {
            throw new Exception("没有权限");
        }

        return body;
    }

    public String getCorrelateList(JSONObject userInfoJson, AuthUserInfoModel userInfo, Long knowledgeId) throws Exception {
        Optional<YthKnowledge> ythKnowledgeOptional = ythKnowledgeReposi.findById(knowledgeId);
        if (ythKnowledgeOptional.isPresent()) {
            if (authorizeService.canReadDir(userInfoJson, ythKnowledgeOptional.get().getParentId())) {
                JSONObject body = new JSONObject();
                JsonUtil.resetJsonKeyValue(body, "sqlId", 1125);
                JsonUtil.resetJsonKeyValue(body, "params2", "{knowledgeId:" + knowledgeId + "}");
                JsonUtil.resetJsonKeyValue(body, "pagenum", 1);
                JsonUtil.resetJsonKeyValue(body, "pagerows", 5);
                JsonUtil.resetJsonKeyValue(body, "orderby", "ORDER BY (browseNum + favorNum + praiseNum + shareNum + commentNum) DESC");

                JSONObject respData = new JSONObject();
                JsonUtil.resetJsonKeyValue(respData, "data",
                        sqlService.queryPageList(userInfo, body).getByPath("data", JSONArray.class));
                return RespUtil.success("成功", respData);
            }
            return RespUtil.error("没有权限", new JSONObject());
        }
        return RespUtil.error("知识不存在", new JSONObject());
    }

    public String getPathInfo(Long knowledgeId) {
        Optional<YthKnowledge> ythKnowledgeOptional = ythKnowledgeReposi.findById(knowledgeId);
        if (ythKnowledgeOptional.isPresent()) {
            String pathCode = ythKnowledgeOptional.get().getPathCode();
            StringBuilder where = new StringBuilder();
            List<KnowledgePathInfoResp> knowledgePathInfoRespList = new ArrayList<>();
            for (int i = 0; i < (pathCode.length() / 4) - 1; i++) {
                KnowledgePathInfoResp knowledgeAncestorResp = new KnowledgePathInfoResp();
                YthKnowledge ythKnowledge = ythKnowledgeReposi.findByPathCode(pathCode.substring(0, (i + 1) * 4));
                BeanUtils.copyProperties(ythKnowledge, knowledgeAncestorResp);
                knowledgePathInfoRespList.add(knowledgeAncestorResp);
            }
            JSONObject respData = new JSONObject();
            JsonUtil.resetJsonKeyValue(respData, "path",
                    knowledgePathInfoRespList);
            return RespUtil.success("成功", respData);
        }
        return RespUtil.error("知识不存在", new JSONObject());
    }
}
