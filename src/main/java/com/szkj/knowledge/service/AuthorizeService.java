package com.szkj.knowledge.service;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.szkj.knowledge.entity.YthKnowledge;
import com.szkj.knowledge.entity.YthKnowledgeTypeMgr;
import com.szkj.knowledge.reposi.YthKnowledgeReposi;
import com.szkj.knowledge.reposi.YthKnowledgeShareScopeReposi;
import com.szkj.knowledge.reposi.YthKnowledgeTypeMgrReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class AuthorizeService {
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgeTypeMgrReposi ythKnowledgeTypeMgrReposi;
    @Autowired
    private YthKnowledgeShareScopeReposi ythKnowledgeShareScopeReposi;

    public final String ROLE_NONE = "none";
    public final String ROLE_USER = "knowledgeUser";
    public final String ROLE_MGR = "knowledgeMgr";
    public final String ROLE_APP_MGR = "knowledgeAppMgr";

    public String getUserRole(JSONObject userInfo) {
        JSONArray roles = userInfo.getJSONArray("roleInfos");

        boolean isUser = false;
        for (int i = 0; i < roles.size(); i++) {
            JSONObject role = roles.getJSONObject(i);
            String roleDesc = role.getStr("roleDesc");
            if (ROLE_USER.equals(roleDesc)) {
                isUser = true;
            }
        }

        boolean isMgr = false;
        for (int i = 0; i < roles.size(); i++) {
            JSONObject role = roles.getJSONObject(i);
            String roleDesc = role.getStr("roleDesc");
            if (ROLE_MGR.equals(roleDesc)) {
                isMgr = true;
            }
        }

        boolean isAppMgr = false;
        for (int i = 0; i < roles.size(); i++) {
            JSONObject role = roles.getJSONObject(i);
            String roleDesc = role.getStr("roleDesc");
            if (ROLE_APP_MGR.equals(roleDesc)) {
                isAppMgr = true;
            }
        }

        return isAppMgr ? ROLE_APP_MGR : (isMgr ? ROLE_MGR : (isUser ? ROLE_USER : ROLE_NONE));
    }

    public JSONObject findUserDefaultOrgInfo(JSONObject userInfo) {
        boolean foundDefaultOrg = false;
        JSONObject orgInfo = null;

        JSONArray userOrgInfo = userInfo.getJSONArray("orgInfos");
        for (int i = 0; i < userOrgInfo.size(); i++) {
            orgInfo = (JSONObject) userOrgInfo.get(i);
            if (1 == orgInfo.getInt("isDefault")) {
                foundDefaultOrg = true;
                break;
            }
        }

        return foundDefaultOrg ? orgInfo: null;
    }

    private boolean isAncestorDir(Long ancestorId, Long knowledgeId) {
        Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(ancestorId);
        if (ythKnowledge.isPresent()) {
            return null != ythKnowledgeReposi.findByPathCodePrefixAndKnowledgeId(ythKnowledge.get().getPathCode(), knowledgeId);
        } else {
            return false;
        }
    }

    public boolean canReadDir(JSONObject userInfo, Long dirId) {
        boolean canRead = false;

        String userRole = getUserRole(userInfo);

        if (ROLE_APP_MGR.equals(userRole)) {
            canRead = true;
        } else if (ROLE_MGR.equals(userRole)) {
            int userId = userInfo.getInt("userId");
            List<YthKnowledgeTypeMgr> dirList = ythKnowledgeTypeMgrReposi.findByUserId(userId);
            for (int i = 0; i < dirList.size(); i++) {
                canRead = isAncestorDir(dirList.get(i).getKnowledgeId(), dirId);
                if (canRead) {
                    break;
                }
            }
        } else if (ROLE_USER.equals(userRole)) {
            JSONObject userDefaultOrgInfo = findUserDefaultOrgInfo(userInfo);
            if (userDefaultOrgInfo != null) {
                int unitId = userDefaultOrgInfo.getInt("orgId");
                int deptId = userDefaultOrgInfo.getInt("deptId");
                int postId = userDefaultOrgInfo.getInt("postId");
                int userId = userInfo.getInt("userId");
                canRead = (null != ythKnowledgeShareScopeReposi.findByDirIdAndCanReadAndUserInfo(dirId, unitId, deptId, postId, userId));
            }
        }

        return canRead;
    }

    public boolean canWriteDir(JSONObject userInfo, Long dirId) {
        boolean canWrite = false;
        int dirLevel = 0;

        Optional<YthKnowledge> ythKnowledgeOptional = ythKnowledgeReposi.findById(dirId);
        if (ythKnowledgeOptional.isPresent()) {
            dirLevel = ythKnowledgeOptional.get().getLevel();
        }

        if (0 != dirLevel && 1 != dirLevel) {
            String userRole = getUserRole(userInfo);
            if (ROLE_APP_MGR.equals(userRole)) {
                canWrite = true;
            } else if (ROLE_MGR.equals(userRole)) {
                int userId = userInfo.getInt("userId");
                List<YthKnowledgeTypeMgr> dirList = ythKnowledgeTypeMgrReposi.findByUserId(userId);
                for (int i = 0; i < dirList.size(); i++) {
                    canWrite = isAncestorDir(dirList.get(i).getKnowledgeId(), dirId);
                    if (canWrite) {
                        break;
                    }
                }
            } else if (ROLE_USER.equals(userRole) && 2 != dirLevel) {
                JSONObject userDefaultOrgInfo = findUserDefaultOrgInfo(userInfo);
                if (userDefaultOrgInfo != null) {
                    int unitId = userDefaultOrgInfo.getInt("orgId");
                    int deptId = userDefaultOrgInfo.getInt("deptId");
                    int postId = userDefaultOrgInfo.getInt("postId");
                    int userId = userInfo.getInt("userId");
                    canWrite = (null != ythKnowledgeShareScopeReposi.findByDirIdAndCanWriteAndUserInfo(dirId, unitId, deptId, postId, userId));
                }
            }
        }

        return canWrite;
    }
}
