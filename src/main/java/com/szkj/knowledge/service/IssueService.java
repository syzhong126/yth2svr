package com.szkj.knowledge.service;

import cn.hutool.json.JSONObject;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.YthKnowledge;
import com.szkj.knowledge.reposi.YthKnowledgeReposi;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.service.YthNoticepushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class IssueService {
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthNoticepushService ythNoticepushService;
    @Autowired
    private YthOperationLogService logservice;
    @Autowired
    private AuthorizeService authorizeService;

    public final int ISSUE_STATUS_DRAFT = 0;
    public final int ISSUE_STATUS_PENDING = 1;
    public final int ISSUE_STATUS_APPROVED = 2;
    public final int ISSUE_STATUS_REFUSED = 3;

    @Transactional(rollbackFor = Exception.class)
    public String issued(Long knowledgeId, int isIssued, String token, JSONObject userInfo) throws Exception{
        try {
            Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(knowledgeId);
            if (ythKnowledge.isPresent()) {
                String userRole = authorizeService.getUserRole(userInfo);
                if (!authorizeService.canWriteDir(userInfo, ythKnowledge.get().getParentId())
                        || !(authorizeService.ROLE_APP_MGR.equals(userRole) || authorizeService.ROLE_MGR.equals(userRole)
                        || ythKnowledge.get().getCreateBy().equals(String.valueOf(userInfo.getInt("userId"))))) {
                    String result = RespUtil.error("没有操作权限。", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                            "/knowledge/issued", "{\"knowledgeId\":" + knowledgeId + ",\"isIssued\":" + isIssued + "}", result, "", userInfo.getInt("userId"));
                    return result;
                }

                int oldStatus = ythKnowledge.get().getIsIssued();
                if ((oldStatus == ISSUE_STATUS_DRAFT && isIssued == ISSUE_STATUS_PENDING)
                        || (oldStatus == ISSUE_STATUS_PENDING && isIssued == ISSUE_STATUS_APPROVED)
                        || (oldStatus == ISSUE_STATUS_PENDING && isIssued == ISSUE_STATUS_REFUSED)
                        || (oldStatus == ISSUE_STATUS_APPROVED && isIssued == ISSUE_STATUS_DRAFT)
                        || (oldStatus == ISSUE_STATUS_REFUSED && isIssued == ISSUE_STATUS_DRAFT)
                        || (oldStatus == ISSUE_STATUS_PENDING && isIssued == ISSUE_STATUS_DRAFT)) {
                    ythKnowledge.get().setIsIssued(isIssued);
                    ythKnowledgeReposi.save(ythKnowledge.get());

                    // 添加推送消息
                    if (isIssued == ISSUE_STATUS_APPROVED) {
                        YthNoticePush ythNoticePush = new YthNoticePush();
                        ythNoticePush.setPushUser(Integer.parseInt(ythKnowledge.get().getCreateBy()))
                                .setPushType(2).setPushStatus(0).setPushTime(System.currentTimeMillis())
                                .setFromSystem("yth").setMoudleDetail("knowledge")
                                .setCreateTime(System.currentTimeMillis())
                                .setUpdateTime(System.currentTimeMillis())
                                .setTitle("文档发布")
                                .setContent("您的文档发布申请已审核通过。文档：" + ythKnowledge.get().getName() + "。")
                                .setMoudleType("3");
                        ythNoticepushService.append(ythNoticePush, token);
                    }
                    String result = RespUtil.success("成功", new JSONObject());
                    logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                            "/knowledge/issued", "{\"knowledgeId\":" + knowledgeId + ",\"isIssued\":" + isIssued + "}", result, "", userInfo.getInt("userId"));
                    return result;
                }
                String result = RespUtil.error("当前发布状态：" + isIssued + " 原发布状态：" + oldStatus + "。", new JSONObject());
                logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                        "/knowledge/issued", "{\"knowledgeId\":" + knowledgeId + ",\"isIssued\":" + isIssued + "}", result, "", userInfo.getInt("userId"));
                return result;
            }
            String result = RespUtil.error("文档不存在", new JSONObject());
            logservice.append(userInfo.getInt("userId"), "update", "knowledge",
                    "/knowledge/issued", "{\"knowledgeId\":" + knowledgeId + ",\"isIssued\":" + isIssued + "}", result, "", userInfo.getInt("userId"));
            return result;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
