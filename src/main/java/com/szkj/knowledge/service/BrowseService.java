package com.szkj.knowledge.service;

import cn.hutool.json.JSONObject;
import com.szkj.common.util.RespUtil;
import com.szkj.knowledge.entity.YthKnowledge;
import com.szkj.knowledge.entity.YthKnowledgeBrowse;
import com.szkj.knowledge.reposi.YthKnowledgeBrowseReposi;
import com.szkj.knowledge.reposi.YthKnowledgeReposi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BrowseService {
    @Autowired
    private YthKnowledgeReposi ythKnowledgeReposi;
    @Autowired
    private YthKnowledgeBrowseReposi ythKnowledgeBrowseReposi;

    private void knowledgeBrowseNumPlusOne(YthKnowledge ythKnowledge) {
        int browseNum = ythKnowledge.getBrowseNum() + 1;
        ythKnowledge.setBrowseNum(browseNum);
        ythKnowledgeReposi.save(ythKnowledge);
    }

    public String add(Long knowledgeId, int userId) {
        Optional<YthKnowledge> ythKnowledge = ythKnowledgeReposi.findById(knowledgeId);
        if (ythKnowledge.isPresent()) {
            // 增加阅读量
            knowledgeBrowseNumPlusOne(ythKnowledge.get());

            YthKnowledgeBrowse ythKnowledgeBrowse = ythKnowledgeBrowseReposi.findByKnowledgeIdAndCreateBy(knowledgeId, userId);
            if (null == ythKnowledgeBrowse) {
                ythKnowledgeBrowse = new YthKnowledgeBrowse();
                ythKnowledgeBrowse.setKnowledgeId(knowledgeId);
                ythKnowledgeBrowse.setCreateBy(userId);
                ythKnowledgeBrowse.setCreateTime(System.currentTimeMillis());
                ythKnowledgeBrowseReposi.save(ythKnowledgeBrowse);
            }
            return RespUtil.success("成功", new JSONObject());
        }
        return RespUtil.error("文档不存在", new JSONObject());
    }
}
