package com.szkj.mobile.reposi;


import com.szkj.mobile.entity.YthAppserverUserext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-20 15:14:24
 */
public interface  YthAppserverUserextReposi extends JpaRepository<YthAppserverUserext, Long>, JpaSpecificationExecutor<YthAppserverUserext> {

    @Query(value = "select * from yth_appserver_userext where user_account=?1 " , nativeQuery = true)
    List<YthAppserverUserext> findByUserAccount(@Param("user_account") String userAccount);
}

