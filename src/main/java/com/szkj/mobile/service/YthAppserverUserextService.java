package com.szkj.mobile.service;

import com.szkj.common.service.SQLService;
import com.szkj.mobile.entity.YthAppserverUserext;
import com.szkj.mobile.reposi.YthAppserverUserextReposi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class YthAppserverUserextService {
    @Autowired
    SQLService sqlUtil;

    @Autowired
    private YthAppserverUserextReposi reposi;

    public YthAppserverUserext append(YthAppserverUserext bean, String userAccount) throws Exception {
        try {
            /*
            * 根据用户账户信息获取历史记录
            * */
            List<YthAppserverUserext> t = reposi.findByUserAccount(userAccount);
            if(t!=null && t.size()>0){
                bean.setId(t.get(0).getId());
            }else{
                bean.setId(0);
            }
            Calendar calendar = Calendar.getInstance();
            long time = calendar.getTimeInMillis();
            bean.setCreateTime((int)time);
            bean.setUserAccount(userAccount);

            if(bean.getUserState()==1){
                bean.setLastLoginTime(bean.getCreateTime());
            }else{
                bean.setLastLogoutTime(bean.getCreateTime());
            }
            bean.setLastLogoutTime(0);

            reposi.save(bean);
            return bean;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

}
