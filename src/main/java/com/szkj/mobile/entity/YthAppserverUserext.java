package com.szkj.mobile.entity;

import javax.persistence.*;

@Entity
@Table(name = "yth_appserver_userext")
public class YthAppserverUserext {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "source_ip", nullable = false, length = 32)
    private String sourceIp;

    @Column(name = "app_version", nullable = false, length = 32)
    private String appVersion;

    @Column(name = "os_version", nullable = false, length = 32)
    private String osVersion;

    @Column(name = "mobile_vendor", nullable = false, length = 32)
    private String mobileVendor;

    @Column(name = "mobile_model", nullable = false, length = 32)
    private String mobileModel;

    @Column(name = "cpu_usage", nullable = false)
    private Double cpuUsage;

    @Column(name = "used_memory", nullable = false)
    private Integer usedMemory;

    @Column(name = "free_memory", nullable = false)
    private Integer freeMemory;

    @Column(name = "total_memory", nullable = false)
    private Integer totalMemory;

    @Column(name = "user_account", nullable = false, length = 32)
    private String userAccount;

    @Column(name = "user_state", nullable = false)
    private Integer userState;

    @Column(name = "device_token", length = 64)
    private String deviceToken;

    @Column(name = "last_login_time", nullable = false)
    private Integer lastLoginTime;

    @Column(name = "last_logout_time", nullable = false)
    private Integer lastLogoutTime;

    @Column(name = "create_time", nullable = false)
    private Integer createTime;

    @Column(name = "device_id", length = 64)
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Integer createTime) {
        this.createTime = createTime;
    }

    public Integer getLastLogoutTime() {
        return lastLogoutTime;
    }

    public void setLastLogoutTime(Integer lastLogoutTime) {
        this.lastLogoutTime = lastLogoutTime;
    }

    public Integer getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Integer lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Integer getUserState() {
        return userState;
    }

    public void setUserState(Integer userState) {
        this.userState = userState;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public Integer getTotalMemory() {
        return totalMemory;
    }

    public void setTotalMemory(Integer totalMemory) {
        this.totalMemory = totalMemory;
    }

    public Integer getFreeMemory() {
        return freeMemory;
    }

    public void setFreeMemory(Integer freeMemory) {
        this.freeMemory = freeMemory;
    }

    public Integer getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(Integer usedMemory) {
        this.usedMemory = usedMemory;
    }

    public Double getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(Double cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public String getMobileModel() {
        return mobileModel;
    }

    public void setMobileModel(String mobileModel) {
        this.mobileModel = mobileModel;
    }

    public String getMobileVendor() {
        return mobileVendor;
    }

    public void setMobileVendor(String mobileVendor) {
        this.mobileVendor = mobileVendor;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}