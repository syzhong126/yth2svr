package com.szkj.mobile.controller;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.SQLService;
import com.szkj.common.util.RespUtil;
import com.szkj.mobile.entity.YthAppserverUserext;
import com.szkj.mobile.service.YthAppserverUserextService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ye
 */
@Component
@RestController
@CrossOrigin("*")
@Slf4j
@RequestMapping(value="/mobile")
public class AppCtrl {

    @Autowired
    private SQLService sqlService;
    @Autowired
    private AuthService authService;
    @Autowired
    private YthAppserverUserextService ythService;

    @PostMapping("/loginUpload")
    public String loginUpload(@RequestBody JSONObject body, HttpServletRequest request) {
        log.info("loginUpload:"+body.toString());
        try {
            String _token = request.getHeader("token");
            //int userId = authService.getUserInfo(_token).getInt("userId");
            String  userAccount = authService.getUserInfo(_token).getStr("account");
            YthAppserverUserext bean = JSONUtil.toBean(body, YthAppserverUserext.class);
            ythService.append(bean,userAccount);
            return null;
        }catch (Exception e) {
            log.error("/common/query {}", e.getMessage());
            e.printStackTrace();
            return RespUtil.error(e.getMessage());
        }
    }

}
