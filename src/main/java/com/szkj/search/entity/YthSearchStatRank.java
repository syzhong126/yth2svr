package com.szkj.search.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文档标签云图数据表
 */
@Data
@Entity
@Table(name = "yth_search_stat_rank")
public class YthSearchStatRank implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 排行榜ID
     */
    @Id
    @Column(name = "rank_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rankId;

    /**
     * 排行榜数据
     */
    @Column(name = "data_txt", nullable = false)
    private String dataTxt;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Integer createTime = 0;

}
