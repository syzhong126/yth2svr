package com.szkj.search.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "yth_search_his_last")
public class YthSearchHisLast implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 最近搜索记录ID
     */
    @Id
    @Column(name = "last_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long lastId;

    /**
     * 搜索关键词
     */
    @Column(name = "keyword", nullable = false)
    private String keyword;

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Integer createTime;

}
