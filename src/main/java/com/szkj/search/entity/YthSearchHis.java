package com.szkj.search.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "yth_search_his")
public class YthSearchHis implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 搜索记录ID
     */
    @Id
    @Column(name = "search_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long searchId;

    /**
     * 搜索内容类型
     */
    @Column(name = "type", nullable = false)
    private String type = "";

    /**
     * 搜索关键词
     */
    @Column(name = "keyword", nullable = false)
    private String keyword = "";

    /**
     * 一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Integer createTime = 0;

}
