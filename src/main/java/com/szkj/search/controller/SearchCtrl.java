package com.szkj.search.controller;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.SQLService;
import com.szkj.common.util.RespUtil;
import com.szkj.search.service.SearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Component
@RestController
@CrossOrigin("*")
@RequestMapping(value="/search")
@Slf4j
public class SearchCtrl {
    @Autowired
    private SQLService sqlService;
    @Autowired
    private SearchService searchService;
    @Autowired
    private AuthService authService;

    @RequestMapping(value="/list", method = {RequestMethod.GET})
    public String search(HttpServletRequest request, @RequestParam("pageNum") int pageNum
            , @RequestParam("pageRows") int pageRows, @RequestParam("keyword") String keyword
            , @RequestParam("type") String type, @RequestParam("pos") String pos
            , @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {
        String token = request.getHeader("token");
        try {
            return searchService.search(authService.getLoginInfo(token).getInt("userId"), pageNum, pageRows
                    , keyword, type, pos, startDate, endDate);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/last/his", method = {RequestMethod.DELETE})
    public String delLastHis(HttpServletRequest request) {
        String token = request.getHeader("token");
        try {
            return searchService.delLastHis(authService.getLoginInfo(token).getInt("userId"));
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/last/his", method = {RequestMethod.POST})
    public String getLastHis(HttpServletRequest request, @RequestBody JSONObject body
            , @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        String token = request.getHeader("token");
        try {
            JSONObject newBody = searchService.resetLastHisListQueryBody(authService.getLoginInfo(token), body);
            JSONArray result = sqlService.queryList(userInfo, newBody);
            return RespUtil.success("成功", result);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/stat/rank", method = {RequestMethod.GET})
    public String getRank() {
        JSONArray result = searchService.getRank();
        return RespUtil.success("成功", result);
    }

}
