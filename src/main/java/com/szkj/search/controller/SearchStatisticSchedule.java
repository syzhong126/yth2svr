package com.szkj.search.controller;

import com.szkj.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SearchStatisticSchedule {
    @Autowired
    private SearchService searchService;

    @Scheduled(fixedRateString = "${keywordRankStatisticInterval}")
    public void keywordRankStatistic() {
        searchService.keywordRankStatistic();
    }

}
