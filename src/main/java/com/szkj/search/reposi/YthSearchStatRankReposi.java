package com.szkj.search.reposi;

import com.szkj.search.entity.YthSearchStatRank;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface YthSearchStatRankReposi extends CrudRepository<YthSearchStatRank, Integer>, JpaSpecificationExecutor<YthSearchStatRank> {

    @Query(value = "select * from yth_search_stat_rank order by create_time desc limit 1", nativeQuery = true)
    YthSearchStatRank findNewest();
}