package com.szkj.search.reposi;

import com.szkj.search.entity.YthSearchHis;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface YthSearchHisReposi extends CrudRepository<YthSearchHis, Long>, JpaSpecificationExecutor<YthSearchHis> {

    @Query(value = "SELECT keyword " +
            "FROM yth_search_his " +
            "WHERE create_time > UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL ?1 DAY)) " +
            "GROUP BY keyword " +
            "ORDER BY COUNT(search_id) DESC " +
            "LIMIT ?2", nativeQuery = true)
    List<String> findTopKeywork(int periodDay, int topNum);
}