package com.szkj.search.reposi;

import com.szkj.search.entity.YthSearchHis;
import com.szkj.search.entity.YthSearchHisLast;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface YthSearchHisLastReposi extends CrudRepository<YthSearchHisLast, Long>, JpaSpecificationExecutor<YthSearchHis> {

    /**
     * 根据createBy进行删除
     * @param createBy
     */
    @Transactional
    void deleteByCreateBy(Integer createBy);

    List<YthSearchHisLast> findByCreateBy(Integer createBy);

    @Query(value = "select count(distinct keyword) from yth_search_his_last where create_by = ?1", nativeQuery = true)
    Integer countDistinctByCreateBy(Integer createBy);

    YthSearchHisLast findByKeywordAndCreateBy(String keyword, Integer createBy);

    @Query(value = "SELECT * FROM yth_search_his_last WHERE create_time IN (SELECT MIN(create_time) FROM yth_search_his_last WHERE create_by = ?1)", nativeQuery = true)
    YthSearchHisLast findMinCreateTimeByCreateBy(Integer createBy);

}