package com.szkj.search.service;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.szkj.common.util.JsonUtil;
import com.szkj.common.util.RespUtil;
import com.szkj.search.entity.YthSearchHis;
import com.szkj.search.entity.YthSearchHisLast;
import com.szkj.search.entity.YthSearchStatRank;
import com.szkj.search.reposi.YthSearchHisLastReposi;
import com.szkj.search.reposi.YthSearchHisReposi;
import com.szkj.search.reposi.YthSearchStatRankReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SearchService {

    @Value("${searchEsAddr}")
    private String SEARCH_ES_ADDR;

    @Value("${searchMaxHisLast}")
    private int SEARCH_MAX_HIS_LAST;

    @Autowired
    private YthSearchHisLastReposi ythSearchHisLastRepo;
    @Autowired
    private YthSearchHisReposi ythSearchHisRepo;
    @Autowired
    private YthSearchStatRankReposi ythSearchStatRankReposi;

    private final byte CODE_SUCCESS = 0;

    private void saveSearchHis(String type, String keyword, int userId) {
        try {
            YthSearchHisLast hisLast = ythSearchHisLastRepo.findByKeywordAndCreateBy(keyword, userId);
            if (null == hisLast) {
                int hisNum = ythSearchHisLastRepo.countDistinctByCreateBy(userId);
                if (hisNum >= SEARCH_MAX_HIS_LAST) {
                    //删除一条最早的记录
                    ythSearchHisLastRepo.deleteById(ythSearchHisLastRepo.findMinCreateTimeByCreateBy(userId).getLastId());
                }
                hisLast = new YthSearchHisLast();
                hisLast.setKeyword(keyword);
                hisLast.setCreateBy(userId);
                hisLast.setCreateTime((int) (System.currentTimeMillis() / 1000));
            } else {
                hisLast.setCreateTime((int) (System.currentTimeMillis() / 1000));
            }
            ythSearchHisLastRepo.save(hisLast);

            YthSearchHis his = new YthSearchHis();
            his.setType(type);
            his.setKeyword(keyword);
            his.setCreateBy(userId);
            hisLast.setCreateTime((int) (System.currentTimeMillis() / 1000));
            ythSearchHisRepo.save(his);
        } catch (Exception e) {
            log.error("保存当前用户检索记录时出现异常。");
            e.printStackTrace();
        }
    }

    private String requestEs(String url) {
        try {
            log.info("请求ES检索，url: [" + url + "]。");
            String resp = HttpRequest.get(url).execute().body();
            JSONObject jsonObject = new JSONObject(resp);
            int code = jsonObject.getInt("code");
            if (CODE_SUCCESS != code) {
                log.error("ES检索失败，code: [" + code + "]。");
            }
            return resp;
        } catch (Exception e) {
            log.error("请求ES检索异常，url: [" + url + "]。");
            e.printStackTrace();
            return RespUtil.error("", new JSONObject());
        }
    }

    public String search(int userId, int pageNum, int pageRows, String keyword, String type, String pos
            , String startDate, String endDate) {
        saveSearchHis(type, keyword, userId);
        return requestEs(SEARCH_ES_ADDR + "csud/list?pageNum=" + pageNum + "&pageRows=" + pageRows
                + "&keyword=" + keyword + "&type=" + type + "&pos=" + pos + "&startDate=" + startDate + "&endDate=" + endDate);
    }

    public String delLastHis(int userId) {
        ythSearchHisLastRepo.deleteByCreateBy(userId);
        return RespUtil.success("成功", new cn.hutool.json.JSONObject());
    }

    public JSONObject resetLastHisListQueryBody(JSONObject userInfo, JSONObject body) {
        JsonUtil.resetJsonKeyValue(body,"params2", "{userId:"+ userInfo.getInt("userId") + "}");
        return body;
    }

    public void keywordRankStatistic() {
        List<String> hisList = ythSearchHisRepo.findTopKeywork(30, 10);
        JSONArray data = new JSONArray();
        for (int i = 0; i < hisList.size(); i++) {
            String keyword = hisList.get(i);
            JSONObject item = new JSONObject();
            item.set("keyword", keyword);
            item.set("order", i + 1);
            data.set(item);
        }
        YthSearchStatRank ythSearchStatRank = new YthSearchStatRank();
        ythSearchStatRank.setDataTxt(data.toStringPretty());
        ythSearchStatRank.setCreateTime((int) (System.currentTimeMillis() / 1000));
        ythSearchStatRankReposi.save(ythSearchStatRank);
    }

    public JSONArray getRank() {
        YthSearchStatRank ythSearchStatRank = ythSearchStatRankReposi.findNewest();
        return new JSONArray(ythSearchStatRank.getDataTxt());
    }

}
