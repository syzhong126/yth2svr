package com.szkj.activity.reposi;

import com.szkj.activity.entity.YthActivityMain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ActivityMainReposi extends JpaRepository<YthActivityMain, Long>, JpaSpecificationExecutor<YthActivityMain> {

    /*@Query(value = "select u.user_id, u.user_name, org.org_name from yth_user  u left join yth_user_org uo on u.user_id=uo.user_id left join yth_organazition org on org.org_id = uo.org_id where u.user_id=?1 " , nativeQuery = true)
    public Map queryUserOrginfo(long user_id);*/

    @Query(value = "SELECT * FROM `yth_activity_main` WHERE act_start>?1 AND act_start<?2 ORDER BY act_start DESC LIMIT 0,1 " , nativeQuery = true)
    public List<YthActivityMain> queryLastActivity(long start, long end);
}