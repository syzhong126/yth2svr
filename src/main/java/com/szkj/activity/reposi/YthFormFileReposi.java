package com.szkj.activity.reposi;

import com.szkj.file.entity.YthFormFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface YthFormFileReposi extends JpaRepository<YthFormFile, Long>, JpaSpecificationExecutor<YthFormFile> {

}