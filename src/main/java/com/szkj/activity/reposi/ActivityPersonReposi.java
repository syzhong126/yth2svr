package com.szkj.activity.reposi;

import com.szkj.activity.entity.YthActivityPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ActivityPersonReposi extends JpaRepository<YthActivityPerson, Long>, JpaSpecificationExecutor<YthActivityPerson> {

    @Query(value = "select * from yth_activity_person  where activity_id=?1 " , nativeQuery = true)
    public List<YthActivityPerson> getAllByActivityId(long activity_id);

    @Query(value = "select * from yth_activity_person  where activity_id=?1 and person_id=?2 limit 0,1" , nativeQuery = true)
    public List<YthActivityPerson> getAllByActivityIdAndPerson(long activity_id, long person_id);

}