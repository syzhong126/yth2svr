package com.szkj.activity.reposi;

import com.szkj.activity.entity.YthActivityPlace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ActivityPlaceReposi extends JpaRepository<YthActivityPlace, Integer>, JpaSpecificationExecutor<YthActivityPlace> {

}