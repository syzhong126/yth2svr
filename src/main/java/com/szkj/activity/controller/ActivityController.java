package com.szkj.activity.controller;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.activity.entity.YthActivityMain;
import com.szkj.activity.entity.YthActivityPerson;
import com.szkj.activity.entity.YthActivityPlace;
import com.szkj.activity.reposi.ActivityMainReposi;
import com.szkj.activity.reposi.ActivityPersonReposi;
import com.szkj.activity.reposi.ActivityPlaceReposi;
import com.szkj.activity.service.YthActivityService;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.AuthService;
import com.szkj.common.util.MapHelper;
import com.szkj.common.util.RespUtil;
import com.szkj.common.util.TimeTool;
import com.szkj.knowledge.reposi.YthOrganazitionReposi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author xiegl
 */
@Component
@RestController
@CrossOrigin("*")
@RequestMapping(value = "/activity")

public class ActivityController {

    @Autowired
    private AuthService authService;
    @Autowired
    private YthActivityService ythActivityService;
    @Autowired
    private ActivityMainReposi activityMainReposi;
    @Autowired
    private ActivityPersonReposi activityPersonReposi;
    @Autowired
    private ActivityPlaceReposi activityPlaceReposi;
    @Autowired
    YthOrganazitionReposi organazitionReposi;

    /**
     * 新增活动信息
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value = "/act_add", method = {RequestMethod.POST}, produces = "application/json")
    public String append(@RequestBody JSONObject param, HttpServletRequest request) {
        YthActivityMain activityMain = JSONUtil.toBean(param, YthActivityMain.class);
        long time0 = TimeTool.getTimeLong(null);
        try {
            String _token = request.getHeader("token");
            JSONObject userInfo = authService.getUserInfo(_token);
            long userId = userInfo.getInt("userId");
            activityMain.setId(param.getLong("actId"));
            if (activityMain.getId() == null || activityMain.getId() < 1) {
                activityMain.setId(null);
                activityMain.setCreateTime(time0);
                activityMain.setActSn(null);
                activityMain.setActQrcode(null);
            }

            Long act_start = param.getLong("actStart");
            Long act_end = param.getLong("actEnd");
            activityMain.setActStart(act_start);
            activityMain.setActEnd(act_end);

            activityMain.setCreateBy(userId);
            activityMain.setUpdateBy(userId);

            activityMain.setUpdateTime(time0);
            //保存
            activityMain = activityMainReposi.save(activityMain);

            if (activityMain.getActSn() == null) {
                //生成活动编号和二维码信息
                activityMain = ythActivityService.saveActQrcodeAndSN(activityMain);
                //保存
                activityMain = activityMainReposi.save(activityMain);
            }

            JSONArray persons = param.getJSONArray("actPerson");

            ythActivityService.savePersonInfo(persons, userInfo, activityMain);

            return RespUtil.success("操作成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error("请求出现错误", null);
        }

    }

    /**
     * 活动提交
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value = "/act_submit", method = {RequestMethod.POST}, produces = "application/json")
    public String submit(@RequestBody JSONObject param) {

        long act_id = param.getLong("actId");
        int act_status = param.getInt("actStatus");
        if (act_status != 1) {
            return RespUtil.error("参数错误：", null);
        }
        YthActivityMain ythActivityMain = activityMainReposi.getOne(act_id);

        if (ythActivityMain.getActStatus() != 0) {
            return RespUtil.error("操作失败：活动已提交", null);
        } else {
            ythActivityMain.setActStatus(act_status);
            activityMainReposi.save(ythActivityMain);
            return RespUtil.success("操作成功", null);
        }
    }

    /**
     * 活动审核
     */
    @RequestMapping(value = "/act_audit", method = {RequestMethod.POST}, produces = "application/json")
    public String audit(@RequestBody JSONObject param, HttpServletRequest request) {

        try {
            String _token = request.getHeader("token");
            JSONObject userInfo = authService.getUserInfo(_token);

            long act_id = param.getLong("actId");
            int act_status = param.getInt("actStatus");
            if (act_status != 2 && act_status != 3) {
                return RespUtil.error("操作失败：参数错误", null);
            }
            YthActivityMain ythActivityMain = activityMainReposi.getOne(act_id);

            if (ythActivityMain.getActStatus() != 1) {
                return RespUtil.error("操作失败：活动已审核", null);
            } else {
                ythActivityMain.setActStatus(act_status);
                activityMainReposi.save(ythActivityMain);
                // 审核完成之后发送日程信息，和消息推送信息
                if (act_status == 2) {
                    YthActivityPerson person;
                    List<YthActivityPerson> persons = activityPersonReposi.getAllByActivityId(act_id);
                    if (persons != null && persons.size() > 0) {
                        for (YthActivityPerson ythActivityPerson : persons) {
                            person = ythActivityPerson;
                            ythActivityService.saveSchedule(person, ythActivityMain, userInfo);
                        }
                    }
                }
                return RespUtil.success("操作成功", null);
            }
        } catch (Exception e) {
            return RespUtil.error("操作失败", null);
        }
    }

    /**
     * 活动转办或邀请
     */
    @RequestMapping(value = "/act_change_person", method = {RequestMethod.POST}, produces = "application/json")
    public String change_person(@RequestBody JSONObject param, HttpServletRequest request) throws Exception {

        int type = param.getInt("type");
        long actId = param.getLong("actId");
        JSONArray users = param.getJSONArray("users");
        String _token = request.getHeader("token");
        JSONObject userInfo = authService.getUserInfo(_token);
        YthActivityMain activityMain = activityMainReposi.getOne(actId);
        //判断此人是否有该活动的权限
        List<YthActivityPerson> p_list = activityPersonReposi.getAllByActivityIdAndPerson(actId, userInfo.getLong("userId"));
        if (p_list == null) {
            return RespUtil.error("你没有报名该活动", null);
        } else if (p_list.size() == 0) {
            return RespUtil.error("你没有报名该活动", null);
        }

        ythActivityService.savePersonInfo(users, userInfo, activityMain);

        if (type == 0) {//转办
            //delete from
            return RespUtil.success("活动转办成功", null);
        } else {
            return RespUtil.success("邀请成功", null);
        }
    }

    /**
     * 活动删除和恢复
     *
     */
    @GetMapping("/act_delete")
    public String personal(@RequestParam Long actId,
                           @RequestParam Integer isDelete,
                           HttpServletRequest request) throws Exception {

        String _token = request.getHeader("token");
        JSONObject userInfo = authService.getUserInfo(_token);

        YthActivityMain activityMain = activityMainReposi.getOne(actId);

        if (activityMain.getIsDelete() == 0 && isDelete == 1) {
            activityMain.setIsDelete(isDelete);
        } else if (activityMain.getIsDelete() == 1 && isDelete == 0) {
            activityMain.setIsDelete(isDelete);
        } else {
            return RespUtil.success("操作错误", null);
        }
        activityMain.setUpdateBy(userInfo.getLong("userId"));
        activityMain.setUpdateTime(TimeTool.getTimeLong(null));
        return RespUtil.success("操作成功", null);
    }

    /**
     * 活动签到
     *
     */
    @GetMapping("/act_sign")
    public String sign(@RequestParam Long actId,
                       @RequestParam String lng,
                       @RequestParam String lat,
                       HttpServletRequest request) throws Exception {

        String _token = request.getHeader("token");
        JSONObject userInfo = authService.getUserInfo(_token);
        long lng_l = Long.parseLong(lng);
        long lat_l = Long.parseLong(lat);

        YthActivityMain activityMain = activityMainReposi.getOne(actId);

        if (!activityMain.getIsSign()) {
            return RespUtil.error("此活动不需要签到", null);
        }
        //判断此人是否需要签到
        List<YthActivityPerson> p_list = activityPersonReposi.getAllByActivityIdAndPerson(actId, userInfo.getLong("userId"));
        if (p_list == null) {
            return RespUtil.error("你没有报名该活动", null);
        } else if (p_list.size() == 0) {
            return RespUtil.error("你没有报名该活动", null);
        }

        YthActivityPerson person = p_list.get(0);
        long timestamp = TimeTool.getTimeLong(null);
        if (timestamp > activityMain.getActStart() && timestamp < activityMain.getActEnd()) {
            //判断地理位置是否在正常签到范围
            long dis = (long) (MapHelper.getDistance(lat_l, lng_l, activityMain.getLat(), activityMain.getLng()) * 1000);
            if (dis > activityMain.getDistance()) {
                return RespUtil.error("不在签到范围", null);
            }
            person.setSignStatus(0);
            person.setSignTime(timestamp);
            activityPersonReposi.save(person);
            return RespUtil.success("签到成功", null);
        } else {
            return RespUtil.error("不在签到时间范围", null);
        }
    }

    /**
     * 活动场所新增和添加
     */
    @RequestMapping(value = "/act_place_add", method = {RequestMethod.POST}, produces = "application/json")
    public String place_append(@RequestBody JSONObject param, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        System.out.println(param.toString());
        YthActivityPlace activityPlace = JSONUtil.toBean(param, YthActivityPlace.class);
        activityPlace.setId(param.getInt("actPlaceId"));
        long time0 = TimeTool.getTimeLong(null);
        activityPlace.setId(param.getInt("actPlaceId"));
        if (activityPlace.getId() == null || activityPlace.getId() < 1) {
            activityPlace.setId(null);
            activityPlace.setUnitId(userinfo.getOrgId());
            organazitionReposi.findById(userinfo.getOrgId()).ifPresent(o -> activityPlace.setUnit(o.getOrgName()));
            activityPlace.setIsDelete(0);
            activityPlace.setCreateTime(time0);
            activityPlace.setCreateBy((long) userinfo.getUserId());
        }
        activityPlace.setUpdateBy((long) userinfo.getUserId());
        activityPlace.setUpdateTime(time0);

        activityPlaceReposi.save(activityPlace);
        return RespUtil.success("操作成功", null);
    }

    /**
     * 删除和恢复活动场所
     *
     */
    @RequestMapping(value = "/act_place_delete", method = {RequestMethod.POST}, produces = "application/json")
    public String place_delete(@RequestBody JSONObject param, HttpServletRequest request) throws Exception {

        long time0 = TimeTool.getTimeLong(null);

        int actPlaceId = param.getInt("actPlaceId");
        int isDelete = param.getInt("isDelete");

        String _token = request.getHeader("token");
        JSONObject userInfo = authService.getUserInfo(_token);
        YthActivityPlace activityPlace = activityPlaceReposi.getOne(actPlaceId);

        if (activityPlace.getIsDelete() == 0 && isDelete == 1) {
            activityPlace.setIsDelete(isDelete);
        } else if (activityPlace.getIsDelete() == 1 && isDelete == 0) {
            activityPlace.setIsDelete(isDelete);
        } else {
            return RespUtil.success("操作错误", null);
        }
        activityPlace.setUpdateBy(userInfo.getLong("userId"));
        activityPlace.setUpdateTime(time0);
        activityPlaceReposi.save(activityPlace);
        return RespUtil.success("操作成功", null);
    }

}
