package com.szkj.activity.entity;

import javax.persistence.*;

@Entity
@Table(name = "yth_activity_person")
public class YthActivityPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "activity_id")
    private Long activityId;

    @Column(name = "person_id")
    private Long personId;

    @Column(name = "person_name", length = 64)
    private String personName;

    @Column(name = "person_uint", length = 64)
    private String personUint;

    @Column(name = "person_depart", length = 64)
    private String personDepart;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name = "update_time")
    private Long updateTime;

    @Column(name = "sign_time")
    private Long signTime;

    @Column(name = "sign_lng")
    private Double signLng;

    @Column(name = "sign_lat")
    private Double signLat;

    @Column(name = "sign_status")
    private Integer signStatus;

    public Integer getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(Integer signStatus) {
        this.signStatus = signStatus;
    }

    public Double getSignLat() {
        return signLat;
    }

    public void setSignLat(Double signLat) {
        this.signLat = signLat;
    }

    public Double getSignLng() {
        return signLng;
    }

    public void setSignLng(Double signLng) {
        this.signLng = signLng;
    }

    public Long getSignTime() {
        return signTime;
    }

    public void setSignTime(Long signTime) {
        this.signTime = signTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getPersonDepart() {
        return personDepart;
    }

    public void setPersonDepart(String personDepart) {
        this.personDepart = personDepart;
    }

    public String getPersonUint() {
        return personUint;
    }

    public void setPersonUint(String personUint) {
        this.personUint = personUint;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}