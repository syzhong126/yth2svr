package com.szkj.activity.entity;

import javax.persistence.*;

@Entity
@Table(name = "yth_activity_place")
public class YthActivityPlace {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "act_place_id", nullable = false)
    private Integer id;

    @Column(name = "act_place_name", length = 64)
    private String actPlaceName;

    @Column(name = "act_place_sn", length = 64)
    private String actPlaceSn;

    @Column(name = "place_type_id", length = 20)
    private String placeTypeId;

    @Column(name = "act_place_status")
    private Integer actPlaceStatus;

    @Column(name = "unit_id")
    private Integer unitId;

    @Column(name = "unit", length = 256)
    private String unit;

    @Column(name = "area")
    private Double area;

    @Column(name = "max_people")
    private Integer maxPeople;

    @Column(name = "location", length = 256)
    private String location;

    @Column(name = "lng")
    private Double lng;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "open_time", length = 128)
    private String openTime;

    @Column(name = "is_delete")
    private Integer isDelete;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name = "update_time")
    private Long updateTime;

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(Integer maxPeople) {
        this.maxPeople = maxPeople;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getActPlaceStatus() {
        return actPlaceStatus;
    }

    public void setActPlaceStatus(Integer actPlaceStatus) {
        this.actPlaceStatus = actPlaceStatus;
    }

    public String getPlaceTypeId() {
        return placeTypeId;
    }

    public void setPlaceTypeId(String placeTypeId) {
        this.placeTypeId = placeTypeId;
    }

    public String getActPlaceSn() {
        return actPlaceSn;
    }

    public void setActPlaceSn(String actPlaceSn) {
        this.actPlaceSn = actPlaceSn;
    }

    public String getActPlaceName() {
        return actPlaceName;
    }

    public void setActPlaceName(String actPlaceName) {
        this.actPlaceName = actPlaceName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}