package com.szkj.activity.entity;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@Proxy(lazy=false)
@Table(name = "yth_activity_main")
public class YthActivityMain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "act_id", nullable = false)
    private Long id;

    @Column(name = "act_name", length = 64)
    private String actName;

    @Column(name = "act_sn", length = 64)
    private String actSn;

    @Column(name = "act_type", length = 20)
    private String actType;

    @Column(name = "act_location", length = 128)
    private String actLocation;

    @Column(name = "lng")
    private Double lng;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "act_start")
    private Long actStart;

    @Column(name = "act_end")
    private Long actEnd;

    @Column(name = "act_unit")
    private Integer actUnit;

    @Column(name = "is_sign")
    private Boolean isSign;

    @Column(name = "act_info", length = 1024)
    private String actInfo;

    @Column(name = "act_qrcode", length = 1024)
    private String actQrcode;

    @Column(name = "is_delete")
    private Integer isDelete;

    @Column(name = "act_status")
    private Integer actStatus;

    @Column(name = "distance")
    private Integer distance;

    @Column(name = "update_time")
    private Long updateTime;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "update_by")
    private Long updateBy;

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getActStatus() {
        return actStatus;
    }

    public void setActStatus(Integer actStatus) {
        this.actStatus = actStatus;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getActQrcode() {
        return actQrcode;
    }

    public void setActQrcode(String actQrcode) {
        this.actQrcode = actQrcode;
    }

    public String getActInfo() {
        return actInfo;
    }

    public void setActInfo(String actInfo) {
        this.actInfo = actInfo;
    }

    public Boolean getIsSign() {
        return isSign;
    }

    public void setIsSign(Boolean isSign) {
        this.isSign = isSign;
    }

    public Integer getActUnit() {
        return actUnit;
    }

    public void setActUnit(Integer actUnit) {
        this.actUnit = actUnit;
    }

    public Long getActEnd() {
        return actEnd;
    }

    public void setActEnd(Long actEnd) {
        this.actEnd = actEnd;
    }

    public Long getActStart() {
        return actStart;
    }

    public void setActStart(Long actStart) {
        this.actStart = actStart;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getActLocation() {
        return actLocation;
    }

    public void setActLocation(String actLocation) {
        this.actLocation = actLocation;
    }

    public String getActType() {
        return actType;
    }

    public void setActType(String actType) {
        this.actType = actType;
    }

    public String getActSn() {
        return actSn;
    }

    public void setActSn(String actSn) {
        this.actSn = actSn;
    }

    public String getActName() {
        return actName;
    }

    public void setActName(String actName) {
        this.actName = actName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}