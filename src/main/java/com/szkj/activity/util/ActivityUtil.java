package com.szkj.activity.util;

import java.util.Date;

public class ActivityUtil {

    public static Date longToDate(long ltm) {
        Date dt = new Date();
        dt.setTime(ltm * 1000);

        return dt;
    }

    public static int getSuffixNo(String xx, String prefix) {
        String num = xx.substring(prefix.length(),xx.length());
        return Integer.parseInt(num);
    }
}
