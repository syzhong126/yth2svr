package com.szkj.activity.service;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.activity.entity.YthActivityMain;
import com.szkj.activity.entity.YthActivityPerson;
import com.szkj.file.entity.YthFormFile;
import com.szkj.activity.reposi.ActivityMainReposi;
import com.szkj.activity.reposi.ActivityPersonReposi;
import com.szkj.activity.reposi.YthFormFileReposi;
import com.szkj.activity.util.ActivityUtil;
import com.szkj.common.util.TimeTool;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.reposi.YthNoticepushReposi;
import com.szkj.schedule.entity.YthSchedulePlan;
import com.szkj.schedule.reposi.YthScheduleReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Slf4j
@Service
public class YthActivityService {

    @Autowired
    ActivityMainReposi activityMainReposi;
    @Autowired
    ActivityPersonReposi activityPersonReposi;
    @Autowired
    YthFormFileReposi ythFormFileReposi;

    @Transactional(rollbackFor = Exception.class)
    public int savePersonInfo(JSONArray persons, JSONObject user_info, YthActivityMain act_info) throws Exception {

        List<YthActivityPerson> saved_persons = activityPersonReposi.getAllByActivityId(act_info.getId());
        List<YthActivityPerson> persons_to_save = new ArrayList<YthActivityPerson>();

        if(saved_persons == null){    saved_persons = new ArrayList<>();    saved_persons.clear();    }
        if(persons == null){  persons = new JSONArray();    persons.clear();   }

        for(int i=0;i<persons.size();i++){
            YthActivityPerson p = JSONUtil.toBean((JSONObject)persons.get(i),YthActivityPerson.class);
            p.setId(null);
            persons_to_save.add(p);
        }
        long time0 = TimeTool.getTimeLong(null);
        //1 如果新的列表为不为空
        if (persons_to_save.size() > 0) {
            //delete 删除历史人员记录
            delNoUseInSaved(persons_to_save,saved_persons,user_info);

            for(int i=0; i<persons_to_save.size(); i++){
                YthActivityPerson p = persons_to_save.get(i);
                p = getPersonInSaved(p,saved_persons);//返回人员在已保存列表中的记录,没有则返回原纪录
                if(p.getId() == null){
                    p.setId(null);
                }
                p.setActivityId(act_info.getId());
                p.setSignStatus(0);
                p.setUpdateTime(time0);
                p.setCreateTime(time0);
                p.setUpdateBy(user_info.getLong("userId"));
                p.setCreateBy(user_info.getLong("userId"));
                activityPersonReposi.save(p);
                /**
                 * 写日程信息和推送信息,待补充
                 *
                saveSchedule(p, act_info,user_info);
                 */
            }
        }else{
            for (int i = 0; i < saved_persons.size(); i++) {
                activityPersonReposi.delete(saved_persons.get(i));
                //删除日程信息？
                //delSchedule(saved_persons.get(i),user_info);
            }
        }
        return 0;
    }

    public YthActivityMain saveActQrcodeAndSN(YthActivityMain activity) {
        String date_str =  TimeTool.longToStr(activity.getActStart()*1000,"yyyy-MM-dd");//(null,"yyyyMMdd");
        long start = TimeTool.stringToLong(date_str+" 00:00:00");
        long end = TimeTool.stringToLong(date_str+"  23:59:59");
        String date_str2 =  TimeTool.longToStr(activity.getActStart()*1000,"yyyyMMdd");//(null,"yyyyMMdd");
        String sn_prefix = "CF-ACT-"+date_str2+"-";
        String sn = "";
        List<YthActivityMain> act_list = activityMainReposi.queryLastActivity(start,end);
        if(act_list !=null && act_list.size()>0){
            String x = act_list.get(0).getActSn();
            int last_sn = 0;
            if(x!=null){
                last_sn = ActivityUtil.getSuffixNo(x,sn_prefix);
            }
            last_sn = last_sn + 1;
            sn = sn_prefix + String.format("%04d", last_sn);
        }else{
            sn = sn_prefix +"0001";
        }
        activity.setActSn(sn);
        String qr_code = "activity||actId="+activity.getId()+"&actSn="+sn+"&actName="+activity.getActName()+"&actLocation="+activity.getActLocation();
        activity.setActQrcode(qr_code);
        return activity;
    }

    public int saveFileInfo(JSONArray files, JSONObject user_info, YthActivityMain act_info) throws Exception {

        /**
         *  首先需要清空之前的附件
         *  重新保存？
         * */
        List<YthFormFile> f_list = new ArrayList<YthFormFile>();
        if(files!=null && files.size()>0){
            for(int i=0; i<files.size(); i++){
                YthFormFile f = new YthFormFile();
                f = JSONUtil.toBean((JSONObject) files.get(i),YthFormFile.class);
                int f_id = Integer.parseInt(act_info.getId().toString());
                f.setFId(f_id);
                f.setFTable("yth_activity_main");//活动主表的名称

                Instant ins = Instant.parse(TimeTool.unixtimeToStr(act_info.getCreateTime()));
             //   f.setTs(ins);
                if(f.getId()>0 && f.getIsDel()==1){
                    ythFormFileReposi.delete(f);
                }else{
                    f_list.add(f);
                }
            }
            ythFormFileReposi.saveAll(f_list);
        }
        return 0;
    }

    /**
     * @param user  当前新增列表
     * @param pps   历史记录列表
     * @return
     */
    public YthActivityPerson getPersonInSaved(YthActivityPerson user,List <YthActivityPerson> pps){
        if(pps!=null){
            for(int i=0; i<pps.size(); i++){
                if(pps.get(i).getPersonId().equals(user.getPersonId())){
                    return pps.get(i);
                }
            }
            return user;
        }else{
            return user;
        }
    }

    /**
     *
     * @param toSave  新增的人员列表
     * @param saved   已存的人员列表
     */
    public void delNoUseInSaved(List <YthActivityPerson> toSave,List <YthActivityPerson> saved, JSONObject user_info){
        if(saved !=null && saved.size()>0){
            for(int i=0; i<saved.size(); i++){
                YthActivityPerson p = saved.get(i);
                boolean del_flag = true;
                if(toSave!=null && toSave.size()>0){
                    for(int j=0; j<toSave.size(); j++){
                        if(toSave.get(j).getPersonId().equals(p.getPersonId())){
                            del_flag = false;
                        }
                    }
                }
                if(del_flag){
                    activityPersonReposi.delete(p);
                    //删除日程信息和推送新的消息
                    //delSchedule(p,user_info);
                }
            }
        }
    }

    @Autowired
    private YthScheduleReposi ythScheduleReposi;
    @Autowired
    private YthNoticepushReposi ythNoticepushReposi;
    /**
     * 活动插入日程信息
     * @param p
     * @param act_info
     * @param user_info
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveSchedule(YthActivityPerson p, YthActivityMain act_info, JSONObject user_info){
        Calendar calendar = Calendar.getInstance();
        long time = calendar.getTimeInMillis();

        YthNoticePush ynp = new YthNoticePush();//添加推送消息
        /**
         * find plan by act_id & person_id
         */
        YthSchedulePlan plan;
        List <YthSchedulePlan> plan_list = ythScheduleReposi.queryScheduleInfo(act_info.getId(),p.getPersonId());
        if(plan_list != null && plan_list.size()>0){
            plan = plan_list.get(0);
        }else{
            plan = new YthSchedulePlan();
            plan.setCreateBy(user_info.getInt("userId")).setCreateTime(time);
        }
        plan.setBeginTime(act_info.getActStart());
        plan.setEndTime(act_info.getActEnd());
        plan.setWorkDesp(act_info.getActName());
        plan.setNoticeTime(3+"");
        plan.setImportantStatus(1);
        plan.setActiveId(act_info.getId().toString());
        plan.setWorkAdress(act_info.getActLocation());
        plan.setUserId(Integer.parseInt(p.getPersonId()+""));
        plan.setDesignStatus(0);
        plan.setDesignNote("");
        plan.setCalendarType("0");
        plan.setUpdateBy(user_info.getInt("userId")).setUpdateTime(time);
        ythScheduleReposi.save(plan);
        if(plan.getScheduleId()>0){//修改
            ynp.setTitle("日程变更")
                    .setContent("您有一个新的日程变更："+plan.getBeginTime()+"，"+plan.getWorkAdress()+"，"+plan.getWorkDesp()+"")
                    .setMoudleType("新的日程变更");
        }else{
            ynp.setTitle("新的日程")
                    .setContent("您有一个新的日程："+plan.getBeginTime()+"，"+plan.getWorkAdress()+"，"+plan.getWorkDesp()+"")
                    .setMoudleType("新的日程");
        }
        ynp.setPushUser(plan.getUserId())
                .setPushType(2).setPushStatus(0).setPushTime(time)
                .setFromSystem("yth").setMoudleDetail("schedule")
                .setCreateTime(time)
                .setUpdateTime(time);
        ynp.setCreateBy(user_info.get("userId").toString())
                .setUpdateBy(user_info.get("userId").toString())
                .setCreateName(user_info.get("userName").toString())
                .setUpdateName(user_info.get("userName").toString());
        ythNoticepushReposi.save(ynp);
    }



    @Transactional(rollbackFor = Exception.class)
    public void delSchedule(YthActivityPerson p,JSONObject user_info){

        Calendar calendar = Calendar.getInstance();
        long time = calendar.getTimeInMillis();
        YthNoticePush ynp = new YthNoticePush();//添加推送消息
        /**
         * find plan by act_id & person_id
         */
        YthSchedulePlan plan;
        List <YthSchedulePlan> plan_list = ythScheduleReposi.queryScheduleInfo(p.getActivityId(),p.getPersonId());
        if(plan_list != null && plan_list.size()>0){
            plan = plan_list.get(0);
            ythScheduleReposi.delete(plan);
            ynp.setTitle("日程取消")
                    .setContent("您有一个日程取消了："+plan.getBeginTime()+"，"+plan.getWorkAdress()+"，"+plan.getWorkDesp()+"")
                    .setMoudleType("新的日程取消");
            ynp.setPushUser(plan.getUserId())
                    .setPushType(2).setPushStatus(0).setPushTime(time)
                    .setFromSystem("yth").setMoudleDetail("schedule")
                    .setCreateTime(time)
                    .setUpdateTime(time);
            ynp.setCreateBy(user_info.get("userId").toString())
                    .setUpdateBy(user_info.get("userId").toString())
                    .setCreateName(user_info.get("userName").toString())
                    .setUpdateName(user_info.get("userName").toString());
            ythNoticepushReposi.save(ynp);
        }
    }

}
