package com.szkj.workplan.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;

import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.file.service.YthFileFormService;
import com.szkj.workplan.entity.WorkplanChecklog;
import com.szkj.workplan.entity.WorkplanList;
import com.szkj.workplan.entity.WorkplanProgress;
import com.szkj.workplan.reposi.WorkplanChecklogReposi;
import com.szkj.workplan.reposi.WorkplanListReposi;
import com.szkj.workplan.reposi.WorkplanProgressReposi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class WorkplanService {
    @Autowired
    private WorkplanListReposi reposi;
    @Autowired
    private WorkplanProgressReposi progressReposi;
    @Autowired
    private WorkplanChecklogReposi checklogReposi;
    @Autowired
    private YthFileFormService fileService;
    
    private static final String DICT_WP_CYCLE_MONTH	="wp-cycle-month";	//按月填报
    private static final String DICT_WP_CYCLE_WEEK	="wp-cycle-week";	//按周填报
    private static final String DICT_WP_CYCLE_STATUS_COMPLETE	="wp-cycle-status-complete";	//已填报
    private static final String DICT_WP_CYCLE_STATUS_NOTFILL	="wp-cycle-status-notfill";	//未填报
    private static final String DICT_WP_PROGRESS_DELAYED	="wp-progress_delayed";	//请求完结(滞后完成)
    private static final String DICT_WP_PROGRESS_INTIME	="wp-progress_intime";	//请求完结(按时完成)
    private static final String DICT_WP_PROGRESS_LATE	="wp-progress_late";	//相对滞后
    private static final String DICT_WP_PROGRESS_NORMAL	="wp-progress_normal";	//正常推进
    private static final String DICT_WP_PROGRESS_SUSPENDED	="wp-progress_suspended";	//请求完结(任务调整或取消)
    private static final String DICT_WP_REPLY_AGREE	="wp-reply-agree";	//同意
    private static final String DICT_WP_REPLY_REJECT	="wp-reply-reject";	//驳回
    private static final String DICT_WP_STATUS_INDOING	="wp-status-indoing";	//正在进行
    private static final String DICT_WP_STATUS_SUSPENDED	="wp-status-suspended";	//已中止
    private static final String DICT_WP_STAUTS_DELAYED	="wp-stauts-delayed";	//滞后完成
    private static final String DICT_WP_STAUTS_INTIME	="wp-stauts-intime";	//及时完成

    @Transactional(rollbackFor = Exception.class)
    public WorkplanList append(WorkplanList body, AuthUserInfoModel userInfo) throws Exception {
        try {
            body.setCreateBy(userInfo.getUserId())
                    .setCreateTime(DateUtil.date())
                    .setUpdateBy(userInfo.getUserId())
                    .setUpdateTime(DateUtil.date())
                    .setStatusCycle(DICT_WP_CYCLE_STATUS_NOTFILL)
                    .setStatus(DICT_WP_STATUS_INDOING)
                    .setStartTimeCycle(DateUtil.date())
                    .setFinishTimeCycle(DateUtil.date())
                    .setProgress(DICT_WP_PROGRESS_NORMAL);
            reposi.save(body);
            fileService.saveFiles(body.getFileList(), "yth_workplan_list", body.getId());
            return body;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    @Transactional(rollbackFor = Exception.class)
    public WorkplanList modify(long planId, WorkplanList body, AuthUserInfoModel userInfo) throws Exception {
        try {
            Optional<WorkplanList> _optional= reposi.findById(planId);
            if (!_optional.isPresent()) {
                throw new Exception("找不到要修改的工作计划, 请确认");
            }
            WorkplanList _old= _optional.get();
            body.setId(planId)
                    .setCreateBy(_old.getCreateBy())
                    .setCreateTime(_old.getCreateTime())
                    .setUpdateBy(userInfo.getUserId())
                    .setUpdateTime(DateUtil.date())
                    .setStatusCycle(_old.getStatusCycle())
                    .setStatus(_old.getStatus())
                    .setStartTimeCycle(_old.getStartTimeCycle())
                    .setFinishTimeCycle(_old.getFinishTimeCycle())
                    .setProgress(_old.getProgress());
            reposi.save(body);
            fileService.saveFiles(body.getFileList(), "yth_workplan_list", body.getId());
            return body;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkplanList delete(long planId,  AuthUserInfoModel userInfo) throws Exception {
        try {
            Optional<WorkplanList> _optional= reposi.findById(planId);
            if (!_optional.isPresent()) {
                throw new Exception("找不到要修改的工作计划, 请确认");
            }
            ArrayList<WorkplanProgress> _ProgressList = progressReposi.queryListByPlanId(planId);
            if (_ProgressList.size()>0) {
                throw new Exception("工作计划已有填报, 不能删除");
            }
            WorkplanList _old= _optional.get();
            _old.setIsDel(1)
                .setUpdateBy(userInfo.getUserId())
                .setUpdateTime(DateUtil.date());
            reposi.save(_old);
            return _old;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkplanList suspend(long planId,  AuthUserInfoModel userInfo) throws Exception {
        try {
            Optional<WorkplanList> _optional= reposi.findById(planId);
            if (!_optional.isPresent()) {
                throw new Exception("找不到要修改的工作计划, 请确认");
            }           
            WorkplanList _old= _optional.get();
            _old.setStatus(DICT_WP_STATUS_SUSPENDED)
                .setUpdateBy(userInfo.getUserId())
                .setUpdateTime(DateUtil.date());
            reposi.save(_old);
            return _old;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkplanProgress appendProgress(WorkplanProgress body, AuthUserInfoModel userInfo) throws Exception {
        try {
            WorkplanList _workplan= reposi.getOne(body.getPlanId());
            if (_workplan.getIsDel()==1) {
                throw new Exception("已经删除的计划不需要填报");
            }
            if (DICT_WP_STATUS_SUSPENDED.equals(_workplan.getStatus())) {
                throw new Exception("已经中止的计划不需要填报");
            }
            body.setCreateBy(userInfo.getUserId())
                    .setCreateTime(DateUtil.date())
                    .setUpdateBy(userInfo.getUserId())
                    .setUpdateTime(DateUtil.date())
                    .setStartTimeCycle(_workplan.getStartTimeCycle())
                    .setFinishTimeCycle(_workplan.getFinishTimeCycle());
            progressReposi.save(body);
            fileService.saveFiles(body.getFileList(), "yth_workplan_progress", body.getId());
            _workplan.setStatusCycle(DICT_WP_CYCLE_STATUS_COMPLETE); //本周期填报完成
            reposi.save(_workplan);
            return body;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkplanProgress modifyProgress(long progressId, WorkplanProgress body, AuthUserInfoModel userInfo) throws Exception {
        try {
            Optional<WorkplanProgress> _optional= progressReposi.findById(progressId);
            if (!_optional.isPresent()) {
                throw new Exception("找不到要修改的填报信息, 请确认");
            }
            if (StrUtil.isNotBlank(_optional.get().getReply())) {
                throw new Exception("不能修改已审核的填报信息");
            }          
            WorkplanProgress _old= _optional.get();
            body.setId(progressId)
                    .setCreateBy(_old.getCreateBy())
                    .setCreateTime(_old.getCreateTime())
                    .setUpdateBy(userInfo.getUserId())
                    .setUpdateTime(DateUtil.date())
                    .setPlanId(_old.getPlanId())
                    .setFinishTimeCycle(_old.getFinishTimeCycle())
                    .setStartTimeCycle(_old.getStartTimeCycle());
            progressReposi.save(body);
            fileService.saveFiles(body.getFileList(), "yth_workplan_progress", body.getId());
            return body;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkplanChecklog appendChecklog(WorkplanChecklog body, AuthUserInfoModel userInfo) throws Exception {
        try {
            Optional<WorkplanProgress> _optionalProgress= progressReposi.findById(body.getProgressId());
            if (!_optionalProgress.isPresent()) {
                throw new Exception("找不到要审核的填报信息, 请确认");
            }
            WorkplanProgress _workProgress= _optionalProgress.get();

            Optional<WorkplanList> _optional= reposi.findById(_workProgress.getPlanId());
            if (!_optional.isPresent()) {
                throw new Exception("找不到要审核的工作计划信息, 请确认");
            }
            WorkplanList _workplan= _optional.get();

            body.setCreateBy(userInfo.getUserId())
                    .setCreateTime(DateUtil.date())
                    .setUpdateBy(userInfo.getUserId())
                    .setUpdateTime(DateUtil.date())
                    .setPlanId(_workProgress.getPlanId());
            _workProgress.setReply(body.getReply())
                    .setReplyContent(body.getReplyContent());
            if (DICT_WP_REPLY_AGREE.equals(body.getReply())) {  //如果同意, 更新状态
                switch (_workProgress.getProgress()) {
                    case DICT_WP_PROGRESS_DELAYED:  //请求完结(滞后完成)
                        _workplan.setStatus(DICT_WP_STAUTS_DELAYED);
                        break;
                    case DICT_WP_PROGRESS_INTIME:   //请求完结(按时完成)
                        _workplan.setStatus(DICT_WP_STAUTS_INTIME);
                        break;
                    case DICT_WP_PROGRESS_LATE:     //正在进行(相对滞后)
                        _workplan.setStatus(DICT_WP_STATUS_INDOING);
                        break;
                    case DICT_WP_PROGRESS_NORMAL:   //正在进行(正常推进)
                        _workplan.setStatus(DICT_WP_STATUS_INDOING);
                        break;
                }
                reposi.save(_workplan);
            }
            checklogReposi.save(body);
            progressReposi.save(_workProgress);
            fileService.saveFiles(body.getFileList(), "yth_workplan_checklog", body.getId());
            return body;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }


}
