package com.szkj.workplan.controller;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.workplan.entity.WorkplanChecklog;
import com.szkj.workplan.entity.WorkplanList;
import com.szkj.workplan.entity.WorkplanProgress;
import com.szkj.workplan.service.WorkplanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

/**
 * @author xiegl
 */
@Component
@RestController
@CrossOrigin("*")

public class WorkplanController {
    @Autowired
    private WorkplanService service;
    @Autowired
    private YthOperationLogService logservice;


    @PostMapping("/workplan")
    public String append(@RequestBody(required=true) @Valid WorkplanList body, BindingResult bindingResult,
                         @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        if (bindingResult.hasErrors()) {
            throw new Exception(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        WorkplanList _result = service.append(body, userInfo);
        logservice.save(0, "insert", "workplan", "/workplan",
                body.toString(), JSONUtil.toJsonStr(_result), "", userInfo.getUserId());
        return RespUtil.success("成功", _result);
    }

    @PutMapping("/workplan/{planId}")
    public String modify(@PathVariable long planId,
                         @RequestBody(required=true)  @Valid WorkplanList body, BindingResult bindingResult,
                         @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        if (bindingResult.hasErrors()) {
            throw new Exception(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        WorkplanList _result = service.modify(planId, body, userInfo);
        logservice.save(0, "modify", "workplan", "/workplan",
                body.toString(), JSONUtil.toJsonStr(_result), "", userInfo.getUserId());
        return RespUtil.success("成功", _result);
    }

    @DeleteMapping("/workplan/{planId}")
    public String delete(@PathVariable long planId,                        
                         @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        WorkplanList _result = service.delete(planId, userInfo);
        logservice.save(0, "delete", "workplan", "/workplan/delete",
            String.valueOf(planId), JSONUtil.toJsonStr(_result), "", userInfo.getUserId());
        return RespUtil.success("成功", _result);
    }

    @PutMapping("/workplan/suspend/{planId}")
    public String suspend(@PathVariable long planId,                        
                         @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        WorkplanList _result = service.suspend(planId, userInfo);
        logservice.save(0, "delete", "workplan", "/workplan/suspend",
            String.valueOf(planId), JSONUtil.toJsonStr(_result), "", userInfo.getUserId());
        return RespUtil.success("成功", _result);
    }


    @PostMapping("/workplan/progress")
    public String appendProgress(@RequestBody(required=true) @Valid WorkplanProgress body, BindingResult bindingResult,
                                 @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        if (bindingResult.hasErrors()) {
            throw new Exception(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        WorkplanProgress _result = service.appendProgress(body, userInfo);
        logservice.save(0, "insert", "workplan", "/workplan/progress",
                body.toString(), JSONUtil.toJsonStr(_result), "", userInfo.getUserId());
        return RespUtil.success("成功", _result);
    }

    @PutMapping("/workplan/progress/{processId}")
    public String modifyProgress(@PathVariable long processId,
                         @RequestBody(required=true)  @Valid WorkplanProgress body, BindingResult bindingResult,
                         @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        if (bindingResult.hasErrors()) {
            throw new Exception(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        WorkplanProgress _result = service.modifyProgress(processId, body, userInfo);
        logservice.save(0, "modify", "workplan", "/workplan/progress",
                body.toString(), JSONUtil.toJsonStr(_result), "", userInfo.getUserId());
        return RespUtil.success("成功", _result);
    }

    @PostMapping("/workplan/checklog")
    public String appendChecklog(@RequestBody(required=true) @Valid WorkplanChecklog body, BindingResult bindingResult,
                                 @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        if (bindingResult.hasErrors()) {
            throw new Exception(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        WorkplanChecklog _result = service.appendChecklog(body, userInfo);
        logservice.save(0, "insert", "workplan", "/workplan/checklog",
                body.toString(), JSONUtil.toJsonStr(_result), "", userInfo.getUserId());
        return RespUtil.success("成功", _result);
    }
}
