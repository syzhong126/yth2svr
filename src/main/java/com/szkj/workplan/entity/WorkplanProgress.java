package com.szkj.workplan.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.szkj.file.entity.YthFormFile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * 工作计划填报表
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_workplan_progress")
public class WorkplanProgress implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 计划id
     */
    @Column(name = "plan_id", nullable = false)
    private Long planId;

    /**
     * 本周期开始时间
     */
    @Column(name = "start_time_cycle", nullable = false)
    private Date startTimeCycle;

    /**
     * 本周期结束时间
     */
    @Column(name = "finish_time_cycle", nullable = false)
    private Date finishTimeCycle;

    /**
     * 进度状态 wp-progress
     */
    @NotBlank(message = "当前状态不能为空")
    @Column(name = "progress", nullable = false)
    private String progress = "0";

    /**
     * 本周完成内容
     */
    @NotBlank(message = "本周期完成情况不能为空")
    @Column(name = "curr_content", nullable = false)
    private String currContent;

    /**
     * 下周计划
     */
    @NotBlank(message = "下个周期工作计划不能为空")
    @Column(name = "next_content", nullable = false)
    private String nextContent;

    /**
     * 存在问题和建议
     */
    @Column(name = "advise", nullable = false)
    private String advise;

    /**
     * 回复类型 字典 wp-replay
     */
    @Column(name = "reply")
    private String reply;

    /**
     * 回复内容
     */
    @Column(name = "reply_content")
    private String replyContent;

    /**
     * 创建人
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    /**
     * 修改人
     */
    @Column(name = "update_by", nullable = false)
    private Integer updateBy;

    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Date updateTime;

    /**
     * 附件列表
     */
    @Transient
    private ArrayList<YthFormFile> fileList;

}
