package com.szkj.workplan.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.szkj.file.entity.YthFormFile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * 工作计划审核表
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_workplan_checklog")
public class WorkplanChecklog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 流程id
     */
    @NotNull(message = "填报记录号不能为空")
    @Column(name = "progress_id", nullable = false)
    private Long progressId;

    /**
     * 计划id
     */
    @Column(name = "plan_id", nullable = false)
    private Long planId;

    /**
     * 回复类型 字典 wp-replay
     */
    @NotBlank(message = "回复类型不能为空")
    @Column(name = "reply")
    private String reply;

    /**
     * 回复内容
     */
    @NotBlank(message = "回复内容不能为空")
    @Column(name = "reply_content")
    private String replyContent;

    /**
     * 创建人
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    /**
     * 修改人
     */
    @Column(name = "update_by", nullable = false)
    private Integer updateBy;

    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Date updateTime;

    /**
     * 附件列表
     */
    @Transient
    private ArrayList<YthFormFile> fileList;

}
