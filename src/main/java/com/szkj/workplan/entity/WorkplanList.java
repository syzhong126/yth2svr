package com.szkj.workplan.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.szkj.common.anno.DictAnno;
import com.szkj.file.entity.YthFormFile;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * 工作计划
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_workplan_list")
public class WorkplanList implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 计划id
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空")
    @Length(message="标题最大长度不能超过{max}字符", max=50)
    @Column(name = "title", nullable = false)
    private String title;

    /**
     * 目标任务
     */
    @NotBlank(message = "任务不能为空")
    @Length(message="任务最大长度不能超过{max}字符", max=200)
    @Column(name = "task", nullable = false)
    private String task;

    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "任务开始日期不能为空")
    @Column(name = "start_time", nullable = false)
    private Date startTime;

    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "任务结束日期不能为空")
    @Column(name = "finish_time", nullable = false)
    private Date finishTime;

    /**
     * 报告周期: 字典wp-cycle
     */
    @NotBlank(message = "任务汇报周期不能为空")
    @DictAnno(dictCode = "wp-cycle", message="无效的汇报周期")
    @Column(name = "cycle", nullable = false)
    private String cycle;

    /**
     * 本周期开始时间 自动更新
     */
    @Column(name = "start_time_cycle", nullable = false)
    private Date startTimeCycle;

    /**
     * 本周期结束时间 自动更新
     */
    @Column(name = "finish_time_cycle", nullable = false)
    private Date finishTimeCycle;

    /**
     * 汇报对象
     */
    @NotNull(message = "汇报对象不能为空")
    @Column(name = "target", nullable = false)
    private Integer target;

    /**
     * 汇报责任人
     */
    @NotNull(message = "汇报人不能为空")
    @Column(name = "reporter", nullable = false)
    private Integer reporter;

    /**
     * 进度 字典wp-progress
     */
//    @DictAnno(dictCode= "wp-progress")
    @Column(name = "progress", nullable = false)
    private String progress;

    /**
     * 本周期填报状态 字典wp-cycle-status
     */
//    @DictAnno(dictCode= "wp-cycle-status")
    @Column(name = "status_cycle", nullable = false)
    private String statusCycle;

    /**
     * 计划状态 字典wp-status
     */
//    @DictAnno(dictCode= "wp-status")
    @Column(name = "status", nullable = false)
    private String status;

    /**
     * 最后填报记录 t_workplan_progress.progress_id
     */
    @Column(name = "progress_id", nullable = false)
    private Long progressId = 0L;

    /**
     * 应该填报的数量 周:跨度间周五的个数 月:跨月的个数
     */
    @Column(name = "progress_num", nullable = false)
    private Integer progressNum = 0;

    /**
     * 删除标记
     */
    @Column(name = "is_del", nullable = false)
    private Integer isDel = 0;

    /**
     * 创建人
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    /**
     * 修改人
     */
    @Column(name = "update_by", nullable = false)
    private Integer updateBy;

    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Date updateTime;

    /**
     * 附件列表
     */
    @Transient
    private ArrayList<YthFormFile> fileList;

}
