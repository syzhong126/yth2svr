package com.szkj.workplan.reposi;

import com.szkj.workplan.entity.WorkplanList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface WorkplanListReposi extends JpaRepository<WorkplanList, Long>, JpaSpecificationExecutor<WorkplanList> {

}