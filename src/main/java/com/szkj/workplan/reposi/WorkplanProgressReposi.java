package com.szkj.workplan.reposi;

import com.szkj.workplan.entity.WorkplanProgress;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface WorkplanProgressReposi extends JpaRepository<WorkplanProgress, Long>, JpaSpecificationExecutor<WorkplanProgress> {
    @Query(value = "SELECT * FROM `yth_workplan_progress` where plan_id=?1 " , nativeQuery = true)
    public ArrayList<WorkplanProgress> queryListByPlanId(long planId);
}