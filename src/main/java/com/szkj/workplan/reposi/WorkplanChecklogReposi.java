package com.szkj.workplan.reposi;

import com.szkj.workplan.entity.WorkplanChecklog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface WorkplanChecklogReposi extends JpaRepository<WorkplanChecklog, Long>, JpaSpecificationExecutor<WorkplanChecklog> {

}