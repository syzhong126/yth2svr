package com.szkj.report.reposi;

import com.szkj.report.entity.YthReportAccept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface YthReportAcceptReposi  extends JpaRepository<YthReportAccept, Long>, JpaSpecificationExecutor<YthReportAccept> {
    @Query(value = "SELECT * FROM `yth_report_accept` WHERE report_id=?1 AND accept_id=?2  " , nativeQuery = true)
    public YthReportAccept queryYthReportAccept(long rId,int acceptId);

    @Query(value = "SELECT * FROM `yth_report_accept` WHERE report_id=?1" , nativeQuery = true)
    public List<YthReportAccept> queryReportAccepts(long rId);
}
