package com.szkj.report.reposi;


import com.szkj.report.entity.YthReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-20 15:14:24
 */
public interface YthReportReposi extends JpaRepository<YthReport, Long>, JpaSpecificationExecutor<YthReport> {


}

