package com.szkj.report.service;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.common.util.UpdateUtil;
import com.szkj.file.entity.YthFormFile;
import com.szkj.file.service.YthFileFormService;
import com.szkj.report.entity.YthReport;
import com.szkj.report.entity.YthReportAccept;
import com.szkj.report.reposi.YthReportAcceptReposi;
import com.szkj.report.reposi.YthReportReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * (YthEntranceFacility)表服务实现类
 *
 * @author Yang
 * @since 2022-04-22 15:30:00
 */
@Slf4j
@Service
public class YthReportService {
    @Autowired
    private YthReportReposi reposi;
    @Autowired
    private YthReportAcceptReposi acceptreposi;
    @Autowired
    private YthFileFormService fileFormService;

    @Transactional(rollbackFor = Exception.class)
    public YthReport append(JSONObject body, int userId) throws Exception {
        try {
            YthReport bean = JSONUtil.toBean(body, YthReport.class);
            Calendar calendar = Calendar.getInstance();
            long time = calendar.getTimeInMillis();
            YthFormFile file= new YthFormFile();
            file.setDownloadUrl(body.getStr("downloadUrl"));
            file.setFileName(body.getStr("fileName"));
            file.setFileSize(body.getInt("fileSize"));
            file.setSaveName(body.getStr("saveName"));
            file.setFileExtension(body.getStr("fileExtension"));
            file.setFTable("yth_report");
            //解析传参的接收者id
            String[] aids = null;
            if(bean.getAcceptIds()!=null && bean.getAcceptIds().length()>0){
                String acceptIds = bean.getAcceptIds();
                if (acceptIds != null && acceptIds != "") {
                    if (acceptIds.contains(",")) {
                        aids = acceptIds.split(",");
                    } else {
                        aids=new String[]{acceptIds};
                    }
                }
            }
            List<YthReportAccept> addList=  new ArrayList<>();
            //编辑
            if(bean.getId()>0){
                YthReport  oldbean=reposi.findById(bean.getId()).get();
                bean.setUpdateBy(userId).setUpdateTime(time);
                UpdateUtil.copyNullProperties(bean,oldbean);  //修改的字段覆盖原对象
                YthReport result= reposi.save(oldbean);
                file.setFId(result.getId());
                file.setTs(time);
                fileFormService.append(file,"yth_report");

                //根据id查找历史记录
                List<YthReportAccept> ythReportAccepts=acceptreposi.queryReportAccepts(bean.getId());
               if(aids!=null){
                   for(YthReportAccept yra:ythReportAccepts){
                       acceptreposi.delete(yra);
                   }
                   for(int i=0;i<aids.length;i++){
                       YthReportAccept yra=new YthReportAccept();
                       yra.setAcceptId(Integer.parseInt(aids[i]));
                       yra.setReportId(bean.getId());
                       addList.add(yra);
                   }
                   acceptreposi.saveAll(addList);
               }
            }else{
                bean.setCreateBy(userId)
                        .setCreateTime(time)
                        .setUpdateBy(userId)
                        .setUpdateTime(time);
                reposi.save(bean);
                file.setFId(bean.getId());
                file.setTs(time);
                fileFormService.append(file,"yth_report");

                if(aids!=null){
                    for(int i=0;i<aids.length;i++){
                        YthReportAccept yra=new YthReportAccept();
                        yra.setAcceptId(Integer.parseInt(aids[i]));
                        yra.setReportId(bean.getId());
                        addList.add(yra);
                    }
                    acceptreposi.saveAll(addList);
                }
            }
            return bean;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

}
