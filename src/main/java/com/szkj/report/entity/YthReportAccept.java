package com.szkj.report.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年06月20日 9:18
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_report_accept")
public class YthReportAccept {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "report_id")
    private long reportId;
    @Column(name = "accept_id")
    private Integer acceptId;

}
