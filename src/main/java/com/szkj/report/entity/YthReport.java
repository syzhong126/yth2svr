package com.szkj.report.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Objects;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年04月25日 15:20
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_report")
public class YthReport {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "time")
    private String time;
    @Column(name = "rpt_user_id")
    private int rptUserId;
    @Column(name = "status")
    private byte status;
    @Column(name = "accept_ids")
    private String acceptIds;
    @Column(name = "rpt_content")
    private String rptContent;
    @Column(name = "type_id")
    private int typeId;
    /**
     * 创建人
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime;

    /**
     * 修改人
     */
    @Column(name = "update_by", nullable = false)
    private Integer updateBy;

    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Long updateTime;

}
