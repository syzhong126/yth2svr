package com.szkj.report.controller;


import cn.hutool.json.JSONObject;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.report.entity.YthReport;
import com.szkj.person.service.YthPersonService;
import com.szkj.report.service.YthReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * 控制层
 *
 * @author makejava
 * @since 2022-04-20 15:39:28
 */
@Slf4j
@RestController
@RequestMapping("/report")
@CrossOrigin("*")
public class YthReportController {
    @Autowired
    private YthReportService service;
    @Autowired
    private YthOperationLogService logservice;
    @Autowired
    private AuthService authService;

    @PostMapping("/add_report_info")
    public String append(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        try {
            YthReport ythReport = service.append(body,userId);
            String result = RespUtil.success("成功", ythReport);
            logservice.append(userId, "insert", "schedule", "add_schedule_plan_append", body.toString(), result, "",userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "insert", "schedule", "add_schedule_plan_append", body.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }

}

