package com.szkj;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author xiegl
 */
@EnableScheduling
@SpringBootApplication
@ServletComponentScan
@EnableTransactionManagement
public class Yth2svr {
    public static void main(String[] args) {
        new SpringApplicationBuilder(Yth2svr.class)
                .properties("spring.config.name:yth2svr")
                .build()
                .run(args);
    }
}