package com.szkj.alarm.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * (YthAlarm)实体类
 *
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_alarm")
public class Alarm implements Serializable {
    private static final long serialVersionUID = 956833238881925900L;
    @Id
    @Column(name = "id", nullable = false)
    private String id;
    /**
     * 数据类型，STORM气象预警，EVENT事件预警
     */
    @Column(name = "data_type")
    private String dataType;
    /**
     * 预警级别
     */
    @Column(name = "alarm_level")
    private String alarmLevel;
    /**
     * 发生时间
     */
    @Column(name = "occur_time")
    private Date occurTime;
    /**
     * 消息类型
     */
    @Column(name = "alarm_type")
    private String alarmType;
    /**
     * 标题
     */
    @Column(name = "title")
    private String title;
    /**
     * 将消息其他字段组装成表格填入内容
     */
    @Column(name = "content")
    private String content;
    /**
     * 原始数据
     */
    @Column(name = "json")
    private String json;
    /**
     * 消息状态，待预警、处置中、生效中、已结束。。
     */
    @Column(name = "status")
    private String status;
    /**
     * 空，或可查看的人账号用逗号隔开
     */
    @Column(name = "scope")
    private String scope;
    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;
    /**
     * 修改人
     */
    @Column(name = "update_by")
    private String updateBy;
    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;
}

