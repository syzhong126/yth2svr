package com.szkj.alarm.reposi;


import com.szkj.alarm.entity.Alarm;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-22 08:49:08
 */
public interface AlarmReposi extends CrudRepository<Alarm, String> {

    @Query(value = "select * from yth_alarm a where a.scope like CONCAT('%,',?1,',%') limit ?2,?3", nativeQuery = true)
    List<Alarm> list(@Param("userAcc") String userAcc, int start, int limit);

    @Query(value = "select count(1) from yth_alarm a where a.scope like CONCAT('%,',?1,',%')", nativeQuery = true)
    int count(@Param("userAcc") String userAcc);
}

