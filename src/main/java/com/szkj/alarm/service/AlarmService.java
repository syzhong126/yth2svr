package com.szkj.alarm.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONNull;
import cn.hutool.json.JSONObject;
import com.szkj.alarm.entity.Alarm;
import com.szkj.alarm.reposi.AlarmReposi;
import com.szkj.attendance.Util.NoticeUtil;
import com.szkj.auth.reposi.UserReposi;
import com.szkj.common.Page;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.reposi.YthNoticepushReposi;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * (YthAlarm)表服务实现类
 *
 * @author makejava
 * @since 2022-04-22 08:49:12
 */
@Slf4j
@Service
public class AlarmService {
    @Autowired
    private AlarmReposi reposi;
    @Autowired
    UserReposi userReposi;
    @Autowired
    YthNoticepushReposi noticepushReposi;

    public Page<?> list(String userAcc, Integer pageSize, Integer pageNo) {
        Page<Alarm> p = new Page<>();
        p.setPagenum(pageNo);
        p.setPagerows(pageSize);
        p.setData(reposi.list(userAcc, (pageNo - 1) * pageSize, pageSize));
        p.setTotalrows(reposi.count(userAcc));
        p.setRows(p.getData().size());
        return p;
    }

    public Alarm queryById(String id) {
        return this.reposi.findById(id).orElse(null);
    }

    @Autowired
    private RestTemplate restTemplate;
    @Value("${yth.synchronize.alarm.storm}")
    private String stormUrl;
    @Value("${yth.synchronize.alarm.event}")
    private String eventUrl;
    @Autowired
    private NoticeUtil noticeUtil;

    @Data
    static class DataFrom implements Serializable {
        private static final long serialVersionUID = 956833238881925902L;
        int code;
        List<Map<String, Object>> data;
        int dataSize;
        String message;
    }

    /**
     * 读取风暴预警消息，转储
     *
     * @param start 开始时间
     * @return 处理数据条数
     */
    public int TransDataStorm(String start) {
        DataFrom from;
        try {
            String url = StrUtil.format("{}?alarm_begin_time={}", stormUrl, start);
            from = restTemplate.getForObject(url, DataFrom.class);
            if (from == null) {
                log.error("风暴预警来源访问错误");
                return 0;
            }
        } catch (Exception e) {
            log.error("风暴预警来源无法访问");
            return 0;
        }
        List<Alarm> list = new ArrayList<>();
        List<YthNoticePush> nList = new ArrayList<>();
        if (from.getCode() == 0 && from.getDataSize() > 0) {
            from.getData().forEach(map -> {
                Alarm alarm = new Alarm().setId(getString(map.get("pk_stormid"))).setDataType("STORM")
                        .setAlarmType(getString(map.get("alarm_type"))).setAlarmLevel(getString(map.get("response_level")))
                        .setStatus(getString(map.get("deal_status"))).setTitle(getString(map.get("alarm_name")))
                        .setCreateBy(getString(map.get("c_person"))).setUpdateBy(getString(map.get("u_person")))
                        .setOccurTime(getDate(map.get("alarm_time"))).setCreateTime(getDate(map.get("c_time")))
                        .setUpdateTime(getDate(map.get("u_time"))).setContent(getString(map.get("alarm_desc")) + "<br>具体情况请登录" +
                                "<a href='http://10.82.1.5:8080/dist/#/index'>城发应急管理系统</a>查看")
                        .setJson(new JSONObject(map).toString());
                list.add(alarm);
                nList.add(noticeUtil.getAlarmNotice(alarm.getTitle(), alarm.getContent(), null));
            });
            if (!list.isEmpty()) reposi.saveAll(list);
            if (!nList.isEmpty()) noticepushReposi.saveAll(nList);
        }
        return from.getDataSize();
    }

    public String getString(Object o) {
        if (o == null) return null;
        else if (o instanceof JSONNull) return null;
        else return o.toString();
    }

    public Date getDate(Object o) {
        if (o == null) return null;
        else if (o instanceof JSONNull) return null;
        else {
            if (o instanceof Long)
                return new Date((long) o);
            else try {
                return new Date(Long.parseLong(o.toString()));
            } catch (Exception e) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    return sdf.parse(o.toString());
                } catch (ParseException ignored) {
                }
            }
            return null;
        }
    }

    /**
     * 读取事件预警消息，转储
     *
     * @param start 开始时间
     * @return 处理数据条数
     */
    public int TransDataEvent(String start) {
        DataFrom from;
        try {
            String url = StrUtil.format("{}?startTime={}", eventUrl, start);
            from = restTemplate.getForObject(url, DataFrom.class);
            if (from == null) {
                log.error("事件预警来源访问错误");
                return 0;
            }
        } catch (Exception e) {
            log.error("事件预警来源无法访问");
            return 0;
        }
        Map<String, String> eTypes = new HashMap<>();
        eTypes.put("1", "安全生产");
        eTypes.put("2", "自然灾害");
        eTypes.put("3", "环境污染");
        eTypes.put("4", "社会安全");

        Map<String, String> eStatuses = new HashMap<>();
        eStatuses.put("1", "待确警");
        eStatuses.put("2", "处置中");
        eStatuses.put("3", "待重发");
        eStatuses.put("4", "结束");
        eStatuses.put("5", "完结");

        Map<String, String> eLevels = new HashMap<>();
        eLevels.put("1", "事件上报流程-I级");
        eLevels.put("2", "事件上报流程-II级");
        eLevels.put("3", "事件上报流程-III级");

        List<Alarm> list = new ArrayList<>();
        List<YthNoticePush> nList = new ArrayList<>();
        if (from.getCode() == 0) {
            from.getData().forEach(map -> {
                Alarm alarm = new Alarm().setId(getString(map.get("pkEid"))).setDataType("EVENT")
                        .setOccurTime(getDate(map.get("eventTime"))).setTitle(getString(map.get("eventName")))
                        .setCreateBy(getString(map.get("cPerson"))).setUpdateBy(getString(map.get("uPerson")))
                        .setCreateTime(getDate(map.get("cTime"))).setUpdateTime(getDate(map.get("uTime")))
                        .setAlarmType(eTypes.get(getString(map.get("eventType").toString())))
                        .setAlarmLevel(eLevels.get(getString(map.get("levelId").toString())))
                        .setStatus(eStatuses.get(getString(map.get("eventStatus").toString())))
                        .setScope("," + getString(map.get("accounts")) + ",")
                        .setJson(new JSONObject(map).toString());
                SimpleDateFormat sdf = new SimpleDateFormat(" yyyy年MM月dd日");
                String content = StrUtil.format("<p>{}，在{}发生{}。</p><p>事件等级：{}</p><p>影响范围：{}</p><p>具体情况请登录"
                                + "<a href='http://10.82.1.5:8080/dist/#/index'>城发应急管理系统</a>查看。</p><p>已采取措施：{}</p>"
                                + "<p>事件发生趋势：{}</p><p>事情经过：{}</p>"
                        //+ "<p>是否结束事件：{}</p>"
                        , sdf.format(alarm.getOccurTime())
                        , getString(map.get("eventAddress"))
                        , eTypes.get(getString(map.get("eventType").toString()))//事件类型
                        , eLevels.get(getString(map.get("levelId").toString()))//事件等级
                        , getString(map.get("sphereOfInfluence"))//影响范围
                        , getString(map.get("measuresTaken"))//已采取措施
                        , getString(map.get("eventDevelopmentTrend"))//事件发生趋势
                        , getString(map.get("eventCourse"))//事件经过
                        //, getString(map.get("endEvent"))
                        //, getString(map.get("endContent"))
                );
                if (map.get("endContent") != null)
                    content += StrUtil.format("<p>是否结束事件：{}</p>"
                            , getString(map.get("endContent")));
                if (map.get("endTime") != null)
                    content += StrUtil.format("<p>结束时间：{}</p>"
                            , sdf.format(getDate(map.get("endTime"))));

                alarm.setContent(content);
                list.add(alarm);
                String[] ss = getString(map.get("accounts")).split(",");
                for (String s : ss) {
                    if (s.equals("")) continue;
                    userReposi.getUserByAccount(s).ifPresent(u -> nList.add(noticeUtil.getAlarmNotice(alarm.getTitle(), alarm.getContent(), u.getUserId())));
                }
            });
            if (!list.isEmpty()) reposi.saveAll(list);
            if (!nList.isEmpty()) noticepushReposi.saveAll(nList);
        }
        return list.size();
    }
}