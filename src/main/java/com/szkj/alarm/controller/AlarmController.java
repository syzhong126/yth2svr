package com.szkj.alarm.controller;


import com.szkj.alarm.entity.Alarm;
import com.szkj.alarm.service.AlarmService;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping("/alarm")
public class AlarmController {
    @Autowired
    private AlarmService service;

    @GetMapping("/list")
    public String list(@RequestParam(required = false, defaultValue = "20") Integer pageSize,
                       @RequestParam(required = false, defaultValue = "1") Integer pageNo,
                       @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        return RespUtil.success("成功", service.list(userinfo.getAccount(), pageSize, pageNo));
    }

    @GetMapping("/get")
    public String get(@RequestParam String id, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        Alarm a = service.queryById(id);
        if (!a.getScope().contains(userinfo.getAccount())) return RespUtil.error("无权访问");
        else return RespUtil.success("成功", a);
    }

    /*@GetMapping("/from")
    public String from() {
        //return RespUtil.success("成功", service.TransDataStorm("2022-06-01"));
        //return RespUtil.success("成功", service.TransDataEvent("2022-06-01"));
        return RespUtil.success("成功", null);
    } */
}

