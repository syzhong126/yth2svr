package com.szkj.alarm;

import com.szkj.alarm.service.AlarmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;

@Slf4j
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class AlarmScheduled {
    @Autowired
    AlarmService alarmService;

    //3.添加定时任务
    //或直接指定时间间隔，例如：5秒
    //@Scheduled(fixedRate=5000)
    //0 0 12 * * ?   每天中午12点触发
    //0 0 * * * ? *     每小时
    @Scheduled(cron = "0 0 * * * ?")
    public void alarmSync() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR_OF_DAY, -1);
        c.add(Calendar.MINUTE, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String start = sdf.format(c.getTime());
        try {
            log.info("执行定时任务同步预警消息：{}", LocalDateTime.now());
            int n = alarmService.TransDataStorm(start);
            if (n > 0) log.info("风暴预警: {}", n);
            n = alarmService.TransDataEvent(start);
            if (n > 0) log.info("事件预警: {}", n);
        } catch (Exception e) {
            log.error("预警消息同步错误：{}", e.getMessage());
        }
    }
}