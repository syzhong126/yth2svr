package com.szkj.schedule.controller;


import cn.hutool.json.JSONObject;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.schedule.entity.YthSchedulePlan;
import com.szkj.schedule.service.YthScheduleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * 控制层
 *
 * @author makejava
 * @since 2022-04-20 15:39:28
 */
@Slf4j
@RestController
@RequestMapping("/schedule")
@CrossOrigin("*")
public class YthScheduleController {
    @Autowired
    private YthScheduleService service;
    @Autowired
    private YthOperationLogService logservice;
    @Autowired
    private AuthService authService;

    @PostMapping("/add_schedule_plan")
    public String append(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        try {
            YthSchedulePlan ythSchedulePlan = service.append(body,userId,_token);
            String result = RespUtil.success("成功", ythSchedulePlan);
            logservice.append(userId, "insert", "schedule", "add_schedule_plan_append", body.toString(), result, "",userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "insert", "schedule", "add_schedule_plan_append", body.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }



    @PostMapping("/add_much_schedule_plan")
    public String appendMuch(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        try {
            String result = RespUtil.success("成功", service.appendMuch(body,_token,userId));
            logservice.append(userId, "insert", "schedule", "add_much_schedule_plan", body.toString(), result, "",userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "insert", "schedule", "add_much_schedule_plan", body.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }

    @PostMapping("/change_much_schedule_plan")
    public String changeMuch(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        try {
            String result = RespUtil.success("成功", service.changeMuch(body,_token,userId));
            logservice.append(userId, "update", "schedule", "change_much_schedule_plan", body.toString(), result, "",userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "update", "schedule", "change_much_schedule_plan", body.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }

}

