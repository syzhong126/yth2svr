package com.szkj.schedule.reposi;


import com.szkj.schedule.entity.YthSchedulePlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-20 15:14:24
 */
public interface YthScheduleReposi extends JpaRepository<YthSchedulePlan, Long>, JpaSpecificationExecutor<YthSchedulePlan> {

    @Query(value = "SELECT * FROM `yth_schedule_plan` WHERE active_id=?1 AND user_id=?2  LIMIT 0,1 ", nativeQuery = true)
    List<YthSchedulePlan> queryScheduleInfo(long active_id, long user_id);


    @Query(value = "SELECT * FROM `yth_schedule_plan` WHERE active_id=?1 AND user_id=?2 and calendar_type=?3  LIMIT 0,1 ", nativeQuery = true)
    Optional<YthSchedulePlan> querySchedule(int activeId, int userId, String type);


    @Query(value = "SELECT * FROM `yth_schedule_plan` WHERE active_id=?1 and calendar_type=0", nativeQuery = true)
    List<YthSchedulePlan> queryScheduleInfos(String active_id);
}

