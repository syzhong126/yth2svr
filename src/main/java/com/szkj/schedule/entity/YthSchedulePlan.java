package com.szkj.schedule.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年04月22日 9:15
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_schedule_plan")
public class YthSchedulePlan {
    @Id
    @Column(name = "schedule_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long scheduleId;

    @Column(name = "begin_time")
    private Long beginTime;

    @Column(name = "end_time")
    private Long endTime;

    @Column(name = "notice_time")
    private String noticeTime;

    @Column(name = "important_status")
    private Integer importantStatus;

    @Column(name = "active_id")
    private String activeId;

    @Column(name = "work_adress")
    private String workAdress;

    @Column(name = "work_desp")
    private String workDesp;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "design_status")
    private Integer designStatus;

    @Column(name = "design_note")
    private String designNote;

    @Column(name = "calendar_type")
    private String calendarType;
    /**
     * 创建人
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime;

    /**
     * 修改人
     */
    @Column(name = "update_by", nullable = false)
    private Integer updateBy;

    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Long updateTime;


}
