package com.szkj.schedule.service;


import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.common.util.UpdateUtil;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.service.YthNoticepushService;
import com.szkj.schedule.entity.YthSchedulePlan;
import com.szkj.schedule.reposi.YthScheduleReposi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Yang
 * @since 2022-04-22 15:30:00
 */
@Slf4j
@Service
public class YthScheduleService {
    @Autowired
    private YthScheduleReposi reposi;
    @Autowired
    private YthNoticepushService ythNoticepushService;

    @Transactional(rollbackFor = Exception.class)
    public YthSchedulePlan append(JSONObject body, int userId,String token) throws Exception {
        try {
            YthSchedulePlan bean = JSONUtil.toBean(body, YthSchedulePlan.class);

            Calendar calendar = Calendar.getInstance();
            long time = calendar.getTimeInMillis();

            YthNoticePush ynp = new YthNoticePush();//添加推送消息
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String beginTime = simpleDateFormat.format(new Date(bean.getBeginTime() * 1000));
            if(bean.getScheduleId()>0){//修改
                YthSchedulePlan  oldbean=reposi.findById(bean.getScheduleId()).get();
                bean.setUpdateBy(userId).setUpdateTime(time);
                UpdateUtil.copyNullProperties(bean,oldbean);  //修改的字段覆盖原对象
                reposi.save(oldbean);
                ynp.setTitle("行程变更")
                        .setContent("您有一个新的行程变更："+beginTime+"，"+bean.getWorkAdress()+"，"+bean.getWorkDesp()+"")
                        .setMoudleType("changeschedule");
            }else{
                bean.setCreateBy(userId)
                        .setCreateTime(time)
                        .setUpdateBy(userId)
                        .setUpdateTime(time).setDesignStatus(0);
                reposi.save(bean);
                ynp.setTitle("新的行程")
                        .setContent("您有一个新的行程："+beginTime+"，"+bean.getWorkAdress()+"，"+bean.getWorkDesp()+"")
                        .setMoudleType("newschedule");
            }
            ynp.setPushUser(bean.getUserId())
                    .setPushType(2).setPushStatus(0).setPushTime(time)
                    .setFromSystem("yth").setMoudleDetail("schedule")
                    .setCreateTime(time)
                    .setUpdateTime(time).setCreateBy(userId+"");
            ythNoticepushService.append(ynp,token);

            if(bean.getBeginTime()!=null ){
                //推送提醒
                YthNoticePush lj_ynp = new YthNoticePush();//添加推送消息
                String noticeTime=bean.getNoticeTime();
                if(noticeTime!=null && noticeTime!=""){
                    int minite=Integer.parseInt(noticeTime);
                    lj_ynp.setPushTime(bean.getBeginTime() * 1000 - minite * 60 * 1000);
                    String ljTime = simpleDateFormat.format(new Date(bean.getBeginTime() * 1000 - minite * 60 * 1000));
                    lj_ynp.setTitle("临近行程")
                            .setContent("您有一个临近行程："+ljTime+"，"+bean.getWorkAdress()+"，"+bean.getWorkDesp()+"。若行程已取消，请忽略本消息。")
                            .setMoudleType("nearschedule");
                    lj_ynp.setPushUser(bean.getUserId())
                            .setPushType(2).setPushStatus(0)
                            .setFromSystem("yth").setMoudleDetail("schedule")
                            .setCreateTime(time)
                            .setUpdateTime(time).setCreateBy(userId+"");
                    ythNoticepushService.append(lj_ynp,token);
                }
            }


            return bean;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }


    @Transactional(rollbackFor = Exception.class)
    public String appendMuch(JSONObject body,String token,int userId) throws Exception {
        try {
            YthSchedulePlan bean = JSONUtil.toBean(body, YthSchedulePlan.class);
            List<YthSchedulePlan> plans = new ArrayList<>();
            String userIds = (String) body.get("userIds");
            String[] uids = null;
            if (userIds != null && userIds != "") {
                if (userIds.contains(",")) {
                    uids = userIds.split(",");
                } else {
                    uids=new String[]{userIds};
                }
            }
            Calendar calendar = Calendar.getInstance();
            long time = calendar.getTimeInMillis();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String beginTime = simpleDateFormat.format(new Date(bean.getBeginTime() * 1000));
            UUID uuid = UUID.randomUUID();
            String c_uuid= uuid.toString().replace("-", "").toUpperCase();
            for (int i = 0; i < uids.length; i++) {
                YthSchedulePlan _temp = new YthSchedulePlan();
                _temp.setBeginTime(bean.getBeginTime())
                        .setEndTime(bean.getEndTime())
                        .setCalendarType("0")
                        .setActiveId(c_uuid)
                        .setDesignNote(bean.getDesignNote())
                        .setDesignStatus(bean.getDesignStatus())
                        .setUserId(Integer.parseInt(uids[i]))
                        .setNoticeTime(bean.getNoticeTime())
                        .setWorkAdress(bean.getWorkAdress())
                        .setWorkDesp(bean.getWorkDesp())
                        .setImportantStatus(bean.getImportantStatus())
                        .setCreateTime(time)
                        .setUpdateTime(time).setDesignStatus(0).setCreateBy(userId);
                plans.add(_temp);
                YthNoticePush ynp = new YthNoticePush();//添加推送消息
                ynp.setPushUser(Integer.parseInt(uids[i]))
                        .setPushType(2).setPushStatus(0).setPushTime(time)
                        .setFromSystem("yth").setMoudleDetail("schedule")
                        .setCreateTime(time)
                        .setUpdateTime(time)
                        .setTitle("新的行程")
                        .setContent("您有一个新的行程："+beginTime+"，"+bean.getWorkAdress()+"，"+bean.getWorkDesp()+"")
                        .setMoudleType("newschedule").setCreateBy(userId+"");
                ythNoticepushService.append(ynp,token);

                if(bean.getBeginTime()!=null ){
                    //推送提醒
                    YthNoticePush lj_ynp = new YthNoticePush();//添加推送消息
                    String noticeTime=bean.getNoticeTime();
                    if(noticeTime!=null && noticeTime!=""){
                        int minite=Integer.parseInt(noticeTime);
                        lj_ynp.setPushTime(bean.getBeginTime() * 1000 - minite * 60 * 1000);
                        String ljTime = simpleDateFormat.format(new Date(bean.getBeginTime() * 1000 - minite * 60 * 1000));
                        lj_ynp.setTitle("临近行程")
                                .setContent("您有一个临近行程："+ljTime+"，"+bean.getWorkAdress()+"，"+bean.getWorkDesp()+"。若行程已取消，请忽略本消息。")
                                .setMoudleType("nearschedule");
                        lj_ynp.setPushUser(Integer.parseInt(uids[i]))
                                .setPushType(2).setPushStatus(0)
                                .setFromSystem("yth").setMoudleDetail("schedule")
                                .setCreateTime(time)
                                .setUpdateTime(time).setCreateBy(userId+"");
                        ythNoticepushService.append(lj_ynp,token);
                    }
                }
            }
            reposi.saveAll(plans);
            return "success";
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String changeMuch(JSONObject body,String token,int userId) throws Exception {
        try {
            YthSchedulePlan bean = JSONUtil.toBean(body, YthSchedulePlan.class);
            List<YthSchedulePlan> sps=reposi.queryScheduleInfos(bean.getActiveId());
            for(YthSchedulePlan ysp:sps){
                ysp.setDesignStatus(3);
                reposi.save(ysp);
            }
            List<YthSchedulePlan> plans = new ArrayList<>();
            String userIds = (String) body.get("userIds");
            String[] uids = null;
            if (userIds != null && userIds != "") {
                if (userIds.contains(",")) {
                    uids = userIds.split(",");
                } else {
                    uids=new String[]{userIds};
                }
            }
            Calendar calendar = Calendar.getInstance();
            long time = calendar.getTimeInMillis();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String beginTime = simpleDateFormat.format(new Date(bean.getBeginTime() * 1000));

            for (int i = 0; i < uids.length; i++) {
                YthSchedulePlan _temp = new YthSchedulePlan();
                _temp.setBeginTime(bean.getBeginTime())
                        .setEndTime(bean.getEndTime())
                        .setCalendarType("0")
                        .setActiveId(bean.getActiveId())
                        .setDesignNote(bean.getDesignNote())
                        .setDesignStatus(bean.getDesignStatus())
                        .setUserId(Integer.parseInt(uids[i]))
                        .setNoticeTime(bean.getNoticeTime())
                        .setWorkAdress(bean.getWorkAdress())
                        .setWorkDesp(bean.getWorkDesp())
                        .setImportantStatus(bean.getImportantStatus())
                        .setCreateTime(time)
                        .setUpdateTime(time).setDesignStatus(0).setCreateBy(userId);
                plans.add(_temp);
                YthNoticePush ynp = new YthNoticePush();//添加推送消息
                ynp.setPushUser(Integer.parseInt(uids[i]))
                        .setPushType(2).setPushStatus(0).setPushTime(time)
                        .setFromSystem("yth").setMoudleDetail("schedule")
                        .setCreateTime(time)
                        .setUpdateTime(time)
                        .setTitle("行程变更")
                        .setContent("您有一个新的行程变更："+beginTime+"，"+bean.getWorkAdress()+"，"+bean.getWorkDesp()+"")
                        .setMoudleType("changeschedule").setCreateBy(userId+"");
                ythNoticepushService.append(ynp,token);

                if(bean.getBeginTime()!=null ){
                    //推送提醒
                    YthNoticePush lj_ynp = new YthNoticePush();//添加推送消息
                    String noticeTime=bean.getNoticeTime();
                    if(noticeTime!=null && noticeTime!=""){
                        int minite=Integer.parseInt(noticeTime);
                        lj_ynp.setPushTime(bean.getBeginTime() * 1000 - minite * 60 * 1000);
                        String ljTime = simpleDateFormat.format(new Date(bean.getBeginTime() * 1000 - minite * 60 * 1000));
                        lj_ynp.setTitle("临近行程")
                                .setContent("您有一个临近行程："+ljTime+"，"+bean.getWorkAdress()+"，"+bean.getWorkDesp()+"。若行程已取消，请忽略本消息。")
                                .setMoudleType("nearschedule");
                        lj_ynp.setPushUser(Integer.parseInt(uids[i]))
                                .setPushType(2).setPushStatus(0)
                                .setFromSystem("yth").setMoudleDetail("schedule")
                                .setCreateTime(time)
                                .setUpdateTime(time).setCreateBy(userId+"");
                        ythNoticepushService.append(lj_ynp,token);
                    }
                }
            }
            reposi.saveAll(plans);
            return "success";
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
