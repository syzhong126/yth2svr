package com.szkj.attendance.service;

import com.szkj.attendance.entity.Out;
import com.szkj.attendance.reposi.OutReposi;
import org.springframework.stereotype.Service;
import com.szkj.common.Page;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 外出审批记录(YthAttendOut)表服务实现类
 *
 * @author makejava
 * @since 2022-04-26 11:32:04
 */
@Service
public class OutService {
    @Autowired
    private OutReposi reposi;

    public Page<Out> list(Integer pageSize, Integer pageNo) {
        Page<Out> p = new Page<>();
        p.setPagenum(pageNo);
        p.setPagerows(pageSize);
        p.setData(reposi.list((pageNo - 1) * pageSize, pageSize));
        p.setTotalrows(reposi.count(""));
        p.setRows(p.getData().size());
        return p;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param outId 主键
     * @return 实例对象
     */
    public Out queryById(Integer outId) {
        return reposi.findById(outId).orElse(null);
    }

    /**
     * 保存数据，有ID修改，无ID新增
     *
     * @param ythAttendOut 实例对象
     */
    public void save(Out ythAttendOut) {
        reposi.save(ythAttendOut);
    }

    public void read(Integer outId) {
        reposi.read(outId);
    }

    /**
     * 通过主键删除数据
     *
     * @param outId 主键
     * @return 是否成功
     */
    public void deleteById(Integer outId) {
        reposi.deleteById(outId);
    }

    public void delete(String ids) {
        reposi.delete(ids);
    }
}
