package com.szkj.attendance.service;

import com.szkj.attendance.entity.Except;
import com.szkj.attendance.reposi.ExceptReposi;
import com.szkj.common.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 异常处理记录(YthAttendExcept)表服务实现类
 */
@Service
public class ExceptService {
    @Autowired
    private ExceptReposi reposi;

    public Page<?> list(Integer pageSize, Integer pageNo) {
        Page<Except> p = new Page<>();
        p.setPagenum(pageNo);
        p.setPagerows(pageSize);
        p.setData(reposi.list((pageNo - 1) * pageSize, pageSize));
        p.setTotalrows(reposi.count(""));
        p.setRows(p.getData().size());
        return p;
    }

    /**
     * s
     * 通过ID查询单条数据
     *
     * @param expId 主键
     * @return 实例对象
     */
    public Except queryById(Integer expId) {
        return reposi.findById(expId).orElse(null);
    }

    /**
     * 保存数据，有ID修改，无ID新增
     *
     * @param ythAttendExcept 实例对象
     */
    public void save(Except ythAttendExcept) {
        reposi.save(ythAttendExcept);
    }

    /**
     * 通过主键删除数据
     *
     * @param expId 主键
     */
    public void deleteById(Integer expId) {
        reposi.deleteById(expId);
    }


    public void delete(String ids) {
        reposi.delete(ids);
    }
}
