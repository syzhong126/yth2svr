package com.szkj.attendance.service;

import com.szkj.attendance.Util.AttendRedisUtil;
import com.szkj.attendance.entity.Group;
import com.szkj.attendance.reposi.GroupReposi;
import com.szkj.common.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 考勤组主信息表(YthAttendGroup)表服务实现类
 *
 * @author makejava
 * @since 2022-04-26 10:11:42
 */
@Service
public class GroupService {
    @Autowired
    private GroupReposi reposi;
    @Autowired
    private AttendRedisUtil redisUtil;

    public Page<Group> list(Integer unitId, String status, String keyword, Integer pageSize, Integer pageNo) {
        Page<Group> p = new Page<>();
        p.setPagenum(pageNo);
        p.setPagerows(pageSize);
        p.setData(reposi.list(unitId, status, keyword, (pageNo - 1) * pageSize, pageSize));
        p.setTotalrows(reposi.count(unitId, status, keyword));
        p.setRows(p.getData().size());
        return p;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param attendId 主键
     * @return 实例对象
     */
    public Group queryById(Integer attendId) {
        return reposi.findById(attendId).orElse(null);
    }

    /**
     * 新增数据
     *
     * @param ythAttendGroup 实例对象
     */
    public void save(Group ythAttendGroup) {
        Group group = reposi.save(ythAttendGroup);
        redisUtil.setRedis(AttendRedisUtil.TypeGroup, group.getGroupId(), group);
    }

    /**
     * 启用禁用考勤组
     *
     * @param groupId 考勤组
     * @param enable  是否有效
     */
    public void enable(Integer groupId, String enable) {
        reposi.enable(groupId, enable);
    }

    /**
     * 通过主键删除数据
     *
     * @param attendId 主键
     */
    public void deleteById(Integer attendId) {
        reposi.deleteById(attendId);
    }

    public void delete(String ids) {
        reposi.delete(ids);
        redisUtil.delRedis(AttendRedisUtil.TypeGroup, ids.split(","));
    }
}
