package com.szkj.attendance.service;

import com.szkj.attendance.Util.AttendRedisUtil;
import com.szkj.attendance.Util.DateUtil;
import com.szkj.attendance.Util.DistanceUtil;
import com.szkj.attendance.entity.*;
import com.szkj.attendance.reposi.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AttendService {
    @Autowired
    private HistoryReposi historyReposi;
    @Autowired
    private GroupReposi groupReposi;
    @Autowired
    private TempReposi tempReposi;
    @Autowired
    private TemplExceptReposi templExceptReposi;
    @Autowired
    private LocationReposi locationReposi;
    @Autowired
    private AuthPersonReposi authPersonReposi;
    @Autowired
    private AuthDeptReposi authDeptReposi;
    @Autowired
    private AttendRedisUtil redisUtil;

    public List<AuthPerson> getGroupUsers(Integer groupId) {
        return authPersonReposi.getGroupUsers(groupId);
    }

    /**
     * 获取部门可用的所有考勤组，携带部门权限
     *
     * @param deptId 部门ID
     * @return 考勤组ID，name，部门是否有权限，是否默认考勤组
     */
    public List<Map<String, Object>> getAuthCorporate(Integer deptId) {
        return authDeptReposi.getDeptGroups(deptId);
    }

    /**
     * 更新部门与考勤组关系
     *
     * @param auths hasAuth为false的记录将北删除
     */
    public void setAuthCorporate(List<AuthDept> auths) {
        List<AuthDept> has = auths.stream().filter(AuthDept::getHasAuth).collect(Collectors.toList());
        authDeptReposi.saveAll(has);
        if (auths.removeAll(has))
            authDeptReposi.deleteInBatch(auths);

    }

    /**
     * 获取部门下的人员列表，携带所属考勤组信息
     * 如果个人没有考勤组关联，就把部门默认考勤组设为个人的考勤组
     *
     * @param deptId 部门ID，不是公司ID
     * @return 人员ID、name，考勤组ID、name
     */
    public List<Map<String, Object>> getAuthPersonal(Integer deptId) {
        if (deptId < 1000) return null;
        List<Map<String, Object>> list = authPersonReposi.getAll(deptId);
        List<Map<String, Object>> noAuthList = list.stream().filter(m -> m.get("groupId") == null).collect(Collectors.toList());
        if (noAuthList.size() > 0) {
            List<Map<String, Object>> groupDept = authDeptReposi.getDeptGroups(deptId);
            Optional<Map<String, Object>> groupDefault = groupDept.stream().filter(m -> ((BigInteger) m.get("isDefault")).intValue() == 1).findFirst();
            List<AuthPerson> authList = new ArrayList<>();
            if (groupDefault.isPresent()) {
                Map<String, Object> gd = groupDefault.get();
                Integer groupId = (Integer) gd.get("groupId");
                for (Map<String, Object> map : noAuthList) {
                    AuthPerson a = new AuthPerson();
                    a.setUserId((Integer) map.get("userId"));
                    a.setGroupId(groupId);
                    authList.add(a);
                }
                authPersonReposi.saveAll(authList);
            }
            list = authPersonReposi.getAll(deptId);
        }
        return list;
    }

    /**
     * 更新用户考勤组设置
     *
     * @param auths 设置
     */
    public void setAuthPersonal(List<AuthPerson> auths) {
        authPersonReposi.saveAll(auths);
    }

    /**
     * 打卡
     *
     * @param token 登录的token
     * @throws Exception 超时或者作废，非打卡时间，非工作日，考勤组设置不正确
     */
    public void attend(String token) throws Exception {
        Attend attend = redisUtil.getRedisAttend(token);
        if (attend.getClockType() == null) throw new Exception("打卡申请超时或者已作废");
        if (attend.getClockType() == Attend.Clock_Not_Time) throw new Exception("非打卡时间");
        if (attend.getClockType() == Attend.Clock_Not_Day) throw new Exception("非工作日");
        if (attend.getClockType() == Attend.Clock_Not_Set) throw new Exception("考勤组设置不正确");
        Optional<History> optionalHis = historyReposi.findById(attend.getAttHisId());
        if (optionalHis.isPresent()) {
            History his = optionalHis.get();
            if (attend.getClockType() == Attend.Clock_On)
                ClockIn(attend, his);
            else if (attend.getClockType() == Attend.Clock_Off)
                ClockOff(attend, his);
            Final(his);
            historyReposi.save(his);
        }
    }

    /**
     * 设置打卡记录的最总结果
     *
     * @param his 打卡记录
     */
    private void Final(History his) {
        String ci = his.getClockinStatus();
        String co = his.getClockoutStatus();
        if ((ci.equals(History.Type_Not) && co.equals(History.Type_Normal)) ||
                (ci.equals(History.Type_Normal) && co.equals(History.Type_Not))) //一个正常，一个没打卡
            his.setFinalStatus(History.Type_Miss);//缺卡
        else if (ci.equals(History.Type_Late) && co.equals(History.Type_Normal))//迟到+正常
            his.setFinalStatus(History.Type_Late);//迟到
        else if (ci.equals(History.Type_Normal) && co.equals(History.Type_Early))//正常+早退
            his.setFinalStatus(History.Type_Early);//早退
        else if (ci.equals(History.Type_Late) && co.equals(History.Type_Early))//迟到+早退
            his.setFinalStatus(History.Type_Late_And_Early);//迟到+早退
        else if (ci.equals(History.Type_Late) && co.equals(History.Type_Not))//迟到+缺卡
            his.setFinalStatus(History.Type_Late_And_Miss);//迟到+缺卡
    }

    /**
     * 打下班卡
     *
     * @param attend 考勤申请
     * @param his    考勤记录
     */
    private void ClockOff(Attend attend, History his) {
        long now = System.currentTimeMillis();
        float along = (float) (now - his.getClockFirst()) / (60 * 60 * 1000);
        along = (float) Math.round(along * 10) / 10;
        his.setClockLast(now).setClockLastLoc(attend.getLocId()).setClockAlong(along)
                .setClockoutStatus(now < attend.getClockOut() ? History.Type_Early : History.Type_Normal)
                .addSum();
    }

    /**
     * 打上班卡
     *
     * @param attend 考勤申请
     * @param his    考勤记录
     */
    private void ClockIn(Attend attend, History his) {
        long now = System.currentTimeMillis();
        his.setClockFirst(now).setClockFirstLoc(attend.getLocId())
                .setClockinStatus(now > attend.getClockIn() ? History.Type_Late : History.Type_Normal)
                .addSum();
    }

    /**
     * 打卡申请
     *
     * @param userId 用户
     * @param lng    经度
     * @param lat    纬度
     * @param token  登录的token
     * @return 打卡申请
     */
    public Attend apply(Integer userId, Double lng, Double lat, String token) {
        Attend att = new Attend();
        History his = null;
        // 获取当前日期
        Calendar today = DateUtil.today();
        att.setClockDate(today.getTimeInMillis());
        //获取当天历史记录
        Optional<History> optionalHis = historyReposi.findByCurrent(userId, today.getTimeInMillis());
        // 判断日期是否有历史记录 // 没有历史记录，就新建
        if (optionalHis.isPresent()) his = optionalHis.get();
        else try {
            his = createHistory(userId, today);
            if (his == null) att.setClockType(Attend.Clock_Not_Day);
        } catch (Exception e) {
            att.setClockType(Attend.Clock_Not_Set);
        }
        if (his != null) {
            att.setAttHisId(his.getAttHisId());
            att.setClockStart(his.getClockFirst());
            att.setClockEnd(his.getClockLast());
            getGroup(his.getGroupId()).ifPresent(group -> getTemp(group.getTempId()).ifPresent(temp -> {
                setLocation(lng, lat, att, group);
                setTemp(att, temp);
            }));
        }
        redisUtil.setRedisAttend(att, token);
        return att;
    }

    /**
     * 补卡，每月两次不许审批补卡机会
     *
     * @param userId 用户
     * @param date   日期
     */
    public void rectify(Integer userId, String date) throws Exception {
        //顶天上月可补
        Calendar today = DateUtil.today();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        long d = fmt.parse(date).getTime();
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(d);
        if (today.get(Calendar.MONTH) - c.get(Calendar.MONTH) > 1)
            throw new Exception("当月工资都已经发了，还补，你想干嘛");
        //获取月份
        fmt = new SimpleDateFormat("yyyy-MM");
        String month = fmt.format(d);
        //获取该月补卡数据
        String data = redisUtil.getRedis(AttendRedisUtil.TypeRectify + month, userId);
        int i = data == null ? 0 : Integer.parseInt(data);
        if (i == 2) throw new Exception("补卡以达到两次，不能再补");
        //可以补卡，补卡，更新补卡次数
        Optional<History> oph = historyReposi.findByCurrent(userId, d);
        if (oph.isPresent()) {
            History h = oph.get();
            if (!h.getFinalStatus().contains("NORMAL")) {
                h.setFinalStatus(History.Type_Normal_Rectify);
                historyReposi.save(h);
                redisUtil.setRedis(AttendRedisUtil.TypeRectify + month, userId, "" + i);
            } else throw new Exception("正常的记录不需要补卡");
        }
    }

    /**
     * 生成打卡记录，如果定时生成全部成员的打卡记录，可通过该方法
     *
     * @param userId 用户
     * @param today  今天
     * @return 打卡记录
     * @throws Exception 没有设置的信息
     */
    private History createHistory(Integer userId, Calendar today) throws Exception {
        Group group;
        //查找用户关联的考勤组
        Optional<Group> optionalGroup = groupReposi.getMyGroups(userId);
        if (optionalGroup.isPresent()) group = optionalGroup.get();
        else {
            // 如果用户没有关联考勤组， 获取用户所在部门的默认考勤组
            optionalGroup = groupReposi.getMyDeptGroups(userId);
            if (optionalGroup.isPresent()) {
                group = optionalGroup.get();
                //通过部门默认考勤组，建立个人考勤组关联
                addAuthPersonal(userId, group.getGroupId());
            } else throw new Exception("没有设置考勤组");
        }
        Optional<Temp> optionalTemp = getTemp(group.getTempId());
        if (!optionalTemp.isPresent()) throw new Exception("没有设置考勤模板");
        else return createHistory(userId, optionalTemp.get(), group.getGroupId(), today);
    }

    /**
     * 新建考勤记录，
     *
     * @param userId  用户
     * @param temp    考勤模板
     * @param groupId 考勤组
     * @param today   今天
     * @return 非工作日返回null
     */
    private History createHistory(int userId, Temp temp, int groupId, Calendar today) {
        boolean isExcept = isExcept(temp.getTempId(), today);
        int d = today.get(Calendar.DAY_OF_WEEK);
        //判断是否工作日，工作日和排除日做个异或操作，工作日+!排除日 或者 !工作日+排除日，上班；
        if (temp.getDate().contains(String.valueOf(d)) ^ isExcept) {
            History his = new History().setClockDate(today.getTimeInMillis())
                    .setGroupId(groupId).setPersonId(userId)
                    .setClockinStatus(History.Type_Not)
                    .setClockoutStatus(History.Type_Not)
                    .setFinalStatus(History.Type_Absent);
            historyReposi.save(his);
            return his;
        }
        return null;
    }

    /**
     * 产生考勤组下的所有人今天的考勤记录
     *
     * @param temp    考勤模板
     * @param groupId 考勤组Id
     */
    public void createHistory(Temp temp, int groupId) {
        Calendar today = DateUtil.today();
        boolean isExcept = isExcept(temp.getTempId(), today);
        int d = today.get(Calendar.DAY_OF_WEEK);
        //log.info("today:{}", today.getTimeInMillis());
        //判断是否工作日，工作日和排除日做个异或操作，工作日+!排除日 或者 !工作日+排除日，上班；
        if (temp.getDate().contains(String.valueOf(d)) ^ isExcept) {
            //log.info("工作日");
            //考勤组考勤开始前1小时
            List<AuthPerson> list = getGroupUsers(groupId);
            //log.info("group users {}", list.size());
            List<History> hisList = new ArrayList<>();
            // 生成考勤原始数据
            list.forEach(ap -> {
                Optional<History> oph = historyReposi.findByCurrent(ap.getUserId(), today.getTimeInMillis());
                if (!oph.isPresent()) {
                    //log.info("历史记录：{}", ap.getUserId());
                    hisList.add(new History().setClockDate(today.getTimeInMillis())
                            .setGroupId(ap.getGroupId()).setPersonId(ap.getUserId())
                            .setClockinStatus(History.Type_Not)
                            .setClockoutStatus(History.Type_Not)
                            .setFinalStatus(History.Type_Absent));
                }
            });
            historyReposi.saveAll(hisList);
        }
    }

    /**
     * 判断今天是否排除日期
     * 将排除信息保存Redis 0|32414341,
     * 格式：（0否|1是）|今天日期的long值，如果不是今天需要更新
     *
     * @param tempId 考勤模板ID
     * @param today  今天
     * @return 否排除日期
     */
    private boolean isExcept(Integer tempId, Calendar today) {
        String except = redisUtil.getRedis(AttendRedisUtil.TypeTempExcept, tempId);
        long day = today.getTimeInMillis();
        if (except != null && except.contains("0|" + day)) return false;
        else if (except != null && except.contains("1|" + day)) return true;
        else {
            Optional<TemplExcept> optionalExcept = templExceptReposi.getExcept(tempId, day);
            boolean result = optionalExcept.isPresent();
            except = (except == null ? "" : except + ",") + (result ? "1|" : "0|") + day;
            redisUtil.setRedis(AttendRedisUtil.TypeTempExcept, tempId, except);
            return result;
        }
    }

    /**
     * 获取考勤组
     * 从Redis取，或者从数据库保存Redis并返回
     *
     * @param groupId 考勤组ID
     * @return 考勤组
     */
    private Optional<Group> getGroup(Integer groupId) {
        Optional<Group> optionalGroup = redisUtil.getRedis(AttendRedisUtil.TypeGroup, groupId, Group.class);
        if (!optionalGroup.isPresent()) {
            optionalGroup = groupReposi.findById(groupId);
            optionalGroup.ifPresent(group -> redisUtil.setRedis(AttendRedisUtil.TypeGroup, groupId, group));
        }
        return optionalGroup;
    }

    /**
     * 获取考勤模板
     * 从Redis取，或者从数据库保存Redis并返回
     *
     * @param tempId 考勤模板ID
     * @return 考勤模板
     */
    private Optional<Temp> getTemp(Integer tempId) {
        Optional<Temp> optionalTemp = redisUtil.getRedis(AttendRedisUtil.TypeTemp, tempId, Temp.class);
        if (!optionalTemp.isPresent()) {
            optionalTemp = tempReposi.findById(tempId);
            optionalTemp.ifPresent(temp -> redisUtil.setRedis(AttendRedisUtil.TypeTemp, tempId, temp));
        }
        return optionalTemp;
    }

    /**
     * 遍历所有考勤地点，距离合适，给考勤申请设置考勤地点信息
     *
     * @param lng   经度
     * @param lat   纬度
     * @param att   考勤申请
     * @param group 考勤组
     */
    private void setLocation(Double lng, Double lat, Attend att, Group group) {
        if (group == null) return;
        //取得考勤组所有可考勤地点
        List<Location> list = locationReposi.getByIds(group.getLocId());
        att.setIsInDistance(false);
        for (Location l : list) {
            double d = DistanceUtil.getDistance(lng, lat, l.getLng(), l.getLat());
            //log.info("地点：{}，距离：{}米", l.getLocName(), d);
            // 如果计算出距离小于distance
            if (d < l.getDistance()) {
                att.setLocation(l.getLocName());
                att.setLocId(l.getLocId());
                att.setIsInDistance(true);
                break;
            }
        }
    }

    /**
     * 给考勤申请设置考勤模板相关信息
     * 是否能打卡，打的什么卡，今天的上下班时间
     *
     * @param att  考勤申请设
     * @param temp 考勤模板
     */
    private void setTemp(Attend att, Temp temp) {
        if (temp == null) return;
        att.setClockIn(temp.getOnTime());
        att.setClockOut(temp.getOffTime());
        // 根据当前时间判断打什么卡
        long now = System.currentTimeMillis();
        if (now < att.getClockIn()) {//上班前2小时内可打上班卡
            long h = (att.getClockIn() - now) / (60 * 60 * 1000);
            att.setClockType(h > 2 ? Attend.Clock_Not_Time : Attend.Clock_On);
        } else if (now > att.getClockOut()) {//下班后4小时内可打下班卡
            long h = (now - att.getClockOut()) / (60 * 60 * 1000);
            att.setClockType(h > 4 ? Attend.Clock_Not_Time : Attend.Clock_Off);
        } else // 上班时间，没打上班卡的，打上班卡，算迟到；打了上班卡的打下班卡，算早退
            att.setClockType(att.getClockStart() == null ? Attend.Clock_On : Attend.Clock_Off);

    }

    /**
     * 添加用户考勤组关联
     *
     * @param userId  用户
     * @param groupId 考勤组
     */
    private void addAuthPersonal(Integer userId, Integer groupId) {
        AuthPerson auth = new AuthPerson();
        auth.setUserId(userId);
        auth.setGroupId(groupId);
        authPersonReposi.save(auth);
    }

    /**
     * @param start   开始时间
     * @param end     结束时间     *
     * @param groupId 考勤组Id
     * @param orgId   部门Id
     * @param userId  用户Id
     * @return 统计数据结果集
     */
    public List<Map<String, Object>> statisticPersonal(Long start, Long end, Integer groupId, Integer orgId, Integer userId) {
        return historyReposi.statisticPersonal(start, end, groupId, orgId, userId);
    }

    /**
     * @param start   开始时间
     * @param end     结束时间     *
     * @param groupId 考勤组Id
     * @param orgId   公司Id
     * @return 统计数据结果集
     */
    public List<Map<String, Object>> statisticCorporate(Long start, Long end, Integer groupId, Integer orgId) {
        return historyReposi.statisticCorporate(start, end, groupId, orgId);
    }

    public List<History> getUnNormal(int groupId, long date) {
        return historyReposi.getUnNormal(groupId, date);
    }

    /**
     * 清除重置Redis，清除考勤日记录，清除上上月的补卡记录
     */
    public void refreshRedis() {
        redisUtil.delRedis(AttendRedisUtil.TypeTempExcept);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -2);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        redisUtil.delRedis(AttendRedisUtil.TypeRectify + sdf.format(c.getTimeInMillis()));
    }
}
