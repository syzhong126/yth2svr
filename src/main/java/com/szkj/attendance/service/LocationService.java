package com.szkj.attendance.service;

import com.szkj.attendance.entity.Location;
import com.szkj.attendance.reposi.LocationReposi;
import org.springframework.stereotype.Service;
import com.szkj.common.Page;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 考勤地点信息表(YthAttendLocation)表服务实现类
 *
 * @author makejava
 * @since 2022-04-26 10:45:49
 */
@Service
public class LocationService {
    @Autowired
    private LocationReposi reposi;

    public Page<Location> list(Integer pageSize, Integer pageNo) {
        Page<Location> p = new Page<>();
        p.setPagenum(pageNo);
        p.setPagerows(pageSize);
        p.setData(reposi.list((pageNo - 1) * pageSize, pageSize));
        p.setTotalrows(reposi.count(""));
        p.setRows(p.getData().size());
        return p;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param locId 主键
     * @return 实例对象
     */
    public Location queryById(Integer locId) {
        return reposi.findById(locId).orElse(null);
    }

    /**
     * 保存数据，有ID修改，无ID新增
     *
     * @param ythAttendLocation 实例对象
     * @return 实例对象
     */
    public void save(Location ythAttendLocation) {
        reposi.save(ythAttendLocation);
    }

    /**
     * 通过主键删除数据
     *
     * @param locId 主键
     * @return 是否成功
     */
    public void deleteById(Integer locId) {
        reposi.deleteById(locId);
    }

    public void delete(String ids) {
        reposi.delete(ids);
    }
}
