package com.szkj.attendance.service;

import com.szkj.attendance.entity.History;
import com.szkj.attendance.reposi.HistoryReposi;
import com.szkj.common.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 考勤历史记录(YthAttendHis)表服务实现类
 *
 * @author makejava
 * @since 2022-04-27 10:26:46
 */
@Service
public class HistoryService {
    @Autowired
    private HistoryReposi reposi;

    public Page<History> list(Integer pageSize, Integer pageNo) {
        Page<History> p = new Page<>();
        p.setPagenum(pageNo);
        p.setPagerows(pageSize);
        p.setData(reposi.list((pageNo - 1) * pageSize, pageSize));
        p.setTotalrows(reposi.count(""));
        p.setRows(p.getData().size());
        return p;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param attHisId 主键
     * @return 实例对象
     */
    public History queryById(Integer attHisId) {
        return reposi.findById(attHisId).orElse(null);
    }

    /**
     * 保存数据，有ID修改，无ID新增
     *
     * @param ythAttendHis 实例对象
     */
    public void save(History ythAttendHis) {
        reposi.save(ythAttendHis);
    }

    /**
     * 通过主键删除数据
     *
     * @param attHisId 主键
     */
    public void deleteById(Integer attHisId) {
        reposi.deleteById(attHisId);
    }

    public void delete(String ids) {
        reposi.delete(ids);
    }
}
