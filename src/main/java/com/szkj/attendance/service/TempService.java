package com.szkj.attendance.service;

import com.szkj.attendance.Util.AttendRedisUtil;
import com.szkj.attendance.entity.Temp;
import com.szkj.attendance.entity.TemplExcept;
import com.szkj.attendance.reposi.TempReposi;
import com.szkj.attendance.reposi.TemplExceptReposi;
import com.szkj.common.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 考勤模板主信息表(YthAttendTemp)表服务实现类
 *
 * @author makejava
 * @since 2022-04-26 11:30:44
 */
@Slf4j
@Service
public class TempService {
    @Autowired
    private TempReposi reposi;
    @Autowired
    private TemplExceptReposi exceptReposi;
    @Autowired
    private AttendRedisUtil redisUtil;

    public Page<Temp> list(Integer pageSize, Integer pageNo) {
        Page<Temp> p = new Page<>();
        p.setPagenum(pageNo);
        p.setPagerows(pageSize);
        p.setData(reposi.list((pageNo - 1) * pageSize, pageSize));
        p.setTotalrows(reposi.count(""));
        p.setRows(p.getData().size());
        return p;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param tempId 主键
     * @return 实例对象
     */
    public Temp queryById(Integer tempId, boolean withChild) {
        if (!withChild) return reposi.findById(tempId).orElse(null);
        else {
            Optional<Temp> o = reposi.findById(tempId);
            if (o.isPresent()) {
                Temp t = o.get();
                t.setExcepts(exceptReposi.getByTemp(tempId, System.currentTimeMillis()));
                return t;
            } else return null;
        }
    }


    /**
     * 保存数据，有ID修改，无ID新增
     *
     * @param ythAttendTemp 实例对象
     * @return 实例对象
     */
    public void save(Temp ythAttendTemp) {
        //保存模板，获得更新后的模板，如果新增，就有了ID
        Temp temp = reposi.save(ythAttendTemp);
        List<TemplExcept> l = ythAttendTemp.getExcepts();
        int tempId = temp.getTempId();
        List<TemplExcept> adds = l.stream().filter(e -> {
            //顺便给模板日期设置模板关联
            e.setTempId(tempId);
            return e.getModuleExtId() == null && e.getExceptDate() > System.currentTimeMillis();
        }).collect(Collectors.toList());
        exceptReposi.saveAll(adds);
        List<TemplExcept> dels = l.stream().filter(e -> e.getDateType().equals("D")).collect(Collectors.toList());
        exceptReposi.deleteInBatch(dels);

        temp = queryById(ythAttendTemp.getTempId(), true);
        redisUtil.setRedis(AttendRedisUtil.TypeTemp, temp.getTempId(), temp);
    }

    /**
     * 通过主键删除数据
     *
     * @param tempId 主键
     * @return 是否成功
     */
    public void deleteById(Integer tempId) {
        reposi.deleteById(tempId);
        exceptReposi.delByTemp(tempId);
    }

    public void delete(String ids) {
        reposi.delete(ids);
        String[] ss = ids.split(",");
        redisUtil.delRedis(AttendRedisUtil.TypeTemp, ss);
        exceptReposi.delByTemp(ids);
    }
}
