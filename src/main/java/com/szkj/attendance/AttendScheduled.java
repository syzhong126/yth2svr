package com.szkj.attendance;

import cn.hutool.core.util.StrUtil;
import com.szkj.attendance.Util.DateUtil;
import com.szkj.attendance.Util.NoticeUtil;
import com.szkj.attendance.entity.Group;
import com.szkj.attendance.entity.Temp;
import com.szkj.attendance.service.AttendService;
import com.szkj.attendance.service.GroupService;
import com.szkj.attendance.service.TempService;
import com.szkj.common.Page;
import com.szkj.noticepush.entity.YthNoticePush;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


@Slf4j
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class AttendScheduled {
    @Autowired
    private AttendService service;
    @Autowired
    private GroupService groupService;
    @Autowired
    private TempService tempService;
    @Autowired
    private NoticeUtil noticeUtil;
    @Resource
    private AttendService attendService;

    //每周一10点
    @Scheduled(cron = "0 0 10 ? * 1")
    //@Scheduled(cron = "0 13 * * * ?")
    public void week() {
        Calendar c = Calendar.getInstance();
        Long start = c.getTimeInMillis();
        c.add(Calendar.DAY_OF_MONTH, -7);
        Long end = c.getTimeInMillis();
        statistic(start, end, 0);
    }

    //每月1号10点
    @Scheduled(cron = "0 0 10 1 * ?")
    public void month() {
        Calendar c = Calendar.getInstance();
        Long start = c.getTimeInMillis();
        c.add(Calendar.MONTH, -1);
        Long end = c.getTimeInMillis();
        statistic(start, end, 1);
    }

    //每小时
    //@Scheduled(cron = "0 0 6 ? * 1,2,3,4,5")
    @Scheduled(cron = "0 5 * * * ?")
    public void autoGenerate() {
        log.info("考勤任务");
        Page<Group> page = generate(1);
        if (page.getPageSum() > 1) for (int i = 2; i < page.getPageSum(); i++) generate(i);
    }

    /**
     * 考勤开始前1小时给所有需要考勤的人员生成考勤记录原始数据
     *
     * @param pageNo 考勤组分页
     * @return 考勤组分页记录
     */
    Page<Group> generate(int pageNo) {
        Map<String, String> map = new HashMap<>();
        map.put("LATE", "迟到");
        map.put("EARLY", "早退");
        map.put("MISS", "缺卡");
        map.put("ABSENT", "旷工");
        map.put("LATE_EARLY", "迟到又早退");
        map.put("LATE_MISS", "迟到又缺卡");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        List<YthNoticePush> list = new ArrayList<>();
        Page<Group> page = groupService.list(null, "Y", null, 100, pageNo);
        if (page.getData() != null)
            page.getData().forEach(g -> {
                Temp temp = tempService.queryById(g.getTempId(), true);
                if (temp == null) return;//考勤模板异常
                long time = temp.getOnTime() - System.currentTimeMillis();
                //>0是开始前，<1000 * 60 * 60是一个小时内
                if (time > 0 && time < 3600000) {
                    // 给今天要考勤的初始化数据
                    attendService.createHistory(temp, g.getGroupId());
                    // 给昨天考勤异常者发异常消息
                    Calendar today = DateUtil.today();
                    today.add(Calendar.DAY_OF_MONTH, -1);
                    attendService.getUnNormal(g.getGroupId(), today.getTimeInMillis()).forEach(h ->
                            list.add(noticeUtil.getAttendNotice(null, "考勤异常",
                                    StrUtil.format("您{}考勤出现异常:{}", sdf.format(today.getTimeInMillis()), map.get(h.getFinalStatus())),
                                    h.getPersonId(), "atd-rmd-ex")));
                }
            });
        //集中更新
        noticeUtil.save(list);
        return page;
    }

    /**
     * 生成考勤周/月报
     *
     * @param start 开始时间
     * @param end   结束时间
     * @param type  类型 0周报，1月报
     */
    void statistic(Long start, Long end, int type) {
        log.info("考勤{}报开始分发", type == 0 ? "周" : "月");
        AtomicInteger count = new AtomicInteger();
        Page<Group> page = sendNotice(start, end, type, 1, count);
        if (page.getPageSum() > 1) for (int i = 2; i < page.getPageSum(); i++)
            sendNotice(start, end, type, i, count);
        log.info("考勤{}报分发完毕，一共分发{}份。", type == 0 ? "周" : "月", count.get());
        //清除上月Redis
        if (type == 1) attendService.refreshRedis();
    }

    /**
     * 给用户发统计周报
     *
     * @param start  开始日期
     * @param end    结束日期
     * @param type   类型，0周报，1月报
     * @param pageNo 页码
     * @param count  计数器，计算发了多少条
     */
    Page<Group> sendNotice(Long start, Long end, int type, int pageNo, AtomicInteger count) {
        Page<Group> page = groupService.list(null, "Y", null, 100, pageNo);
        List<YthNoticePush> list = new ArrayList<>();
        if (page.getData() != null) page.getData().forEach(g -> {
            SimpleDateFormat dateFmt = new SimpleDateFormat("yyyy年MM月dd日");
            SimpleDateFormat monthFmt = new SimpleDateFormat("yyyy年MM月");
            service.statisticPersonal(start, end, g.getGroupId(), null, null).forEach(map -> {
                int sum = Integer.parseInt(map.get("sum").toString());
                //没有打卡的不管
                if (sum == 0) return;
                int id = (Integer) map.get("id");
                String name = (String) map.get("name");
                //Integer normal = (Integer) map.get("normal");
                //Integer deal = (Integer) map.get("deal");
                //Integer out = (Integer) map.get("out");
                int late = Integer.parseInt(map.get("late").toString());
                int absent = Integer.parseInt(map.get("absent").toString());
                int early = Integer.parseInt(map.get("early").toString());
                int miss = Integer.parseInt(map.get("miss").toString());
                String title = StrUtil.format("您上{}考勤情况", type == 0 ? "周" : "月");
                String content = StrUtil.format("{},您好：<br>您的{}的考勤统计已生成：<br>打卡：{}次<br>迟到：{}次<br>早退：{}次<br>缺卡：{}次<br>旷工：{}次<br>以上数据来自城发考勤系统，最终结果请以本公司制度为准。",
                        name, type == 0 ? dateFmt.format(start) + "-" + dateFmt.format(end) : monthFmt.format(start)
                        , sum, late, early, miss, absent);
                count.getAndIncrement();
                list.add(noticeUtil.getAttendNotice(null, title, content, id, type == 0 ? "atd-rpt-week" : "atd-rpt-month"));
            });
        });
        //集中更新
        noticeUtil.save(list);
        return page;
    }

}