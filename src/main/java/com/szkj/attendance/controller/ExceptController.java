package com.szkj.attendance.controller;


import cn.hutool.core.util.StrUtil;
import com.szkj.attendance.Util.NoticeUtil;
import com.szkj.attendance.entity.Except;
import com.szkj.attendance.entity.History;
import com.szkj.attendance.service.ExceptService;
import com.szkj.attendance.service.HistoryService;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;

/**
 * 异常处理记录控制层
 *
 * @author makejava
 * @since 2022-04-26 11:28:37
 */
@Slf4j
@RestController
@RequestMapping("/attendance/except")
public class ExceptController {
    @Autowired
    private ExceptService service;
    @Autowired
    private HistoryService hisService;
    @Autowired
    private NoticeUtil noticeUtil;

    /**
     * 获取异常处理记录列表(分页)
     */
    @GetMapping("/list")
    public String list(@RequestParam(defaultValue = "20") Integer pageSize,
                       @RequestParam(defaultValue = "1") Integer pageNo) {
        return RespUtil.success("成功", service.list(pageSize, pageNo));
    }

    /**
     * 添加异常处理记录
     */
    @PostMapping("/deal")
    public String add(@RequestBody Except ythAttendExcept, @ModelAttribute("authUserInfo") AuthUserInfoModel user) {
        History his = hisService.queryById(ythAttendExcept.getAttHisId());
        ythAttendExcept.setExpId(null)
                .setPersonId(his.getPersonId())
                .setStatusPre(his.getFinalStatus())
                .setStatusCur(History.Type_Normal_Deal)
                .setChgTime(System.currentTimeMillis())
                .setCreateBy(user.getAccount())
                .setCreateTime(System.currentTimeMillis())
                .setUpdateBy(null)
                .setUpdateTime(null);
        service.save(ythAttendExcept);
        his.setFinalStatus(History.Type_Normal_Deal);
        hisService.save(his);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        noticeUtil.save(noticeUtil.getAttendNotice(user.getAccount(), sdf.format(his.getClockDate()) + "异常处理",
                StrUtil.format("您{}的打卡异常，经由{}于{}处理，恢复正常。",
                        sdf.format(his.getClockDate()),
                        user.getUserName(),
                        sdf.format(ythAttendExcept.getChgTime())),
                his.getPersonId(), "atd-rmd-out-deal"));
        return RespUtil.success("成功", "");
    }
}

