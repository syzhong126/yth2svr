package com.szkj.attendance.controller;


import com.szkj.attendance.entity.Temp;
import com.szkj.attendance.entity.TemplExcept;
import com.szkj.attendance.service.TempService;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.Result;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 考勤模板主信息表控制层
 *
 * @author makejava
 * @since 2022-04-26 11:30:42
 */
@Slf4j
@RestController
@RequestMapping("/attendance/temp")
public class TempController {
    @Autowired
    private TempService service;

    /**
     * 获取考勤模板主信息表列表(分页)
     */
    @GetMapping("/list")
    public Result<?> list(@RequestParam(defaultValue = "20") Integer pageSize,
                          @RequestParam(defaultValue = "1") Integer pageNo) {
        return Result.OK("成功", service.list(pageSize, pageNo));
    }

    /**
     * 获取考勤模板主信息表
     */
    @GetMapping("/detail")
    public String get(Integer id) {
        return RespUtil.success("成功", service.queryById(id, true));
    }

    /**
     * 添加考勤模板主信息表
     */
    @PostMapping("/add")
    public String add(@RequestBody Temp ythAttendTemp, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        ythAttendTemp.setCreateBy(userinfo.getAccount())
                .setUnitId(userinfo.getOrgId())
                .setCreateTime(System.currentTimeMillis())
                .setUpdateBy(null)
                .setUpdateTime(null);
        service.save(ythAttendTemp);
        return RespUtil.success("成功", "");
    }


    /**
     * 修改考勤模板主信息表
     */
    @PutMapping("/edit")
    public String update(@RequestBody Temp ythAttendTemp, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        Temp entity = service.queryById(ythAttendTemp.getTempId(), false);
        if (entity == null) return RespUtil.error("没有该条记录");
        if (entity.getUnitId() != userinfo.getOrgId())
            return RespUtil.error("不能修改非本公司外的考勤模板");
        List<TemplExcept> l = ythAttendTemp.getExcepts();
        BeanUtils.copyProperties(ythAttendTemp, entity, "createBy", "createTime");
        entity.setExcepts(l).setUnitId(userinfo.getOrgId())
                .setUpdateBy(userinfo.getAccount())
                .setUpdateTime(System.currentTimeMillis());
        service.save(entity);
        return RespUtil.success("成功", "");
    }

    /**
     * 删除考勤模板主信息表
     */
    @DeleteMapping("/delete")
    public String delete(String ids) {
        service.delete(ids);
        return RespUtil.success("成功", "");
    }

    /**
     * 删除考勤模板主信息表
     */
    @DeleteMapping("/deleteById")
    public String deleteById(Integer id) {
        service.deleteById(id);
        return RespUtil.success("成功", "");
    }

}

