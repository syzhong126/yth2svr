package com.szkj.attendance.controller;


import com.szkj.attendance.Util.BeanCopyUtil;
import com.szkj.attendance.entity.Group;
import com.szkj.attendance.service.GroupService;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 考勤组主信息表控制层
 *
 * @author makejava
 * @since 2022-04-25 15:53:46
 */
@Slf4j
@RestController
@RequestMapping("/attendance/group")
public class GroupController {
    @Autowired
    private GroupService service;

    /**
     * 获取考勤组主信息表列表(分页)
     */
    @GetMapping("/list")
    public String list(@RequestParam(required = false) Integer unitId,
                       @RequestParam(required = false) String status,
                       @RequestParam(required = false) String keyword,
                       @RequestParam(defaultValue = "20") Integer pageSize,
                       @RequestParam(defaultValue = "1") Integer pageNo) {
        return RespUtil.success("成功", service.list(unitId, status, keyword, pageSize, pageNo));
    }


    /**
     * 启用禁用考勤组
     */
    @GetMapping("/enable")
    public String enable(@RequestParam(defaultValue = "1") Integer groupId,
                         @RequestParam(defaultValue = "Y") String enable) {
        service.enable(groupId, enable);
        return RespUtil.success("成功", "");
    }

    /**
     * 获取考勤组主信息表
     */
    @GetMapping("/detail")
    public String get(Integer id) {
        return RespUtil.success("成功", service.queryById(id));
    }

    /**
     * 添加考勤组主信息表
     */
    @PostMapping("/add")
    public String add(@RequestBody Group ythAttendGroup, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        ythAttendGroup.setGroupId(null)
                .setUnitId(userinfo.getOrgId())
                .setCreateBy(userinfo.getAccount())
                .setCreateTime(System.currentTimeMillis())
                .setUpdateBy(null)
                .setUpdateTime(null);
        service.save(ythAttendGroup);
        return RespUtil.success("成功", "");
    }

    /**
     * 修改考勤组主信息表
     */
    @PutMapping("/edit")
    public String update(@RequestBody Group ythAttendGroup, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        Group entity = service.queryById(ythAttendGroup.getGroupId());
        if (entity == null) return RespUtil.error("没有该条记录");
        if (entity.getUnitId() != userinfo.getOrgId())
            return RespUtil.error("不能修改非本公司外的考勤组");
        BeanUtils.copyProperties(ythAttendGroup, entity, BeanCopyUtil.getNullPropertyNames(ythAttendGroup));
        entity.setUnitId(userinfo.getOrgId())
                .setUpdateBy(userinfo.getAccount())
                .setUpdateTime(System.currentTimeMillis());
        service.save(entity);
        return RespUtil.success("成功", "");
    }

    /**
     * 删除考勤组主信息表
     */
    @DeleteMapping("/delete")
    public String delete(String ids) {
        service.delete(ids);
        return RespUtil.success("成功", "");
    }

    /**
     * 删除考勤组主信息表
     */
    @PostMapping("/deleteById")
    public String deleteById(Integer id) {
        service.deleteById(id);
        return RespUtil.success("成功", "");
    }

}

