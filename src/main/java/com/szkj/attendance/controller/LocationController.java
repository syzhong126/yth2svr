package com.szkj.attendance.controller;


import com.szkj.attendance.entity.Location;
import com.szkj.attendance.service.LocationService;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 考勤地点信息表控制层
 *
 * @author makejava
 * @since 2022-04-26 11:28:25
 */
@Slf4j
@RestController
@RequestMapping("/attendance/location")
public class LocationController {
    @Autowired
    private LocationService service;

    /**
     * 获取考勤地点信息表列表(分页)
     */
    @GetMapping("/list")
    public String list(@RequestParam(defaultValue = "20") Integer pageSize,
                       @RequestParam(defaultValue = "1") Integer pageNo) {
        return RespUtil.success("成功", service.list(pageSize, pageNo));
    }

    /**
     * 获取考勤地点信息表
     */
    @GetMapping("/detail")
    public String get(Integer id) {
        return RespUtil.success("成功", service.queryById(id));
    }


    /**
     * 添加考勤地点信息表
     */
    @PostMapping("/add")
    public String add(@RequestBody Location ythAttendLocation,
                      @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        ythAttendLocation.setLocId(null)
                .setUnitId(userinfo.getOrgId())
                .setCreateBy(userinfo.getAccount())
                .setCreateTime(System.currentTimeMillis())
                .setUpdateBy(null)
                .setUpdateTime(null);
        service.save(ythAttendLocation);
        return RespUtil.success("成功", "");
    }


    /**
     * 修改考勤地点信息表
     */
    @PutMapping("/edit")
    public String update(@RequestBody Location ythAttendLocation,
                         @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        Location entity = service.queryById(ythAttendLocation.getLocId());
        if (entity == null) return RespUtil.error("没有该条记录");
        if (entity.getUnitId() != userinfo.getOrgId())
            return RespUtil.error("不能修改非本公司外的考勤地点");
        BeanUtils.copyProperties(ythAttendLocation, entity, "createBy", "createTime");
        entity.setUnitId(userinfo.getOrgId())
                .setUpdateBy(userinfo.getAccount())
                .setUpdateTime(System.currentTimeMillis());
        service.save(entity);
        return RespUtil.success("成功", "");
    }

    /**
     * 删除考勤地点信息表
     */
    @DeleteMapping("/delete")
    public String delete(String ids) {
        service.delete(ids);
        return RespUtil.success("成功", "");
    }

    /**
     * 删除考勤地点信息表
     */
    @DeleteMapping("/deleteById")
    public String deleteById(Integer id) {
        service.deleteById(id);
        return RespUtil.success("成功", "");
    }

}

