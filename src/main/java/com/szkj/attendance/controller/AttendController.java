package com.szkj.attendance.controller;

import com.szkj.attendance.entity.Attend;
import com.szkj.attendance.entity.AuthDept;
import com.szkj.attendance.entity.AuthPerson;
import com.szkj.attendance.service.AttendService;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/attendance")
public class AttendController {
    @Autowired
    private AttendService service;

    /**
     * 打卡申请，用户
     *
     * @param lng 经度
     * @param lat 纬度
     * @return 申请结果
     */
    @GetMapping("/apply")
    public String apply(@RequestParam Double lng,
                        @RequestParam Double lat,
                        @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        Attend attend = service.apply(userinfo.getUserId(), lng, lat, userinfo.getToken());
        return RespUtil.success("成功", attend);
    }

    /**
     * 打卡
     *
     * @return 打卡结果
     */
    @GetMapping("/do")
    public String attend(@ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        try {
            service.attend(userinfo.getToken());
        } catch (Exception e) {
            return RespUtil.raw(2501, e.getMessage(), null);
        }
        return RespUtil.success("打卡成功", "");
    }

    /**
     * 补卡，每月两次补卡
     *
     * @param date 补卡日期
     */

    @GetMapping("/rectify")
    public String rectify(@RequestParam String date,
                          @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        try {
            service.rectify(userinfo.getUserId(), date);
        } catch (Exception e) {
            return RespUtil.raw(2502, e.getMessage(), null);
        }
        return RespUtil.success("补卡成功", "");
    }

    /**
     * 部门可用的所有考勤组，携带部门权限
     *
     * @param orgId 部门Id
     */
    @GetMapping("/auth/corporate")
    public String authCorporate(@RequestParam Integer orgId) {
        return RespUtil.success("成功", service.getAuthCorporate(orgId));
    }

    /**
     * 获取部门下的人员列表，携带所属考勤组信息
     *
     * @param orgId 部门Id
     */
    @GetMapping("/auth/personal")
    public String authPersonal(@RequestParam Integer orgId) {
        return RespUtil.success("成功", service.getAuthPersonal(orgId));
    }

    /**
     * 设置部门考勤权限
     *
     * @param auths 权限列表
     */
    @PostMapping("/auth/set/corporate")
    public String setAuthCorporate(@RequestBody List<AuthDept> auths) {
        service.setAuthCorporate(auths);
        return RespUtil.success("成功", "");
    }

    /**
     * 设置个人考勤权限
     *
     * @param auths 权限列表
     */
    @PostMapping("/auth/set/personal")
    public String setAuthPersonal(@RequestBody List<AuthPerson> auths) {
        service.setAuthPersonal(auths);
        return RespUtil.success("成功", "");
    }

    /**
     * 个人考勤统计
     *
     * @param start   开始时间
     * @param end     结束时间
     * @param month   月份，与起止时间冲突
     * @param groupId 考勤组
     * @param deptId  部门
     * @param userId  用户
     */
    @GetMapping("/statistic/personal")
    public String statisticPersonal(@RequestParam(required = false) String start,
                                    @RequestParam(required = false) String end,
                                    @RequestParam(required = false) String month,
                                    @RequestParam Integer groupId,
                                    @RequestParam Integer deptId,
                                    @RequestParam(required = false) Integer userId) {
        return getStatistics(start, end, month, groupId, deptId, userId, 0);
    }

    /**
     * 部门考勤统计
     *
     * @param start   开始时间
     * @param end     结束时间
     * @param month   月份，与起止时间冲突
     * @param groupId 考勤组
     * @param orgId   公司，部门
     */
    @GetMapping("/statistic/corporate")
    public String statisticCorporate(@RequestParam(required = false) String start,
                                     @RequestParam(required = false) String end,
                                     @RequestParam(required = false) String month,
                                     @RequestParam Integer groupId,
                                     @RequestParam Integer orgId) {
        return getStatistics(start, end, month, groupId, orgId, null, 1);
    }

    /**
     * 考勤统计
     *
     * @param start   开始时间
     * @param end     结束时间
     * @param month   月份，时间优先处理月份，月份有数据则不考虑起止时间
     * @param groupId 考勤组
     * @param orgId   部门
     * @param userId  用户
     * @param type    类型，1单位，0个人
     * @return 考勤结果
     */
    private String getStatistics(String start, String end, String month, Integer groupId, Integer orgId, Integer userId, int type) {
        Long startTime = null, endTime = null;
        SimpleDateFormat monthFmt = new SimpleDateFormat("yyyy-MM");
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(monthFmt.parse(month));
            startTime = c.getTimeInMillis();
            c.add(Calendar.MONTH, 1);
            endTime = c.getTimeInMillis();
        } catch (Exception e) {
            SimpleDateFormat dateFmt = new SimpleDateFormat("yyyy-MM-dd");
            try {
                startTime = dateFmt.parse(start).getTime();
            } catch (Exception ignored) {
            }
            try {
                endTime = dateFmt.parse(end).getTime();
            } catch (Exception ignored) {
            }
        }
        if (type == 1) return RespUtil.success("成功", service.statisticCorporate(startTime, endTime, groupId, orgId));
        else return RespUtil.success("成功", service.statisticPersonal(startTime, endTime, groupId, orgId, userId));
    }
}
