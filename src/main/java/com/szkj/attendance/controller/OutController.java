package com.szkj.attendance.controller;


import cn.hutool.core.util.StrUtil;
import com.szkj.attendance.Util.BeanCopyUtil;
import com.szkj.attendance.Util.NoticeUtil;
import com.szkj.attendance.entity.History;
import com.szkj.attendance.entity.Out;
import com.szkj.attendance.service.HistoryService;
import com.szkj.attendance.service.OutService;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;

/**
 * 外出审批记录控制层
 *
 * @author makejava
 * @since 2022-04-26 11:32:03
 */
@Slf4j
@RestController
@RequestMapping("/attendance/out")
public class OutController {
    @Autowired
    private OutService service;
    @Autowired
    private HistoryService hisService;
    @Autowired
    private NoticeUtil noticeUtil;

    /**
     * 获取外出审批记录列表(分页)
     */
    @GetMapping("/list")
    public String list(@RequestParam(defaultValue = "20") Integer pageSize,
                       @RequestParam(defaultValue = "1") Integer pageNo) {
        return RespUtil.success("成功", service.list(pageSize, pageNo));
    }

    /**
     * 添加外出审批记录
     */
    @PostMapping("/apply")
    public String add(@RequestBody Out ythAttendOut, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        ythAttendOut.setPersonId(userinfo.getUserId())
                .setPersonName(userinfo.getUserName())
                .setCreateBy(userinfo.getAccount())
                .setCreateTime(System.currentTimeMillis())
                .setReadStatus("N")
                .setApproveStatus("N")
                .setUpdateBy(null)
                .setUpdateTime(null);
        service.save(ythAttendOut);
        return RespUtil.success("成功", "");
    }

    /**
     * 外出审批操作
     */
    @PutMapping("/approve")
    public String approve(@RequestBody Out ythAttendOut, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        Out entity = service.queryById(ythAttendOut.getOutId());
        if (entity == null) return RespUtil.error("没有该条记录");
        //log.info("approve:{}", ythAttendOut.getApproveStatus());
        BeanUtils.copyProperties(ythAttendOut, entity, BeanCopyUtil.getNullPropertyNames(ythAttendOut));
        entity.setApproveId(userinfo.getUserId())
                .setApproveName(userinfo.getUserName())
                .setApproveTime(System.currentTimeMillis())
                .setUpdateBy(userinfo.getAccount())
                .setUpdateTime(System.currentTimeMillis());
        service.save(entity);
        History his = hisService.queryById(entity.getAttHisId());
        his.setFinalStatus(History.Type_Normal_Out);
        hisService.save(his);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        noticeUtil.save(noticeUtil.getAttendNotice(userinfo.getAccount(), sdf.format(ythAttendOut.getCreateTime()) + "外出申请结果",
                StrUtil.format("您{}的外出申请，经由{}于{}审批，结果{}。",
                        sdf.format(ythAttendOut.getCreateTime()),
                        ythAttendOut.getApproveName(),
                        sdf.format(ythAttendOut.getApproveTime()),
                        (ythAttendOut.getApproveStatus().equals("P") ? "通过" : "不通过")),
                ythAttendOut.getPersonId(), "atd-rmd-out-deal"));
        return RespUtil.success("成功", "");
    }

    /**
     * 外出申请已读
     *
     * @param outId 外出记录id
     */
    @GetMapping("/read")
    public String Read(@RequestParam(defaultValue = "20") Integer outId) {
        service.read(outId);
        return RespUtil.success("成功", "");
    }

}

