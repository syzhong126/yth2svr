package com.szkj.attendance.reposi;


import com.szkj.attendance.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 考勤地点信息表持久层
 *
 * @author makejava
 * @since 2022-04-26 10:45:48
 */
public interface LocationReposi extends JpaRepository<Location, Integer> {
    @Transactional
    @Modifying
    @Query(value = "delete from yth_attend_location where loc_id in (?1)", nativeQuery = true)
    int delete(String ids);

    @Query(value = "select * from yth_attend_location a limit ?2,?3", nativeQuery = true)
    List<Location> list(int start, int limit);

    @Query(value = "select count(1) from yth_attend_location a ", nativeQuery = true)
    int count(@Param("where") String where);

    @Query(value = "select * from yth_attend_location a where a.loc_id in (?1)", nativeQuery = true)
    List<Location> getByIds(@Param("ids") String ids);
}

