package com.szkj.attendance.reposi;


import com.szkj.attendance.entity.Except;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 异常处理记录持久层
 *
 * @author makejava
 * @since 2022-04-26 10:43:53
 */
public interface ExceptReposi extends JpaRepository<Except, Integer> {
    @Transactional
    @Modifying
    @Query(value = "delete from yth_attend_except where exp_id in (?1)", nativeQuery = true)
    int delete(String ids);

    @Query(value = "select * from yth_attend_except a limit ?2,?3", nativeQuery = true)
    List<Except> list(int start, int limit);

    @Query(value = "select count(1) from yth_attend_except a ", nativeQuery = true)
    int count(@Param("where") String where);
}

