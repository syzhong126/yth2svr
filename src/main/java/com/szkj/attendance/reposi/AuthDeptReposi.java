package com.szkj.attendance.reposi;


import com.szkj.attendance.entity.AuthDept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

/**
 * 部门考勤关系持久层
 *
 * @author makejava
 * @since 2022-04-27 11:30:03
 */
public interface AuthDeptReposi extends JpaRepository<AuthDept, Integer> {

    /**
     * 获取全部考勤组，携带部门权限
     *
     * @param deptId 部门ID
     * @return Map
     */
    @Query(value = "select g.group_id groupId,g.`name` groupName,d.auth_id authId,IFNULL(d.is_defalut,false) isDefault,!ISNULL(d.auth_id) hasAuth from yth_attend_group g " +
            "left join yth_attend_auth_dept d on g.group_id=d.group_id and d.unit_id=?1 " +
            "left join yth_organazition o on o.org_id=d.unit_id and (o.parent_id=g.unit_id or g.unit_id=45)", nativeQuery = true)
    List<Map<String, Object>> getDeptGroups(@Param("deptId") Integer deptId);
}

