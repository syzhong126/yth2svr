package com.szkj.attendance.reposi;


import com.szkj.attendance.entity.TemplExcept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 考勤模板-调班时间，每年年底的时候统一添加持久层
 *
 * @author makejava
 * @since 2022-04-26 11:32:23
 */
public interface TemplExceptReposi extends JpaRepository<TemplExcept, Integer> {
    @Transactional
    @Modifying
    @Query(value = "delete from yth_attend_templ_except where module_ext_id in (?1)", nativeQuery = true)
    int delete(String ids);

    @Query(value = "select * from yth_attend_templ_except a limit ?2,?3", nativeQuery = true)
    List<TemplExcept> list(int start, int limit);

    @Query(value = "select count(1) from yth_attend_templ_except a ", nativeQuery = true)
    int count(@Param("where") String where);

    @Query(value = "select * from yth_attend_templ_except t where t.except_date>=?2 and t.temp_id=?1 order by t.except_date asc", nativeQuery = true)
    List<TemplExcept> getByTemp(Integer tempId, long now);

    @Transactional
    @Modifying
    @Query(value = "delete from yth_attend_templ_except where temp_id in (?1)", nativeQuery = true)
    void delByTemp(Object tempIds);


    @Query(value = "select * from yth_attend_templ_except t where t.except_date=?2 and t.temp_id=?1", nativeQuery = true)
    Optional<TemplExcept> getExcept(Integer tempId, long now);
}

