package com.szkj.attendance.reposi;


import com.szkj.attendance.entity.Out;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 外出审批记录持久层
 *
 * @author makejava
 * @since 2022-04-26 11:32:04
 */
public interface OutReposi extends JpaRepository<Out, Integer> {
    @Transactional
    @Modifying
    @Query(value = "delete from yth_attend_out where out_id in (?1)", nativeQuery = true)
    int delete(String ids);

    @Query(value = "select * from yth_attend_out a limit ?2,?3", nativeQuery = true)
    List<Out> list(int start, int limit);

    @Query(value = "select count(1) from yth_attend_out a ", nativeQuery = true)
    int count(@Param("where") String where);

    @Transactional
    @Modifying
    @Query(value = "update yth_attend_out o set o.read_status='Y' where o.out_id=?1", nativeQuery = true)
    int read(Integer outId);
}

