package com.szkj.attendance.reposi;


import com.szkj.attendance.entity.AuthPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

/**
 * 人员考勤关系持久层
 *
 * @author makejava
 * @since 2022-04-27 16:57:47
 */
public interface AuthPersonReposi extends JpaRepository<AuthPerson, Integer> {

    @Query(value = "select r.user_id userId,u.user_name userName,g.group_id groupId,g.group_name groupName " +
            "from (yth_user_org r ,yth_user u) " +
            "left join yth_attend_auth_person p on p.user_id=r.user_id " +
            "left join yth_group g on p.group_id=g.group_id " +
            "where u.user_id=r.user_id and (r.org_id=?1 or r.org_id2=?1)", nativeQuery = true)
    List<Map<String, Object>> getAll(Integer orgId);


    @Query(value = "select * from (" +
            "select u.user_id,IFNULL(" +
            "(select ap.group_id from yth_attend_auth_person ap where ap.user_id=u.user_id),IFNULL(" +
            "(select ad.group_id from yth_attend_auth_dept ad,yth_user_org r where ad.unit_id=r.org_id2 and r.user_id=u.user_id)," +
            "(select ad.group_id from yth_attend_auth_dept ad,yth_user_org r where ad.unit_id=r.org_id and r.user_id=u.user_id)" +
            ")) group_id from yth_user u) t where t.group_id=?1", nativeQuery = true)
    List<AuthPerson> getGroupUsers(@Param("groupId") Integer groupId);
}

