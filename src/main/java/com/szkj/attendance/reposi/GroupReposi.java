package com.szkj.attendance.reposi;


import com.szkj.attendance.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 考勤组主信息表持久层
 *
 * @author makejava
 * @since 2022-04-25 15:13:14
 */
public interface GroupReposi extends JpaRepository<Group, Integer> {
    @Transactional
    @Modifying
    @Query(value = "delete from yth_attend_group where group_id in (?1)", nativeQuery = true)
    public int delete(String ids);

    @Query(value = "select * from yth_attend_group a where (a.unit_id=45 or isnull(?1) or ?1=0 or a.unit_id=?1) and (isnull(?2) or a.`status`=?2) " +
            "and (isnull(?3) or ?3='' or a.`name` like CONCAT('%',?3,'%')) limit ?4,?5", nativeQuery = true)
    List<Group> list(@Param("unitId") Integer unitId,
                     @Param("status") String status,
                     @Param("keyword") String keyword,
                     @Param("start") int start,
                     @Param("limit") int limit);

    @Query(value = "select count(1) from yth_attend_group a where  (a.unit_id=45 or isnull(?1) or ?1=0 or a.unit_id=?1) and (isnull(?2) or a.`status`=?2) " +
            "and (isnull(?3) or ?3='' or a.`name` like CONCAT('%',?3,'%'))", nativeQuery = true)
    int count(@Param("unitId") Integer unitId,
              @Param("status") String status,
              @Param("keyword") String keyword);

    /**
     * 启用/禁用考勤组
     *
     * @param groupId 考勤组ID
     * @param enable  是否可用
     */
    @Transactional
    @Modifying
    @Query(value = "update yth_attend_group t set t.status=?2  where t.group_id=?1", nativeQuery = true)
    void enable(@Param("attendId") Integer groupId, @Param("enable") String enable);

    /**
     * 获取我的部门默认考勤组
     *
     * @param personId 人员Id
     * @return 考勤组
     */
    @Query(value = "select g.* from yth_attend_group g,yth_attend_auth_dept a,yth_user_org r where " +
            "a.is_defalut=true and a.group_id=g.group_id and r.org_id2=a.unit_id and r.user_id=?1", nativeQuery = true)
    Optional<Group> getMyDeptGroups(@Param("personId") Integer personId);

    /**
     * 获取我的考勤组
     *
     * @param personId 人员Id
     * @return 考勤组
     */
    @Query(value = "select g.* from yth_attend_group g,yth_attend_auth_person r where r.group_id=g.group_id and r.user_id=?1", nativeQuery = true)
    Optional<Group> getMyGroups(@Param("personId") Integer personId);

}

