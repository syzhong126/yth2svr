package com.szkj.attendance.reposi;


import com.szkj.attendance.entity.Temp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 考勤模板主信息表持久层
 *
 * @author makejava
 * @since 2022-04-26 11:30:43
 */
public interface TempReposi extends JpaRepository<Temp, Integer> {
    @Transactional
    @Modifying
    @Query(value = "delete from yth_attend_temp where temp_id in (?1)", nativeQuery = true)
    int delete(String ids);

    @Query(value = "select * from yth_attend_temp a limit ?1,?2", nativeQuery = true)
    List<Temp> list(int start, int limit);

    @Query(value = "select count(1) from yth_attend_temp a ", nativeQuery = true)
    int count(@Param("where") String where);

    /**
     * 获取我的考勤组的考勤模板
     *
     * @param personId 人员ID
     * @return 考勤模板
     */
    @Query(value = "select t.* from yth_attend_temp t,yth_attend_group g,yth_attend_auth_person r " +
            "where t.temp_id=g.temp_id and r.group_id=g.group_id and r.user_id=?1", nativeQuery = true)
    Optional<Temp> getMyTemp(@Param("personId") Integer personId);
}

