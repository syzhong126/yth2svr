package com.szkj.attendance.reposi;


import com.szkj.attendance.entity.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 考勤历史记录持久层
 *
 * @author makejava
 * @since 2022-04-27 10:26:45
 */
public interface HistoryReposi extends JpaRepository<History, Integer> {
    @Modifying
    @Query(value = "delete from yth_attend_his where att_his_id in (?1)", nativeQuery = true)
    int delete(String ids);

    @Query(value = "select * from yth_attend_his a limit ?2,?3", nativeQuery = true)
    List<History> list(int start, int limit);

    @Query(value = "select count(1) from yth_attend_his a ", nativeQuery = true)
    int count(@Param("where") String where);

    @Query(value = "select * from yth_attend_his t where t.person_id=?1 and t.clock_date=?2", nativeQuery = true)
    Optional<History> findByCurrent(@Param("personId") Integer personId, @Param("date") Long date);

    @Query(value = "select u.user_id as id,u.user_name as `name`," +
            "(select count(1) from yth_attend_his h where u.user_id=h.person_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2)) as `sum`," +
            "(select count(1) from yth_attend_his h where u.user_id=h.person_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%NORMAL%') as normal," +
            "(select count(1) from yth_attend_his h where u.user_id=h.person_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%LATE%') as late," +
            "(select count(1) from yth_attend_his h where u.user_id=h.person_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%EARLY%') as early," +
            "(select count(1) from yth_attend_his h where u.user_id=h.person_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%MISS%') as miss," +
            "(select count(1) from yth_attend_his h where u.user_id=h.person_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%ABSENT%') as absent," +
            "(select count(1) from yth_attend_his h where u.user_id=h.person_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%OUT%') as `out`," +
            "(select count(1) from yth_attend_his h where u.user_id=h.person_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%DEAL%') as deal " +
            "from yth_user u,yth_user_org r where r.user_id=u.user_id and u.is_del=false and u.enabled=true and (isnull(?4) or r.org_id2=?4) and (isnull(?5) or u.user_id=?5)", nativeQuery = true)
    List<Map<String, Object>> statisticPersonal(Long start, Long end, Integer groupId, Integer orgId, Integer userId);


    @Query(value = "select o.org_name as `name`," +
            "(select count(1) from yth_attend_his h,yth_user_org r where r.user_id=h.person_id and r.org_id2=o.org_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2)) as `sum`," +
            "(select count(1) from yth_attend_his h,yth_user_org r where r.user_id=h.person_id and r.org_id2=o.org_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%NORMAL%') as normal," +
            "(select count(1) from yth_attend_his h,yth_user_org r where r.user_id=h.person_id and r.org_id2=o.org_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%LATE%') as late," +
            "(select count(1) from yth_attend_his h,yth_user_org r where r.user_id=h.person_id and r.org_id2=o.org_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%EARLY%') as early," +
            "(select count(1) from yth_attend_his h,yth_user_org r where r.user_id=h.person_id and r.org_id2=o.org_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%MISS%') as miss," +
            "(select count(1) from yth_attend_his h,yth_user_org r where r.user_id=h.person_id and r.org_id2=o.org_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%ABSENT%') as absent," +
            "(select count(1) from yth_attend_his h,yth_user_org r where r.user_id=h.person_id and r.org_id2=o.org_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%OUT%') as `out`," +
            "(select count(1) from yth_attend_his h,yth_user_org r where r.user_id=h.person_id and r.org_id2=o.org_id and (isnull(?3) or h.group_id=?3) and (isnull(?1) or h.clock_date>=?1) and (isnull(?2) or h.clock_date<?2) and h.final_status like '%DEAL%') as deal " +
            "from yth_organazition o where o.is_del=false and (isnull(?4) or o.parent_id=?4)", nativeQuery = true)
    List<Map<String, Object>> statisticCorporate(Long start, Long end, Integer groupId, Integer orgId);

    @Query(value = "select * from yth_attend_his t where t.final_status not like '%NORMAL%' and t.group_id=?1 and t.clock_date=?2", nativeQuery = true)
    List<History> getUnNormal(int groupId, long date);
}

