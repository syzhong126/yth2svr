package com.szkj.attendance.Util;

import com.szkj.auth.reposi.UserReposi;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.noticepush.reposi.YthNoticepushReposi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoticeUtil {
    @Autowired
    UserReposi userReposi;
    @Autowired
    YthNoticepushReposi noticeReposi;

    /**
     * 生成考勤的消息对象
     *
     * @param account    用户账户
     * @param title      标题
     * @param content    内容
     * @param pushUser   发送目标
     * @param moudleType 消息类别
     * @return YthNoticePush对象
     */
    public YthNoticePush getAttendNotice(String account, String title, String content, Integer pushUser, String moudleType) {
        return getNotice(account, title, content, System.currentTimeMillis(), pushUser, "attendance", moudleType, 2);
    }

    /**
     * 生成会议消息对象
     *
     * @param account    用户账户
     * @param title      标题
     * @param content    内容
     * @param pushUser   发送目标
     * @param moudleType 消息类别
     * @return YthNoticePush对象
     */

    public YthNoticePush getMeetingNotice(String account, String title, String content, Integer pushUser, String moudleType) {
        return getNotice(account, title, content, System.currentTimeMillis(), pushUser, "schedule", moudleType, 3);
    }

    /**
     * 生成预警消息的系统消息对象
     *
     * @param title    标题
     * @param content  内容
     * @param pushUser 发送目标
     * @return YthNoticePush对象
     */
    public YthNoticePush getAlarmNotice(String title, String content, Integer pushUser) {
        return getNotice(null, title, content, System.currentTimeMillis(), pushUser,
                "alarm", pushUser == null ? "alarm-storm" : "alarm-event", 3);
    }

    /**
     * 获取没有推送的系统消息
     *
     * @param userId 用户Id
     * @return 结果
     */
    public List<YthNoticePush> getNoticeNotPush(Integer userId) {
        return noticeReposi.getNotPush(userId);
    }

    /**
     * 生成YthNoticePush对象
     *
     * @param account      用户账户
     * @param title        标题
     * @param content      内容
     * @param pushTime     发送时间
     * @param pushUser     发送目标
     * @param moudleDetail 消息类型
     * @param moudleType   消息类别
     * @param pushType     推送类型
     * @return YthNoticePush对象
     */
    public YthNoticePush getNotice(String account, String title, String content, Long pushTime, Integer pushUser,
                                   String moudleDetail, String moudleType, Integer pushType) {
        YthNoticePush notice = new YthNoticePush()
                .setPushType(pushType == null ? 2 : pushType)   //推送类型，1仅短信 2仅app推送 3,短信+app推送
                .setTitle(title).setContent(content).setPushTime(pushTime)
                .setFromSystem("yth").setMoudleDetail(moudleDetail).setMoudleType(moudleType)
                .setMsgType(pushUser != null ? 1 : 0).setPushUser(pushUser)//推送人
                .setDelFlag(0).setPushStatus(0).setCreateTime(System.currentTimeMillis()); //0：待推送 1：已推送 2:已读
        if (account != null)
            userReposi.getUserByAccount(account).ifPresent(u -> notice.setCreateBy(u.getUserId().toString())
                    .setCreateName(u.getUserName()));
        else notice.setCreateBy("0").setCreateName("系统消息");
        return notice;
    }

    public void save(YthNoticePush notice) {
        noticeReposi.save(notice);
    }

    /**
     * 批量保存
     */
    public void save(List<YthNoticePush> notices) {
        noticeReposi.saveAll(notices);
    }
}
