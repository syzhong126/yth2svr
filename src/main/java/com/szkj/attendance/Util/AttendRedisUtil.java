package com.szkj.attendance.Util;

import cn.hutool.json.JSONUtil;
import com.szkj.attendance.entity.Attend;
import com.szkj.common.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.Optional;

@Slf4j
@Service
public class AttendRedisUtil {
    public static final String TypeGroup = "Attend_Group";
    public static final String TypeTemp = "Attend_Temp";
    /**
     * 考勤例外日期
     */
    public static final String TypeTempExcept = "Attend_Temp_Ex";
    /**
     * 考勤补卡次数
     */
    public static final String TypeRectify = "Attend_Rectify";
    @Value("${authsvc.token.expire.time:86400}")
    private int expireTime;

    public void setRedisAttend(Attend attend, String token) {
        String att = JSONUtil.toJsonStr(attend);
        try (Jedis jedis = RedisUtil.getJedis()) {
            jedis.set(token + "-Attend", att);
            jedis.expire(token + "-Attend", expireTime);
        }
    }

    public Attend getRedisAttend(String token) {
        try (Jedis jedis = RedisUtil.getJedis()) {
            Attend attend = JSONUtil.toBean(jedis.get(token + "-Attend"), Attend.class);
            jedis.del(token + "-Attend");
            return attend;
        }
    }

    public void setRedis(String type, int id, Object value) {
        //log.info("setRedis:{},{}", type, value);
        try (Jedis jedis = RedisUtil.getJedis()) {
            jedis.hset(type, String.valueOf(id), JSONUtil.toJsonStr(value));
        }
    }

    public <T> Optional<T> getRedis(String type, int id, Class<T> clazz) {
        try (Jedis jedis = RedisUtil.getJedis()) {
            String data = jedis.hget(type, String.valueOf(id));
            if (data == null) return Optional.empty();
            //log.info("getRedis:{},{}", type, data);
            return Optional.ofNullable(JSONUtil.toBean(data, clazz));
        }
    }

    public String getRedis(String type, int id) {
        try (Jedis jedis = RedisUtil.getJedis()) {
            //log.info("getRedis:{},{}", type, data);
            return jedis.hget(type, String.valueOf(id));
        }
    }

    public void delRedis(String type, String... ids) {
        try (Jedis jedis = RedisUtil.getJedis()) {
            if (ids.length == 0) jedis.del(type);
            else jedis.hdel(type, ids);
        }
    }
}
