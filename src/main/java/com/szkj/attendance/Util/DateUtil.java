package com.szkj.attendance.Util;

import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;

@Slf4j
public class DateUtil {
    /**
     * 今天纯日期的 Calendar对象
     *
     * @return Calendar对象
     */
    public static Calendar today() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.clear(Calendar.HOUR);
        c.clear(Calendar.MINUTE);
        c.clear(Calendar.SECOND);
        c.clear(Calendar.MILLISECOND);
        return c;
    }
}
