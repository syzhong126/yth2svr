package com.szkj.attendance.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 * 考勤模板主信息表(YthAttendTemp)实体类
 *
 * @author makejava
 * @since 2022-04-26 11:30:43
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_attend_temp")
public class Temp implements Serializable {
    private static final long serialVersionUID = 481616614648173221L;
    /**
     * 模板ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "temp_id", nullable = false)
    private Integer tempId;
    /**
     * 考模板名称
     */
    @Column(name = "temp_name")
    private String tempName;
    /**
     * 所属单位ID
     */
    @Column(name = "unit_id")
    private Integer unitId;
    /**
     * 星期几，逗号隔开，0,1,2,3,4,5,6
     */
    @Column(name = "date")
    private String date;
    /**
     * 是否分夏令时和冬令时
     */
    @Column(name = "if_season")
    private Boolean ifSeason;
    /**
     * 夏季上班时间：09:00
     */
    @Column(name = "on_summer")
    private String onSummer;
    /**
     * 夏季下班时间：17:00
     */
    @Column(name = "off_summer")
    private String offSummer;
    /**
     * 冬季上班时间：09:30
     */
    @Column(name = "on_winter")
    private String onWinter;
    /**
     * 冬季下班时间：17:30
     */
    @Column(name = "off_winter")
    private String offWinter;
    /**
     * 夏季开始日期：07.01
     */
    @Column(name = "start_summser")
    private String startSummser;
    /**
     * 冬季开始日期：10.01
     */
    @Column(name = "start_winter")
    private String startWinter;
    @Column(name = "descs")
    private String descs;
    /**
     * 创建人ID
     */
    @Column(name = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 更新人ID
     */
    @Column(name = "update_by")
    private String updateBy;
    /**
     * 最后更新时间
     */
    @Column(name = "update_time")
    private Long updateTime;

    private transient List<TemplExcept> excepts;

    private transient long onTime;

    public long getOnTime() {
        if (onTime == 0)
            onTime = getTime(ifSeason && isWinter() ? onWinter : onSummer);
        return onTime;
    }

    public long getOffTime() {
        if (offTime == 0)
            offTime = getTime(ifSeason && isWinter() ? offWinter : offSummer);
        return offTime;
    }

    private boolean isWinter() {
        long sm = getDate(startSummser);
        long wt = getDate(startWinter);
        long td = System.currentTimeMillis();
        return !(td > sm && td < wt);
    }

    private transient long offTime;

    private long getTime(String s) {
        String[] ss = s.split(":");
        int h = Integer.parseInt(ss[0]);
        int m = Integer.parseInt(ss[1]);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, h);
        c.set(Calendar.MINUTE, m);
        c.clear(Calendar.SECOND);
        c.clear(Calendar.MILLISECOND);
        return c.getTimeInMillis();
    }

    /**
     * 将日期字符串 "2022.08.08"，转化为日期
     *
     * @param s "2022.08.08"
     * @return 2022.08.08 Date
     */
    private long getDate(String s) {
        String[] ss = s.split("\\.");
        int m = Integer.parseInt(ss[0]);
        int d = Integer.parseInt(ss[1]);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, m - 1);
        c.set(Calendar.DAY_OF_MONTH, d);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.clear(Calendar.MINUTE);
        c.clear(Calendar.SECOND);
        c.clear(Calendar.MILLISECOND);
        return c.getTimeInMillis();
    }
}

