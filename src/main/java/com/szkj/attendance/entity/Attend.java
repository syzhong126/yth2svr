package com.szkj.attendance.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Attend {
    /**
     * 1上班卡
     */
    public static final int Clock_On = 1;
    /**
     * 2下班卡
     */
    public static final int Clock_Off = 2;
    /**
     * 3非打卡时段
     */
    public static final int Clock_Not_Time = 3;
    /**
     * 4非工作日
     */
    public static final int Clock_Not_Day = 4;
    /**
     * 5考勤组未设置
     */
    public static final int Clock_Not_Set = 5;
    /**
     * 考勤记录ID，非工作日没有，因为不需要打卡
     */
    Integer attHisId;
    /**
     * 考勤班次日期，晚班早上下班，就是昨天日期
     */
    Long clockDate;
    /**
     * 上班时间
     */
    Long clockIn;
    /**
     * 下班时间
     */
    Long clockOut;
    /**
     * 上班打卡时间
     */
    Long clockStart;
    /**
     * 下班打卡时间
     */
    Long clockEnd;
    /**
     * 当前打卡类型，1上班卡，2下班卡，3非打卡时段，4非工作日,5考勤组未设置
     */
    Integer clockType;
    /**
     * 打卡地点
     */
    String location;
    /**
     * 打卡地点Id
     */
    Integer locId;
    /**
     * 是否在打卡范围
     */
    Boolean isInDistance;

}
