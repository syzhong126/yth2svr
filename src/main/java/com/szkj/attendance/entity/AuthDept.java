package com.szkj.attendance.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 部门考勤关系(YthAttendAuthDept)实体类
 *
 * @author makejava
 * @since 2022-04-27 11:30:03
 */
@Data
@Entity
@Table(name = "yth_attend_auth_dept")
public class AuthDept implements Serializable {
    private static final long serialVersionUID = 684145448993574657L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "auth_id", nullable = false)
    private Integer authId;
    /**
     * 部门ID
     */
    @Column(name = "unit_id")
    private Integer unitId;
    /**
     * 考勤组ID
     */
    @Column(name = "group_id")
    private Integer groupId;
    /**
     * 是否默认
     */
    @Column(name = "is_defalut")
    private Boolean isDefalut;

    private transient Boolean hasAuth = true;
}

