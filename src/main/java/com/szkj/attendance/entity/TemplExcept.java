package com.szkj.attendance.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 考勤模板-调班时间，每年年底的时候统一添加(YthAttendTemplExcept)实体类
 *
 * @author makejava
 * @since 2022-04-26 11:32:23
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_attend_templ_except")
public class TemplExcept implements Serializable {
    private static final long serialVersionUID = -34224200350486578L;
    /**
     * 日期类型：工作日
     */
    public static final String Type_Work_Day = "W";

    /**
     * 日期类型：休息日
     */
    public static final String Type_Not_Work_Day = "N";
    /**
     * 序号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "module_ext_id", nullable = false)
    private Integer moduleExtId;
    /**
     * 考勤模板编号
     */
    @Column(name = "temp_id")
    private Integer tempId;
    /**
     * 日期
     */
    @Column(name = "except_date")
    private Long exceptDate;
    /**
     * 日期类型：W工作日，N休息日
     */
    @Column(name = "date_type")
    private String dateType;
}

