package com.szkj.attendance.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 人员考勤关系(YthAttendAuthPerson)实体类
 *
 * @author makejava
 * @since 2022-04-27 16:57:47
 */
@Data
@Entity
@Table(name = "yth_attend_auth_person")
public class AuthPerson implements Serializable {
    private static final long serialVersionUID = 400811400753797247L;
    /**
     * 用户ID，主键
     */
    @Id
    @Column(name = "user_id", nullable = false)
    private Integer userId;
    /**
     * 考勤组ID
     */
    @Column(name = "group_id", nullable = false)
    private Integer groupId;
}

