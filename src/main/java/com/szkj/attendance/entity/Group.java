package com.szkj.attendance.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 考勤组主信息表(YthAttendGroup)实体类
 *
 * @author makejava
 * @since 2022-04-26 10:09:24
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_attend_group")
public class Group implements Serializable {
    private static final long serialVersionUID = 379698840287597348L;
    /**
     * 考勤组ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id", nullable = false)
    private Integer groupId;
    /**
     * 考勤组名称
     */
    @Column(name = "name")
    private String name;
    /**
     * 所属公司ID
     */
    @Column(name = "unit_id")
    private Integer unitId;
    /**
     * 考勤模板ID
     */
    @Column(name = "temp_id")
    private Integer tempId;
    /**
     * 考勤地点ID
     */
    @Column(name = "loc_id")
    private String locId;
    /**
     * 使用状态 2，未启用  1，已启用
     */
    @Column(name = "status")
    private String status;
    /**
     * 创建人ID
     */
    @Column(name = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 更新人ID
     */
    @Column(name = "update_by")
    private String updateBy;
    /**
     * 最后更新时间
     */
    @Column(name = "update_time")
    private Long updateTime;
}

