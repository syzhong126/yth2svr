package com.szkj.attendance.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 外出审批记录(YthAttendOut)实体类
 *
 * @author makejava
 * @since 2022-04-26 16:03:11
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_attend_out")
public class Out implements Serializable {
    private static final long serialVersionUID = -27355658778010372L;
    /**
     * 序号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "out_id", nullable = false)
    private Integer outId;
    /**
     * 考勤记录ID
     */
    @Column(name = "att_his_id")
    private Integer attHisId;
    /**
     * 用户ID
     */
    @Column(name = "person_id")
    private Integer personId;
    /**
     * 用户姓名
     */
    @Column(name = "person_name")
    private String personName;
    /**
     * 外出事由
     */
    @Column(name = "subject")
    private String subject;
    /**
     * 外出开始时间
     */
    @Column(name = "out_start")
    private Long outStart;
    /**
     * 外出结束时间
     */
    @Column(name = "out_end")
    private Long outEnd;
    /**
     * 审批人ID
     */
    @Column(name = "approve_id")
    private Integer approveId;
    /**
     * 审批人姓名
     */
    @Column(name = "approve_name")
    private String approveName;
    /**
     * 审批时间
     */
    @Column(name = "approve_time")
    private Long approveTime;
    /**
     * 审批状态 N未审批  Y已审批
     */
    @Column(name = "approve_status")
    private String approveStatus;
    /**
     * 是否已读 N未读 Y已读
     */
    @Column(name = "read_status")
    private String readStatus;
    /**
     * 创建人ID
     */
    @Column(name = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 更新人ID
     */
    @Column(name = "update_by")
    private String updateBy;
    /**
     * 最后更新时间
     */
    @Column(name = "update_time")
    private Long updateTime;
}

