package com.szkj.attendance.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 考勤地点信息表(YthAttendLocation)实体类
 *
 * @author makejava
 * @since 2022-04-26 10:45:48
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_attend_location")
public class Location implements Serializable {
    private static final long serialVersionUID = -15135059077411376L;
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "loc_id", nullable = false)
    private Integer locId;
    /**
     * 所属单位ID
     */
    @Column(name = "unit_id")
    private Integer unitId;
    /**
     * 地点名称
     */
    @Column(name = "loc_name")
    private String locName;
    /**
     * 地点经度
     */
    @Column(name = "lng")
    private Double lng;
    /**
     * 地点纬度
     */
    @Column(name = "lat")
    private Double lat;
    /**
     * 打卡范围，单位米，距中心点多少米有效
     */
    @Column(name = "distance")
    private Integer distance;
    /**
     * 地点说明
     */
    @Column(name = "descs")
    private String descs;
    /**
     * 创建人ID
     */
    @Column(name = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 更新人ID
     */
    @Column(name = "update_by")
    private String updateBy;
    /**
     * 最后更新时间
     */
    @Column(name = "update_time")
    private Long updateTime;
}

