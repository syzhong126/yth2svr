package com.szkj.attendance.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 异常处理记录(YthAttendExcept)实体类
 *
 * @author makejava
 * @since 2022-04-26 17:20:50
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_attend_except")
public class Except implements Serializable {
    private static final long serialVersionUID = 221113373183069083L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "exp_id", nullable = false)
    private Integer expId;
    /**
     * 考勤记录ID
     */
    @Column(name = "att_his_id")
    private Integer attHisId;
    /**
     * 审批记录ID
     */
    @Column(name = "out_id")
    private Integer outId;
    /**
     * 用户ID
     */
    @Column(name = "person_id")
    private Integer personId;
    /**
     * 调整前状
     */
    @Column(name = "status_pre")
    private String statusPre;
    /**
     * 调整后状态
     */
    @Column(name = "status_cur")
    private String statusCur;
    /**
     * 调整时间
     */
    @Column(name = "chg_time")
    private Long chgTime;
    /**
     * 调整事由
     */
    @Column(name = "subject")
    private String subject;
    /**
     * 创建人ID
     */
    @Column(name = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 更新人ID
     */
    @Column(name = "update_by")
    private String updateBy;
    /**
     * 最后更新时间
     */
    @Column(name = "update_time")
    private Long updateTime;
}

