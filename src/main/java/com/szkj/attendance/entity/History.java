package com.szkj.attendance.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 考勤历史记录(YthAttendHis)实体类
 *
 * @author makejava
 * @since 2022-04-27 10:26:35
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_attend_his")
public class History implements Serializable {
    /**
     * 未打卡
     */
    public static final String Type_Not = "NOT";
    /**
     * 正常
     */
    public static final String Type_Normal = "NORMAL";
    /**
     * 外出
     */
    public static final String Type_Normal_Out = "NORMAL_OUT";
    /**
     * 请假（手工异常处理结果）
     */
    public static final String Type_Normal_Deal = "NORMAL_DEAL";
    /**
     * 补卡
     */
    public static final String Type_Normal_Rectify = "NORMAL_RECTIFY";
    /**
     * 迟到
     */
    public static final String Type_Late = "LATE";
    /**
     * 早退
     */
    public static final String Type_Early = "EARLY";
    /**
     * 缺卡
     */
    public static final String Type_Miss = "MISS";
    /**
     * 旷工
     */
    public static final String Type_Absent = "ABSENT";
    /**
     * 迟到+早退
     */
    public static final String Type_Late_And_Early = "LATE_EARLY";
    /**
     * 迟到+缺卡
     */
    public static final String Type_Late_And_Miss = "LATE_MISS";

    private static final long serialVersionUID = -74915666817723805L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "att_his_id", nullable = false)
    private Integer attHisId;
    /**
     * 考勤组ID
     */
    @Column(name = "group_id")
    private Integer groupId;
    /**
     * 用户ID
     */
    @Column(name = "person_id")
    private Integer personId;
    /**
     * 上班考勤地点ID
     */
    @Column(name = "clock_first_loc")
    private Integer clockFirstLoc;
    /**
     * 下班考勤地点ID
     */
    @Column(name = "clock_last_loc")
    private Integer clockLastLoc;
    /**
     * 日期
     */
    @Column(name = "clock_date")
    private Long clockDate;
    /**
     * 打卡次数
     */
    @Column(name = "clock_sum")
    private Integer clockSum;

    /**
     * clockSum++
     */
    public void addSum() {
        Integer cs = getClockSum();
        setClockSum(cs == null ? 1 : cs + 1);
    }

    /**
     * 工时
     */
    @Column(name = "clock_along")
    private Float clockAlong;
    /**
     * 上班打卡时间
     */
    @Column(name = "clock_first")
    private Long clockFirst;
    /**
     * 下班打卡时间
     */
    @Column(name = "clock_last")
    private Long clockLast;
    /**
     * 上班打卡状态 0：未打 1，正常打卡  2， 迟到 3，外出
     */
    @Column(name = "clockin_status")
    private String clockinStatus;
    /**
     * 下班打卡状态 0：未打 1，正常打卡  2， 早退 3，外出
     */
    @Column(name = "clockout_status")
    private String clockoutStatus;
    /**
     * 最终状态 0:正常 1，缺卡 2，迟到 3，早退 4，迟到+早退 5，旷工 6，外出
     */
    @Column(name = "final_status")
    private String finalStatus;
}

