package com.szkj.visitor.vo;

import lombok.Data;

import java.util.List;

@Data
public class InvitationReq {
    // 拜访类型 1：商务拜访、2：客户维护、3：生意洽谈、4：其他
    int bookType;
    // 开始日期
    String startDate;
    // 截止日期
    String endDate;
    // 备注
    String remark;
    // 来访者单位
    String company;
    // 来访者对象数组
    List<Booker> bookers;
}
