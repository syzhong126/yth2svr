package com.szkj.visitor.vo;

import lombok.Data;

@Data
public class AddBookOrderWithQrCodeReq extends DsenseReq {

    String subject;
    int bookType;
    String company;
    String bookUser;
    String bookUserMobile;
    String receptionUserId;
    String remark;
    String verificationCode;
    int directPass;
    String startTime;
    String endTime;

    public AddBookOrderWithQrCodeReq() {
        super();
    }

}
