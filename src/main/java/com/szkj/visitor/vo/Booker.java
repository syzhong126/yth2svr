package com.szkj.visitor.vo;

import lombok.Data;

@Data
public class Booker {
    // 同行者姓名
    String name;
    // 同行者手机号
    String mobile;
    // 短信发送状态 0:待发送 1:发送成功 2:发送失败
    int sendStatus;
}
