package com.szkj.visitor.vo;

import lombok.Data;

import java.util.List;

@Data
public class OrderInfoResp {
    int orderId;
    int bookType;
    String startDate;
    String endDate;
    String remark;
    String company;
    String createBy;
    Long createTime;
    String qrCodeUrl;
    List<Booker> bookers;
}
