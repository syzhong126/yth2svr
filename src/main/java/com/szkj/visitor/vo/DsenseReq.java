package com.szkj.visitor.vo;

import lombok.Data;
import org.springframework.util.DigestUtils;
import java.nio.charset.StandardCharsets;

@Data
public class DsenseReq {

    String appKey;
    long timestamp;
    String sign;

    public DsenseReq() {}

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp() {
        this.timestamp = System.currentTimeMillis() / 1000;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String appSecret) {
        this.sign = DigestUtils.md5DigestAsHex((appSecret + "#" + timestamp).getBytes(StandardCharsets.UTF_8));
    }
}
