package com.szkj.visitor.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 来访申请表
 */
@Data
@Entity
@Table(name = "yth_visitor_apply")
public class YthVisitorApply implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "apply_id", nullable = false)
    private Integer applyId = 0;

    /**
     * 状态 0 待审核 1 通过 2未通过
     */
    @Column(name = "status")
    private Integer status = 0;

    /**
     * 预约主题
     */
    @Column(name = "subject")
    private String subject;

    /**
     * 可能用不上
     */
    @Column(name = "peoples")
    private Integer peoples = 0;

    /**
     * 被拜访人名称
     */
    @Column(name = "app_name")
    private String appName;

    /**
     * 预约类型
     */
    @Column(name = "reception_user")
    private String receptionUser;

    @Column(name = "book_type")
    private String bookType;

    /**
     * 被拜访人手机
     */
    @Column(name = "book_user_mobile")
    private String bookUserMobile;

    @Column(name = "audit_time")
    private Date auditTime;

    @Column(name = "company")
    private String company;

    @Column(name = "audit_user")
    private String auditUser;

    @Column(name = "passing_time")
    private String passingTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "edit_time")
    private Date editTime;

    /**
     * oa 账号
     */
    @Column(name = "oa_name")
    private String oaName;

    /**
     * 访客身份证号
     */
    @Column(name = "id_number")
    private String idNumber;

    /**
     * 访客手机号
     */
    @Column(name = "mobile")
    private String mobile;

    /**
     * 访客头像
     */
    @Column(name = "avatar")
    private String avatar;

    /**
     * 访客姓名
     */
    @Column(name = "name")
    private String name;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

}
