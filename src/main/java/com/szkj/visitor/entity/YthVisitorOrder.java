package com.szkj.visitor.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * 访客邀约记录
 */
@Data
@Entity
@Table(name = "yth_visitor_order")
public class YthVisitorOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 邀约ID
     */
    @Id
    @Column(name = "order_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orderId;

    /**
     * 拜访主题
     */
    @Column(name = "subject", nullable = false)
    private String subject = "";

    /**
     * 拜访类型，1：商务拜访、2：客户维护、3：生意洽谈、4：其他
     */
    @Column(name = "book_type", nullable = false)
    private int bookType = 0;

    /**
     * 来访者单位
     */
    @Column(name = "company", nullable = false)
    private String company = "";

    /**
     * 来访者姓名
     */
    @Column(name = "booker", nullable = false)
    private String booker = "";

    /**
     * 来访者手机号
     */
    @Column(name = "booker_mobile", nullable = false)
    private String bookerMobile = "";

    /**
     * 创建人，一体化平台用户账号
     */
    @Column(name = "receiver", nullable = false)
    private String receiver = "";

    /**
     * 备注
     */
    @Column(name = "remark", nullable = false)
    private String remark = "";

    /**
     * 二维码中验证码
     */
    @Column(name = "verify", nullable = false)
    private String verify = "";

    /**
     * 通行方式，0：自助（未预约）、1：直接通行、2：均有
     */
    @Column(name = "pass_type", nullable = false)
    private Integer passType = 0;

    /**
     * 通行开始日期
     */
    @Column(name = "start_date")
    private Date startDate;

    /**
     * 通行截止日期
     */
    @Column(name = "end_date")
    private Date endDate;

    /**
     * 请求ID
     */
    @Column(name = "req_id", nullable = false)
    private Integer reqId = 0;

    /**
     * 短信发送状态 0:待发送 1:部分成功 2:发送成功 3:发送失败
     */
    @Column(name = "send_status", nullable = false)
    private Integer sendStatus = 0;

    /**
     * 创建人，一体化平台用户ID
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy = 0;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime = 0L;

    /**
     * 二维码图片的长url
     */
    @Column(name = "qrcode_url", nullable = false)
    private String qrCodeUrl = "";

    /**
     * 二维码图片的短url
     */
    @Column(name = "qrcode_short_url", nullable = false)
    private String qrCodeShortUrl = "";

    /**
     * 0：未删除，1：删除
     */
    @Column(name = "is_del", nullable = false)
    private int del = 0;

}
