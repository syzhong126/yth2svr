package com.szkj.visitor.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 邀约来访者
 */
@Data
@Entity
@Table(name = "yth_visitor_order_booker")
public class YthVisitorOrderBooker implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 来访者ID
     */
    @Id
    @Column(name = "booker_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer bookerId;

    /**
     * 邀约ID
     */
    @Column(name = "order_id", nullable = false)
    private Integer orderId;

    /**
     * 来访者姓名
     */
    @Column(name = "name", nullable = false)
    private String name = "";

    /**
     * 来访者手机号
     */
    @Column(name = "mobile", nullable = false)
    private String mobile = "";

    /**
     * 短信发送状态 0:待发送 1:发送成功 2:发送失败
     */
    @Column(name = "send_status", nullable = false)
    private Integer sendStatus = 0;

}
