package com.szkj.visitor.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 邀约请求记录
 */
@Data
@Entity
@Table(name = "yth_visitor_order_dsense")
public class YthVisitorOrderDsense implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 请求ID
     */
    @Id
    @Column(name = "req_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reqId;

    /**
     * 邀约ID
     */
    @Column(name = "order_id", nullable = false)
    private Integer orderId;

    /**
     * 通行日期
     */
    @Column(name = "date", nullable = false)
    private Date date;

    /**
     * 开始时间
     */
    @Column(name = "start_time", nullable = false)
    private Date startTime;

    /**
     * 截止时间
     */
    @Column(name = "end_time", nullable = false)
    private Date endTime;

    /**
     * 毫秒时间戳
     */
    @Column(name = "timestamp", nullable = false)
    private Long timestamp = 0L;

    /**
     * 签名=md5(密钥#毫秒时间戳)
     */
    @Column(name = "sign", nullable = false)
    private String sign = "";

    /**
     * 返回的code
     */
    @Column(name = "resp_code", nullable = false)
    private String respCode = "";

    /**
     * 返回的orderId
     */
    @Column(name = "resp_order_id", nullable = false)
    private String respOrderId = "";

    /**
     * 返回的icNumber
     */
    @Column(name = "resp_ic_number", nullable = false)
    private String respIcNumber = "";

}
