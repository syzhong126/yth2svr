package com.szkj.visitor.reposi;

import com.szkj.visitor.entity.YthVisitorOrderDsense;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface YthVisitorOrderDsenseReposi extends JpaRepository<YthVisitorOrderDsense, Integer>, JpaSpecificationExecutor<YthVisitorOrderDsense> {

}