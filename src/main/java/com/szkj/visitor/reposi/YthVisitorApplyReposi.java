package com.szkj.visitor.reposi;

import com.szkj.visitor.entity.YthVisitorApply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface YthVisitorApplyReposi extends JpaRepository<YthVisitorApply, Integer>, JpaSpecificationExecutor<YthVisitorApply> {

}