package com.szkj.visitor.reposi;

import com.szkj.visitor.entity.YthVisitorOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface YthVisitorOrderReposi extends JpaRepository<YthVisitorOrder, Integer>, JpaSpecificationExecutor<YthVisitorOrder> {

    @Query(value = "select * " +
            "from yth_visitor_order " +
            "where booker_mobile = ?1 " +
            "and (" +
            "(start_date <= DATE(?2) and end_date >= DATE(?2)) " +
            "or (start_date <= DATE(?3) and end_date >= DATE(?3))" +
            ")"
            , nativeQuery = true)
    YthVisitorOrder findByBookerMobileAndStartDateAndEndDate(String bookerMobile, String startDate, String endDate);

    @Query(value = "select a.* " +
            "from yth_visitor_order a " +
            "join yth_visitor_order_dsense b on a.req_id = b.req_id " +
            "where a.end_date != b.date", nativeQuery = true)
    List<YthVisitorOrder> findRenewal();
}