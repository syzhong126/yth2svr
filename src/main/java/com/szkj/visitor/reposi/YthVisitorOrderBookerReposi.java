package com.szkj.visitor.reposi;

import com.szkj.visitor.entity.YthVisitorOrderBooker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface YthVisitorOrderBookerReposi extends JpaRepository<YthVisitorOrderBooker, Integer>, JpaSpecificationExecutor<YthVisitorOrderBooker> {

    @Query(value = "select * from yth_visitor_order_booker where order_id = ?1", nativeQuery = true)
    List<YthVisitorOrderBooker> findByOrderId(int orderId);
}