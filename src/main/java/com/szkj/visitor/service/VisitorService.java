package com.szkj.visitor.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import com.szkj.auth.entity.User;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.reposi.UserReposi;
import com.szkj.common.service.AliSMSSerice;
import com.szkj.common.util.ConvertUtil;
import com.szkj.common.util.JsonUtil;
import com.szkj.common.util.RespUtil;
import com.szkj.visitor.entity.YthVisitorOrder;
import com.szkj.visitor.entity.YthVisitorOrderDsense;
import com.szkj.visitor.entity.YthVisitorOrderBooker;
import com.szkj.visitor.reposi.YthVisitorOrderDsenseReposi;
import com.szkj.visitor.reposi.YthVisitorOrderBookerReposi;
import com.szkj.visitor.reposi.YthVisitorOrderReposi;
import com.szkj.visitor.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import javax.annotation.Resource;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@Slf4j
@Service
public class VisitorService {

    @Value("${dsenseAddr}")
    private String DSENSE_ADDR;
    @Value("${dsenseAppKey}")
    private String DSENSE_APP_KEY;
    @Value("${dsenseAppSecret}")
    private String DSENSE_APP_SECRET;
    @Value("${qrCodeShortUrlPrefix}")
    private String SHORT_URL_PREFIX;
    @Value("${fileUrlPrefix}")
    private String FILE_URL_PREFIX;
    @Value("${qrCodeImgDir}")
    private String QR_CODE_IMG_DIR;

    @Autowired
    private AliSMSSerice aliSMSSerice;
    @Autowired
    private YthVisitorOrderReposi ythVisitorOrderReposi;
    @Autowired
    private YthVisitorOrderBookerReposi ythVisitorOrderBookerReposi;
    @Autowired
    private YthVisitorOrderDsenseReposi ythVisitorOrderDsenseReposi;
    @Autowired
    private UserReposi ythUserReposi;

    @Resource
    QrConfig qrConfig;

    private String postInvitation(AddBookOrderWithQrCodeReq req) {
        String url = StrUtil.format("{}/api/guest/addBookOrderWithQrCode" +
                        "?subject={}&bookType={}&company={}&bookUser={}&bookUserMobile={}&receptionUserId={}&remark={}" +
                        "&verificationCode={}&startTime={}&endTime={}&appKey={}&timestamp={}&sign={}",
                DSENSE_ADDR, req.getSubject(), req.getBookType(), req.getCompany(), req.getBookUser()
                , req.getBookUserMobile(), req.getReceptionUserId(), req.getRemark()
                , req.getVerificationCode(), req.getStartTime(), req.getEndTime()
                , req.getAppKey(), req.getTimestamp(), req.getSign());
        String resp = HttpRequest.post(url).execute().body();
        log.info("访客邀约请求: {}\n响应:{}", url, resp);
        return resp;
    }

    private String postAudit(int orderId, int status, String startTime, String endTime) {
        long timestamp = System.currentTimeMillis();
        String sign = DigestUtils.md5DigestAsHex((DSENSE_APP_SECRET + "#" + timestamp).getBytes(StandardCharsets.UTF_8));
        String url = StrUtil.format("{}/api/guest/auditsuccXianDao" +
                        "?orderId={}&status={}&startTime={}&endTime={}&appKey={}&timestamp={}&sign={}",
                DSENSE_ADDR, orderId, status, startTime, endTime, DSENSE_APP_KEY, timestamp, sign);
        return HttpRequest.post(url).execute().body();
    }

    private String getApplyList() {
        long timestamp = System.currentTimeMillis();
        String sign = DigestUtils.md5DigestAsHex((DSENSE_APP_SECRET + "#" + timestamp).getBytes(StandardCharsets.UTF_8));
        String url = StrUtil.format("{}/api/guest/xiandaolist?appKey={}&timestamp={}&sign={}",
                DSENSE_ADDR, DSENSE_APP_KEY, timestamp, sign);
        return HttpRequest.get(url).execute().body();
    }

    private String getApplyDetail(int id) {
        long timestamp = System.currentTimeMillis();
        String sign = DigestUtils.md5DigestAsHex((DSENSE_APP_SECRET + "#" + timestamp).getBytes(StandardCharsets.UTF_8));
        String url = StrUtil.format("{}/api/guest/detail?id={}&appKey={}&timestamp={}&sign={}",
                DSENSE_ADDR, id, DSENSE_APP_KEY, timestamp, sign);
        return HttpRequest.get(url).execute().body();
    }

    public String addInvitation(AuthUserInfoModel userInfo, InvitationReq body) {
        // 判断邀约请求能不能发
        // 同一来访者，如果已存在时间上有交集的邀约记录，为了保证来访者之前已得到的二维码在原本的时间段内有效，不能新增邀约。
        // 因为如果新增，来访者又会收到一个二维码，之前的二维码会失效。来访者并不知情，通行时，如果用了失效的二维码，会刷不开门。
        boolean allConflict = true;
        int primaryBookerIndex = 0;
        List<Booker> bookerList = body.getBookers();
        for (int i = 0; i < bookerList.size(); i++) {
            YthVisitorOrder ythVisitorOrder = ythVisitorOrderReposi.findByBookerMobileAndStartDateAndEndDate(
                    bookerList.get(i).getMobile()
                    , body.getStartDate()
                    , body.getEndDate());
            if (null == ythVisitorOrder) {
                allConflict = false;
                primaryBookerIndex = i;
            }
        }

        if (allConflict) {
            return RespUtil.error("所有的来访者都已存在时间段有交集的邀约记录", new JSONObject());
        }

        // 发邀约请求
        AddBookOrderWithQrCodeReq req = new AddBookOrderWithQrCodeReq();
        req.setSubject("其他");
        req.setBookType(body.getBookType());
        req.setCompany(body.getCompany());
        req.setBookUser(bookerList.get(primaryBookerIndex).getName());
        // 用来访者手机号生成二维码
        req.setBookUserMobile(bookerList.get(primaryBookerIndex).getMobile());
        req.setReceptionUserId(userInfo.getAccount());
        req.setRemark(body.getRemark());
        // 二维码值
        req.setVerificationCode(String.valueOf(System.currentTimeMillis() / 1000));
        req.setDirectPass(2);
        // 设置通行开始时间
        // 如果开始日期为当天，则需要考虑以下情况：
        // 1、根据办公区域的安全管理要求，访客通行的时间为08:00至18:00
        // 2、商汤系统会在邀约请求中的开始时间和结束时间基础上前后各延长1小时作为通行时间
        // 3、商汤的邀约请求接口会要求开始时间不能早于当前时间（商汤服务器的系统时间），并且开始时间不能晚于结束时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String currentDate = formatter.format(date);
        if (currentDate.equals(body.getStartDate())) {
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                // 推迟10分钟作为开始时间，避免因服务器时钟不同步导致商汤接口认为开始时间早于当前时间的问题
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.MINUTE, +10);
                if (calendar.getTime().after(formatter.parse(currentDate + " 17:00:00"))) {
                    return RespUtil.error("16点50分之前才可预约当天！", new JSONObject());
                } else if (calendar.getTime().before(formatter.parse(currentDate + " 09:00:00"))) {
                    req.setStartTime(body.getStartDate() + " 09:00:00");
                } else {
                    req.setStartTime(formatter.format(calendar.getTime()));
                }
            } catch (ParseException e) {
                e.printStackTrace();
                return RespUtil.error("处理开始时间时发生异常：" + e.getMessage(), new JSONObject());
            }
        } else {
            req.setStartTime(body.getStartDate() + " 09:00:00");
        }
        // 设置通行截止时间
        // 如果多天，后台每天凌晨自动去生成一次
        req.setEndTime(body.getStartDate() + " 17:00:00");
        req.setAppKey(DSENSE_APP_KEY);
        req.setTimestamp();
        req.setSign(DSENSE_APP_SECRET);
        // 请求商汤接口
//        JSONObject respJson = new JSONObject(postInvitation(req));
        JSONObject respJson = new JSONObject("{\n" +
                "    \"data\": {\n" +
                "        \"icNumber\": \"1654328772\",\n" +
                "        \"orderId\": 2224450,\n" +
                "        \"guestId\": 8947\n" +
                "    },\n" +
                "    \"code\": \"0\",\n" +
                "    \"message\": \"success.\"\n" +
                "}");
        if ("0".equals(respJson.getStr("code"))) {
            // 保存邀约记录
            YthVisitorOrder ythVisitorOrder = new YthVisitorOrder();
            BeanUtils.copyProperties(body, ythVisitorOrder);
            ythVisitorOrder.setSubject(req.getSubject());
            ythVisitorOrder.setCompany(req.getCompany());
            ythVisitorOrder.setBooker(req.getBookUser());
            ythVisitorOrder.setBookerMobile(req.getBookUserMobile());
            ythVisitorOrder.setReceiver(userInfo.getAccount());
            ythVisitorOrder.setVerify(req.getVerificationCode());
            ythVisitorOrder.setPassType(req.getDirectPass());
            ythVisitorOrder.setStartDate(java.sql.Date.valueOf(body.getStartDate()));
            ythVisitorOrder.setEndDate(java.sql.Date.valueOf(body.getEndDate()));
            ythVisitorOrder.setCreateBy(userInfo.getUserId());
            ythVisitorOrder.setCreateTime(System.currentTimeMillis());
            YthVisitorOrder savedYthVisitorOrder = ythVisitorOrderReposi.save(ythVisitorOrder);
            // 保存邀约来访者记录
            for (int i = 0; i < bookerList.size(); i++) {
                YthVisitorOrderBooker ythVisitorOrderBooker = new YthVisitorOrderBooker();
                ythVisitorOrderBooker.setOrderId(savedYthVisitorOrder.getOrderId());
                BeanUtils.copyProperties(bookerList.get(i), ythVisitorOrderBooker);
                ythVisitorOrderBookerReposi.save(ythVisitorOrderBooker);
            }
            // 保存请求记录
            YthVisitorOrderDsense ythVisitorOrderDsense = new YthVisitorOrderDsense();
            ythVisitorOrderDsense.setOrderId(ythVisitorOrder.getOrderId());
            ythVisitorOrderDsense.setDate(java.sql.Date.valueOf(body.getStartDate()));
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                ythVisitorOrderDsense.setStartTime(formatter.parse(req.getStartTime()));
                ythVisitorOrderDsense.setEndTime(formatter.parse(req.getEndTime()));
            } catch (ParseException e) {
                e.printStackTrace();
                return RespUtil.error("保存邀约请求信息时发生异常：" + e.getMessage(), new JSONObject());
            }
            ythVisitorOrderDsense.setTimestamp(req.getTimestamp());
            ythVisitorOrderDsense.setSign(req.getSign());
            ythVisitorOrderDsense.setRespCode(respJson.getStr("code"));
            if (respJson.getJSONObject("data").containsKey("orderId")) {
                ythVisitorOrderDsense.setRespOrderId(String.valueOf(respJson.getJSONObject("data").getInt("orderId")));
            }
            if (respJson.getJSONObject("data").containsKey("icNumber")) {
                ythVisitorOrderDsense.setRespIcNumber(respJson.getJSONObject("data").getStr("icNumber"));
            }
            YthVisitorOrderDsense savedYthVisitorOrderDsense = ythVisitorOrderDsenseReposi.save(ythVisitorOrderDsense);
            // 关联请求ID
            ythVisitorOrder.setReqId(savedYthVisitorOrderDsense.getReqId());
            ythVisitorOrderReposi.save(ythVisitorOrder);
            // 生成二维码图片并保存
            generateQrCodeImgFileByOrderId(savedYthVisitorOrder.getOrderId());
            // 发短信给来访者和同行者
//            invitationSendSMSByOrderId(savedYthVisitorOrder.getOrderId());
        } else if ("y5020".equals(respJson.getStr("code"))) {
            log.error("请确认来访者手机号(" + bookerList.get(primaryBookerIndex).getMobile() + ")非员工手机号！");
            return RespUtil.error("请确认来访者手机号(" + bookerList.get(primaryBookerIndex).getMobile() + ")非员工手机号！"
                    , new JSONObject());
        } else if ("E00002".equals(respJson.getStr("code"))) {
            log.error("预计达到时间不能早于当前时间！");
            return RespUtil.error("预计达到时间不能早于当前时间！", new JSONObject());
        } else if ("y5004".equals(respJson.getStr("code"))) {
            log.error("一体化账号(" + userInfo.getAccount() + ")在人脸识别系统中不存在！");
            return RespUtil.error("一体化账号(" + userInfo.getAccount() + ")在人脸识别系统中不存在！", new JSONObject());
        }

        return RespUtil.success("成功", new JSONObject());
    }

    // 连续多天的邀约，在邀约期限内，为新的一天续期，每天凌晨执行
    public void renewalReq() throws Exception {
        List<YthVisitorOrder> ythVisitorOrderList = ythVisitorOrderReposi.findRenewal();
        for (int i = 0; i < ythVisitorOrderList.size(); i++) {
            AddBookOrderWithQrCodeReq req = new AddBookOrderWithQrCodeReq();
            req.setSubject("其他");
            req.setBookType(ythVisitorOrderList.get(i).getBookType());
            req.setCompany("来访者单位");
            req.setBookUser(ythVisitorOrderList.get(i).getBooker());
            // 用来访者手机号生成二维码
            req.setBookUserMobile(ythVisitorOrderList.get(i).getBookerMobile());
            req.setReceptionUserId(ythVisitorOrderList.get(i).getReceiver());
            req.setRemark(ythVisitorOrderList.get(i).getRemark());
            // 二维码的值
            req.setVerificationCode(ythVisitorOrderList.get(i).getVerify());
            req.setDirectPass(2);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String currentDate = formatter.format(date);
            req.setStartTime(currentDate + " 09:00:00");
            req.setEndTime(currentDate + " 17:00:00");
            req.setAppKey(DSENSE_APP_KEY);
            req.setTimestamp();
            req.setSign(DSENSE_APP_SECRET);
            // 请求商汤接口
//            JSONObject respJson = new JSONObject(postInvitation(req));
            JSONObject respJson = new JSONObject("{\n" +
                    "    \"data\": {\n" +
                    "        \"icNumber\": \"1654328772\",\n" +
                    "        \"orderId\": 2224450,\n" +
                    "        \"guestId\": 8947\n" +
                    "    },\n" +
                    "    \"code\": \"0\",\n" +
                    "    \"message\": \"success.\"\n" +
                    "}");
            if ("0".equals(respJson.getStr("code"))) {
                // 保存请求记录
                YthVisitorOrderDsense ythVisitorOrderDsense = new YthVisitorOrderDsense();
                ythVisitorOrderDsense.setOrderId(ythVisitorOrderList.get(i).getOrderId());
                ythVisitorOrderDsense.setDate(java.sql.Date.valueOf(currentDate));
                formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    ythVisitorOrderDsense.setStartTime(formatter.parse(req.getStartTime()));
                    ythVisitorOrderDsense.setEndTime(formatter.parse(req.getEndTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                    throw new Exception("保存邀约请求信息时发生异常：" + e.getMessage());
                }
                ythVisitorOrderDsense.setTimestamp(req.getTimestamp());
                ythVisitorOrderDsense.setSign(req.getSign());
                ythVisitorOrderDsense.setRespCode(respJson.getStr("code"));
                if (respJson.getJSONObject("data").containsKey("orderId")) {
                    ythVisitorOrderDsense.setRespOrderId(String.valueOf(respJson.getJSONObject("data").getInt("orderId")));
                }
                if (respJson.getJSONObject("data").containsKey("icNumber")) {
                    ythVisitorOrderDsense.setRespIcNumber(respJson.getJSONObject("data").getStr("icNumber"));
                }
                YthVisitorOrderDsense savedYthVisitorOrderDsense = ythVisitorOrderDsenseReposi.save(ythVisitorOrderDsense);
                // 关联请求ID
                ythVisitorOrderList.get(i).setReqId(savedYthVisitorOrderDsense.getReqId());
                ythVisitorOrderReposi.save(ythVisitorOrderList.get(i));
            } else if ("y5020".equals(respJson.getStr("code"))) {
                log.error("请确认来访者手机号(" + ythVisitorOrderList.get(i).getBookerMobile() + ")非员工手机号！");
                throw new Exception("请确认来访者手机号(" + ythVisitorOrderList.get(i).getBookerMobile() + ")非员工手机号！");
            } else if ("E00002".equals(respJson.getStr("code"))) {
                log.error("预计达到时间不能早于当前时间！");
                throw new Exception("预计达到时间不能早于当前时间！");
            } else if ("y5004".equals(respJson.getStr("code"))) {
                log.error("一体化账号(" + ythVisitorOrderList.get(i).getReceiver() + ")在人脸识别系统中不存在！");
                throw new Exception("一体化账号(" + ythVisitorOrderList.get(i).getReceiver() + ")在人脸识别系统中不存在！");
            }
        }
    }

    /**
     * 生成二维码图片文件
     *
     * 注意：调用商汤接口成功了，才生成二维码图片文件，否则无法生成。
     *
     * @param orderId 邀约ID
     */
    private void generateQrCodeImgFileByOrderId(int orderId) {
        Optional<YthVisitorOrder> ythVisitorOrderOptional = ythVisitorOrderReposi.findById(orderId);
        if (ythVisitorOrderOptional.isPresent()) {
            Optional<YthVisitorOrderDsense> ythVisitorOrderDsenseOptional = ythVisitorOrderDsenseReposi.findById(ythVisitorOrderOptional.get().getReqId());
            if (ythVisitorOrderDsenseOptional.isPresent() && "0".equals(ythVisitorOrderDsenseOptional.get().getRespCode())) {
                String fileFullName = QR_CODE_IMG_DIR
                        + "/"
                        + new SimpleDateFormat("yyyy-MM").format(new Date())
                        + "/"
                        + UUID.randomUUID().toString()
                        + ".png";
                File file = new File(fileFullName);
                // 生成二维码图片文件
                QrCodeUtil.generate(ythVisitorOrderDsenseOptional.get().getRespIcNumber(), qrConfig, file);
                // 生成二维码图片下载地址
                ythVisitorOrderOptional.get().setQrCodeUrl(FILE_URL_PREFIX + fileFullName);
                // 生成二维码图片下载短地址
                ythVisitorOrderOptional.get().setQrCodeShortUrl(SHORT_URL_PREFIX + ConvertUtil.encode(orderId));
                ythVisitorOrderReposi.save(ythVisitorOrderOptional.get());
            }
        }
    }

    /**
     * 发短信给来访者
     *
     * @param orderId 邀约ID
     *
     * 【长沙城发集团】尊敬的XXX，您好！
     * 您计划于X月X日在湘江时代拜访XXX的预约已成功，
     * 请使用二维码通过闸机（门禁小白盒）, http://vst.nicity.cn/r5
     * 刷二维码时请关闭手机NFC或距离5cm以上。
     */
    public void invitationSendSMSByOrderId(int orderId) {
        Optional<YthVisitorOrder> ythVisitorOrderOptional = ythVisitorOrderReposi.findById(orderId);
        if (ythVisitorOrderOptional.isPresent()) {
            LocalDate localDate = ythVisitorOrderOptional.get().getStartDate().toLocalDate();
            int month = localDate.getMonthValue();
            int day = localDate.getDayOfMonth();
            String sendResult = "";
            boolean allFailed = true;
            boolean allSuccess = true;

            // 发短信给来访者，发送成功的不再发送。
            List<YthVisitorOrderBooker> ythVisitorOrderBookerList = ythVisitorOrderBookerReposi.findByOrderId(orderId);
            for (YthVisitorOrderBooker ythVisitorOrderBooker : ythVisitorOrderBookerList) {
                if (1 != ythVisitorOrderBooker.getSendStatus()) {
                    sendResult = aliSMSSerice.sendVisitorInvitation(ythVisitorOrderBooker.getMobile()
                            , ythVisitorOrderBooker.getName()
                            , month
                            , day
                            , convertUserIdToUserName(ythVisitorOrderOptional.get().getCreateBy())
                            , ConvertUtil.encode(orderId));
                    if ("OK".equals(sendResult)) {
                        allFailed = false;
                        ythVisitorOrderBooker.setSendStatus(1);
                    } else {
                        allSuccess = false;
                        ythVisitorOrderBooker.setSendStatus(2);
                    }
                    ythVisitorOrderBookerReposi.save(ythVisitorOrderBooker);
                }
            }

            //保存发送状态
            if (allFailed) {
                ythVisitorOrderOptional.get().setSendStatus(3);
            } else if (allSuccess) {
                ythVisitorOrderOptional.get().setSendStatus(2);
            } else {
                ythVisitorOrderOptional.get().setSendStatus(1);
            }
            ythVisitorOrderReposi.save(ythVisitorOrderOptional.get());
        }
    }

    public JSONObject resetInvitationListQueryBody(AuthUserInfoModel userInfo, JSONObject body) throws Exception {
        JsonUtil.resetJsonKeyValue(body, "sqlId", 1201);
        JsonUtil.resetJsonKeyValue(body, "params2", "{userId:"+ userInfo.getUserId() + "}");
        return body;
    }

    private String convertUserIdToUserName(int userId) {
        Optional<User> ythUser = ythUserReposi.findById(userId);
        if (ythUser.isPresent()) {
            return ythUser.get().getUserName();
        }
        return null;
    }

    private List<Booker> getBookerListByOrderId(int orderId) {
        List<Booker> bookerList = new ArrayList<>();
        List<YthVisitorOrderBooker> ythVisitorOrderBookerList = ythVisitorOrderBookerReposi.findByOrderId(orderId);
        for (YthVisitorOrderBooker ythVisitorOrderBooker : ythVisitorOrderBookerList) {
            Booker booker = new Booker();
            booker.setName(ythVisitorOrderBooker.getName());
            booker.setMobile(ythVisitorOrderBooker.getMobile());
            bookerList.add(booker);
        }
        return bookerList;
    }

    public String getInvitationInfo(int orderId) {
        OrderInfoResp info = new OrderInfoResp();
        Optional<YthVisitorOrder> ythVisitorOrderOptional = ythVisitorOrderReposi.findById(orderId);
        if (ythVisitorOrderOptional.isPresent()) {
            info = new OrderInfoResp();
            BeanUtils.copyProperties(ythVisitorOrderOptional.get(), info);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            info.setStartDate(formatter.format(ythVisitorOrderOptional.get().getStartDate()));
            info.setEndDate(formatter.format(ythVisitorOrderOptional.get().getEndDate()));
            info.setCreateBy(convertUserIdToUserName(ythVisitorOrderOptional.get().getCreateBy()));
            info.setBookers(getBookerListByOrderId(ythVisitorOrderOptional.get().getOrderId()));
        }
        return RespUtil.success("成功", info);
    }

    public String getInvitationQrCodeByShortUrl(String uid) {
        Optional<YthVisitorOrder> ythVisitorOrderOptional = ythVisitorOrderReposi.findById((int) ConvertUtil.decode(uid));
        if (ythVisitorOrderOptional.isPresent()) {
            JSONObject respData = new JSONObject();
            JsonUtil.resetJsonKeyValue(respData,"qrCodeUrl", ythVisitorOrderOptional.get().getQrCodeUrl());
            return RespUtil.success("成功", respData);
        }
        return RespUtil.error("该邀约不存在！");
    }
}
