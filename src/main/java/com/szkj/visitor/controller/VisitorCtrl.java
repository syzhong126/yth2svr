package com.szkj.visitor.controller;

import cn.hutool.json.JSONObject;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.service.SQLService;
import com.szkj.common.util.RespUtil;
import com.szkj.visitor.entity.YthVisitorOrder;
import com.szkj.visitor.service.VisitorService;
import com.szkj.visitor.vo.InvitationReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@RestController
@CrossOrigin("*")
@RequestMapping(value="/visitor")
@Slf4j
public class VisitorCtrl {
    @Autowired
    private VisitorService visitorService;
    @Autowired
    private SQLService sqlService;

    @RequestMapping(value="/invitation", method = {RequestMethod.POST})
    public String addInvitation(@ModelAttribute("authUserInfo") AuthUserInfoModel userInfo, @RequestBody InvitationReq body) {
        try {
            return visitorService.addInvitation(userInfo, body);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/invitation/list", method = {RequestMethod.POST})
    public String getInvitationList(@ModelAttribute("authUserInfo") AuthUserInfoModel userInfo, @RequestBody JSONObject body) {
        try {
            JSONObject newBody = visitorService.resetInvitationListQueryBody(userInfo, body);
            JSONObject data = sqlService.queryPageList(userInfo, newBody);
            return RespUtil.success("成功", data);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/invitation", method = {RequestMethod.GET})
    public String getInvitation(@RequestParam int orderId) {
        try {
            return visitorService.getInvitationInfo(orderId);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/invitation/send", method = {RequestMethod.PUT})
    public String invitationSend(@RequestBody YthVisitorOrder ythVisitorOrder) {
        try {
            visitorService.invitationSendSMSByOrderId(ythVisitorOrder.getOrderId());
            return RespUtil.success("成功", new JSONObject());
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }

    @RequestMapping(value="/invitation/qrcode", method = {RequestMethod.GET})
    public String invitationQrCodeByShortUrl(@RequestParam String uid) {
        try {
            return visitorService.getInvitationQrCodeByShortUrl(uid);
        } catch (Exception e) {
            e.printStackTrace();
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }
}
