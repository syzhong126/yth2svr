package com.szkj.visitor.controller;

import com.szkj.visitor.service.VisitorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class RenewalSchedule {
    @Autowired
    private VisitorService visitorService;

    @Scheduled(cron = "0 0 3 * * ?")
    public void docRankStatistic() {
        try {
            visitorService.renewalReq();
        } catch (Exception e) {
            log.error("在邀约期限内续期失败!");
            e.printStackTrace();
        }
    }
}
