package com.szkj.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class UserLoginConfig implements WebMvcConfigurer {
    /**
     * 拦截器配置
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(new UserLoginInterceptor());
        registration.addPathPatterns("/**"); //所有路径都被拦截
        registration.excludePathPatterns(    //添加不拦截路径
                "/system/login",             //登录接口
                "/system/sendVerifyCode",    //重置密码验证短信
                "/system/resetPwd",          //重置密码
                "/system/test",
                "/meeting/file/down/**",    //下载会议文件
                "/meeting/latest",          //会议室的最近会议
                "/meeting/today",           //今天的会议
                "/meeting/now",             //现在的会议
                "/card/img/**",             //名片地址
                "/error"
        );
    }
}