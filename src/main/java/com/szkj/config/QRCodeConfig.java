package com.szkj.config;

import cn.hutool.extra.qrcode.QrConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.awt.*;

@Configuration
public class QRCodeConfig {

    @Bean
    public QrConfig qrConfig() {
        QrConfig qrConfig = new QrConfig();
        qrConfig.setBackColor(Color.white);
        qrConfig.setForeColor(Color.black);
        return qrConfig;
    }
}
