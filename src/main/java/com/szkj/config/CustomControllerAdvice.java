package com.szkj.config;

import cn.hutool.json.JSONObject;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.Objects;

/**
 * 控制器注入用户登录信息和统一异常处理
 */
@Slf4j
@ControllerAdvice
public class CustomControllerAdvice {
    @Autowired
    private YthOperationLogService logservice;
    @Autowired
    private AuthService authService;

    @ModelAttribute("authUserInfo")
    public AuthUserInfoModel modelAttribute(HttpServletRequest request) {
        return authService.getUserInfoModel(request);
    }
    /**
     * 统一异常处理
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public String handleValidException(Exception e, HttpServletRequest request){
        try{
            AuthUserInfoModel userInfo= authService.getUserInfoModel(request);
//            String str, wholeStr = "";
//            BufferedReader br = request.getReader();
//            while((str = br.readLine()) != null){
//                wholeStr += str;
//            }
            String wholeStr= new RequestWrapper(request).getBody();
            logservice.save(userInfo.getUserId(), "insert", "workplan", "workplan",
                    wholeStr, "", e.toString(), userInfo.getUserId());
            log.error("访问{}发生错误: {}", request.getRequestURL(), wholeStr, e);
        }catch(Exception ex) {
        }
        return RespUtil.error(e.getMessage());
    }
}
