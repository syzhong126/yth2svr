package com.szkj.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Order(1)
@Slf4j
@WebFilter(filterName = "RequestFilter", urlPatterns = "/*")
public class RequestFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
    @Override
    public void destroy() {
    }
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String contentType = servletRequest.getContentType();
        String method = "multipart/form-data";
        if (contentType != null && contentType.contains(method)) {
            request = new StandardServletMultipartResolver().resolveMultipart(request);
        }
        // 扩展request，使其能够能够重复读取requestBody
        ServletRequest requestWrapper = new RequestWrapper(request);
        // 这里需要放行，但是要注意放行的 request是requestWrapper
        filterChain.doFilter(requestWrapper, servletResponse);

    }
}