package com.szkj.auth.reposi;

import com.szkj.auth.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface UserReposi extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {
    @Modifying
    @Query(value = "UPDATE yth_user set change_pwd= 0 WHERE account=?1 ", nativeQuery = true)
    int clearChangePwd(String account);

    @Modifying
    @Query(value = "UPDATE yth_user set is_del= 2 WHERE account=?1 ", nativeQuery = true)
    int unregist(String account);

    @Query(value = "select * from yth_user u where u.account=?1 limit 1", nativeQuery = true)
    Optional<User> getUserByAccount(@Param("account") String account);

    @Modifying
    @Query(value = "UPDATE yth_user set theme_id= ?2 WHERE user_id=?1 ", nativeQuery = true)
    int changeTheme(int userId,int theme);
}
