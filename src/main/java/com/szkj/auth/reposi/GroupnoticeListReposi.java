package com.szkj.auth.reposi;

import com.szkj.auth.entity.GroupnoticeList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface GroupnoticeListReposi extends JpaRepository<GroupnoticeList, Long>, JpaSpecificationExecutor<GroupnoticeList> {
    @Query(value = "select * from yth_groupnotice_list WHERE notice_id=?1 and user_id=?2 limit 1", nativeQuery = true)
    GroupnoticeList findByUserId(long noticeId, Long userId);
}