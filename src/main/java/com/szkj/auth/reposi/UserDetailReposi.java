package com.szkj.auth.reposi;

import com.szkj.auth.entity.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserDetailReposi extends JpaRepository<UserDetail, Integer>, JpaSpecificationExecutor<UserDetail> {

}