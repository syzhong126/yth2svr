package com.szkj.auth.reposi;

import com.szkj.auth.entity.GroupnewsReaded;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface GroupnewsReadedReposi extends JpaRepository<GroupnewsReaded, Long>, JpaSpecificationExecutor<GroupnewsReaded> {
    @Query(value = "SELECT * FROM yth_groupnews_readed WHERE news_id=?1 AND user_id=?2 limit 1", nativeQuery = true)
    Optional<GroupnewsReaded> findOldRecInfo(long newsId, long userId);
}