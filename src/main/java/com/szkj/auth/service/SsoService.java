package com.szkj.auth.service;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.Method;
import cn.hutool.http.webservice.SoapClient;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

import com.szkj.auth.entity.GroupnewsReaded;
import com.szkj.auth.entity.GroupnoticeList;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.reposi.GroupnewsReadedReposi;
import com.szkj.auth.reposi.GroupnoticeListReposi;
import com.szkj.common.service.SQLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.HttpCookie;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class SsoService {
    @Value("${esbsvr.url}")
    private String esbSvrUrl;
    @Value("${esbsvr.appId}")
    private int esbsvrAppId;
    @Value("${esbsvr.secretKey}")
    private String esbsvrSecretKey;

    @Value("${yxt.interface.apikey:427944fe-2f66-4ea2-8c07-a5ffdfbdccad}")
    private String YXT_INTERFACE_APIKEY;        //企业大学云学堂apikey(正式)
    @Value("${yxt.interface.secretkey:afc4c03e-dfac-4de3-9766-04978a005f81}")
    private String YXT_INTERFACE_SECRETKEY;     //企业大学云学堂secretkey(正式)
    @Value("${yxt.sso.login_url_pc:https://apic1.yunxuetang.cn/el/sso}")
    private String YXT_SSO_LOGIN_URL_PC;           //企业大学云学堂单点登录地址(正式)
    @Value("${yxt.sso.login_url_app:https://api-qidac1.yunxuetang.cn/v1/users/thirdtokens}")
    private String YXT_SSO_LOGIN_URL_APP;           //企业大学云学堂单点登录地址(正式)

    @Value("${oa.sso.token_url:http://192.168.100.220:8888/seeyon/rest/token/h5SSO/cb66e82d-92b1-46a1-8060-075cdaa0eae4}")
    private String OA_SSO_TOKEN_URL;
    @Value("${oa.sso.detail_url_app:http://oa.cpih.com:8888/seeyon/H5/collaboration/index.html}")
    private String OA_SSO_DETAIL_URL_APP;

    @Value("${lmh.default.password:Lmh@0610}")
    private String LMH_DEFAULT_PASSWORD;
    @Value("${erp.sso.token_url:http://192.168.100.3:8180/Ajax/AjaxHepler.ashx}")
    private String ERP_SSO_TOKEN_URL_PC;
    @Value("${erp.sso.home_url_pc:http://192.168.100.3:8180/SSO.aspx?returnUrl=http://192.168.100.3:8060/Product/Interface/SSO/Login.aspx}")
    private String ERP_SSO_HOME_URL_PC;
    @Value("${erp.sso.token_url:http://192.168.100.3:8180/Services/AuthTokenService.asmx}")
    private String ERP_SSO_TOKEN_URL_APP;
    @Value("${erp.sso.home_url_app:http://www.fdccloud.com/workflow-micro/my56559b451c734/lists/process-list/index?kindType=5&__from=mall}")
    private String ERP_SSO_HOME_URL_APP;
    @Value("${da.sso.home_url_pc:http://192.168.100.3:8180/SSO.aspx?returnUrl=http://192.168.100.221:8080/sso/index?}")
    private String DA_SSO_HOME_URL_PC;

    @Autowired
    private SQLService sqlService;
    @Autowired
    private GroupnewsReadedReposi readedReposi;
    @Autowired
    private GroupnoticeListReposi groupnoticeListReposi;
    /**
     * 封装ESB单点登录接口
     * @param userInfo  用户信息
     * @param loginAppId 单点登录esb的appId
     * @param clientType 客户端类型 0:PC 1:APP
     * @param loginType 单点登录类型: 0:登录系统 1:待办详情
     * @param pageUrl 待办中的跳转链接(只在loginType=1时有效)
     * @return 链接地址
     * @throws Exception
     */
    private String esbSsoLogin(AuthUserInfoModel userInfo, int loginAppId, int clientType, int loginType, String pageUrl) throws Exception{
        long _timestamp= System.currentTimeMillis() / 1000;
        String _sign= SecureUtil.md5(_timestamp+ esbsvrSecretKey);
        String _url= StrUtil.format("{}/yth/sso/login/{}/{}/{}/{}/{}/{}/{}/{}",
                esbSvrUrl, loginAppId, clientType, loginType,
                userInfo.getAccount(), userInfo.getToken(),
                esbsvrAppId, _timestamp, _sign);
        JSONObject _params = new JSONObject();
        _params.putOpt("approvalUrl", pageUrl);
        String _result =  HttpRequest.get(_url)
                .method(Method.GET)
                .body(_params.toString())
                .execute()
                .body();
        JSONObject _resultObj = new JSONObject(_result);
        if (_resultObj.getInt ("code", 1)==0) {
            if (_resultObj.isNull("data")) {
                throw new Exception("单点登录服务无效的返回值: "+_resultObj.toString());
            }
            _url= _resultObj.getJSONObject("data").getStr("ssoLoginUrl");            
            return _url;
        } else {
            throw new Exception(_resultObj.getStr("msg", _resultObj.toString()));
        }
    }

    /**
     * 云学堂单点登录地址封装
     * @param account
     * @param clientType 
     * @return 链接地址
     * @throws Exception
     */
    private String ssoLoginYxt(String account, String clientType, long recId) throws Exception {
        int _salt= RandomUtil.randomInt(1,99999);
        String _url= ("pc".equalsIgnoreCase(clientType))?YXT_SSO_LOGIN_URL_PC:YXT_SSO_LOGIN_URL_APP;
        JSONObject _params= new JSONObject();
        if ("pc".equalsIgnoreCase(clientType)) {
            _params.putOpt("apikey", YXT_INTERFACE_APIKEY);
            _params.putOpt("uname", account);
            _params.putOpt("salt", _salt);
            _params.putOpt("signature", DigestUtil.sha256Hex( YXT_INTERFACE_SECRETKEY+_salt));
        } else {
            _params.putOpt("apiKey", YXT_INTERFACE_APIKEY);
            _params.putOpt("userName", account);
            _params.putOpt("salt", _salt);
            _params.putOpt("signature", DigestUtil.sha256Hex( YXT_INTERFACE_SECRETKEY+YXT_INTERFACE_APIKEY+_salt+account));
        }
        String _result= HttpRequest.post(_url)
                .body(_params.toString())
                .execute()
                .body();
        _params= JSONUtil.parseObj(_result);

        String _detailUrl="";
        if (0!=recId) {
            JSONObject _value= sqlService.customQueryOne(3033, StrUtil.format("{recId:{}}", recId));
            if (null==_value) {
                throw new Exception("无效的课程id:"+recId);
            }
            _detailUrl=("pc".equalsIgnoreCase(clientType))?_value.getStr("pageUrl") :_value.getStr("appUrl");
        }

        if ("pc".equalsIgnoreCase(clientType)) {  //PC端
            if (_params.getInt("code")!=0) throw new Exception("登录企业大学发生错误: "+_params.getStr("message"));
            _result= _params.getStr("data");
            if (!"".equals(_detailUrl)) {
                _result= _result+"&fromurl="+URLUtil.encodeAll(_detailUrl);
            }
            return _result;
        } else if ("app".equalsIgnoreCase(clientType)){  //{"error":{"key":"global.oauth.failed","message":"Oauth flow failed."}}
            if (!_params.isNull("error")) throw new Exception("登录企业大学发生错误: "+_params.getJSONObject("error").getStr("message"));
            _result= _params.getStr("url");
            if (!"".equals(_detailUrl)) {
                _result= _result+"&returnurl="+URLUtil.encodeAll(_detailUrl);
            }
            return _result;
        } else {
            throw new Exception("无效的客户端类型值:"+clientType);
        }
    }

    /**
     * 单点登录ERP系统
     * @param account
     * @param clientType
     * @return
     * @throws Exception
     */
    private String ssoLoginERP(String account, String clientType, String pageUrl) throws Exception {
        if ("pc".equalsIgnoreCase(clientType)) {
            List<HttpCookie> cookieList= HttpRequest.get(ERP_SSO_TOKEN_URL_PC+"?type=login&loginPwd="+LMH_DEFAULT_PASSWORD+"&loginName="+account)
                .method(Method.GET)
                .execute()
                .getCookies();
            String _cookieStr= "";
            for(HttpCookie _cookie: cookieList) {
                _cookieStr += _cookie.getName()+"="+_cookie.getValue()+";";            
            }
            String _result= HttpRequest.get(ERP_SSO_HOME_URL_PC)
                .method(Method.GET)
                .header("Cookie", _cookieStr)
                .execute()
                .body();
            if ("".equals(pageUrl)) {
                Pattern pattern = Pattern.compile("(http)([^\"]*)");
                Matcher matcher = pattern.matcher(_result);
                _result= matcher.find()?matcher.group():"";
            } else {
                Pattern pattern = Pattern.compile("(token=)([^\"]*)");
                Matcher matcher = pattern.matcher(_result);
                _result= matcher.find()?matcher.group():"";     //获取token
                _result= pageUrl.replaceAll("&", "[AND]")+"&"+_result;            
            }
            return _result;
        } else {
            String token= UUID.randomUUID().toString().replaceAll("-", "");
            SoapClient client = SoapClient.create(ERP_SSO_TOKEN_URL_APP)
                .setMethod("AddToken", "http://tempuri.org/")
                .setParam("tokenID", token)
                .setParam("userCode", "renb")
                .setParam("SessionID", token);
            String _result= client.send(true);
            return ERP_SSO_HOME_URL_APP+"&usercode="+account+"&token="+token;
        }
    }

/**
     * 单点登录档案系统
     * @param account
     * @param clientType
     * @return
     * @throws Exception
     */
    private String ssoLoginDA(String account, String clientType, String pageUrl) throws Exception {
        if ("pc".equalsIgnoreCase(clientType)) {
            List<HttpCookie> cookieList= HttpRequest.get(ERP_SSO_TOKEN_URL_PC+"?type=login&loginPwd="+LMH_DEFAULT_PASSWORD+"&loginName="+account)
                .method(Method.GET)
                .execute()
                .getCookies();
            String _cookieStr= "";
            for(HttpCookie _cookie: cookieList) {
                _cookieStr += _cookie.getName()+"="+_cookie.getValue()+";";            
            }
            String _result= HttpRequest.get(DA_SSO_HOME_URL_PC)
                .method(Method.GET)
                .header("Cookie", _cookieStr)
                .execute()
                .body();
            if ("".equals(pageUrl)) {
                Pattern pattern = Pattern.compile("(http)([^\"]*)");
                Matcher matcher = pattern.matcher(_result);
                _result= matcher.find()?matcher.group():"";
                _result= _result.replaceAll("&amp;", "");
            } else {
                Pattern pattern = Pattern.compile("(token=)([^\"]*)");
                Matcher matcher = pattern.matcher(_result);
                _result= matcher.find()?matcher.group():"";     //获取token
                _result= pageUrl.replaceAll("&", "[AND]")+"&"+_result;
            }
            return _result;
        } else {
            return "";
        }
    }
    /**
     * OA APP详情跳转
     * @param pageUrl
     * @return
     */
    private String getOADetailUrl(String pageUrl) {
        String _result= HttpRequest.get(OA_SSO_TOKEN_URL).execute().body();
        JSONObject _jsonObject= new JSONObject(_result);
        String _token=_jsonObject.getStr("id");
        return OA_SSO_DETAIL_URL_APP+"?token="+_token+"&timestr="+System.currentTimeMillis()+"&html="+URLUtil.encodeAll(pageUrl);
        // return OA_SSO_DETAIL_URL_APP+"?token="+_token+"&timestr="+System.currentTimeMillis()+"&html="+URLEncoder.encode(pageUrl, "utf-8");
    }

    
    /**
     * 根据系统编码查询系统在esb中的定义 集团新闻和集团公告实际为SystemOA
     * @param systemCode
     * @return
     * @throws Exception
     */
    private JSONObject getParamInfo(String systemCode) throws Exception {
        switch (systemCode.toLowerCase()) {
            case "groupnews": systemCode= "SystemOA"; break;
            case "groupnotify": systemCode= "SystemOA"; break;
        }
        JSONObject _result = sqlService.customQueryOne(6, StrUtil.format("{sourse:{}}", systemCode));
        if (null==_result) {
            throw new Exception("无效的系统编码");
        }
        return _result;
    }
    
    /**
     * 查询待办详情链接地址
     * @param systemCode 系统来源 见 sys_sourse 表
     * @param recId 记录id
     * @return
     * @throws Exception
     */
    private String getPageDetailUrl(String systemCode, Long recId, int iClientType) throws Exception {
        String _result= "";
        JSONObject _value= sqlService.customQueryOne(3020, StrUtil.format("{recId:{}}", recId));

        switch (systemCode.toLowerCase()) {
            case "groupnews": _result= "method=newsSsoOA&newsId="+recId; break;
            case "groupnotify": _result= "method=bulSsoOA&bulId="+recId; break;
            default:
                _result= (iClientType==0)?_value.getStr("pageUrl", ""):_value.getStr("appUrl", "");
                if (!_result.contains("?") && !_result.contains("http"))  //如果待办详情url是完整URL,需要转码
                    _result = URLUtil.encode(_result);
                break;
        }
        return _result;
    }

    // TODO: 2022/5/25 暂未实现, 假返回
    //流程发起页
    public String getAppStartUrl(AuthUserInfoModel userInfo,  String clientType, String systemCode) throws Exception {
        String _ssoLoginUrl= "";
        JSONObject _params= getParamInfo(systemCode);
        int _appId= _params.getInt("appId");
        int iClientType;
        if ("pc".equalsIgnoreCase(clientType)) {  //PC端
            iClientType = 0;
        } else if ("app".equalsIgnoreCase(clientType)){ 
            iClientType = 1;
        } else {
            throw new Exception("无效的客户端类型值:" + clientType);
        }
        _ssoLoginUrl= esbSsoLogin(userInfo, _appId, iClientType, 3, "");
        return _ssoLoginUrl;
    }

    //TODO: 暂时使用的万能token
    public String getSsoLoginUrl(AuthUserInfoModel userInfo, String clientType, String systemCode, String returnUrl) throws Exception {
        String _ssoLoginUrl= "";
        JSONObject _params= getParamInfo(systemCode);
        int extraInfo= _params.getInt("extraInfo", 0);      //extraInfo 0 不带附属信息 1 带附属信息 2替换为附属信息(用于跳转一体化老系统)
        int _appId= _params.getInt("appId", 0);
        if (_appId<=0) throw new Exception("系统未定义单点登录方式");
        int iClientType;
        if ("pc".equalsIgnoreCase(clientType)) {  //PC端
            iClientType = 0;
        } else if ("app".equalsIgnoreCase(clientType)){
            iClientType = 1;
        } else {
            throw new Exception("无效的客户端类型值:" + clientType);
        }
        switch (systemCode.toLowerCase()) {
            case "systemyxt":       //企业大学(云学堂)单独处理
                _ssoLoginUrl= ssoLoginYxt(userInfo.getAccount(), clientType, 0);
                break;
            case "systemerp":       //ERP 老门户模拟登录 单独处理
                _ssoLoginUrl= ssoLoginERP(userInfo.getAccount(), clientType, "");
                break;
            case "systemda":       //DA 老门户模拟登录 单独处理
                _ssoLoginUrl= ssoLoginDA(userInfo.getAccount(), clientType, "");
                break;
            default:
                _ssoLoginUrl = esbSsoLogin(userInfo, _appId, iClientType, 0, "");
                if (extraInfo==1) {  //如果需要带上一体化token
                    _ssoLoginUrl+= "&ythAccount="+userInfo.getAccount();
                    _ssoLoginUrl+= "&ythToken="+userInfo.getToken();
                    _ssoLoginUrl+= "&ythOrgId="+userInfo.getOrgId();
                    _ssoLoginUrl+= "&ctype="+userInfo.getCtype();
                    _ssoLoginUrl+= "&sysId="+userInfo.getSysId();
                } else if (extraInfo==2){  //如果是老一体化应用
                    _ssoLoginUrl= _ssoLoginUrl.substring(0, _ssoLoginUrl.indexOf("?"));
                    _ssoLoginUrl+= "?account="+userInfo.getAccount();
                    _ssoLoginUrl+= "&token="+userInfo.getToken();
                    _ssoLoginUrl+= "&orgId="+userInfo.getOrgId();
                    _ssoLoginUrl+= "&ctype="+userInfo.getCtype();
                }
                //替换为万能key 临时处理方式           
                String GoldKey = (iClientType==0 || _appId==109 || _appId==106)?"F135A173A9C603C2A914FDAB4A6F0D55":"F1354173A9C623C2A714FDABBA6F0D77";
                if (extraInfo!=2 && (_appId>=100)){    //老系统才需要替换为万能Key
                    String[] _temp=_ssoLoginUrl.split("&");
                    _ssoLoginUrl="";
                    for(String _str: _temp) {
                        if ("".equals(_str)) continue;
                        if ("token=".equals(_str.substring(0,6))) {                           
                            _str= "token="+GoldKey;
                        }
                        _ssoLoginUrl+="".equals(_ssoLoginUrl)?_str:"&"+_str;
                    }
                }
                //替换为万能key
                break;
        }
        //OA移动端必须加上html_src才能正常退回
        if ("SystemOA".equalsIgnoreCase(systemCode) && (1==iClientType) && !"".equals(returnUrl)) {
            _ssoLoginUrl += "&html_src="+ Base64.encode(returnUrl);
        }
        return _ssoLoginUrl;
    }
    /**
     * 获取待办详情页地址
     * @param userInfo
     * @param clientType
     * @param systemCode
     * @param recId
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public String getPiDetailUrl(AuthUserInfoModel userInfo, String clientType, String systemCode, long recId) throws Exception {
        JSONObject _params= getParamInfo(systemCode);
        if (null==_params) {
            throw new Exception("无效的系统编码:"+systemCode);
        }
        int extraInfo= _params.getInt("extraInfo", 0);      //extraInfo 0 不带附属信息 1 带附属信息 2替换为附属信息(用于跳转一体化老系统)
        int _appId= _params.getInt("appId", 0);
        int iClientType;
        if ("pc".equalsIgnoreCase(clientType)) {  //PC端
            iClientType = 0;
        } else if ("app".equalsIgnoreCase(clientType)){
            iClientType = 1;
        } else {
            throw new Exception("无效的客户端类型值:" + clientType);
        }      
        //获取详情链接
        String pageUrl = getPageDetailUrl(systemCode.toLowerCase(), recId, iClientType);
        //获取带用户和token信息的详情链接
        String _ssoLoginUrl= "";
        if (iClientType==1 && "SystemOA".equalsIgnoreCase(systemCode)) {  //OA的APP版特殊处理
            _ssoLoginUrl= getOADetailUrl(pageUrl);
        } if (iClientType==0 && "SystemERP".equalsIgnoreCase(systemCode)) { //ERP的PC版特殊处理
            _ssoLoginUrl= ssoLoginERP(userInfo.getAccount(), clientType, pageUrl);
        } else {
            _ssoLoginUrl= esbSsoLogin(userInfo, _appId, iClientType, 1, pageUrl);
        }

        //集团新闻和集团公告需要记录阅读状态
        if ("groupnews".equalsIgnoreCase(systemCode)){
            GroupnewsReaded _bean= readedReposi.findOldRecInfo(recId, userInfo.getUserId())
                    .orElse(new GroupnewsReaded());
            _bean.setNewsId(recId)
                    .setUserId(Long.valueOf(userInfo.getUserId()))
                    .setGmtModify(new Date());
            readedReposi.save(_bean);
        } else if ("groupnotify".equalsIgnoreCase(systemCode)){
            GroupnoticeList _bean= groupnoticeListReposi.findByUserId(recId, Long.valueOf(userInfo.getUserId()));
            if (null!=_bean) {
                _bean.setReaded(true);
                groupnoticeListReposi.save(_bean);
            }
        }

        //替换为万能key 临时处理方式   
        if (extraInfo==1) {  //如果需要带上一体化token
            _ssoLoginUrl+= "&ythAccount="+userInfo.getAccount();
            _ssoLoginUrl+= "&ythToken="+userInfo.getToken();
            _ssoLoginUrl+= "&ythOrgId="+userInfo.getOrgId();
            _ssoLoginUrl+= "&ctype="+userInfo.getCtype();
            _ssoLoginUrl+= "&sysId="+userInfo.getSysId();
        } else if (extraInfo==2){  //如果是老一体化应用
            _ssoLoginUrl= _ssoLoginUrl.substring(0, _ssoLoginUrl.indexOf("?"));
            _ssoLoginUrl+= "?account="+userInfo.getAccount();
            _ssoLoginUrl+= "&token="+userInfo.getToken();
            _ssoLoginUrl+= "&orgId="+userInfo.getOrgId();
            _ssoLoginUrl+= "&ctype="+userInfo.getCtype();
            _ssoLoginUrl+= "&id="+recId;
        }

        String GoldKey = (iClientType==0 || _appId==109 || _appId==106)?"F135A173A9C603C2A914FDAB4A6F0D55":"F1354173A9C623C2A714FDABBA6F0D77";
        if (extraInfo!=2){
            String[] _temp=_ssoLoginUrl.split("&");
            _ssoLoginUrl="";
            for(String _str: _temp) {
                if ("".equals(_str)) continue;
                if ("token=".equals(_str.substring(0,6))) {
                    _str= ("systemerp".equals(systemCode.toLowerCase()))?_str:"token="+GoldKey;
                }
                _ssoLoginUrl+="".equals(_ssoLoginUrl)?_str:"&"+_str;
            }
        }
        //替换为万能key

        return _ssoLoginUrl;
    }

    public String getDetailUrl(AuthUserInfoModel userInfo, String clientType, String systemCode, long recId) throws Exception {
        String _result= "";
        switch (systemCode.toLowerCase()) {
            case "systemyxt": 
                _result= ssoLoginYxt(userInfo.getAccount(), clientType, recId);
                break;
            default : 
                _result= getPiDetailUrl(userInfo, clientType, systemCode, recId);
                break;
        }        
        return _result;
    }

}
