package com.szkj.auth.service;

import com.szkj.auth.reposi.UserReposi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    @Autowired
    private UserReposi userReposi;

    @Transactional(rollbackFor = Exception.class)
    public void clearChangePwd(String account) {
        userReposi.clearChangePwd(account);
    };

    @Transactional(rollbackFor = Exception.class)
    public void unregist(String account) {
        userReposi.unregist(account);
    };

    @Transactional(rollbackFor = Exception.class)
    public void changeTheme(int userId,int theme) {
        userReposi.changeTheme(userId,theme);
    };
}
