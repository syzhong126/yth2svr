package com.szkj.auth.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;

import cn.hutool.db.nosql.redis.RedisDS;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.service.AliSMSSerice;
import com.szkj.common.service.SQLService;
import com.szkj.common.util.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;

@Log4j2
@Service
public class AuthService {
    @Value("${authsvc.url}")
    private String authSvrUrl;
    @Value("${authsvc.rsa.publickey}")
    private String authPublicKey;

    @Value("${authsvc.token.expire.time:86400}")
    private int expireTime;

    @Autowired
    private SQLService sqlService;
    @Autowired
    private AliSMSSerice aliSMSSerice;
    @Autowired
    private UserService userService;

    private static final String redisPrefix = "yth2svr_";

    /**
     * 鉴权服务接口返回值解析
     * @param jsonStr
     * @return
     * @throws Exception
     */
    private Object getJSONData(String jsonStr) throws Exception{
        try {
            JSONObject _jsonObj= new JSONObject(jsonStr);
            if (_jsonObj.getInt("code")==0) {
                return _jsonObj.get("data");
            } else {
                if (_jsonObj.containsKey("msg")) {
                    throw new Exception(_jsonObj.getStr("msg"));
                } else {
                    throw new Exception("解析接口返回值错误:"+jsonStr);
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    private String rsaEncrypt(String str, String publicKey) throws Exception {
        byte[] _decoded = Base64.decodeBase64(publicKey);
        RSAPublicKey _pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(_decoded));
        Cipher _cipher = Cipher.getInstance("RSA");
        _cipher.init(1, _pubKey);
        String _outStr = Base64.encodeBase64String(_cipher.doFinal(str.getBytes("UTF-8")));
        return _outStr;
    }


    /**
     * 存储account token
     * @param account
     * @param token
     * @param expireTime
     */
    private void saveRedisToken(String account, String token, JSONObject params, int expireTime) {
        Jedis _jedis = RedisUtil.getJedis();
        try {
            String _accountKey = account+"_"+params.getStr("sysId")+"_"+params.getStr("ctype");
            String _oldToken = _jedis.hget(redisPrefix+"tokens", _accountKey);
            if (_oldToken!=null && _jedis.exists(_oldToken)) {
                _jedis.del(_oldToken);
            }
            _jedis.set(redisPrefix+token, params.toString());
            _jedis.hset(redisPrefix+"tokens", _accountKey, token);
            _jedis.expire(redisPrefix+token, expireTime);
        }finally{
            _jedis.close();
        }
    }
    public void deleteRedisToken(String token) throws Exception {
        Jedis _jedis = RedisUtil.getJedis();
        try {
            JSONObject params = getLoginInfo(token);
            String _accountKey = params.getStr("account")+"_"+params.getStr("sysId")+"_"+params.getStr("ctype");
            _jedis.del(redisPrefix+token);
            _jedis.hdel(redisPrefix+"tokens", _accountKey);
        }finally{
            _jedis.close();
        }
    }

    /**
     * 根据token获取用户登录信息, 建议使用 getUserInfoModel
     * @param token
     * @return
     */
    @Deprecated
    public JSONObject getLoginInfo(String token) throws Exception {
        Jedis _jedis = RedisUtil.getJedis();
        try {
            String _userInfo = _jedis.get(redisPrefix + token);
            if ("".equals(_userInfo)) {
                throw new Exception("用户授权已失效, 请重新登录");
            }
            return new JSONObject(_userInfo);
        } finally {
            _jedis.close();
        }
    }
    /**
     * 检测token是否有效, 如果有效 延长token有效期
     * @param token
     * @return
     */
    public static Boolean checkToken(String token) {
        Jedis _jedis= RedisUtil.getJedis();
        try{
            boolean _result= StrUtil.isNotBlank(_jedis.get(redisPrefix+token));
            if (_result) {
                _jedis.expire(redisPrefix+token, 86400);  //如果验证成功, 重置token有效期
            }
            return _result;
        } finally {
            _jedis.close();
        }
    }
    /**
     * 查询用户信息
     * @param account
     * @param token
     * @param sysId
     * @returngetUserInfo
     * @throws Exception
     */
    public JSONObject getUserInfo(String account, String token, int sysId, int ctype) throws Exception {
        JSONObject _jsonObject;
        _jsonObject= sqlService.customQueryOne(1, StrUtil.format("{account:{}}", account));
        if (_jsonObject.isEmpty()) {
            throw new Exception("找不到用户信息:"+token);
        }
        _jsonObject.remove("password");
        int _userId = _jsonObject.getInt("userId");
        _jsonObject.putOpt("token", token);
        _jsonObject.putOpt("sysId", sysId);
        _jsonObject.putOpt("ctype", ctype);
        //查询组织信息
        JSONArray _jsonArray= sqlService.customQueryList(2, StrUtil.format("{userId:{}}", _userId));
        _jsonObject.putOpt("orgInfos", _jsonArray);
        int _orgId= 0;
        for(Object _obj: _jsonArray) {
            JSONObject _temp = (JSONObject) _obj;
            if (_temp.getInt("isDefault")==1) {
                _orgId= _temp.getInt("orgId");
                break;
            } else {
                continue;
            }
        }
        _jsonObject.putOpt("orgId", _orgId);
        //查询角色信息
        _jsonArray= sqlService.customQueryList(3, StrUtil.format("{userId:{}, sysId:{}}", _userId, sysId));
        _jsonObject.putOpt("roleInfos", _jsonArray);
        //查询扩展信息
        // JSONObject _json= sqlService.customQueryOne(4, StrUtil.format("{userId:{}}", _userId));
        // _jsonObject.putOpt("detailInfos", _json);

        return _jsonObject;
    }

    public JSONObject getUserInfo(String token) throws Exception {
        return getLoginInfo(token);
    }

    /**
     * 从请求中获取用户信息
     * @param request
     * @return
     */
    public AuthUserInfoModel getUserInfoModel(HttpServletRequest request) {
        Jedis _jedis = RedisUtil.getJedis();
        try {
            String token= request.getHeader("token");
            String _userInfo = _jedis.get(redisPrefix+token);
            if (StrUtil.isNotBlank(_userInfo)) {
                return JSONUtil.toBean(JSONUtil.toJsonStr(getLoginInfo(token)), AuthUserInfoModel.class);
            } else{
                return new AuthUserInfoModel();
            }
        } catch (Exception e) {
            return new AuthUserInfoModel();
        } finally {
            _jedis.close();
        }
    }

    /**
     * 登录
     * @param params
     * @return
     */
    public JSONObject doLogin(JSONObject params) throws Exception{
        String _result =  HttpRequest.post(authSvrUrl+"/login")
                .body(params.toString())
                .execute()
                .body();
        JSONObject _jsonObject = new JSONObject(_result);
        if (_jsonObject.getInt("code").equals(0)) {
            String _account= _jsonObject.getByPath("data.account").toString();
            String _token= _jsonObject.getByPath("data.access_token").toString();
            int _sysId= params.getInt("sysId");
            int _ctype= params.getInt("ctype");
            _jsonObject= getUserInfo(_account, _token, _sysId, _ctype);
            saveRedisToken(_account, _token,  _jsonObject, expireTime);
            return _jsonObject;
        } else {
            throw new Exception(_jsonObject.getStr("msg"));
        }
    }

    /**
     * 用户注销
     * @param token
     * @return
     * @throws Exception
     */
    public String doLogout(String token) throws Exception{
        JSONObject _jsonObject= getLoginInfo(token);
        deleteRedisToken(token);
        String _interfaceUrl = StrUtil.format("{}/logout/{}/{}/{}",authSvrUrl, token, _jsonObject.getStr("sysId"), _jsonObject.getStr("ctype"));
        String _result =  HttpRequest.get(_interfaceUrl)
                .execute()
                .body();
        return _result;
    }

    /**
     * 给用户发送验证码
     * @param params
     * @return
     * @throws Exception
     */
    public String sendVerifyCode(JSONObject params) throws Exception {
        String _account= params.getStr("account");
        String _mobile= params.getStr("mobile");
        JSONObject _userInfo= sqlService.customQueryOne(1, "{account:"+_account+"}");
        if (_userInfo.isEmpty()) {
            throw new Exception(StrUtil.format("无效的用户名{}", _account));
        }
        if (!_mobile.equals(_userInfo.getStr("mobile"))) {
            throw new Exception(StrUtil.format("手机号{}与账户{}的绑定手机号不符", _mobile, _account));
        }
        String _verifyCode= String.valueOf(RandomUtil.randomInt(1000,9999));
        String _result= aliSMSSerice.sendVerifyCode(_mobile, _verifyCode);
        Jedis _jedis = RedisUtil.getJedis();
        try{
            if ("OK".equals(_result)) {
                _jedis.set(redisPrefix+"verifyCode_"+_mobile, _verifyCode);
                _jedis.expire(redisPrefix+"verifyCode_"+_mobile, 120);
            } else {
                throw new Exception("发送短信时发生错误:"+_result);
            }
            return _result;
        }finally{
            _jedis.close();
        }
    }

    /**
     * 从鉴权服务器获取超级用户token
     * @param account
     * @param rnum
     * @return
     * @throws Exception
     */
    public String getSuperToken(String account, long rnum) throws Exception {
        try{
            int _vnum = RandomUtil.randomInt(100,999);
            String _timeStr= DateUtil.format(DateUtil.date(), "yyyyMMddHH");
            String _password = StrUtil.format("Xdzcroot@)20|{}|{}", _timeStr, _vnum);
            _password= rsaEncrypt(_password, authPublicKey);
            JSONObject _params= JSONUtil.createObj();
            _params.putOpt("account", account);
            _params.putOpt("password", _password);
            _params.putOpt("rnum", rnum);
            String _result =  HttpRequest.post(authSvrUrl+"/slogin")
                    .body(_params.toString())
                    .execute()
                    .body();
            JSONObject _resultObj = new JSONObject(getJSONData(_result));
            return _resultObj.getStr("stoken");
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("获取超级用户时发生错误:"+e.getMessage());
        }
    }

    /**
     * 重置密码
     * @param params
     * @throws Exception
     */
    public void resetPassword(JSONObject params) throws Exception {
        String _account= params.getStr("account");
        String _mobile= params.getStr("mobile");
        String _verifyCode= params.getStr("verifyCode");
        Jedis _jedis = RedisUtil.getJedis();
        try{
            JSONObject _userInfo= sqlService.customQueryOne(1, "{account:"+_account+"}");
            if (_userInfo.isEmpty()) {
                throw new Exception(StrUtil.format("无效的用户名{}", _account));
            }
            if (!_userInfo.getStr("mobile").equals(_mobile)) {
                throw new Exception(StrUtil.format("手机号{}与账户{}的绑定手机号不符", _mobile, _account));
            }
            if (!_verifyCode.equals(_jedis.get(redisPrefix+"verifyCode_"+_mobile))) {
                throw new Exception("重置密码时发生错误:无效的验证码");
            }
            long _rnum = RandomUtil.randomLong(10000000000L,999999999999L);
            String _sToken= getSuperToken(_account, _rnum);
            String _newPwd=RandomUtil.randomString("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", 7)
                    +RandomUtil.randomString("!@#$%^&*", 1);
            String _resetUrl = StrUtil.format("{}/rstpwd/{}/{}/{}/{}", authSvrUrl, _account, _sToken, SecureUtil.md5(_newPwd), _rnum);
            String _result =  HttpRequest.get(_resetUrl)
                    .execute()
                    .body();
            _result= _result.substring(6, _result.length()-1); //原返回值为 jsonp({code:0,...})
            if("".equals(getJSONData(_result))) {
                userService.clearChangePwd(_account);
                _result= aliSMSSerice.sendNewPassword(_mobile, _newPwd);
                if (!"OK".equals(_result)) {
                    throw new Exception("发送短信时发生错误:"+_result);
                }
            }
        }finally{
            _jedis.close();
        }
    }

    public void modifyPassword(String token, JSONObject params) throws Exception {
        JSONObject _loginInfo = getLoginInfo(token);
        String _oldPwd= params.getStr("oldPwd");
        String _newPwd= params.getStr("newPwd");
        String _account= params.getStr("account");;
        int _ctype= _loginInfo.getInt("ctype");
        if (!_account.equals(_loginInfo.getStr("account"))) {
            throw new Exception("用户身份验证错误");
        }
        if (StrUtil.isEmpty(_oldPwd) || StrUtil.isBlank(_newPwd)) {
            throw new Exception("原密码和新密码都不能为空");
        }
        JSONObject _userInfo= sqlService.customQueryOne(1, "{account:"+_account+"}");
        if (!_oldPwd.equals(_userInfo.getStr("password"))) {
            throw new Exception("原密码错误");
        }
        String _resetUrl = StrUtil.format("{}/chngpwd/{}/{}/{}/{}/{}", authSvrUrl, token, _account, _ctype, _oldPwd, _newPwd);
        String _result =  HttpRequest.get(_resetUrl)
                .execute()
                .body();
        _result= _result.substring(6, _result.length()-1); //原返回值为 jsonp({code:0,...})
        if("".equals(getJSONData(_result))) {
            userService.clearChangePwd(_account);
        }
    }

    /**
     * 获取菜单资源权限
     * @param token
     * @param params
     * @return
     * @throws Exception
     */
    public Object getMenu(String token, JSONObject params) throws Exception {
        JSONObject _loginInfo = getLoginInfo(token);
        String _orgId= params.getStr("orgId");
        if (StrUtil.isEmpty(_orgId)) {
            throw new Exception("缺少必要的参数: orgId");
        }
        int _sysId= _loginInfo.getInt("sysId");
        int _ctype= _loginInfo.getInt("ctype");
        String _resetUrl = StrUtil.format("{}/menu/{}/{}/{}?orgid={}", authSvrUrl, token, _sysId, _ctype, _orgId);
        String _result =  HttpRequest.get(_resetUrl)
                .execute()
                .body();
        _result= _result.substring(6, _result.length()-1); //原返回值为 jsonp({code:0,...})
        return getJSONData(_result);
    }

    /**
     * 获取资源操作权限
     * @param token
     * @param params
     * @return
     * @throws Exception
     */
    public JSONObject getMenuAction(String token, JSONObject params) throws Exception {
        JSONObject _loginInfo = getLoginInfo(token);
        String _authKey= params.getStr("resKey");
        int _sysId= _loginInfo.getInt("sysId");
        int _ctype= _loginInfo.getInt("ctype");
        String _resetUrl = StrUtil.format("{}/pgrole/{}/{}/{}/{}", authSvrUrl, token, _authKey, _sysId, _ctype);
        String _result =  HttpRequest.get(_resetUrl)
                .execute()
                .body();
        _result= _result.substring(6, _result.length()-1); //原返回值为 jsonp({code:0,...})
        return new JSONObject(getJSONData(_result));
    }

}
