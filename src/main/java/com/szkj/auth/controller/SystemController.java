package com.szkj.auth.controller;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.Method;
import cn.hutool.http.webservice.SoapClient;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.AuthService;
import com.szkj.auth.service.SsoService;
import com.szkj.auth.service.UserService;
import com.szkj.common.service.AliSMSSerice;
import com.szkj.common.service.SQLService;
import com.szkj.common.util.RedisUtil;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author xiegl
 */
@Component
@RestController
@CrossOrigin("*")
@RequestMapping(value="/system")
@Slf4j
public class SystemController {
    @Value("${authsvc.url}")
    private String authSvrUrl;

    @Autowired
    private AuthService authService;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private SQLService sqlService;
    @Autowired
    private SsoService ssoService;
    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public String doLogin(@RequestBody JSONObject body) throws Exception {
        return RespUtil.success("成功", authService.doLogin(body));
    }

    @PostMapping("/logout")
    public String doLogout(HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        return authService.doLogout(_token);
    }

    @PostMapping("/unregist")
    public String unregist(HttpServletRequest request, @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        String _token = request.getHeader("token");
        userService.unregist(userInfo.getAccount());
        authService.doLogout(_token);
        return RespUtil.success("成功", null);
    }

    @PostMapping("/sendVerifyCode")
    public String doSendCode(@RequestBody JSONObject body) throws Exception {
        authService.sendVerifyCode(body);
        return RespUtil.success("成功", null);
    }

    @PostMapping("/resetPwd")
    public String doRestPwd(@RequestBody JSONObject body) throws Exception {
        authService.resetPassword(body);
        return RespUtil.success("成功", null);
    }

    @PostMapping("/modifyPwd")
    public String doModifyPwd(HttpServletRequest request, @RequestBody JSONObject body) throws Exception {
        String _token = request.getHeader("token");
        authService.modifyPassword(_token, body);
        return RespUtil.success("成功", null);
    }

    @PostMapping("/menu")
    public String doGetMenu(HttpServletRequest request, @RequestBody JSONObject body) throws Exception {
//    public String doGetMenu(HttpServletRequest request) throws Exception {
//        JSONObject body = new JSONObject(request.getAttribute("body"));
        String _token = request.getHeader("token");
        return RespUtil.success("成功", authService.getMenu(_token, body));
    }

    @PostMapping("/menu/action")
    public String doGetMenuAction(HttpServletRequest request, @RequestBody JSONObject body) throws Exception {
        String _token = request.getHeader("token");
        JSONObject _resultObj= authService.getMenuAction(_token, body);
        return RespUtil.success("成功", _resultObj);
    }

    @PostMapping("/user")
    public String doGetUserInfo(@ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        JSONObject _resultObj= authService.getUserInfo(userInfo.getAccount(), userInfo.getToken(), userInfo.getSysId(), userInfo.getCtype());
        return RespUtil.success("成功", _resultObj);
    }

    @PostMapping("/checkToken")
    public String doCheckToken(HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        return RespUtil.success("成功", null);
    }

    @Autowired
    private AliSMSSerice aliSMSSerice;
    @GetMapping("/test2")
    public String test(HttpServletRequest request, @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        //return aliSMSSerice.sendVerifyCode("13951654237", "1234");
        Jedis jedis= RedisUtil.getJedis();
        try{
            jedis.set("test", "abcd1234啊");
            jedis.expire("test", 600);
            return jedis.get("test");
        }finally {
            jedis.close();
        }


        //return "111";
        //return userInfo.toString();
//        String _token = request.getHeader("token");
//        try{
//            //return sqlService.customQueryOne(1, "{account:xiegl}").toString();
//            return authService.getLoginInfo(_token).toString();
//        }catch(Exception e){
//            return e.getMessage();
//        }
    }
    @RequestMapping(path="/test", method={RequestMethod.GET, RequestMethod.POST})
    // public String test1(@RequestParam("files[]") MultipartFile[] uploadFile) throws Exception {
    public String test1(@ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
//        throw new Exception("aaaa");
//        String files="";
//        for(MultipartFile _file: uploadFile) {
//            files += " "+_file.getOriginalFilename();
//        }
        JSONArray jsonArray= new JSONArray();
        jsonArray.add(JSONUtil.createObj().putOpt("sysCode", "SystemOA").putOpt("clientType", "pc").putOpt("recId", -1));

        return "";
    }


}
