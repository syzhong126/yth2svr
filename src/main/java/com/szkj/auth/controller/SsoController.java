package com.szkj.auth.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.json.JSONObject;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.SsoService;
import com.szkj.common.util.RespUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Log4j2
@CrossOrigin("*")
@RestController
public class SsoController {
    @Autowired
    private SsoService ssoService;

    /**
     * 单点登录
     * @param clientType
     * @param sysCode
     * @param userInfo
     * @return
     * @throws Exception
     */
    @PostMapping("/sso/{clientType}/{sysCode}")
    public String doGetSsoUrl(@PathVariable String clientType, @PathVariable String sysCode,
                              @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception{
        String _url= ssoService.getSsoLoginUrl(userInfo, clientType, sysCode, "");
        System.out.println(StrUtil.format("单点登录{}/{}:{}", clientType, sysCode, _url));
        return RespUtil.success("成功", _url);
    }

    @PostMapping("/sso")
    public String doGetSsoUrl(@RequestBody JSONObject body,
                              @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception{
        String clientType= body.getStr("clientType");
        String sysCode= body.getStr("systemCode");
        String returnUrl= body.getStr("returnUrl");
        String _url= ssoService.getSsoLoginUrl(userInfo, clientType, sysCode, returnUrl);
        System.out.println(StrUtil.format("单点登录{}/{}:{}", clientType, sysCode, _url));
        return RespUtil.success("成功", _url);
    }

    /**
     * 详情页
     * @param clientType
     * @param sysCode
     * @param recId
     * @param userInfo
     * @return
     * @throws Exception
     */
    @PostMapping("/sso/detail")
    public String doGetDetaulUrl(@RequestBody JSONObject body,
                                 @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception{
        String clientType= body.getStr("clientType");
        String sysCode= body.getStr("systemCode");
        String returnUrl= body.getStr("returnUrl");
        Long recId= body.getLong("recId");
        String _url= ssoService.getDetailUrl(userInfo, clientType, sysCode, recId);
        if (StrUtil.isNotEmpty(returnUrl)) {
            _url+="&html_src="+URLUtil.encodeAll(returnUrl);
        }
        return RespUtil.success("成功", _url);
    }

//    @PostMapping("/groupnews/detail")
//    public String doGetGroupnewsDetaulUrl(@RequestBody JSONObject body,
//                                 @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception{
//        String clientType= body.getStr("clientType");
//        Long recId= body.getLong("recId");
//        String _url= ssoService.getDetailUrl(userInfo, clientType, "groupnews", recId);
//        System.out.println(StrUtil.format("查看详情{}/{}:{}", clientType, "groupnews", _url));
//        return RespUtil.success("成功", _url);
//    }
//
//    @PostMapping("/groupnotify/detail")
//    public String doGetGroupnotifyDetaulUrl(@RequestBody JSONObject body,
//                                          @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception{
//        String clientType= body.getStr("clientType");
//        Long recId= body.getLong("recId");
//        String _url= ssoService.getDetailUrl(userInfo, clientType, "groupnotify", recId);
//        System.out.println(StrUtil.format("查看详情{}/{}:{}", clientType, "groupnotify", _url));
//        return RespUtil.success("成功", _url);
//    }

    @PostMapping("/sso/process")
    public String doGetAppStartUrl(@RequestBody JSONObject body,
                                   @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception{
        String clientType= body.getStr("clientType");
        String sysCode= body.getStr("systemCode");
        String _url= ssoService.getAppStartUrl(userInfo, clientType, sysCode);
        System.out.println(StrUtil.format("发起流程页面{}/{}:{}", clientType, sysCode, _url));
        return RespUtil.success("成功", _url);
    }

}
