package com.szkj.auth.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthUserInfoModel {
    private int userId;
    private String account;
    private String userName;
    private String token;
    private String mobile;
    private int changePwd;
    private int ctype;
    private int sysId;
    private int orgId;
}
