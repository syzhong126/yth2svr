package com.szkj.auth.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DataServerRequestModel {
    private int sqlId = -1;
    private String params = "";
    private String params2 = "";
    private String orderby = "";
    private int pageNum = 1;
    private int pagerows = 200;

    public DataServerRequestModel(int sqlId, String params, String[] params2, String orderby, int pageNum, int pagerows) {
        String strParams2 = "";
        for(String str: params2) {
            strParams2 += "".equals(strParams2)?str:","+str;
        }
        this.sqlId = sqlId;
        this.params = params;
        this.params2 = "{"+strParams2+"}";
        this.orderby = orderby;
        this.pageNum = pageNum;
        this.pagerows = pagerows;
    }

}
