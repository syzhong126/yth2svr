package com.szkj.auth.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 集团新闻已读记录
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_groupnews_readed")
public class GroupnewsReaded implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 公告id
     */
    @Column(name = "news_id", nullable = false)
    private Long newsId;

    /**
     * 用户id
     */
    @Column(name = "user_id", nullable = false)
    private Long userId;

    /**
     * 创建时间
     */
    @Column(name = "gmt_created", nullable = false)
    private Date gmtCreated;

    /**
     * 修改时间
     */
    @Column(name = "gmt_modify", nullable = false)
    private Date gmtModify;

    public GroupnewsReaded(){
        this.setGmtCreated(new Date())
            .setGmtModify(new Date());
    }
}
