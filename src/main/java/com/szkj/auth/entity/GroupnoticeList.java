package com.szkj.auth.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 集团公告列表 数据来源:OA
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_groupnotice_list")
public class GroupnoticeList implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 用户id
     */
    @Column(name = "user_id", nullable = false)
    private Long userId;

    /**
     * 公告id
     */
    @Column(name = "notice_id", nullable = false)
    private Long noticeId;

    /**
     * 类型id
     */
    @Column(name = "type_id", nullable = false)
    private Long typeId;

    /**
     * 标题
     */
    @Column(name = "title", nullable = false)
    private String title;

    /**
     * 发布时间
     */
    @Column(name = "publish_date", nullable = false)
    private Date publishDate;

    /**
     * 已读标记
     */
    @Column(name = "is_readed", nullable = false)
    private Boolean readed = Boolean.FALSE;

    /**
     * 创建时间
     */
    @Column(name = "gmt_created", nullable = false)
    private Date gmtCreated;

    /**
     * 修改时间
     */
    @Column(name = "gmt_modify", nullable = false)
    private Date gmtModify;

}
