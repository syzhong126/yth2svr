package com.szkj.auth.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用户表，管理员表
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @Id
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    /**
     * 0 鉴权服务用户，1 平台用户
     */
    @Column(name = "user_type")
    private Integer userType = 1;

    /**
     * 登录账号
     */
    @Column(name = "account", nullable = false)
    private String account;

    /**
     * 登录密码
     */
    @Column(name = "password", nullable = false)
    private String password;

    /**
     * 手机号码
     */
    @Column(name = "mobile")
    private String mobile;

    /**
     * 删除状态，1：删除，0：正常
     */
    @Column(name = "is_del", nullable = false)
    private Boolean del = Boolean.FALSE;

    /**
     * 用户真实姓名
     */
    @Column(name = "user_name")
    private String userName = "";

    /**
     * 系统id
     */
    @Column(name = "sys_id")
    private Integer sysId = 0;

    /**
     * 锁定标记0否1是
     */
    @Column(name = "lock_flag")
    private Integer lockFlag = 0;

    /**
     * 是否可用
     */
    @Column(name = "enabled")
    private Integer enabled = 1;

    /**
     * 登录别名
     */
    @Column(name = "login_acc", nullable = false)
    private String loginAcc = "";

    /**
     * erp账号
     */
    @Column(name = "erp_account")
    private String erpAccount;

    /**
     * 登录时是否需要修改密码
     */
    @Column(name = "change_pwd")
    private Integer changePwd;

    /**
     * 人员类型 A01正式员工 A03离职 A21:劳务派遣
     */
    @Column(name = "hr_class_type")
    private String hrClassType;

    /**
     * 对应的hr记录编码 同步自动生成
     */
    @Column(name = "hr_user_id")
    private String hrUserId;

    @Column(name = "theme_id")
    private Integer themeId;
}
