package com.szkj.auth.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_user_detail")
public class UserDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 系统用户ID
     */
    @Id
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    /**
     * 电话号码
     */
    @Column(name = "phone_num")
    private String phoneNum;

    /**
     * 头像
     */
    @Column(name = "head_photo")
    private String headPhoto;

    /**
     * 邮箱
     */
    @Column(name = "e_mail")
    private String eMail;

    /**
     * 公司地址
     */
    @Column(name = "compony_address")
    private String componyAddress;

    /**
     * 学历
     */
    @Column(name = "education")
    private String education;

    /**
     * 性别1男2女
     */
    @Column(name = "sex")
    private Integer sex;

    /**
     * 入司时间
     */
    @Column(name = "entry_date")
    private LocalDate entryDate;

    /**
     * 离职时间
     */
    @Column(name = "departure_date")
    private LocalDate departureDate;

    @Column(name = "ext_attr_1")
    private String extAttr1;

    @Column(name = "ext_attr_2")
    private String extAttr2;

    @Column(name = "ext_attr_3")
    private String extAttr3;

    @Column(name = "ext_attr_4")
    private String extAttr4;

    @Column(name = "ext_attr_5")
    private String extAttr5;

}
