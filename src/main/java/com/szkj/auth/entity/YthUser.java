package com.szkj.auth.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "yth_user", schema = "db_yth2")
public class YthUser {
    private int userId;
    private Integer userType;
    private String account;
    private String password;
    private String mobile;
    private int isDel;
    private String userName;
    private Integer sysId;
    private Integer lockFlag;
    private Integer enabled;
    private String loginAcc;
    private String erpAccount;
    private Integer changePwd;
    private String hrClassType;
    private String hrUserId;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "user_type")
    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    @Basic
    @Column(name = "account")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "mobile")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "is_del")
    public int getIsDel() {
        return isDel;
    }

    public void setIsDel(int isDel) {
        this.isDel = isDel;
    }

    @Basic
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "sys_id")
    public Integer getSysId() {
        return sysId;
    }

    public void setSysId(Integer sysId) {
        this.sysId = sysId;
    }

    @Basic
    @Column(name = "lock_flag")
    public Integer getLockFlag() {
        return lockFlag;
    }

    public void setLockFlag(Integer lockFlag) {
        this.lockFlag = lockFlag;
    }

    @Basic
    @Column(name = "enabled")
    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "login_acc")
    public String getLoginAcc() {
        return loginAcc;
    }

    public void setLoginAcc(String loginAcc) {
        this.loginAcc = loginAcc;
    }

    @Basic
    @Column(name = "erp_account")
    public String getErpAccount() {
        return erpAccount;
    }

    public void setErpAccount(String erpAccount) {
        this.erpAccount = erpAccount;
    }

    @Basic
    @Column(name = "change_pwd")
    public Integer getChangePwd() {
        return changePwd;
    }

    public void setChangePwd(Integer changePwd) {
        this.changePwd = changePwd;
    }

    @Basic
    @Column(name = "hr_class_type")
    public String getHrClassType() {
        return hrClassType;
    }

    public void setHrClassType(String hrClassType) {
        this.hrClassType = hrClassType;
    }

    @Basic
    @Column(name = "hr_user_id")
    public String getHrUserId() {
        return hrUserId;
    }

    public void setHrUserId(String hrUserId) {
        this.hrUserId = hrUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YthUser ythUser = (YthUser) o;
        return userId == ythUser.userId &&
                isDel == ythUser.isDel &&
                Objects.equals(userType, ythUser.userType) &&
                Objects.equals(account, ythUser.account) &&
                Objects.equals(password, ythUser.password) &&
                Objects.equals(mobile, ythUser.mobile) &&
                Objects.equals(userName, ythUser.userName) &&
                Objects.equals(sysId, ythUser.sysId) &&
                Objects.equals(lockFlag, ythUser.lockFlag) &&
                Objects.equals(enabled, ythUser.enabled) &&
                Objects.equals(loginAcc, ythUser.loginAcc) &&
                Objects.equals(erpAccount, ythUser.erpAccount) &&
                Objects.equals(changePwd, ythUser.changePwd) &&
                Objects.equals(hrClassType, ythUser.hrClassType) &&
                Objects.equals(hrUserId, ythUser.hrUserId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userType, account, password, mobile, isDel, userName, sysId, lockFlag, enabled, loginAcc, erpAccount, changePwd, hrClassType, hrUserId);
    }
}
