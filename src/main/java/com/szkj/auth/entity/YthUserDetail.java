package com.szkj.auth.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "yth_user_detail", schema = "db_yth2")
public class YthUserDetail {
    private int userId;
    private String phoneNum;
    private String headPhoto;
    private String eMail;
    private String componyAddress;
    private String education;
    private Integer sex;
    private Date entryDate;
    private Date departureDate;
    private String extAttr1;
    private String extAttr2;
    private String extAttr3;
    private String extAttr4;
    private String extAttr5;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "phone_num")
    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    @Basic
    @Column(name = "head_photo")
    public String getHeadPhoto() {
        return headPhoto;
    }

    public void setHeadPhoto(String headPhoto) {
        this.headPhoto = headPhoto;
    }

    @Basic
    @Column(name = "e_mail")
    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    @Basic
    @Column(name = "compony_address")
    public String getComponyAddress() {
        return componyAddress;
    }

    public void setComponyAddress(String componyAddress) {
        this.componyAddress = componyAddress;
    }

    @Basic
    @Column(name = "education")
    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Basic
    @Column(name = "sex")
    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "entry_date")
    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    @Basic
    @Column(name = "departure_date")
    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    @Basic
    @Column(name = "ext_attr_1")
    public String getExtAttr1() {
        return extAttr1;
    }

    public void setExtAttr1(String extAttr1) {
        this.extAttr1 = extAttr1;
    }

    @Basic
    @Column(name = "ext_attr_2")
    public String getExtAttr2() {
        return extAttr2;
    }

    public void setExtAttr2(String extAttr2) {
        this.extAttr2 = extAttr2;
    }

    @Basic
    @Column(name = "ext_attr_3")
    public String getExtAttr3() {
        return extAttr3;
    }

    public void setExtAttr3(String extAttr3) {
        this.extAttr3 = extAttr3;
    }

    @Basic
    @Column(name = "ext_attr_4")
    public String getExtAttr4() {
        return extAttr4;
    }

    public void setExtAttr4(String extAttr4) {
        this.extAttr4 = extAttr4;
    }

    @Basic
    @Column(name = "ext_attr_5")
    public String getExtAttr5() {
        return extAttr5;
    }

    public void setExtAttr5(String extAttr5) {
        this.extAttr5 = extAttr5;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YthUserDetail that = (YthUserDetail) o;
        return userId == that.userId &&
                Objects.equals(phoneNum, that.phoneNum) &&
                Objects.equals(headPhoto, that.headPhoto) &&
                Objects.equals(eMail, that.eMail) &&
                Objects.equals(componyAddress, that.componyAddress) &&
                Objects.equals(education, that.education) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(entryDate, that.entryDate) &&
                Objects.equals(departureDate, that.departureDate) &&
                Objects.equals(extAttr1, that.extAttr1) &&
                Objects.equals(extAttr2, that.extAttr2) &&
                Objects.equals(extAttr3, that.extAttr3) &&
                Objects.equals(extAttr4, that.extAttr4) &&
                Objects.equals(extAttr5, that.extAttr5);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, phoneNum, headPhoto, eMail, componyAddress, education, sex, entryDate, departureDate, extAttr1, extAttr2, extAttr3, extAttr4, extAttr5);
    }
}
