package com.szkj.indexpage.controller;


import cn.hutool.json.JSONObject;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.indexpage.service.IndexPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年05月25日 15:38
 */
@Component
@RestController
@CrossOrigin("*")
@RequestMapping(value="/indexpage")
public class IndexPageController {
    @Autowired
    private IndexPageService indexPageService;
    @Autowired
    private AuthService authService;
    @Autowired
    private YthOperationLogService logservice;

    /**
     * @param param
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_index_sechacti", method = {RequestMethod.POST}, produces = "application/json")
    public String getMySechActi(@RequestBody JSONObject param, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        try {
            String result = RespUtil.success("成功", indexPageService.getMySechActi(userId));
            logservice.append(userId, "select", "indexpage", "get_index_sechacti", param.toString(), "", "",userId);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            logservice.append(userId, "select", "indexpage", "get_index_sechacti", param.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), new JSONObject());
        }
    }


    @RequestMapping(value = "/get_index_top", method = {RequestMethod.POST}, produces = "application/json")
    public String getPageTop(@RequestBody JSONObject param, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        String account=authService.getUserInfo(_token).getStr("account");
        try {
            String result = RespUtil.success("成功", indexPageService.getPageTop(userId,account));
            logservice.append(userId, "select", "indexpage", "get_index_top", param.toString(), "", "",userId);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            logservice.append(userId, "select", "indexpage", "get_index_top", param.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), new JSONObject());
        }

    }

    @RequestMapping(value = "/get_index_middle", method = {RequestMethod.POST}, produces = "application/json")
    public String getMiddlePage(@RequestBody JSONObject param, HttpServletRequest request, @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        JSONObject juserInfo =authService.getLoginInfo(_token);
        try {
            String result = RespUtil.success("成功", indexPageService.getMiddlePage(userId,userInfo,juserInfo));
            logservice.append(userId, "select", "indexpage", "get_index_middle", param.toString(), "", "",userId);
            return result;
        }catch (Exception e) {
            e.printStackTrace();
            logservice.append(userId, "select", "indexpage", "get_index_middle", param.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), new JSONObject());
        }

    }


    @RequestMapping(value = "/get_app_index_top", method = {RequestMethod.POST}, produces = "application/json")
    public String getAppPageTop(@RequestBody JSONObject param, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        String account=authService.getUserInfo(_token).getStr("account");
        try {
            String result = RespUtil.success("成功", indexPageService.getAppPageTop(userId,account));
            logservice.append(userId, "select", "indexpage", "get_app_index_top", param.toString(), "", "",userId);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            logservice.append(userId, "select", "indexpage", "get_app_index_top", param.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), new JSONObject());
        }

    }

    @PostMapping("/change_theme")
    public String changeTheme(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        try {
            String result = RespUtil.success("成功", indexPageService.changeTheme(body,userId));
            logservice.append(userId, "update", "indexpage", "change_theme", body.toString(), result, "",userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "update", "indexpage", "change_theme", body.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }
}
