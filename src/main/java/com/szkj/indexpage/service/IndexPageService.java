package com.szkj.indexpage.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.UserService;
import com.szkj.common.service.SQLService;
import com.szkj.knowledge.service.KnowledgeService;
import com.szkj.schedule.entity.YthSchedulePlan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年05月25日 15:30
 */
@Slf4j
@Service
public class IndexPageService {
    @Autowired
    private SQLService sqlService;
    @Autowired
    private KnowledgeService knowledgeService;
    @Autowired
    private UserService userService;


    public Map<String, Object> getMySechActi(@Param("userId") Integer userId) throws Exception {
        Map<String, Object> result = new HashMap<>();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long nowTime = c.getTimeInMillis() / 1000;

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long weekTime = cal.getTimeInMillis() / 1000;

        JSONArray activityInfo = sqlService.customQueryList(6503, "{personId:" + userId + "}");
        JSONArray scheduleInfo = sqlService.customQueryList(6101, StrUtil.format("{userId:{}, beginTime:{}, endTime:{}}", userId, nowTime, weekTime));
        JSONArray leaderconInfo = sqlService.customQueryList(6102, "{userId:" + userId + "}");
        result.put("activity", activityInfo);
        result.put("schedule", scheduleInfo);
        result.put("leaderconInfo", leaderconInfo);
        return result;
    }

    public Map<String, Object> getPageTop(@Param("userId") Integer userId,@Param("account") String account) throws Exception {
        Map<String, Object> result = new HashMap<>();
    //    JSONArray noticeInfo = sqlService.customQueryList(6502, "{userId:" + userId + "}");
        JSONArray noticeInfo = sqlService.customQueryList(6502, StrUtil.format("{userId:{}, account:{}}", userId, account));
        JSONArray appInfo = sqlService.customQueryList(6507, "{userId:" + userId + "}");
        JSONArray newsInfo = sqlService.customQueryList(6501, "{userId:" + userId + "}");
        result.put("notice", noticeInfo);
        result.put("appInfo", appInfo);
        result.put("newsInfo", newsInfo);
        return result;
    }


    public Map<String, Object> getMiddlePage(@Param("userId") Integer userId, AuthUserInfoModel userInfo, JSONObject juserInfo) throws Exception {
        Map<String, Object> result = new HashMap<>();
        JSONObject body = new JSONObject();
        body.set("sqlId", 0);
        body.set("params", "type=1");
        body.set("params2", "");
        body.set("orderby", "order by createTime desc");
        body.set("pagenum", 1);
        body.set("pagerows", 10);

        JSONObject newBody = knowledgeService.resetKnowledgeListQueryBody(juserInfo, body);
        JSONObject knowledgeInfo = sqlService.queryPageList(userInfo, newBody);


        JSONArray pimessageInfo = sqlService.customQueryList(6504, "{userId:" + userId + "}");
        JSONArray groupnotice = sqlService.customQueryList(6505, "{userId:" + userId + "}");
        JSONArray cfsq = sqlService.customQueryList(6506, "{userId:" + userId + "}");
        result.put("pimessage", pimessageInfo);
        result.put("groupnotice", groupnotice);
        result.put("knowledge", knowledgeInfo);
        result.put("cfsq", cfsq);
        return result;
    }


    public Map<String, Object> getAppPageTop(@Param("userId") Integer userId,@Param("account") String account) throws Exception {
        Map<String, Object> result = new HashMap<>();
        JSONArray noticeInfo = sqlService.customQueryList(6502, StrUtil.format("{userId:{}, account:{}}", userId, account));
        JSONArray appInfo = sqlService.customQueryList(6508, "{userId:" + userId + "}");
        JSONArray newsInfo = sqlService.customQueryList(6501, "{userId:" + userId + "}");
        result.put("notice", noticeInfo);
        result.put("appInfo", appInfo);
        result.put("newsInfo", newsInfo);
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public String changeTheme(JSONObject body, int userId) throws Exception {
        try {
            int theme = (Integer) body.get("theme");
            userService.changeTheme(userId, theme);
            return "success";
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }

    }
}
