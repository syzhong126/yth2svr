package com.szkj.person.controller;


import cn.hutool.json.JSONObject;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.person.entity.YthUserNewPhoneNum;
import com.szkj.person.service.YthPersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * 控制层
 *
 * @author makejava
 * @since 2022-04-20 15:39:28
 */
@Slf4j
@RestController
@RequestMapping("/person")
@CrossOrigin("*")
public class YthPersonController {
    @Autowired
    private YthPersonService service;
    @Autowired
    private YthOperationLogService logservice;
    @Autowired
    private AuthService authService;


    @PostMapping("/sys_new_phone_num")
    public String modify(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId = authService.getUserInfo(_token).getInt("userId");
        try {
            YthUserNewPhoneNum ythUserNewPhoneNum = service.modify(body, userId);
            String result = RespUtil.success("成功", ythUserNewPhoneNum);
            //操作日志
            logservice.append(userId, "update", "application", "change_often", body.toString(), result, "", userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "update", "application", "change_often", body.toString(), "", e.toString(), userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }

}

