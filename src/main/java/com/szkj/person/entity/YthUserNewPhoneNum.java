package com.szkj.person.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Objects;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年04月25日 15:15
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_user_new_phone_num")
public class YthUserNewPhoneNum {

    @Id
    @Column(name = "sys_phone_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long sysPhoneId;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "new_phone_num")
    private String newPhoneNum;
    @Column(name = "sys_status")
    private Integer sysStatus;
    /**
     * 创建人
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime;

    /**
     * 修改人
     */
    @Column(name = "update_by", nullable = false)
    private Integer updateBy;

    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Long updateTime;


}
