package com.szkj.person.reposi;


import com.szkj.person.entity.YthUserNewPhoneNum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-20 15:14:24
 */
public interface YthPhoneNumReposi extends JpaRepository<YthUserNewPhoneNum, Long>, JpaSpecificationExecutor<YthUserNewPhoneNum> {


}

