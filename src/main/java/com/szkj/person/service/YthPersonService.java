package com.szkj.person.service;

import antlr.StringUtils;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.common.util.UpdateUtil;
import com.szkj.person.entity.YthUserNewPhoneNum;
import com.szkj.person.reposi.YthPhoneNumReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

/**
 * (YthEntranceFacility)表服务实现类
 *
 * @author Yang
 * @since 2022-04-22 15:30:00
 */
@Slf4j
@Service
public class YthPersonService {
    @Autowired
    private YthPhoneNumReposi numreposi;


    @Transactional(rollbackFor = Exception.class)
    public YthUserNewPhoneNum modify(JSONObject body, int userId) throws Exception {
        try {
            YthUserNewPhoneNum bean = JSONUtil.toBean(body, YthUserNewPhoneNum.class);
            YthUserNewPhoneNum  oldbean=null;
            if(bean.getSysPhoneId()>0){
              oldbean=numreposi.findById(bean.getSysPhoneId()).get();
            }
            if(oldbean!=null){
                Calendar calendar = Calendar.getInstance();
                long time = calendar.getTimeInMillis();
                bean.setUpdateBy(userId).setUpdateTime(time);
                //将前端传来的不为空参数(也即是要修改值)copy覆盖原始对象属性值
                UpdateUtil.copyNullProperties(bean,oldbean);  //修改的字段覆盖原对象
                numreposi.save(oldbean);
            }
            return oldbean;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
