package com.szkj.file.controller;

import cn.hutool.json.JSONObject;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import com.szkj.file.entity.YthFormFile;
import com.szkj.file.service.YthFileFormService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年06月23日 9:55
 */
@Slf4j
@RestController
@RequestMapping("/file")
@CrossOrigin("*")
public class FileFormCrl {
    @Autowired
    private AuthService authService;
    @Autowired
    private YthFileFormService fileService;
    @Autowired
    private YthOperationLogService logservice;

    @PostMapping("/add_head_file")
    public String append(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId=authService.getUserInfo(_token).getInt("userId");
        try {
            YthFormFile ythFormFile = fileService.append(body,userId,"yth_user");
            String result = RespUtil.success("成功", ythFormFile);
            logservice.append(userId, "insert", "schedule", "add_schedule_plan_append", body.toString(), result, "",userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "insert", "schedule", "add_schedule_plan_append", body.toString(), "", e.toString(),userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }
}
