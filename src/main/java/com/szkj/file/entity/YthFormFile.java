package com.szkj.file.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

import cn.hutool.json.JSONObject;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年06月16日 16:17
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_form_file", schema = "db_yth2")
public class YthFormFile {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "f_table")
    private String fTable;
    @Column(name = "f_id")
    private long fId;
    @Column(name = "file_name")
    private String fileName;
    @Column(name = "file_extension")
    private String fileExtension;
    @Column(name = "file_size")
    private long fileSize;
    @Column(name = "save_name")
    private String saveName;
    @Column(name = "save_path")
    private String savePath;
    @Column(name = "save_full_name")
    private String saveFullName;
    @Column(name = "download_url")
    private String downloadUrl;
    @Column(name = "ts")
    private long ts;
    @Column(name = "is_del")
    private byte isDel;

    public YthFormFile(JSONObject json, String tableName, long fId) {
        this.id = json.getLong("id", null);
        this.fTable= tableName;
        this.fId= fId;
        this.fileName= json.getStr("fileName", null);
        this.fileExtension= json.getStr("fileExtension", null);
        this.fileSize= json.getLong("fileSize", null);
        this.saveName= json.getStr("saveName", null);
        this.savePath= json.getStr("savePath", null);
        this.saveFullName= json.getStr("saveFullName", null);
        this.downloadUrl= json.getStr("downloadUrl", null);
        this.ts= System.currentTimeMillis();
        this.isDel= (byte) 0;
    }

    public YthFormFile() {
    }
}
