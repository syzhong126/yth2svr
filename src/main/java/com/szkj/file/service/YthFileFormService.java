package com.szkj.file.service;

import cn.hutool.json.JSONUtil;
import com.szkj.common.util.UpdateUtil;
import com.szkj.file.entity.YthFormFile;
import com.szkj.file.reposi.YthFileReposi;

import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: TODO
 * @author: yang
 * @date: 2022年06月16日 16:28
 */
@Slf4j
@Service
public class YthFileFormService {
    @Autowired
    private YthFileReposi reposi;

    @Transactional(rollbackFor = Exception.class)
    public YthFormFile append(YthFormFile bean,String table) throws Exception {
        try {
            YthFormFile  oldbean=reposi.queryYthFormFile(table,bean.getFId());
            if(oldbean!=null){
                long id=oldbean.getId();
                UpdateUtil.copyNullProperties(bean,oldbean);
                oldbean.setId(id);
                reposi.save(oldbean);
            }else{
                reposi.save(bean);
            }
            return bean;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public YthFormFile append(JSONObject body, int userId,String table) throws Exception {
        try {
            YthFormFile bean = JSONUtil.toBean(body, YthFormFile.class);
            Calendar calendar = Calendar.getInstance();
            long time = calendar.getTimeInMillis();
            YthFormFile  oldbean=reposi.queryYthFormFile(table,userId);
              if(oldbean!=null){//修改
                  long id=oldbean.getId();
                  UpdateUtil.copyNullProperties(bean,oldbean);
                  oldbean.setId(id);
                  oldbean.setFId(userId);
                  oldbean.setTs(time);
                  reposi.save(oldbean);
            }else{
                  bean.setFId(userId);
                  bean.setFTable(table);
                  bean.setTs(time);
                  reposi.save(bean);
            }
            return bean;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveFiles(ArrayList<YthFormFile> fileList, String tableName, long fId) throws Exception {
        if (null==fileList) fileList= new ArrayList<YthFormFile>();  //如果没传文件列表 则设为空文件list
        ArrayList<YthFormFile> _oldFiles = reposi.queryYthFormFileList(tableName, fId); 
        for(YthFormFile _file : _oldFiles) {
            _file.setIsDel(Byte.valueOf("1"));
        }

        for(YthFormFile _curFile: fileList) {
            _curFile.setFTable(tableName);
            _curFile.setFId(fId);
            if (_oldFiles.stream().filter((item)->item.getFileName().equals(_curFile.getFileName())).count()==0) {
                _oldFiles.add(_curFile);
            } else {
                _oldFiles.stream().filter((item)->item.getFileName().equals(_curFile.getFileName())).forEach(item->item.setIsDel((byte) 0));
            }            
        }
        
        for(YthFormFile _file : _oldFiles) {
            reposi.save(_file);
        }   
    }
}
