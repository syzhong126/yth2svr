package com.szkj.file.reposi;

import com.szkj.file.entity.YthFormFile;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface YthFileReposi extends JpaRepository<YthFormFile, Long>, JpaSpecificationExecutor<YthFormFile> {
    @Query(value = "SELECT * FROM `yth_form_file` WHERE f_table=?1 AND f_id=?2 and is_del=0 limit 1 " , nativeQuery = true)
    public YthFormFile queryYthFormFile(String table, long fID);

    @Query(value = "SELECT * FROM `yth_form_file` WHERE f_table=?1 AND f_id=?2 and is_del=0" , nativeQuery = true)
    public ArrayList<YthFormFile> queryYthFormFileList(String table, long fID);
}

