package com.szkj.process.reposi;


import com.szkj.process.entity.Process;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-22 09:10:41
 */
public interface ProcessReposi extends CrudRepository<Process, Long> {

    @Query(value = "select p.id,p.process_name as processName,p.sort_id as sortId,p.url_pc as urlPc,p.url_app as urlApp," +
            "pp.user_id=?1 as `check` from yth_process p " +
            "left join yth_process_personal pp on pp.process_id = p.id and pp.user_id=?1 " +
            "where p.del_flag=false order by `check` desc,p.sort_id", nativeQuery = true)
    List<Map<String, Object>> list(@Param("userId") Integer userId);

    @Transactional
    @Modifying
    @Query(value = "insert into yth_process_personal values(?1,?2)", nativeQuery = true)
    int addAuth(@Param("userId") Integer userId, @Param("processId") Integer processId);

    @Transactional
    @Modifying
    @Query(value = "delete from yth_process_personal where user_id=?1 and process_id=?2", nativeQuery = true)
    int delAuth(@Param("userId") Integer userId, @Param("processId") Integer processId);
}

