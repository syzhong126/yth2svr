package com.szkj.process.reposi;


import com.szkj.process.entity.ProcessPersonal;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * 用户流程表持久层
 *
 * @author makejava
 * @since 2022-04-29 13:35:09
 */
public interface ProcessPersonalReposi extends JpaRepository<ProcessPersonal, Integer> {

}

