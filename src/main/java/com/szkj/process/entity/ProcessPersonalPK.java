package com.szkj.process.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProcessPersonalPK implements Serializable {

    private Integer userId;
    private Integer processId;
}