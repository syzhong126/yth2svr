package com.szkj.process.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * (YthProcess)实体类
 *
 * @author makejava
 * @since 2022-04-22 09:10:37
 */
@Data
@Entity
@Table(name = "yth_process")
public class Process implements Serializable {
    private static final long serialVersionUID = -44277287156345182L;
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    /** 系统编码 */
    @Column(name = "system_code")
    private String systemCode;
    /** 名称 */
    @Column(name = "process_name")
    private String processName;
    /** 排序 */
    @Column(name = "sort_id")
    private Integer sortId;
    /** URL_PC */
    @Column(name = "url_pc")
    private String urlPc;
    /** URL_APP */
    @Column(name = "url_app")
    private String urlApp;
    /** LOGO  */
    @Column(name = "logo")
    private String logo;
}

