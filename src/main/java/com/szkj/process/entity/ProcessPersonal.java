package com.szkj.process.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用户流程表(YthProcessPersonal)实体类
 *
 * @author makejava
 * @since 2022-04-29 13:35:09
 */
@Data
@Entity
@Table(name = "yth_process_personal")
@IdClass(ProcessPersonalPK.class)
public class ProcessPersonal implements Serializable {
    private static final long serialVersionUID = -57240265928740718L;
    /**
     * 用户ID，外键
     */
    @Id
    @Column(name = "user_id", nullable = false)
    private Integer userId;
    /**
     * 流程ID，外键
     */
    @Id
    @Column(name = "process_id", nullable = false)
    private Integer processId;

    private transient boolean check = true;
}

