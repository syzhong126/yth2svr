package com.szkj.process.controller;


import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.util.RespUtil;
import com.szkj.process.entity.ProcessPersonal;
import com.szkj.process.service.ProcessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 控制层
 *
 * @author makejava
 * @since 2022-04-22 09:10:32
 */
@Slf4j
@RestController
@RequestMapping("/process")
public class ProcessController {
    @Autowired
    private ProcessService service;

    /**
     * 获取列表(分页)
     */
    @GetMapping("/list")
    public String list(@ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        return RespUtil.success("成功", service.list(userinfo.getUserId()));

    }

    /**
     * 获取
     */
    @PostMapping("/set")
    public String setPersonal(@RequestBody List<ProcessPersonal> auths, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        return RespUtil.success("成功", service.setPersonal(auths, userinfo.getUserId()));
    }


}

