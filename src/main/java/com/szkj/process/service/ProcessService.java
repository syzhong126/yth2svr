package com.szkj.process.service;

import com.szkj.process.entity.ProcessPersonal;
import com.szkj.process.reposi.ProcessPersonalReposi;
import com.szkj.process.reposi.ProcessReposi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * (YthProcess)表服务实现类
 *
 * @author makejava
 * @since 2022-04-22 09:10:45
 */
@Service("ythProcessService")
public class ProcessService {
    @Autowired
    private ProcessReposi reposi;
    @Autowired
    private ProcessPersonalReposi personalReposi;

    public List<Map<String, Object>> list(Integer userId) {
        return reposi.list(userId);
    }

    public int setPersonal(List<ProcessPersonal> auths, Integer userId) {
        AtomicInteger r = new AtomicInteger();
        List<ProcessPersonal> add = auths.stream().filter(p -> {
            p.setUserId(userId);
            return p.isCheck();
        }).collect(Collectors.toList());
        personalReposi.saveAll(add);
        auths.removeAll(add);
        personalReposi.deleteInBatch(auths);
        return r.get();
    }

    public int set(List<Map<String, Object>> auths, Integer userId) {
        AtomicInteger r = new AtomicInteger();
        auths.forEach(m -> {
            Integer pid = (Integer) m.get("processId");
            boolean check = (boolean) m.get("check");
            if (check) try {
                r.addAndGet(reposi.addAuth(userId, pid));
            } catch (Exception ignored) {
            }
            else r.addAndGet(reposi.delAuth(userId, pid));
            //log.info((t == 0 ? "新增" : "删除") + fid + "," + oid);
        });
        return r.get();
    }
}
