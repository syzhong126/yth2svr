package com.szkj.facility.controller;


import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.util.RespUtil;
import com.szkj.facility.entity.AuthorityCorporate;
import com.szkj.facility.entity.AuthorityPersonal;
import com.szkj.facility.service.FacilityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 控制层
 *
 * @author makejava
 * @since 2022-04-20 15:39:28
 */
@Slf4j
@RestController
@RequestMapping("/facility")
@CrossOrigin("*")
public class FacilityController {
    @Autowired
    private FacilityService facilityService;

    /**
     * 读取设备所有有操作权限的公司
     *
     * @param facilityId 设备Id
     * @return 结果
     */
    @GetMapping("/orgs")
    public String orgs(@RequestParam Integer facilityId) {
        return RespUtil.success("成功", facilityService.orgs(facilityId));
    }

    /**
     * 读取设备额外有权限操作的人员
     *
     * @param facilityId 设备ID
     * @return 结果
     */
    @GetMapping("/users")
    public String users(@RequestParam Integer facilityId) {
        return RespUtil.success("成功", facilityService.users(facilityId));
    }

    /**
     * 读取公司可管理的门禁设备
     *
     * @param orgId 公司Id
     * @return 结果
     */
    @GetMapping("/corporate")
    public String corporate(@RequestParam Integer orgId) {
        return RespUtil.success("成功", facilityService.corporate(orgId));
    }

    /**
     * 读取用户可操作的门禁设备
     *
     * @param userId     用户ID
     * @param floor      楼层
     * @param searchText 搜索字符串
     * @return 结果
     */
    @GetMapping("/personal")
    public String personal(@RequestParam Integer userId,
                           @RequestParam(required = false) Integer floor,
                           @RequestParam(required = false) String searchText,
                           @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        if (userId == null) userId = userinfo.getUserId();
        return RespUtil.success("成功", facilityService.personal(userId, floor, searchText));
    }

    /**
     * 设置权限，包括用户和公司的权限
     *
     * @param auths 包含字段：userId, facilityId, orgId, type。其中userId和orgId互斥，type:0新增,1删除
     * @return 结果
     */
    @PostMapping("/set")
    public String add(@RequestBody List<Map<String, Object>> auths) {
        return RespUtil.success("成功", facilityService.set(auths));
    }

    @PostMapping("/set/personal")
    public String setPersonal(@RequestBody List<AuthorityPersonal> auths) {
        facilityService.setPersonal(auths);
        return RespUtil.success("成功", null);
    }

    @PostMapping("/set/corporate")
    public String setCorporate(@RequestBody List<AuthorityCorporate> auths) {
        facilityService.setCorporate(auths);
        return RespUtil.success("成功", null);
    }

    /**
     * 操作设备开门
     *
     * @param facilityId 设施Id
     * @return 结果
     */
    @GetMapping("/open")
    public String open(@RequestParam Integer facilityId) {
        //TODO: 根据facilityId查找设备信息，根据设备信息调用设备Api开门
        return RespUtil.success("成功", "");
    }

    /**
     * 所有设备列表你
     *
     * @param floor      楼层
     * @param searchText 搜索字符串
     * @return 结果
     */
    @GetMapping("/list")
    public String list(@RequestParam(required = false) Integer floor,
                       @RequestParam(required = false) String searchText) {
        return RespUtil.success("成功", facilityService.list(floor, searchText));
    }

    /**
     * 所有有设备的楼层，设备表楼层floor的distinct值
     *
     * @return 结果
     */
    @GetMapping("/floors")
    public String floors() {
        return RespUtil.success("成功", facilityService.allFloors());
    }
}

