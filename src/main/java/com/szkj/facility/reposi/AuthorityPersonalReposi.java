package com.szkj.facility.reposi;


import com.szkj.facility.entity.AuthorityPersonal;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 门禁人员权限表持久层
 *
 * @author makejava
 * @since 2022-04-29 11:28:14
 */
public interface AuthorityPersonalReposi extends JpaRepository<AuthorityPersonal, Integer> {

}

