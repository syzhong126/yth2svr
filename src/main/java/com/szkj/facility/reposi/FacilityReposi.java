package com.szkj.facility.reposi;


import com.szkj.facility.entity.Facility;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-20 15:14:24
 */
public interface FacilityReposi extends CrudRepository<Facility, Integer> {
    @Query(value = "SELECT  c.facility_id,o.org_id,o.org_name " +
            "FROM yth_organazition o, yth_entrance_authority_corporate c " +
            "WHERE c.org_id=o.org_id AND c.facility_id=?1", nativeQuery = true)
    Iterable<Map<String, Object>> getOrgs(Integer facilityId);

    @Query(value = "SELECT  p.facility_id,u.user_id,u.user_name " +
            "FROM yth_user u, yth_entrance_authority_personal p " +
            "WHERE u.user_id=p.user_id AND p.facility_id=?1", nativeQuery = true)
    Iterable<Map<String, Object>> getUsers(Integer facilityId);

    @Query(value = "SELECT  c.org_id,f.id,f.facility_name " +
            "FROM yth_entrance_authority_corporate c, yth_entrance_facility f " +
            "WHERE c.facility_id=f.id AND c.org_id=?1", nativeQuery = true)
    Iterable<Map<String, Object>> corporate(Integer orgId);

    @Query(value = "SELECT distinct f.floor FROM yth_entrance_facility f order by f.floor", nativeQuery = true)
    Iterable<Integer> allFloors();

    @Query(value = "SELECT  f.* FROM yth_user_org r, yth_entrance_facility f, yth_entrance_authority_corporate c "
            + "WHERE c.org_id = r.org_id AND c.facility_id=f.id AND r.user_id=?1 "
            + "and (?2 is null or ?2=0 or f.floor=?2) "
            + "and (?3 is null or ?3='' or f.facility_name like CONCAT('%',?3,'%')) "
            + "union SELECT  f.* FROM yth_entrance_facility f, yth_entrance_authority_personal p "
            + "WHERE p.facility_id=f.id AND p.user_id=?1 "
            + "and (?2 is null or ?2=0 or f.floor=?2) "
            + "and (?3 is null or ?3='' or f.facility_name like CONCAT('%',?3,'%')) "
            , nativeQuery = true)
    Iterable<Facility> personal(@Param("userId") Integer userId,
                                @Param("floor") Integer floor,
                                @Param("searchText") String searchText);

    @Query(value = "SELECT f.* FROM yth_entrance_facility f "
            + "WHERE (?1 is null or ?1='' or f.floor=?1) "
            + "AND (?2 is null or ?2='' or f.facility_name like CONCAT('%',?2,'%')) ", nativeQuery = true)
    Iterable<Facility> list(@Param("floor") Integer floor,
                            @Param("searchText") String searchText);

    @Transactional
    @Modifying
    @Query(value = "insert into yth_entrance_authority_corporate values(?1,?2)", nativeQuery = true)
    int addAuthCorporate(@Param("facilityId") Integer facilityId, @Param("orgId") Integer orgId);

    @Transactional
    @Modifying
    @Query(value = "delete from yth_entrance_authority_corporate where facility_id=?1 and org_id=?2", nativeQuery = true)
    int delAuthCorporate(@Param("facilityId") Integer facilityId, @Param("orgId") Integer orgId);

    @Transactional
    @Modifying
    @Query(value = "insert into yth_entrance_authority_personal values(?1,?2)", nativeQuery = true)
    int addAuthPersonal(@Param("facilityId") Integer facilityId, @Param("userId") Integer userId);

    @Transactional
    @Modifying
    @Query(value = "delete from yth_entrance_authority_personal where facility_id=?1 and user_id=?2", nativeQuery = true)
    int delAuthPersonal(@Param("facilityId") Integer facilityId, @Param("userId") Integer userId);
}

