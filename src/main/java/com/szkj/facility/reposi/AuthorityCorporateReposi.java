package com.szkj.facility.reposi;


import com.szkj.facility.entity.AuthorityCorporate;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * 门禁公司权限表持久层
 *
 * @author makejava
 * @since 2022-04-29 11:28:13
 */
public interface AuthorityCorporateReposi extends JpaRepository<AuthorityCorporate, Integer> {

}

