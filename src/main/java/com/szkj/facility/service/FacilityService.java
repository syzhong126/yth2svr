package com.szkj.facility.service;

import cn.hutool.http.HttpRequest;
import com.szkj.facility.entity.Authority;
import com.szkj.facility.entity.AuthorityCorporate;
import com.szkj.facility.entity.AuthorityPersonal;
import com.szkj.facility.entity.Facility;
import com.szkj.facility.reposi.AuthorityCorporateReposi;
import com.szkj.facility.reposi.AuthorityPersonalReposi;
import com.szkj.facility.reposi.FacilityReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * (YthEntranceFacility)表服务实现类
 *
 * @author makejava
 * @since 2022-04-20 15:30:00
 */
@Slf4j
@Service
public class FacilityService {
    @Autowired
    private FacilityReposi reposi;
    @Autowired
    private AuthorityPersonalReposi personalReposi;
    @Autowired
    private AuthorityCorporateReposi corporateReposi;

    public Iterable<Map<String, Object>> orgs(Integer facilityId) {
        return reposi.getOrgs(facilityId);
    }

    public Iterable<Map<String, Object>> users(Integer facilityId) {
        return reposi.getUsers(facilityId);
    }

    public Iterable<Map<String, Object>> corporate(Integer orgId) {
        return reposi.corporate(orgId);
    }

    public Iterable<Facility> personal(Integer userId, Integer floor, String searchText) {
        return reposi.personal(userId, floor, searchText);
    }

    public List<Facility> list(Integer floor, String searchText) {
        return (List<Facility>) reposi.list(floor, searchText);
    }

    public void setPersonal(List<AuthorityPersonal> auths) {
        List<AuthorityPersonal> adds = auths.stream().filter(a -> a.getType() == Authority.TypeAdd).collect(Collectors.toList());
        personalReposi.saveAll(adds);
        auths.removeAll(adds);
        personalReposi.deleteInBatch(auths);
    }

    public void setCorporate(List<AuthorityCorporate> auths) {
        List<AuthorityCorporate> adds = auths.stream().filter(a -> a.getType() == Authority.TypeAdd).collect(Collectors.toList());
        corporateReposi.saveAll(adds);
        auths.removeAll(adds);
        corporateReposi.deleteInBatch(auths);
    }

    public void open(Integer id) {
        String endpoint = "http://10.248.50.253:11002/SDK.Service/mex?wsdl";

        /*$action：表示操作码，操作码根据设备类型不同而不同，是可扩展的
        01：打开        02：关闭        06：撤消报警
        "<params method='NISSP_ACION_MANIPULATE_EQUIPMENT'><record>" +
        "<CardId>039C895B</CardId>" +
        "<EquipmentId>{$guardid}|{$doorno}</EquipmentId>" +
        "<action>{$action}</action>" +
        "</record></params>";*/

        String str = "<params method='NISSP_ACION_MANIPULATE_EQUIPMENT'><record>" +
                "<CardId>039C895B</CardId>" +
                "<EquipmentId>32|2</EquipmentId>" +
                "<action>01</action>" +
                "</record></params>";

        String url = "http://10.248.50.253:11002/SDK.Service/NISSP_ACION_MANIPULATE_EQUIPMENT";
        String result = HttpRequest.post(url).body(str).execute().body();
        log.info("result:{}", result);
        //Call soapCall = new Call();


    }


    /**
     * 设置设备权限，公司以及个人
     *
     * @param auths 设置公司，userId空；设置个人orgId空
     * @return 影响行数
     */
    public int set(List<Map<String, Object>> auths) {
        //log.info("size:" + auths.size());
        List<Map<String, Object>> l = auths.stream().filter(m -> m.get("userId") == null).collect(Collectors.toList());
        //log.info("corporate:" + l.size());
        AtomicInteger r = new AtomicInteger();
        l.forEach(m -> {
            int t = (int) m.get("type");
            Integer fid = (Integer) m.get("facilityId");
            Integer oid = (Integer) m.get("orgId");
            if (t == 0) try {
                r.addAndGet(reposi.addAuthCorporate(fid, oid));
            } catch (Exception ignored) {
            }
            else r.addAndGet(reposi.delAuthCorporate(fid, oid));
            //log.info((t == 0 ? "新增" : "删除") + fid + "," + oid);
        });
        l = auths.stream().filter(m -> m.get("orgId") == null).collect(Collectors.toList());
        //log.info("personal:" + l.size());
        l.forEach(m -> {
            int t = (int) m.get("type");
            Integer fid = (Integer) m.get("facilityId");
            Integer uid = (Integer) m.get("userId");
            if (t == 0) try {
                r.addAndGet(reposi.addAuthPersonal(fid, uid));
            } catch (Exception ignored) {
            }
            else r.addAndGet(reposi.delAuthPersonal(fid, uid));
            //log.info((t == 0 ? "新增" : "删除") + fid + "," + uid);
        });
        return r.get();
    }

    /**
     * 通过ID查询单条数据
     *
     * @param facilityId 主键
     * @return 实例对象
     */
    public Facility queryById(Integer facilityId) {
        return this.reposi.findById(facilityId).get();
    }

    public Iterable<Integer> allFloors() {
        return this.reposi.allFloors();
    }

    /**
     * 新增数据
     *
     * @param facility 实例对象
     * @return 实例对象
     */
    public Facility insert(Facility facility) {
        this.reposi.save(facility);
        return facility;
    }

    /**
     * 修改数据
     *
     * @param facility 实例对象
     * @return 实例对象
     */
    public Facility update(Facility facility) {
        this.reposi.save(facility);
        return this.queryById(facility.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param facilityId 主键
     */
    public void deleteById(Integer facilityId) {
        this.reposi.deleteById(facilityId);
    }
}
