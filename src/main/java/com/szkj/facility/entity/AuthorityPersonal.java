package com.szkj.facility.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 门禁人员权限表(YthEntranceAuthorityPersonal)实体类
 *
 * @author makejava
 * @since 2022-04-29 11:32:38
 */
@Data
@Entity
@Table(name = "yth_entrance_authority_personal")
@IdClass(AuthPsnlPK.class)
public class AuthorityPersonal extends Authority implements Serializable {
    private static final long serialVersionUID = -66539864336545710L;
    /**
     * 设备ID，关联设备表
     */
    @Id
    @Column(name = "facility_id", nullable = false)
    private Integer facilityId;

    /**
     * 人ID，关联人员表
     */
    @Id
    @Column(name = "user_id", nullable = false)
    private Integer userId;
}

