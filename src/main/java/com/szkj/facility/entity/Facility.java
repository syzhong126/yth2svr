package com.szkj.facility.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * (YthEntranceFacility)实体类
 *
 * @author makejava
 * @since 2022-04-27 15:33:09
 */
@Data
@Entity
@Table(name = "yth_entrance_facility")
public class Facility implements Serializable {
    private static final long serialVersionUID = -94488081520658967L;
    /**
     * 设备ID，主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    /**
     * 楼层：一楼、二楼
     */
    @Column(name = "floor")
    private Integer floor;
    /**
     * 名称
     */
    @Column(name = "facility_name")
    private String facilityName;
    /**
     * 设备型号，不同类型有不同操作接口，1，门禁-JSY07ID 2,门禁-JSELingYu
     */
    @Column(name = "facility_type")
    private String facilityType;
    /**
     * 门禁ID - 从门禁服务器同步过来的ID
     */
    @Column(name = "guard_id")
    private Integer guardId;
    /**
     * 门编号
     */
    @Column(name = "door_no")
    private Integer doorNo;
    /**
     * 控制器IP
     */
    @Column(name = "ip")
    private String ip;
    /**
     * 工作站IP
     */
    @Column(name = "work_ip")
    private String workIp;
    /**
     * 状态：0正常，1损坏
     */
    @Column(name = "status")
    private Integer status;
    /**
     * 逻辑删除
     */
    @Column(name = "del_flag")
    private Integer delFlag;
    @Column(name = "create_by")
    private String createBy;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_by")
    private String updateBy;
    @Column(name = "update_time")
    private Date updateTime;
}

