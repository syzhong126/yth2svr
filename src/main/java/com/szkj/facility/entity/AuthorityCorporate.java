package com.szkj.facility.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 门禁公司权限表(YthEntranceAuthorityCorporate)实体类
 *
 * @author makejava
 * @since 2022-04-29 11:32:37
 */
@Data
@Entity
@Table(name = "yth_entrance_authority_corporate")
@IdClass(AuthCorpPk.class)
public class AuthorityCorporate extends Authority implements Serializable {
    private static final long serialVersionUID = 319468125229586044L;
    /**
     * 设备ID，关联设备表
     */
    @Id
    @Column(name = "facility_id", nullable = false)
    private Integer facilityId;
    /**
     * 公司ID，关联人员表
     */
    @Id
    @Column(name = "org_id", nullable = false)
    private Integer orgId;
}

