package com.szkj.facility.entity;

import lombok.Data;

@Data
public abstract class Authority {
    public static final int TypeAdd = 0;
    public static final int TypeDel = 1;
    private transient int type = 0;
}
