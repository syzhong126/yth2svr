package com.szkj.application.controller;


import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.szkj.application.entity.YthApplicationProMine;
import com.szkj.application.service.YthApplicationService;
import com.szkj.auth.service.AuthService;
import com.szkj.common.service.YthOperationLogService;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * 控制层
 *
 * @author makejava
 * @since 2022-04-20 15:39:28
 */
@Slf4j
@RestController
@RequestMapping("/application")
@CrossOrigin("*")
public class YthApplicationController {
    @Autowired
    private YthApplicationService service;
    @Autowired
    private YthOperationLogService logservice;
    @Autowired
    private AuthService authService;


    @PostMapping("/change_often")
    public String modify(@RequestBody(required = true) JSONObject body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId = authService.getUserInfo(_token).getInt("userId");
        try {
            String back_result = service.modify(body, userId);
            String result =null;
            if("success".equals(back_result)){
                result = RespUtil.success("成功", back_result);
            }else{
                result = RespUtil.error("失败", back_result);
            }

            //操作日志
            logservice.append(userId, "update", "application", "change_often", body.toString(), result, "", userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "update", "application", "change_often", body.toString(), "", e.toString(), userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }

    @PostMapping("/change_appoften")
    public String modifyMuch(@RequestBody(required = true) JSONArray body, HttpServletRequest request) throws Exception {
        String _token = request.getHeader("token");
        int userId = authService.getUserInfo(_token).getInt("userId");
        try {
            String back_result = service.modifyMuch(body, userId);
            String result =null;
            if("success".equals(back_result)){
                result = RespUtil.success("成功", back_result);
            }else{
                result = RespUtil.error("失败", back_result);
            }
            //操作日志
            logservice.append(userId, "update", "application", "change_often", body.toString(), result, "", userId);
            return result;
        } catch (Exception e) {
            logservice.append(userId, "update", "application", "change_often", body.toString(), "", e.toString(), userId);
            return RespUtil.error(e.getMessage(), null);
        }
    }

}

