package com.szkj.application.service;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.application.entity.YthApplicationProMine;
import com.szkj.application.reposi.YthApplicationReposi;
import com.szkj.common.util.UpdateUtil;
import com.szkj.schedule.entity.YthSchedulePlan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * (YthEntranceFacility)表服务实现类
 *
 * @author Yang
 * @since 2022-04-22 15:30:00
 */
@Slf4j
@Service
public class YthApplicationService {
    @Autowired
    private YthApplicationReposi reposi;


    @Transactional(rollbackFor = Exception.class)
    public String modify(JSONObject body, int userId) throws Exception {
        try {
            String ids = (String) body.get("id");
            String oftens = (String) body.get("isOften");

            String[] id = null;String[] often = null;
            if (ids != null && ids != "") {
                if (ids.contains(",")) {
                    id = ids.split(",");
                } else {
                    id=new String[]{ids};
                }
            }
            if (oftens != null && oftens != "") {
                if (oftens.contains(",")) {
                    often = oftens.split(",");
                } else {
                    often=new String[]{oftens};
                }
            }
            if(often.length==id.length){
                for (int i = 0; i < id.length; i++) {
                    YthApplicationProMine  oldbean=reposi.findById(Long.parseLong(id[i])).get();
                    YthApplicationProMine bean =  new YthApplicationProMine();
                    bean.setId(Long.parseLong(id[i]));
                    bean.setIsOften(Integer.parseInt(often[i]));
                    Calendar calendar = Calendar.getInstance();
                    long time = calendar.getTimeInMillis();
                    bean.setUpdateBy(userId).setUpdateTime(time);
                    UpdateUtil.copyNullProperties(bean,oldbean);  //修改的字段覆盖原对象
                    reposi.save(oldbean);
                }
                return "success";
            }else{
                return "传参的id和状态数据不匹配";
            }

        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public String modifyMuch(JSONArray body, int userId) throws Exception {

        try {
            for (int i = 0; i < body.size(); i++) {
                JSONObject infojson = body.getJSONObject(i);
                YthApplicationProMine bean = JSONUtil.toBean(infojson, YthApplicationProMine.class);

                Calendar calendar = Calendar.getInstance();
                long time = calendar.getTimeInMillis();
                if(bean.getId()>0){//修改
                    YthApplicationProMine  oldbean=reposi.findById(bean.getId()).get();
                    bean.setUpdateBy(userId).setUpdateTime(time);
                    UpdateUtil.copyNullProperties(bean,oldbean);  //修改的字段覆盖原对象
                    reposi.save(oldbean);

                }else{
                    bean.setUserId(userId+"")
                        .setCreateBy(userId).setCreateTime(time).setUpdateBy(userId).setUpdateTime(time);
                    reposi.save(bean);
                }
            }

           return "success";

        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
