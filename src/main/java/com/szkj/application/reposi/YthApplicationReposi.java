package com.szkj.application.reposi;


import com.szkj.application.entity.YthApplicationProMine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-20 15:14:24
 */
public interface YthApplicationReposi extends JpaRepository<YthApplicationProMine, Long>, JpaSpecificationExecutor<YthApplicationProMine> {


}

