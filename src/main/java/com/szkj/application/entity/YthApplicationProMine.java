package com.szkj.application.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Objects;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年05月20日 14:50
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_application_pro_mine")
public class YthApplicationProMine {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "application_id")
    private String applicationId;
    @Column(name = "user_id")
    private String userId;
    @Column(name = "order_num")
    private Integer orderNum;
    @Column(name = "is_often")
    private Integer isOften;
    /**
     * 创建人
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy;
    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime;
    /**
     * 修改人
     */
    @Column(name = "update_by", nullable = false)
    private Integer updateBy;
    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Long updateTime;
}
