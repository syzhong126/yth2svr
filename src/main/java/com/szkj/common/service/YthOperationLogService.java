package com.szkj.common.service;

import com.szkj.common.reposi.YthOperationLogReposi;
import com.szkj.common.entity.YthOperationLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

/**
 * @Description: TODO
 * @author: Yang
 * @date: 2022年04月24日 15:42
 */
@Slf4j
@Service
public class YthOperationLogService {
    @Autowired
    private YthOperationLogReposi reposi;

    @Transactional(rollbackFor = Exception.class)
    public YthOperationLog append(int opUser, String askType, String modelName, String interfaceName, String postMsg, String responseStatus, String error_message,int userId) throws Exception {
        try {
            YthOperationLog bean = new YthOperationLog();
            Calendar calendar = Calendar.getInstance();
            long time = calendar.getTimeInMillis();
            bean.setOpUser(opUser)
                    .setAskType(askType)
                    .setModelName(modelName)
                    .setInterfaceName(interfaceName)
                    .setPostMsg(postMsg)
                    .setResponseStatus(responseStatus)
                    .setErrorMessage(error_message)
                    .setCreateBy(userId)
                    .setCreateTime(time)
                    .setUpdateBy(userId)
                    .setUpdateTime(time);
            reposi.save(bean);
            return bean;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(int opUser, String askType, String modelName, String interfaceName, String postMsg, String responseStatus, String error_message,int userId) {
        try {
            append(opUser, askType, modelName, interfaceName, postMsg, responseStatus, error_message, userId);
        } catch (Exception e) {
            log.error("保存日志时发生错误:", e);
        }
    }
}
