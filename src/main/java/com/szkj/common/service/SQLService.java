package com.szkj.common.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.tokenizer.Result;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.service.AuthService;
import com.szkj.file.entity.YthFormFile;
import com.szkj.file.reposi.YthFileReposi;
import com.szkj.file.service.YthFileFormService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@Service
public class SQLService {
    @Value("${datasvr.url}")
    private String dataSvrUrl;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AuthService authService;
    @Autowired
    private YthFileReposi fileReposi;
    /**
     * 自定义查询, 注意返回的数据量
     * @param sqlId  data_svr_sql.id
     * @param params 字符串形式: {key1:value1, key2:value2...}
     * @return
     * @throws Exception
     */
    public List queryFromJDBC(int sqlId, String params) throws Exception{
        String _strSQL = "select sql_sentence from data_svr_sql where id="+sqlId;
        _strSQL= jdbcTemplate.queryForObject(_strSQL, String.class);
        String[] list= params.replaceAll("[ {}]","").split(",");
        for(String str: list) {
            String[] temp = str.split(":");
            if (temp.length!=2) {
                throw new Exception("params不合规范:"+params);
            }
            _strSQL= _strSQL.replaceAll("\\{"+temp[0]+"\\}", temp[1]);
        }
        return jdbcTemplate.queryForList(_strSQL);
    }

    @SuppressWarnings("unchecked")
    public JSONArray customQueryList(int sqlId, String params) throws Exception{
        return new JSONArray(queryFromJDBC(sqlId, params));
    }
    @SuppressWarnings("unchecked")
    public JSONObject customQueryOne(int sqlId, String params) throws Exception{
        JSONArray _jsonArray= new JSONArray(queryFromJDBC(sqlId, params));
        return (_jsonArray.size()>0)?_jsonArray.getJSONObject(0): JSONUtil.createObj();
    }

    /**
     * 查询sql定义的附加属性
     */
    public JSONObject queryAddition(long sqlId) throws Exception{
        String _strSQL = "select addition from data_svr_sql where id="+sqlId;
        _strSQL= jdbcTemplate.queryForObject(_strSQL, String.class);
        return new JSONObject(_strSQL);
    }

    /**
     * 从{userId:111} 这种格式的参数中中获取参数值
     */
    public String getParamsValue(String params, String keyName) throws Exception {
        String[] list= params.replaceAll("[ {}]","").split(",");
        for(String str: list) {
            String[] temp = str.split(":");
            if (temp.length!=2) {
                throw new Exception("params不合规范:"+params);
            }
            if (keyName.equals(temp[0])) return temp[1];
        }
        return "";
    }

    /**
     * 向{userId:111} 这种格式的参数中设置参数值, 存在keyName就替换值, 不存在KeyName就添加, params2中参数值带,的不做处理
     */
    public String setParamsValue(String params, String keyName, String keyValue) throws Exception {
        String[] _list= params.replaceAll("[ {}]","").split(",");
        String _result= "";
        boolean _isExists= false;
        for(String str: _list) {
            String[] temp = str.split(":");
            if (temp.length==2 && (keyName.equals(temp[0]))) {
                temp[1]=keyValue;
                _isExists= true;
                _result+= "".equals(_result)?temp[0]+":"+temp[1]:","+temp[0]+":"+temp[1];
            } else {
                _result+= "".equals(_result)?str:","+str;
            }
        }
        if (!_isExists) {
            _result+= "".equals(_result)?keyName+":"+keyValue:","+keyName+":"+keyValue;
        }
        return "{"+_result+"}";
    }
//    public JSONObject getUserInfo(String account)  {
//        String strSql= StrUtil.format("select * from yth_user where account='{}'", account);
//        return jdbcTemplate.queryForObject(strSql, JSONObject.class);
//    }


    public JSONObject ParamsOfDataServer(int sqlId, String params, String params2, String orderby, int pageNum, int pagerows) {
        JSONObject _jsonObject = new JSONObject();
        _jsonObject.putOpt("sqlId", sqlId);
        _jsonObject.putOpt("params", params);
        _jsonObject.putOpt("params2", params2);
        _jsonObject.putOpt("orderby", orderby);
        _jsonObject.putOpt("pageNum", pageNum);
        _jsonObject.putOpt("pagerows", pagerows);
        return _jsonObject;
    }

    public JSONObject ParamsOfDataServer(int sqlId, String params2) {
        return ParamsOfDataServer(sqlId, "", params2, "", 1, 200);
    }

    /**
     * 查询服务器封装
     * @param userInfo
     * @param params
     * @return
     * @throws Exception
     */
    public JSONObject queryFromDataServer(AuthUserInfoModel userInfo, JSONObject params) throws Exception {
        if (params.isNull("params")) params.putOpt("params", "");
        if (params.isNull("params2")) params.putOpt("params2", "");
        String clientType= (userInfo.getCtype()==0)?"pc":"app";
        String _params2= setParamsValue(params.getStr("params2",""), "_clientType", clientType);
        params.putOpt("params2", _params2);
        Integer _sqlId = params.getInt("sqlId");
        if (_sqlId==null) {
            throw new Exception("缺少参数 sqlId");
        }
        String _dataSvrQueryUrl = StrUtil.format("{}/ds/{}?token={}&ctype={}&sysid={}",
                dataSvrUrl, _sqlId, userInfo.getToken(), userInfo.getCtype(), userInfo.getSysId());
        long lTime= System.currentTimeMillis();
        String _result= HttpRequest.post(_dataSvrQueryUrl)
                .body(params.toString())
                .execute()
                .body();
        lTime= System.currentTimeMillis()- lTime;
        System.out.println(StrUtil.format("{} dataServer查询服务: {}\n返回:{}",  lTime, _dataSvrQueryUrl, _result));
        //异常处理: {"rowdata":{"code":-1,"msg":"Token 无效"}}
        JSONObject _jsonObject = new JSONObject(_result);
        if (_jsonObject.containsKey("rowdata")) {
            _jsonObject= _jsonObject.getJSONObject("rowdata");
        }
        _jsonObject.putOpt("castTime", lTime);
        return _jsonObject;
    }

    /**
     * 查询服务器封装, 直接返回data.data
     * @param userInfo
     * @param params
     * @return
     * @throws Exception
     */
    public JSONArray queryList(AuthUserInfoModel userInfo, JSONObject params) throws Exception {
        if (params.isNull("params")) params.putOpt("params", "");
        if (params.isNull("params2")) params.putOpt("params2", "");
        long _sqlId= params.getLong("sqlId", 0L);
        if (_sqlId==0) {
            throw new Exception("缺少参数 sqlId");
        }
        JSONObject _addition= queryAddition(_sqlId);
        //统一处理 增加限制查询本人信息
        String _userIdParam= _addition.getStr("userIdParam", "");
        String _accountParam= _addition.getStr("accountParam", "");
        if (!"".equals(_userIdParam)) {
            String _value= getParamsValue(params.getStr("params2"), _userIdParam);
            if (!String.valueOf(userInfo.getUserId()).equals(_value)) {
                throw new Exception("不能查询他人数据");
            }
        }
        if (!"".equals(_accountParam)) {
            String _value= getParamsValue(params.getStr("params2"), _userIdParam);
            if (!String.valueOf(userInfo.getAccount()).equals(_value)) {
                throw new Exception("不能查询他人数据");
            }
        }
        JSONObject _resultObj= queryFromDataServer(userInfo, params);
        if (_resultObj.getInt("code")==0) {
            JSONArray _result= _resultObj.getByPath("data.data", JSONArray.class);
            //统一处理 增加附件信息
            String _fileTable= _addition.getStr("fileTable", "");
            String _fileField= _addition.getStr("fileField", "");
            if (!"".equals(_fileTable) && !"".equals(_fileField)) {
                for(int i=0; i<_result.size(); i++) {
                    long fId= _result.getJSONObject(i).getLong(_fileField, 0L);
                    ArrayList<YthFormFile> _fileList= fileReposi.queryYthFormFileList(_fileTable, fId);
                    _result.getJSONObject(i).putOpt("fileList", _fileList);
                }
            }
            return _result;
        } else {
            String _errorInfo = StrUtil.format("dataServer查询服务发生异常: {}", _resultObj.getStr("msg"));
            throw new Exception(_errorInfo);
        }
    }

    /**
     * 查询服务器封装, 直接返回data
     * @param userInfo
     * @param params
     * @return
     * @throws Exception
     */
    public JSONObject queryPageList(AuthUserInfoModel userInfo, JSONObject params) throws Exception {
        if (params.isNull("params")) params.putOpt("params", "");
        if (params.isNull("params2")) params.putOpt("params2", "");
        JSONObject _resultObj= queryFromDataServer(userInfo, params);
        if (_resultObj.getInt("code")==0) {
            return _resultObj.getByPath("data", JSONObject.class);
        } else {
            String _errorInfo = StrUtil.format("dataServer查询服务发生异常: {}", _resultObj.getStr("msg"));
            throw new Exception(_errorInfo);
        }
    }

    /**
     * 查询服务器封装, 直接返回data.data
     * @param userInfo
     * @param params
     * @return
     * @throws Exception
     */
    public JSONObject queryOne(AuthUserInfoModel userInfo, JSONObject params) throws Exception {
        if (params.isNull("params")) params.putOpt("params", "");
        if (params.isNull("params2")) params.putOpt("params2", "");
        JSONArray _result= queryList(userInfo, params);
        return (_result.size()>0)?_result.getJSONObject(0):new JSONObject();
    }

    /**
     * 查询服务器封装
     * @param token
     * @param params
     * @return
     * @throws Exception
     */
    @Deprecated
    public JSONObject queryFromDataServer(String token, JSONObject params) throws Exception {
        Integer sqlId = params.getInt("sqlId");
        if (sqlId==null) {
            throw new Exception("缺少参数 sqlId");
        }
        JSONObject _loginInfo = authService.getLoginInfo(token);
        String _dataSvrQueryUrl = StrUtil.format("{}/ds/{}?token={}&ctype={}&sysid={}",
                dataSvrUrl, sqlId, token, _loginInfo.getStr("ctype"), _loginInfo.getStr("sysId"));
        String result= HttpRequest.post(_dataSvrQueryUrl)
                .body(params.toString())
                .execute()
                .body();
        log.info("dataServer查询服务: {}\n返回:{}", _dataSvrQueryUrl, result);
        return new JSONObject(result);
    }

    /**
     * 查询服务器封装, 直接返回data.data
     * @param token
     * @param params
     * @return
     * @throws Exception
     */
    @Deprecated
    public JSONArray queryList(String token, JSONObject params) throws Exception {
        JSONObject _resultObj= queryFromDataServer(token, params);
        if (_resultObj.getInt("code")==0) {
            return _resultObj.getByPath("data.data", JSONArray.class);
        } else {
            String _errorInfo = StrUtil.format("dataServer查询服务发生异常: {}",  _resultObj);
            throw new Exception(_errorInfo);
        }
    }

    /**
     * 查询服务器封装, 直接返回data.data
     * @param token
     * @param params
     * @return
     * @throws Exception
     */
    @Deprecated
    public JSONObject queryOne(String token, JSONObject params) throws Exception {
        JSONArray _result= queryList(token, params);
        return (_result.size()>0)?_result.getJSONObject(0):new JSONObject();
    }
 
}
