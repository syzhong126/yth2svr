package com.szkj.common.service;

import cn.hutool.json.JSONObject;
import com.szkj.common.util.AliSmsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AliSMSSerice {
    @Value("${alisms.accessKeyId:LTAI5tQg6KCTVoyqCFg8Us22}")
    private String ALISMS_ACCESSKEYID;
    @Value("${alisms.secred:E7NcF9VSn31k6NrPhDYg31GBzJSlea}")
    private String ALISMS_SECRED;
    @Value("${alisms.signName:长沙城发集团}")
    private String ALISMS_SIGNNAME;
    @Value("${alisms.template.sendcode:SMS_219406005}")
    private String ALISMS_TEMPLATE_SENDCODE;
    @Value("${alisms.template.sendnewpwd:SMS_219395769}")
    private String ALISMS_TEMPLATE_SENDNEWPWD;
    @Value("${alisms.template.sendvisitorinvitation:SMS_219405716}")
    private String ALISMS_TEMPLATE_SENDVISITORINVITATION;

    /**
     * 给手机号发送重置密码验证码
     * @param mobile
     * @param verifyCode
     * @return
     */
    public String sendVerifyCode(String mobile, String verifyCode) {
        JSONObject _params = new JSONObject();
        _params.putOpt("no", verifyCode);
        String _result= AliSmsUtil.sendSms(mobile, ALISMS_TEMPLATE_SENDCODE,
                _params.toString(), ALISMS_SIGNNAME, ALISMS_ACCESSKEYID, ALISMS_SECRED);
        return _result;
    }

    /**
     * 给手机号发送新密码
     * @param mobile
     * @param newPwd
     * @return
     */
    public String sendNewPassword(String mobile, String newPwd) {
        JSONObject _params = new JSONObject();
        _params.putOpt("newcode", newPwd);
        String _result= AliSmsUtil.sendSms(mobile, ALISMS_TEMPLATE_SENDNEWPWD,
                _params.toString(), ALISMS_SIGNNAME, ALISMS_ACCESSKEYID, ALISMS_SECRED);
        return _result;
    }

    /**
     * 发送邀约短信
     * @param mobile
     * @param booker
     * @param month
     * @param day
     * @param userName
     * @param uid
     * @return
     */
    public String sendVisitorInvitation(String mobile, String booker, int month, int day, String userName, String uid) {
        JSONObject _params = new JSONObject();
        _params.putOpt("name", booker);
        _params.putOpt("month", month);
        _params.putOpt("day", day);
        _params.putOpt("jtname", userName);
        _params.putOpt("uid", uid);
        String _result= AliSmsUtil.sendSms(mobile, ALISMS_TEMPLATE_SENDVISITORINVITATION,
                _params.toString(), ALISMS_SIGNNAME, ALISMS_ACCESSKEYID, ALISMS_SECRED);
        return _result;
    }
}
