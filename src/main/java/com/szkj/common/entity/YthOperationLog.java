package com.szkj.common.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Objects;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2022年04月24日 15:38
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_operation_log")
public class YthOperationLog {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "op_user")
    private Integer opUser;
    @Column(name = "ask_type")
    private String askType;
    @Column(name = "model_name")
    private String modelName;
    @Column(name = "interface_name")
    private String interfaceName;
    @Column(name = "post_msg")
    private String postMsg;
    @Column(name = "response_status")
    private String responseStatus;
    @Column(name = "error_message")
    private String errorMessage;
    /**
     * 创建人
     */
    @Column(name = "create_by", nullable = false)
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Long createTime;

    /**
     * 修改人
     */
    @Column(name = "update_by", nullable = false)
    private Integer updateBy;

    /**
     * 修改时间
     */
    @Column(name = "update_time", nullable = false)
    private Long updateTime;


}
