package com.szkj.common.util;

import cn.hutool.json.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
@Slf4j
public class AliSmsUtil {

    public static String sendSms(String PhoneNumbers, String TemplateCode, String TemplateParam, String SignName, String accessKeyId, String secred) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, secred);
        IAcsClient client = new DefaultAcsClient(profile);
        CommonResponse response = null;
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", PhoneNumbers);
        request.putQueryParameter("SignName", SignName);
        request.putQueryParameter("TemplateCode", TemplateCode);
        request.putQueryParameter("TemplateParam", TemplateParam);

        try {
            response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException var10) {
            var10.printStackTrace();
        } catch (ClientException var11) {
            var11.printStackTrace();
        }

        JSONObject _result = new JSONObject(response.getData());
        log.info("发送短信: {} {} {}\n返回:{}", PhoneNumbers, TemplateCode, TemplateParam, _result);
        return !_result.getStr("Code").equals("OK") ? response.getData() : "OK";
    }
}
