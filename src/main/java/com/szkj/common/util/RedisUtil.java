package com.szkj.common.util;

import cn.hutool.db.nosql.redis.RedisDS;
import cn.hutool.json.JSONUtil;
import cn.hutool.setting.Setting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

@Slf4j
@Component
public class RedisUtil {
    private static final String REDIS_SETTING = "config/redis.setting";
    private static RedisDS redisDS;
    public RedisUtil() {
        Setting setting = new Setting(REDIS_SETTING);
        redisDS = RedisDS.create(setting, null);
    }

    public static Jedis getJedis() {
        return redisDS.getJedis();
    }
}