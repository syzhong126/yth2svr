package com.szkj.common.util;

import cn.hutool.json.JSONObject;

public class JsonUtil {

    public static void resetJsonKeyValue(JSONObject body, String key, Object value) {
        if (body.containsKey(key)) {
            body.replace(key, value);
        } else {
            body.set(key, value);
        }
    }
}
