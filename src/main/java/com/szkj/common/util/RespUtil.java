package com.szkj.common.util;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

public class RespUtil {
    private static final int CODE_SUCCESS = 0;
    private static final int CODE_ERROR = 1;

    public static String success(String message, Object data) {
        return JSONUtil.createObj()
                .putOpt("code", CODE_SUCCESS)
                .putOpt("msg", message)
                .putOpt("data", data)
                .toString();
    }

    public static String error(String message, Object data) {
        return JSONUtil.createObj()
                .putOpt("code", CODE_ERROR)
                .putOpt("msg", message)
                .putOpt("data", data)
                .toString();
    }

    public static String error(String message) {
        return error(message, null);
    }

    public static String raw(int code, String message, Object data) {
        return JSONUtil.createObj()
                .putOpt("code", code)
                .putOpt("msg", message)
                .putOpt("data", data)
                .toString();
    }
}
