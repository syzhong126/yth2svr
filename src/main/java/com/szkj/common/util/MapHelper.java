package com.szkj.common.util;

import java.math.BigDecimal;

public class MapHelper {
	
	 /**
     * 地球半径
     */
    private static double EarthRadius = 6378.137;
 
    /**
     * 经纬度转化成弧度
     *
     * @param d 经度/纬度
     * @return
     */
    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }
 
    /**
     * 计算两个坐标点之间的距离
     *
     * @param firstLatitude   第一个坐标的纬度
     * @param firstLongitude  第一个坐标的经度
     * @param secondLatitude  第二个坐标的纬度
     * @param secondLongitude 第二个坐标的经度
     * @return 返回两点之间的距离，单位：公里/千米
     */
    public static double getDistance(double firstLatitude, double firstLongitude,
                                     double secondLatitude, double secondLongitude) {
        double firstRadLat = rad(firstLatitude);
        double firstRadLng = rad(firstLongitude);
        double secondRadLat = rad(secondLatitude);
        double secondRadLng = rad(secondLongitude);
 
        double a = firstRadLat - secondRadLat;
        double b = firstRadLng - secondRadLng;
        double cal = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(firstRadLat)
                * Math.cos(secondRadLat) * Math.pow(Math.sin(b / 2), 2))) * EarthRadius;
        double result = Math.round(cal * 10000d) / 10000d;
        
        BigDecimal   r   =   new   BigDecimal(result); 
        double   f1   =   r.setScale(2,   BigDecimal.ROUND_HALF_UP).doubleValue(); 
        
        return f1;
    }
 
    /**
     * 计算两个坐标点之间的距离
     *
     * @param firstPoint  第一个坐标点的（纬度,经度） 例如："31.2553210000,121.4620020000"
     * @param secondPoint 第二个坐标点的（纬度,经度） 例如："31.2005470000,121.3269970000"
     * @return 返回两点之间的距离，单位：公里/千米
     */
    public static double GetPointDistance(String firstPoint, String secondPoint) {
        String[] firstArray = firstPoint.split(",");
        String[] secondArray = secondPoint.split(",");
        double firstLatitude = Double.valueOf(firstArray[0].trim());
        double firstLongitude = Double.valueOf(firstArray[1].trim());
        double secondLatitude = Double.valueOf(secondArray[0].trim());
        double secondLongitude = Double.valueOf(secondArray[1].trim());
        return getDistance(firstLatitude, firstLongitude, secondLatitude, secondLongitude);
    }
    
    public static void main( String[] args ) {
    	
		String firstPoint = "28.987896" + "," + "111.664206";
		String secondPoint = "29.061293" + "," + "111.55998";
		
		String local = "25.715972900390625,112.98456573486328";
		String point1 = "28.987896,111.664206";
		String point2 = "25.713055,112.989719";
		String point3 = "29.058031,111.565362";
		String point4 = "26.43352,112.885446";
		String point5 = "26.452963,112.836685";
		String point6 = "29.068212,111.707544";
		String point7 = "28.214731,113.482513";
		String point8 = "27.885168,112.971392";
		String point9 = "26.82495,112.622143";
		String point10 = "28.21587,113.445534";
		String point11 = "28.454338,112.438868";
		String point12 = "25.968618,113.015785";
		
		double distance1 = MapHelper.GetPointDistance(local, point1);
		System.out.println("distance1:"+distance1);
		double distance2 = MapHelper.GetPointDistance(local, point2);
		System.out.println("distance2:"+distance2);
		double distance3 = MapHelper.GetPointDistance(local, point3);
		System.out.println("distance3:"+distance3);
		double distance4 = MapHelper.GetPointDistance(local, point4);
		System.out.println("distance4:"+distance4);
		double distance5 = MapHelper.GetPointDistance(local, point5);
		System.out.println("distance5:"+distance5);
		double distance6 = MapHelper.GetPointDistance(local, point6);
		System.out.println("distance6:"+distance6);
		double distance7 = MapHelper.GetPointDistance(local, point7);
		System.out.println("distance7:"+distance7);
		double distance8 = MapHelper.GetPointDistance(local, point8);
		System.out.println("distance8:"+distance8);
		double distance9 = MapHelper.GetPointDistance(local, point9);
		System.out.println("distance9:"+distance9);
		double distance10 = MapHelper.GetPointDistance(local, point10);
		System.out.println("distance10:"+distance10);
		double distance11 = MapHelper.GetPointDistance(local, point11);
		System.out.println("distance11:"+distance11);
		double distance12 = MapHelper.GetPointDistance(local, point12);
		System.out.println("distance12:"+distance12);
		
    }

}
