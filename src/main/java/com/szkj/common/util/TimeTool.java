package com.szkj.common.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeTool {

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static Date longToDate(long ltm) {
		Date dt = new Date();
		dt.setTime(ltm * 1000);

		return dt;
	}
	
	public static Date stringToDate( String dateString){
		Date date= null;
		
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;		
	}
	
	public static long stringToLong( String dateString){
		Date date= stringToDate( dateString);
		
		return date.getTime()/1000;
	}
	
	/***
	 * 获取unixtime时间
	 * @param dt：可为null，则默认当前时间
	 * @return
	 */
	public static long getTimeLong(Date dt){
		if ( dt== null)
			dt= new Date();
		return dt.getTime()/1000;
	}


	public static String longToStr( long ltm){
		Date dt = new Date();
		dt.setTime(ltm );
		
		return sdf.format( dt);  
	}

	public static String longToStr( long ltm, String format){
		Date dt = new Date();
		dt.setTime(ltm );
		SimpleDateFormat sdf2 = new SimpleDateFormat(format);
		return sdf2.format( dt);
	}
	
	public static String unixtimeToStr( long ltm){
		Date dt= longToDate( ltm);
		String str= sdf.format(dt);
		
		return str;
	}

	public static String getTimeStr( Date dt, String sdfstr){
		if( !sdfstr.isEmpty())
			sdf = new SimpleDateFormat( sdfstr);
		if ( null== dt)
			dt= new Date();
		return sdf.format(dt);
	}
	
	public static String timeStampToStr( Timestamp tt){
		return sdf.format( tt);
	}
}
