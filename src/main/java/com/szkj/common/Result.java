package com.szkj.common;

import lombok.Data;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 8996400044539864L;
    private String msg;
    private int code;
    private T data;

    public static Result<Object> OK() {
        Result<Object> r = new Result<>();
        r.code = 0;
        r.msg = "成功";
        return r;
    }

    public static <T> Result<T> Raw(int code, String msg, T data) {
        Result<T> r = new Result<T>();
        r.code = code;
        r.msg = msg;
        r.data = data;
        return r;
    }

    public static <T> Result<T> OK(String msg, T o) {
        Result<T> r = new Result<T>();
        r.code = 0;
        r.msg = msg;
        r.data = o;
        return r;
    }

    public static Result<Object> OK(String msg) {
        Result<Object> r = new Result<>();
        r.code = 0;
        r.msg = msg;
        r.data = null;
        return r;
    }


    public static Result<Object> Error(String msg) {
        Result<Object> r = new Result<>();
        r.code = 1;
        r.msg = msg;
        return r;
    }
}
