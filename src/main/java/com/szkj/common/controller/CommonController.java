package com.szkj.common.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.service.SQLService;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author xiegl
 */
@Component
@RestController
@CrossOrigin("*")
@RequestMapping(value="/common")
@Slf4j
public class CommonController {
    @Autowired
    private SQLService sqlService;

    @PostMapping("/query")
    public String doSearchObject(@RequestBody JSONObject body,
                                 @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        try {
            return sqlService.queryFromDataServer(userInfo, body).toString();
        } catch (Exception e) {
            log.error("/common/query {}", e.getMessage(), e);
            e.printStackTrace();
            return RespUtil.error(e.getMessage());
        }
    }

    @PostMapping("/queryList")
    public String doSearchList(@RequestBody JSONObject body,
                               @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        try {
            JSONArray _result= sqlService.queryList(userInfo, body);
            return RespUtil.success("成功", _result);
        } catch (Exception e) {
            log.error("/common/queryList {}", e.getMessage(), e);
            e.printStackTrace();
            return RespUtil.error(e.getMessage());
        }
    }
    @PostMapping("/queryOne")
    public String doSearchOne(@RequestBody JSONObject body,
                              @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        try {
            JSONObject _result= sqlService.queryOne(userInfo, body);
            return RespUtil.success("成功", _result);
        } catch (Exception e) {
            log.error("/common/queryOne {}", e.getMessage(), e);
            e.printStackTrace();
            return RespUtil.error(e.getMessage());
        }
    }
    @PostMapping("/todoCount")
    public String doGetTodoCount(@RequestBody(required = false) JSONObject body,
                              @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) throws Exception {
        JSONArray _list= sqlService.customQueryList(3024, StrUtil.format("userId:{}", userInfo.getUserId()));
        String _dataList= (null==body)? "": body.getStr("dataList");
        _dataList = StrUtil.isBlank(_dataList)?"pimessage,supervision,groupnews,groupnotice":_dataList;
        boolean _hasDetail= (null==body)? false: body.getBool("hasDetail", false);
        String[] _dataArr= _dataList.split(",");
        JSONObject _result= new JSONObject();
        for(Object _item: _list) {
            JSONObject _temp = (JSONObject) _item;
            if (Arrays.asList(_dataArr).contains("pimessage") && "pimessage".equals(_temp.getStr("todoName"))) {
                JSONObject _val= new JSONObject();
                _val.putOpt("total", _temp.getInt("todoNum"));
                if (_hasDetail) {
                    JSONArray _tempList= sqlService.customQueryList(3025, StrUtil.format("userId:{}", userInfo.getUserId()));
                    _val.putOpt("detail", _tempList);
                }
                _result.putOpt("pimessage", _val);
            }
            if (Arrays.asList(_dataArr).contains("supervision") && "supervision".equals(_temp.getStr("todoName"))) {
                JSONObject _val= new JSONObject();
                _val.putOpt("total", _temp.getInt("todoNum"));
                if (_hasDetail) {
                    JSONArray _tempList= sqlService.customQueryList(3026, StrUtil.format("userId:{}", userInfo.getUserId()));
                    _val.putOpt("detail", _tempList);
                }
                _result.putOpt("supervision", _val);
            }
            if (Arrays.asList(_dataArr).contains("groupnews") && "groupnews".equals(_temp.getStr("todoName"))) {
                JSONObject _val= new JSONObject();
                _val.putOpt("total", _temp.getInt("todoNum"));
                if (_hasDetail) {
                    JSONArray _tempList= sqlService.customQueryList(3027, StrUtil.format("userId:{}", userInfo.getUserId()));
                    _val.putOpt("detail", _tempList);
                }
                _result.putOpt("groupnews", _val);
            }
            if (Arrays.asList(_dataArr).contains("groupnotice") && "groupnotice".equals(_temp.getStr("todoName"))) {
                JSONObject _val= new JSONObject();
                _val.putOpt("total", _temp.getInt("todoNum"));
                if (_hasDetail) {
                    JSONArray _tempList= sqlService.customQueryList(3028, StrUtil.format("userId:{}", userInfo.getUserId()));
                    _val.putOpt("detail", _tempList);
                }
                _result.putOpt("groupnotice", _val);
            }
        }
        return RespUtil.success("成功", _result);
    }
}
