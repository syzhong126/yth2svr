package com.szkj.common;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Page<T> implements Serializable {
    private static final long serialVersionUID = 89964003464L;
    Integer pagerows;
    Integer totalrows;
    Integer id;
    Integer pagenum;
    String sqlname;
    Integer rows;
    List<T> data;
    Integer pageSum;

    public Integer getPageSum() {
        if (pageSum == null) return 0;
        if (pageSum > 0) return pageSum;
        pageSum = totalrows / pagerows;
        if (totalrows % pagerows > 0) pageSum++; //计算页数
        return pageSum;
    }
}
