package com.szkj.common.anno;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import com.szkj.common.service.SQLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DictAnnoValidator implements ConstraintValidator<DictAnno, Object> {
    @Autowired
    private SQLService sqlService;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String dictCode;
    /**
     *@param constraintAnnotation 验证注解的实例
     */
    @Override
    public void initialize(DictAnno constraintAnnotation) {
        dictCode = constraintAnnotation.dictCode();
    }
    /**
     *实现验证逻辑
     *@param  o 需要验证的对象
     *@param constraintValidatorContext 约束验证器的上下文
     */
    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        try {
            if (sqlService==null) return true; //生命周期问题, 取不到service时默认通过
            JSONArray _result= sqlService.customQueryList(998, StrUtil.format("{dictCode:{},dictVal:{}}", dictCode, String.valueOf(o)));
            return _result.size() > 0;
        }catch (Exception e) {
            return false;
        }
    }
}
