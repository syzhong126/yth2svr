package com.szkj.common.anno;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {DictAnnoValidator.class})
public @interface UserAnno {
    String dictCode();
    String message() default "无效的字典值";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };

}
