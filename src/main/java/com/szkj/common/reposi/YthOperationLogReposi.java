package com.szkj.common.reposi;

import com.szkj.common.entity.YthOperationLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface YthOperationLogReposi extends JpaRepository<YthOperationLog, Long>, JpaSpecificationExecutor<YthOperationLog> {
}
