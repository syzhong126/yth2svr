package com.szkj.meeting.controller;

import com.szkj.attendance.Util.NoticeUtil;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.common.Page;
import com.szkj.common.Result;
import com.szkj.meeting.entity.Meeting;
import com.szkj.meeting.entity.MeetingFile;
import com.szkj.meeting.entity.MeetingItem;
import com.szkj.meeting.service.MFileService;
import com.szkj.meeting.service.MItemService;
import com.szkj.meeting.service.MUserService;
import com.szkj.meeting.service.MeetingService;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.schedule.entity.YthSchedulePlan;
import com.szkj.schedule.reposi.YthScheduleReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@Component
@RestController
@CrossOrigin("*")
@RequestMapping("/meeting")
@Slf4j
public class MeetingController {
    @Autowired
    private MeetingService meetingService;
    @Autowired
    private MItemService itemService;
    @Autowired
    private MFileService fileService;
    @Autowired
    private MUserService userService;
    @Autowired
    private YthScheduleReposi scheduleReposi;
    @Autowired
    private NoticeUtil noticeUtil;

    @GetMapping("/places")
    public List<Map<String, Object>> places(@RequestParam(name = "keyword", required = false) String keyword,
                                            @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        return meetingService.places(userInfo.getOrgId(), keyword);
    }

    /**
     * 会议查询
     *
     * @param location 会议室ID
     * @param start    开始时
     * @param end      结束时间
     * @param orgId    公司Id
     * @param pageSize 页条数
     * @param pageNo   页码
     */
    @GetMapping("/list")
    public Result<?> list(@RequestParam(name = "location", required = false) Integer location,
                          @RequestParam(name = "start", required = false) String start,
                          @RequestParam(name = "end", required = false) String end,
                          @RequestParam(name = "orgId", required = false) Integer orgId,
                          @RequestParam(name = "status", required = false, defaultValue = "1") Integer status,
                          @RequestParam(name = "pageSize", required = false, defaultValue = "10") Integer pageSize,
                          @RequestParam(name = "pageNo", required = false, defaultValue = "1") Integer pageNo,
                          @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        Page<Meeting> page = meetingService.query(null, location, start, end,
                orgId == null ? userInfo.getOrgId() : orgId, status, pageSize, pageNo);
        //if (page.getData() != null) page.getData().forEach(this::setItems);
        return Result.OK("读取成功", page);
    }

    /**
     * 我的会议查询
     *
     * @param location 会议室ID
     * @param start    开始时
     * @param end      结束时间
     * @param pageSize 页条数
     * @param pageNo   页码
     */
    @GetMapping("/my")
    public Result<?> my(@RequestParam(name = "location", required = false) Integer location,
                        @RequestParam(name = "start", required = false) String start,
                        @RequestParam(name = "end", required = false) String end,
                        @RequestParam(name = "pageSize", required = false, defaultValue = "10") Integer pageSize,
                        @RequestParam(name = "pageNo", required = false, defaultValue = "1") Integer pageNo,
                        @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        Page<Meeting> page = meetingService.query(userinfo.getUserId(), location, start, end, null, 1, pageSize, pageNo);
        //if (page.getData() != null) page.getData().forEach(this::setItems);
        return Result.OK("读取成功", page);
    }

    @GetMapping("/getById")
    public Result<?> getById(@RequestParam(name = "id") Integer id, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        Optional<Meeting> opm = meetingService.getById(id);
        opm.ifPresent(this::setItems);
        return Result.OK("读取成功", opm.orElse(null));
    }

    /**
     * 会议附件
     *
     * @param meetingId 会议ID
     * @param type      0,1
     * @return 附件
     */
    @GetMapping("/file")
    public Result<?> file(@RequestParam(name = "meetingId") Integer meetingId,
                          @RequestParam(name = "type") Integer type) {
        if (type > 1) return Result.Error("只提供会议附件");
        else return Result.OK("读取成功", fileService.getBySource(type, meetingId, 0));
    }

    /**
     * 现在正在进行的的会议
     *
     * @param location 会议室Id
     */
    @GetMapping(value = "/now")
    public Result<?> now(@RequestParam(name = "location") Integer location) {
        List<Meeting> list = meetingService.meetingNow(location);
        list.forEach(this::setItems);
        return Result.OK("读取成功", list);
    }

    /**
     * 今天的会议
     *
     * @param location 会议室Id
     */
    @GetMapping(value = "/today")
    public Result<?> today(@RequestParam(name = "location") Integer location) {
        List<Meeting> list = meetingService.meetingToday(location);
        list.forEach(this::setItems);
        return Result.OK("读取成功", list);
    }

    /**
     * 最靠后的会议
     *
     * @param location 会议室Id
     */
    @GetMapping(value = "/latest")
    public Result<?> latest(@RequestParam(name = "location") Integer location) {
        Optional<Meeting> meeting = meetingService.latest(location);
        meeting.ifPresent(this::setItems);
        return Result.OK("读取成功", meeting.orElse(null));
    }

    /**
     * 给会议对象加上议题，用户，附件，议题也带上附件
     *
     * @param meeting 会议
     */
    public void setItems(Meeting meeting) {
        meeting.setUsers(userService.getUsers(meeting.getId(), null));
        meeting.setPlans(fileService.getBySource(MeetingFile.File_Type_Plan, meeting.getId(), 0));
        meeting.setSummaries(fileService.getBySource(MeetingFile.File_Type_Summary, meeting.getId(), 0));

        meeting.setItems(itemService.getByMeeting(meeting.getId()));
        meeting.getItems().forEach(i -> {
            i.setUsers(userService.getUsers(i.getMeetingId(), i.getId()));
            i.setFiles(fileService.getBySource(MeetingFile.File_Type_Annex, meeting.getId(), i.getId()));
            i.setSummaries(fileService.getBySource(MeetingFile.File_Type_Item_Summary, meeting.getId(), i.getId()));
        });
    }

    /**
     * 添加会议
     *
     * @param meeting 会议
     */
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody Meeting meeting, @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        meeting.setOrgId(userInfo.getOrgId());
        meeting.setStatus(Meeting.StatusEdit);
        userService.saveUsers(meeting.getUsers(), meeting.getId(), null);
        meetingService.save(meeting);
        return Result.OK("添加成功", meeting);
    }

    @PostMapping(value = "/save")
    public Result<?> save(@RequestBody Meeting meeting, @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        AtomicLong start = new AtomicLong();
        if (meeting.getId() != null)
            meetingService.getById(meeting.getId()).ifPresent(m -> start.set(m.getStartTime()));
        meeting.setOrgId(userInfo.getOrgId());
        //保存会议，必须先保存会议，因为保存了才有id
        meetingService.save(meeting);
        //保存参会人员
        userService.saveUsers(meeting.getUsers(), meeting.getId(), null);
        //更新会议附件
        fileService.saveFiles(MeetingFile.File_Type_Plan, meeting.getId(), meeting.getPlans());
        fileService.saveFiles(MeetingFile.File_Type_Summary, meeting.getId(), meeting.getSummaries());
        //更新议题
        saveItems(meeting.getItems(), meeting.getId());
        //清理临时文件
        fileService.clearTemp();
        meetingService.updateSchedule(start.get(), meeting, userInfo);
        return Result.OK("添加成功", meeting);
    }

    public void saveItems(List<MeetingItem> list, Integer meetingId) {
        if (list != null) {//更新参会人员
            List<MeetingItem> oList = itemService.getByMeeting(meetingId);//读取库中已有
            list.forEach(u -> {
                u.setMeetingId(meetingId);
                //保存议题，必须先保存议题，因为保存了才有id
                itemService.save(u);
                //保存议题参与者
                userService.saveUsers(u.getUsers(), meetingId, u.getId());
                //保存议题附件
                fileService.saveFiles(MeetingFile.File_Type_Annex, u.getId(), u.getFiles());
                fileService.saveFiles(MeetingFile.File_Type_Item_Summary, u.getId(), u.getSummaries());
                //是否原来的议题
                oList.stream().filter(m -> Objects.equals(m.getId(), u.getId())).findFirst().ifPresent(oList::remove);
            });
            //消失的议题，删除
            oList.forEach(i -> {
                itemService.deleteById(i.getId());//删除议题
                userService.deleteUsers(meetingId, i.getId());
                fileService.delFile(i.getMeetingId(), i.getId());//删除议题附件目录
            });
        }
    }

    @GetMapping("/getSummary")
    public Result<?> getSummary(@RequestParam(name = "meetingId") Integer meetingId) {
        Optional<Meeting> opm = meetingService.getById(meetingId);
        if (!opm.isPresent()) return Result.Error("会议不存在");
        Meeting meeting = opm.get();
        meeting.setSummaries(fileService.getBySource(MeetingFile.File_Type_Summary, meeting.getId(), 0));
        meeting.setItems(itemService.getByMeeting(meeting.getId()));
        meeting.getItems().forEach(i -> {
            i.setSummaries(fileService.getBySource(MeetingFile.File_Type_Item_Summary, meeting.getId(), i.getId()));
        });
        return Result.OK("成功", meeting);
    }

    @PostMapping(value = "/saveSummary")
    public Result<?> saveSummary(@RequestBody Meeting meeting) {
        fileService.saveFiles(MeetingFile.File_Type_Summary, meeting.getId(), meeting.getSummaries());
        meeting.getItems().forEach(u -> {
            fileService.saveFiles(MeetingFile.File_Type_Item_Summary, u.getId(), u.getSummaries());
        });
        return Result.OK("成功");
    }

    /**
     * 编辑会议
     *
     * @param meeting 会议
     */
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody Meeting meeting, @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        meeting.setOrgId(userInfo.getOrgId());
        meeting.setStatus(Meeting.StatusEdit);
        userService.saveUsers(meeting.getUsers(), meeting.getId(), null);
        meetingService.save(meeting);
        return Result.OK("修改成功", meeting);
    }

    /**
     * 删除会议，执行逻辑删除
     *
     * @param id 会议id
     */
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id") Integer id) {
        Optional<Meeting> opm = meetingService.getById(id);
        if (opm.isPresent()) {
            Meeting m = opm.get();
            //log.info("会议：{}", m);
            if (m.getStatus() == Meeting.StatusPublish)
                return Result.Error("已发布会议不能删除");
            else if (m.getStatus() == Meeting.StatusDelete) {
                itemService.getByMeeting(id).forEach(i -> {
                    fileService.delFile(id, i.getId());
                    userService.deleteUsers(id, i.getId());
                    itemService.deleteById(i.getId());
                });
                fileService.delFile(id, null);
                userService.deleteUsers(id, null);
                meetingService.deleteById(id);//删除会议
            } else {
                m.setStatus(Meeting.StatusDelete);
                meetingService.save(m);
            }
            return Result.OK("删除成功");
        } else return Result.Error("会议不存在");
    }

    /**
     * 会议发布
     *
     * @param id 会议Id
     */
    @GetMapping(value = "/publish")
    public Result<?> publish(@RequestParam(name = "id") Integer id,
                             @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        changeStatus(id, Meeting.StatusPublish, userInfo.getUserId(), userInfo.getAccount());
        return Result.OK();
    }

    /**
     * 会议取消
     *
     * @param id 会议Id
     */
    @GetMapping(value = "/cancel")
    public Result<?> cancel(@RequestParam(name = "id") Integer id,
                            @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        changeStatus(id, Meeting.StatusCancel, userInfo.getUserId(), userInfo.getAccount());
        return Result.OK();
    }

    /**
     * 会议添加日程信息，
     *
     * @param id      会议Id
     * @param status  会议状态
     * @param userId  用户Id
     * @param account 用户Account
     */
    private void changeStatus(Integer id, int status, Integer userId, String account) {
        //log.info("meeting changeStatus:{}", id);
        Optional<Meeting> optionalMeeting = meetingService.getById(id);
        optionalMeeting.ifPresent(meeting -> {
            //log.info("status:{},{}", meeting.getStatus(), status);
            if (meeting.getStatus() == status) return;
            //long time = meeting.getDate();
            meeting.setStatus(status);
            meetingService.save(meeting);
            //log.info("update:{}", meeting.getStatus());
            List<YthSchedulePlan> planList = new ArrayList<>();
            List<YthNoticePush> noticeList = new ArrayList<>();
            //给所有会议关联的人员发消息
            userService.getUsers(meeting.getId(), null).forEach(map -> {
                //添加日程
                YthSchedulePlan plan = meetingService.getSchedule(meeting, (Integer) map.get("userId"), userId);
                planList.add(plan);
                //添加消息
                YthNoticePush n = meetingService.getNotice(plan, meeting, account);
                noticeList.add(n);//添加消息
                //附加消息，会议之前30分钟还需要第二次提醒
                if (meeting.getStatus() == Meeting.StatusPublish) {//如果发布会议
                    YthNoticePush notice = new YthNoticePush();
                    BeanUtils.copyProperties(n, notice, "id");
                    notice.setMoudleType("nearschedule");
                    notice.setTitle(notice.getTitle());
                    notice.setPushTime(plan.getBeginTime() * 1000 - 30 * 60 * 1000);
                    noticeList.add(notice);//添加或修改消息
                }
            });
            scheduleReposi.saveAll(planList);
            noticeUtil.save(noticeList);
        });
    }

}
