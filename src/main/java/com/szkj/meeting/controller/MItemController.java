package com.szkj.meeting.controller;

import com.szkj.common.Result;
import com.szkj.meeting.entity.MeetingFile;
import com.szkj.meeting.entity.MeetingItem;
import com.szkj.meeting.service.MFileService;
import com.szkj.meeting.service.MItemService;
import com.szkj.meeting.service.MUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/meeting/item")
@Slf4j
public class MItemController {
    @Autowired
    private MItemService itemService;
    @Autowired
    private MFileService fileService;
    @Autowired
    private MUserService userService;

    /**
     * 议题列表
     *
     * @param meetingId 会议Id
     */
    @GetMapping("/list")
    public Result<?> list(@RequestParam(name = "meetingId") Integer meetingId) {
        List<MeetingItem> list = itemService.getByMeeting(meetingId);
        list.forEach(this::setItems);
        return Result.OK("读取成功", list);
    }

    /**
     * 议题附件列表
     *
     * @param itemId 议题Id
     */
    @GetMapping("/file")
    public Result<?> file(@RequestParam(name = "itemId") Integer itemId) {
        Optional<MeetingItem> opi = itemService.getById(itemId);
        if (!opi.isPresent()) return Result.Error("议题不存在");
        return Result.OK("读取成功", fileService.getBySource(MeetingFile.File_Type_Annex, opi.get().getMeetingId(), itemId));
    }

    /**
     * 议题纪要
     *
     * @param itemId 议题Id
     */
    @GetMapping("/summary")
    public Result<?> summary(@RequestParam(name = "itemId") Integer itemId) {
        Optional<MeetingItem> opi = itemService.getById(itemId);
        if (!opi.isPresent()) return Result.Error("议题不存在");
        return Result.OK("读取成功", fileService.getBySource(MeetingFile.File_Type_Item_Summary, opi.get().getMeetingId(), itemId));
    }

    /**
     * 给议题附件附件、纪要，人员等
     *
     * @param item 议题
     */
    public void setItems(MeetingItem item) {
        item.setUsers(userService.getUsers(item.getMeetingId(), item.getId()));
        item.setFiles(fileService.getBySource(MeetingFile.File_Type_Annex, item.getMeetingId(), item.getId()));
        item.setSummaries(fileService.getBySource(MeetingFile.File_Type_Item_Summary, item.getMeetingId(), item.getId()));
    }

    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MeetingItem meetingItem) {
        itemService.save(meetingItem);
        userService.saveUsers(meetingItem.getUsers(), meetingItem.getMeetingId(), meetingItem.getId());
        itemService.updateEndTime(meetingItem.getMeetingId());
        return Result.OK("添加成功", meetingItem);
    }

    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MeetingItem meetingItem) {
        itemService.save(meetingItem);
        userService.saveUsers(meetingItem.getUsers(), meetingItem.getMeetingId(), meetingItem.getId());
        itemService.updateEndTime(meetingItem.getMeetingId());
        return Result.OK("修改成功", meetingItem);
    }

    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id") Integer id) {
        itemService.getById(id).ifPresent(i -> {
            itemService.deleteById(id);//删除议题
            userService.deleteUsers(i.getMeetingId(), i.getId());
            fileService.delFile(i.getMeetingId(), i.getId());//删除议题附件目录
        });
        return Result.OK("删除成功");
    }
}
