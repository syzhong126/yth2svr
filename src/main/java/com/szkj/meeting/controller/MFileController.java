package com.szkj.meeting.controller;

import com.szkj.common.Result;
import com.szkj.meeting.entity.MeetingFile;
import com.szkj.meeting.service.MFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("/meeting/file")
@Slf4j
public class MFileController {
    @Autowired
    private MFileService service;


    /**
     * 文件列表
     *
     * @param type     文件类型，0，1，2，3
     * @param sourceId 源Id，01，会议Id；23议题Id
     */
    @GetMapping("/list")
    public Result<?> list(@RequestParam(name = "type") Integer type,
                          @RequestParam(name = "sourceId") Integer sourceId) {
        return Result.OK("读取成功", service.getBySource(type, sourceId));
    }

    /**
     * 删除文件
     *
     * @param id 文件Id
     */
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id") Integer id) {
        return service.delete(id) ? Result.OK("删除成功") : Result.Error("删除失败");
    }

    /**
     * 上传文件，使用formdata提交
     *
     * @param type     文件类型
     * @param sourceId 所属会议或议题
     * @param file     文件对象
     */
    @PostMapping(value = "/upload")
    public Result<?> upload(@RequestParam(name = "type") Integer type,
                            @RequestParam(name = "sourceId", required = false, defaultValue = "-1") Integer sourceId,
                            @RequestParam(name = "file") MultipartFile file) {
        MeetingFile f = service.uploadLocal(file, type, sourceId);
        if (f != null) return Result.OK("上传成功", f);
        else return Result.Error("上传失败:" + file.getOriginalFilename());
    }


    /**
     * 文件顺序调整
     *
     * @param list 批量操作
     */
    @PostMapping(value = "/sort")
    public Result<?> sort(@RequestBody List<Map<String, Integer>> list) {
        list.forEach(map -> service.sort(map.get("id"), map.get("sort")));
        return Result.OK("排序成功");
    }

    /**
     * 根据文件路径下载文件
     */
    @GetMapping(value = "/down/**")
    public void down(HttpServletRequest request, HttpServletResponse response) {
        String path = String.join(File.separator, service.getBasePath(), request.getRequestURL().substring(request.getRequestURL().indexOf("down") + 5));
        String fileName = path.substring(path.lastIndexOf("/") + 1);
        service.downLoad(response, fileName, path);
    }

    /**
     * 根据Id下载文件
     *
     * @param id 文件Id
     */
    @GetMapping(value = "/down")
    public void view(@RequestParam(name = "id") Integer id,
                     HttpServletResponse response) {
        service.down(id, response);
    }

}
