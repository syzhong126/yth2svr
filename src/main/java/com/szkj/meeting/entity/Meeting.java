package com.szkj.meeting.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Table(name = "yth_meeting")
public class Meeting implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int StatusEdit = 0;
    public static final int StatusPublish = 1;
    public static final int StatusCancel = 2;
    public static final int StatusDelete = 3;


    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private java.lang.Integer id;
    /**
     * 名称
     */
    @Column(name = "name")
    private java.lang.String name;
    /**
     * 日期
     */
    @Column(name = "date")
    private java.lang.Long date;
    @Column(name = "org_id")
    private java.lang.Integer orgId;
    private transient String orgName;
    /**
     * 地点
     */
    @Column(name = "location")
    private java.lang.Integer location;
    private transient java.lang.String locName;
    private transient java.lang.String locAddress;
    private transient java.lang.Double locArea;
    private transient java.lang.Integer locMaxPeople;
    /**
     * 主持
     */
    @Column(name = "host")
    private java.lang.Integer host;
    private transient java.lang.String hostName;
    @Column(name = "persons")
    private java.lang.String persons;
    private transient List<Map<String, Object>> users;
    /**
     * 开始时间
     */
    @Column(name = "start")
    private java.lang.String start;
    /**
     * 结束时间
     */
    @Column(name = "end")
    private java.lang.String end;
    /**
     * 结束时间
     */
    @Column(name = "status")
    private java.lang.Integer status = 0;
    private transient List<MeetingItem> items;
    /**
     * 会议纪要
     */
    private transient List<MeetingFile> Plans;
    /**
     * 会议纪要
     */
    private transient List<MeetingFile> Summaries;

    public long getStartTime() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(date);
        if (start != null) setTime(c, start);
        return c.getTimeInMillis();
    }

    public long getEndTime() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(date);
        if (end != null) setTime(c, end);
        return c.getTimeInMillis();
    }

    private void setTime(Calendar c, String time) {
        if (!time.contains(":")) return;
        String[] ss = time.split(":");
        int h = Integer.parseInt(ss[0]);
        int m = Integer.parseInt(ss[1]);
        c.set(Calendar.HOUR_OF_DAY, h);
        c.set(Calendar.MINUTE, m);
        c.clear(Calendar.SECOND);
        c.clear(Calendar.MILLISECOND);
    }
}
