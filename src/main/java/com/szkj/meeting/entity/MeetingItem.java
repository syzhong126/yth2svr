package com.szkj.meeting.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;


@Data
@Entity
@Table(name = "yth_meeting_item")
public class MeetingItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private java.lang.Integer id;
	/**会议_ID*/
    @Column(name = "meeting_id")
    private java.lang.Integer meetingId;
	/**议题*/
    @Column(name = "name")
    private java.lang.String name;
	/**开始时间*/
    @Column(name = "start")
    private java.lang.String start;
	/**结束时间*/
    @Column(name = "end")
    private java.lang.String end;
	/**参会人员*/
    @Column(name = "persons")
    private java.lang.String persons;
    private  transient List<Map<String, Object>> users;
	/**议程*/
    @Column(name = "process")
    private java.lang.String process;
    private  transient List<MeetingFile> files;
    private  transient List<MeetingFile> Summaries;
}
