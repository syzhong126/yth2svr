package com.szkj.meeting.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "yth_meeting_file")
public class MeetingFile implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会议议案
     */
    public static final int File_Type_Plan = 0;
    /**
     * 会议纪要
     */
    public static final int File_Type_Summary = 1;
    /**
     * 附件
     */
    public static final int File_Type_Annex = 2;
    /**
     * 附件
     */
    public static final int File_Type_Item_Summary = 3;

    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private java.lang.Integer id;
    @Column(name = "type")
    private java.lang.Integer type;
    /**
     * itemId
     */
    @Column(name = "source_id")
    private java.lang.Integer sourceId;
    @Column(name = "bucket")
    private String bucket;
    /**
     * fileName
     */
    @Column(name = "file_name")
    private java.lang.String fileName;
    /**
     * storeName
     */
    @Column(name = "store_name")
    private java.lang.String storeName;
    /**
     * sort
     */
    @Column(name = "sort")
    private java.lang.Integer sort;
    /**
     * date
     */
    @Column(name = "date")
    private java.lang.Long date;

    private transient String path;
}
