package com.szkj.meeting.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "yth_meeting_user")
public class MeetingUser implements Serializable {
    private static final long serialVersionUID = 123423L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "meeting_id")
    private Integer meetingId;

    @Column(name = "item_id")
    private Integer itemId;
    
    @Column(name = "user_id")
    private Integer userId;
}
