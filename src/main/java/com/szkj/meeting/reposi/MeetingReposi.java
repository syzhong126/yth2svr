package com.szkj.meeting.reposi;

import com.szkj.meeting.entity.Meeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;
import java.util.Optional;

//adddate(str_to_date(concat(date_format(FROM_UNIXTIME(m.date/1000),'%Y-%m-%d'),' ',m.start),'%Y-%m-%d %H:%i'), interval -1 hour) < now()

public interface MeetingReposi extends JpaRepository<Meeting, Integer> {
    /**
     * 当前正在进行的会议
     *
     * @param location 会议地点
     */
    @Query(value = "select * FROM yth_meeting m where m.date=UNIX_TIMESTAMP(CURRENT_DATE)*1000 " +
            "and m.`start`<DATE_FORMAT(ADDDATE(CURRENT_TIME,interval 30 minute),'%H:%i') " +
            "and m.`end`>DATE_FORMAT(ADDDATE(CURRENT_TIME,interval -1 hour),'%H:%i') " +
            "and m.location=?1 and status=1", nativeQuery = true)
    List<Meeting> meetingNow(@Param("location") Integer location);

    /**
     * 今天的所有会议
     *
     * @param location 地点Id
     */
    @Query(value = "select * from yth_meeting m where m.date=UNIX_TIMESTAMP(CURRENT_DATE)*1000 and m.status=1 and m.location=?1", nativeQuery = true)
    List<Meeting> meetingToday(@Param("location") Integer location);

    /**
     * 最新的一条会议
     *
     * @param location 地点Id
     */
    @Query(value = "select * from yth_meeting m where m.location=?1 and m.status=1 order by m.date desc,m.start desc limit 0,1", nativeQuery = true)
    Optional<Meeting> latest(@Param("location") Integer location);

    /**
     * 查询会议
     *
     * @param location 地点Id
     * @param start    开始时间
     * @param end      结束时间
     * @param userId   用户Id
     * @param status   状态
     * @param orgId    单位Id
     * @param st       limit start
     * @param lmt      limit size
     */
    @Query(value = "select distinct m.* from yth_meeting m left join yth_meeting_user r on r.meeting_id=m.id where " +
            "(isnull(?1) or m.location=?1) and " +
            "(isnull(?2) or ?2='' or m.date>=UNIX_TIMESTAMP(STR_TO_DATE(?2,'%Y-%m-%d'))*1000) and " +
            "(isnull(?3) or ?3='' or m.date<=UNIX_TIMESTAMP(STR_TO_DATE(?3,'%Y-%m-%d'))*1000) and " +
            "(isnull(?4) or r.user_id=?4 or m.host=?4) and " +
            "(isnull(?5) or m.org_id=?5) and " +
            "(isnull(?6) or m.status=?6) order by m.date desc,m.start desc LIMIT ?7,?8 ", nativeQuery = true)
    List<Meeting> query(@Param("location") Integer location,
                        @Param("start") String start,
                        @Param("end") String end,
                        @Param("userId") Integer userId,
                        @Param("orgId") Integer orgId,
                        @Param("status") Integer status,
                        @Param("st") Integer st,
                        @Param("lmt") Integer lmt);

    /**
     * 会议查询总数
     *
     * @param location 地点Id
     * @param start    开始时间
     * @param end      结束时间
     * @param userId   用户Id
     * @param status   状态
     * @param orgId    单位Id
     */
    @Query(value = "select count(distinct m.id) from yth_meeting m left join yth_meeting_user r on r.meeting_id=m.id where " +
            "(isnull(?1) or m.location=?1) and " +
            "(isnull(?2) or ?2='' or m.date>=UNIX_TIMESTAMP(STR_TO_DATE(?2,'%Y-%m-%d'))*1000) and " +
            "(isnull(?3) or ?3='' or m.date<=UNIX_TIMESTAMP(STR_TO_DATE(?3,'%Y-%m-%d'))*1000) and " +
            "(isnull(?4) or r.user_id=?4 or m.host=?4) and " +
            "(isnull(?5) or m.org_id=?5) and " +
            "(isnull(?6) or m.status=?6) ", nativeQuery = true)
    int sum(@Param("location") Integer location,
            @Param("start") String start,
            @Param("end") String end,
            @Param("userId") Integer userId,
            @Param("orgId") Integer orgId,
            @Param("status") Integer status);

    /**
     * 本公司所有会议地点
     *
     * @param orgId   公司Id
     * @param keyword 搜索串
     */
    @Query(value = "select p.act_place_id id,p.act_place_name `name`,p.location,p.max_people maxPeople,p.area " +
            "from yth_activity_place p where p.place_type_id='place_type_meeting' " +
            "and (isnull(?1) or p.unit_id=?1) and " +
            "(isnull(?2) or p.act_place_name like CONCAT('%',?2,'%') or p.location like CONCAT('%',?2,'%'))", nativeQuery = true)
    List<Map<String, Object>> places(@Param("orgId") Integer orgId,
                                     @Param("keyword") String keyword);
}
