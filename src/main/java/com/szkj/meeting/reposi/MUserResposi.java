package com.szkj.meeting.reposi;

import com.szkj.meeting.entity.MeetingUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface MUserResposi extends JpaRepository<MeetingUser, Integer> {
    /**
     * 获取某个会议，议题的所有参与者
     *
     * @param meetingId 会议Id
     * @param itemId    议题Id
     */
    @Query(value = " select distinct u.user_id as userId,u.user_name as userName " +
            "from yth_user u,yth_meeting_user r where r.user_id=u.user_id and " +
            "(isnull(?1) or r.meeting_id=?1) and (isnull(?2) or r.item_id=?2) ", nativeQuery = true)
    List<Map<String, Object>> getUser(Integer meetingId, Integer itemId);

    @Modifying
    @Transactional
    @Query(value = "delete from yth_meeting_user where meeting_id=?1 ", nativeQuery = true)
    void deleteUserByMeetingId(Integer meetingId);

    @Modifying
    @Transactional
    @Query(value = "delete from yth_meeting_user where item_id=?1", nativeQuery = true)
    void deleteUserByItemId(Integer itemId);


    @Modifying
    @Transactional
    @Query(value = "delete from yth_meeting_user where meeting_id=?1 and (isnull(?2) or item_id=?2)", nativeQuery = true)
    void deleteUsers(Integer meetingId, Integer itemId);

    @Modifying
    @Transactional
    @Query(value = " delete from yth_meeting_user where meeting_id=?1 and isnull(item_id) and user_id=?2", nativeQuery = true)
    void deleteMeetingUser(Integer meetingId, Integer userId);

    @Modifying
    @Transactional
    @Query(value = " delete  from yth_meeting_user where item_id=?1 and user_id=?2", nativeQuery = true)
    void deleteItemUser(Integer itemId, Integer userId);

}