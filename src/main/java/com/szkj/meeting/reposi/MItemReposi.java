package com.szkj.meeting.reposi;

import com.szkj.meeting.entity.MeetingItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface MItemReposi extends JpaRepository<MeetingItem, Integer> {
    /**
     * 更新所属会议的开始结束时间
     *
     * @param metId 会议Id
     */
    @Transactional
    @Modifying
    @Query(value = "update yth_meeting m " +
            " set m.end=(select max(i.end) from yth_meeting_item i where i.meeting_id = ?1), " +
            " m.start=(select min(i.start) from yth_meeting_item i where i.meeting_id = ?1) " +
            " where m.ID = ?1", nativeQuery = true)
    void updateEndTime(@Param("metId") Integer metId);

    /**
     * 根据会议Id读取议题
     *
     * @param meetingId 会议Id
     */
    @Query(value = "select * from yth_meeting_item m where m.meeting_id = ?1 order by m.start", nativeQuery = true)
    List<MeetingItem> getByMeeting(Integer meetingId);

    @Transactional
    @Modifying
    @Query(value = "delete from yth_meeting_item where meeting_id = ?1", nativeQuery = true)
    void deleteByMeeting(Integer meetingId);
}
