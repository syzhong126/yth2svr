package com.szkj.meeting.reposi;

import com.szkj.meeting.entity.MeetingFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface MFileReposi extends JpaRepository<MeetingFile, Integer> {
    /**
     * 获取某会议/议题附件最大的排序号
     *
     * @param type   类型，0123
     * @param source 会议Id/议题Id
     */
    @Query(value = "select IFNULL(max(sort), 0) FROM `yth_meeting_file` WHERE type=?1 and source_id=?2", nativeQuery = true)
    int getMaxSort(Integer type, Integer source);

    /**
     * 获取某个会议/议题的某类型所有附件
     *
     * @param type     类型，0123
     * @param sourceId 会议Id/议题Id
     */
    @Query(value = "select * from yth_meeting_file t  where (?1<0 or t.type=?1) and t.source_id=?2 order by t.sort ", nativeQuery = true)
    List<MeetingFile> getBySource(int type, int sourceId);

    /**
     * 更新排序号
     *
     * @param id   文件Id
     * @param sort 排序号
     */
    @Modifying
    @Transactional
    @Query(value = "update yth_meeting_file set sort=?2 where id=?1", nativeQuery = true)
    void sort(int id, int sort);

    @Transactional
    @Modifying
    @Query(value = "delete from yth_meeting_file where source_id = ?1 and type in (2,3)", nativeQuery = true)
    void deleteByItem(Integer itemId);

    @Transactional
    @Modifying
    @Query(value = "delete from yth_meeting_file where source_id = ?1 and type in (0,1)", nativeQuery = true)
    void deleteByMeeting(Integer meetingId);
}
