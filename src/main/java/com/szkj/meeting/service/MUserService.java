package com.szkj.meeting.service;

import com.szkj.meeting.entity.MeetingUser;
import com.szkj.meeting.reposi.MUserResposi;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class MUserService {
    @Resource
    MUserResposi userResposi;

    public void saveUsers(List<Map<String, Object>> list, Integer meetingId, Integer itemId) {
        if (list != null) {//更新参会人员
            List<Map<String, Object>> oList = userResposi.getUser(meetingId, itemId);//读取库中已有
            list.forEach(u -> {
                Optional<Map<String, Object>> op = oList.stream().filter(m -> m.get("userId").equals(u.get("userId"))).findFirst();
                if (!op.isPresent()) {//不在已有列表的，新增
                    MeetingUser user = new MeetingUser();
                    user.setMeetingId(meetingId);
                    user.setItemId(itemId);
                    user.setUserId((Integer) u.get("userId"));
                    userResposi.save(user);
                } else oList.remove(op.get());//已有的，从已有列表移除
            });
            //剩下的就是，修改后从人员表移除的
            if (itemId != null) oList.forEach(u -> userResposi.deleteItemUser(itemId, (Integer) u.get("userId")));
            else oList.forEach(u -> userResposi.deleteMeetingUser(meetingId, (Integer) u.get("userId")));
        }
    }

    public List<Map<String, Object>> getUsers(Integer meetingId, Integer itemId) {
        return userResposi.getUser(meetingId, itemId);
    }

    public void deleteUsers(Integer meetingId, Integer itemId) {
        userResposi.deleteUsers(meetingId, itemId);
    }
}
