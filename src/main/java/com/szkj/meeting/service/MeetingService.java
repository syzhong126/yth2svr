package com.szkj.meeting.service;

import cn.hutool.core.util.StrUtil;
import com.szkj.activity.reposi.ActivityPlaceReposi;
import com.szkj.attendance.Util.NoticeUtil;
import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.reposi.UserReposi;
import com.szkj.common.Page;
import com.szkj.knowledge.reposi.YthOrganazitionReposi;
import com.szkj.meeting.entity.Meeting;
import com.szkj.meeting.entity.MeetingItem;
import com.szkj.meeting.reposi.MeetingReposi;
import com.szkj.noticepush.entity.YthNoticePush;
import com.szkj.schedule.entity.YthSchedulePlan;
import com.szkj.schedule.reposi.YthScheduleReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MeetingService {
    @Resource
    MeetingReposi meetingReposi;
    @Resource
    ActivityPlaceReposi placeReposi;
    @Resource
    UserReposi userReposi;
    @Resource
    YthOrganazitionReposi orgReposi;
    @Autowired
    private YthScheduleReposi scheduleReposi;
    @Autowired
    private NoticeUtil noticeUtil;

    /**
     * 读取所有会议场地
     *
     * @param orgId   公司id
     * @param keyword 关键词
     */
    public List<Map<String, Object>> places(Integer orgId, String keyword) {
        return meetingReposi.places(orgId, keyword);
    }

    /**
     * 搜索会议
     *
     * @param userId   用户Id
     * @param location 会议室Id
     * @param start    开始时间
     * @param end      结束时间
     * @param status   状态，编辑0，发布1、取消2
     * @param orgId    公司Id
     * @param pageSize 页条数
     * @param pageNo   页码
     */
    public Page<Meeting> query(Integer userId, Integer location, String start, String end, Integer orgId,
                               Integer status, Integer pageSize, Integer pageNo) {
        Page<Meeting> page = new Page<>();
        page.setPagenum(pageNo);
        page.setPagerows(pageSize);
        page.setTotalrows(meetingReposi.sum(location, start, end, userId, orgId, status));
        List<Meeting> list = meetingReposi.query(location, start, end, userId, orgId, status, pageSize * (pageNo - 1), pageSize);
        page.setRows(list.size());
        page.setData(list);
        list.forEach(this::setName);
        return page;
    }

    /**
     * 现在的会议
     *
     * @param location 会议室Id
     */
    public List<Meeting> meetingNow(Integer location) {
        List<Meeting> list = meetingReposi.meetingNow(location);
        list.forEach(this::setName);
        return list;
    }

    /**
     * 今天的会议
     *
     * @param location 会议室Id
     */
    public List<Meeting> meetingToday(Integer location) {
        List<Meeting> list = meetingReposi.meetingToday(location);
        list.forEach(this::setName);
        return list;
    }


    /**
     * 最新的会议
     *
     * @param location 会议室Id
     */
    public Optional<Meeting> latest(Integer location) {
        Optional<Meeting> optionalMeeting = meetingReposi.latest(location);
        optionalMeeting.ifPresent(this::setName);
        return optionalMeeting;
    }

    /**
     * 主持人名和，所属公司名
     *
     * @param m 会议对象
     */
    private void setName(Meeting m) {
        //设置主持人
        if (m.getHost() != null)
            userReposi.findById(m.getHost()).ifPresent(user -> m.setHostName(user.getUserName()));
        //设置公司
        if (m.getOrgId() != null)
            orgReposi.findById(m.getOrgId()).ifPresent(org -> m.setOrgName(org.getOrgName()));
        //设置地点
        if (m.getLocation() != null)
            placeReposi.findById(m.getLocation()).ifPresent(p -> {
                m.setLocName(p.getActPlaceName());
                m.setLocAddress(p.getLocation());
                m.setLocArea(p.getArea());
                m.setLocMaxPeople(p.getMaxPeople());
            });
    }

    /**
     * 保存会议，附属的参与者会被保存
     *
     * @param m 会议对象
     */
    public void save(Meeting m) {
        meetingReposi.save(m);
        setName(m);
    }

    public void deleteById(Integer id) {
        meetingReposi.deleteById(id);//删除会议
    }

    public Optional<Meeting> getById(Integer id) {
        Optional<Meeting> opM = meetingReposi.findById(id);
        opM.ifPresent(this::setName);
        return opM;
    }

    public void updateSchedule(long start, Meeting m, AuthUserInfoModel userInfo) {
        List<Map<String, Object>> users = m.getUsers() == null ? new ArrayList<>() : new ArrayList<>(m.getUsers());
        //host加入
        if (m.getHost() != null) {
            Map<String, Object> ma = new HashMap<>();
            ma.put("userId", m.getHost());
            ma.put("userName", m.getHostName());
            users.add(ma);
        }
        if (m.getItems() != null)
            for (MeetingItem i : m.getItems())
                if (i.getUsers() != null) users.addAll(i.getUsers());
        log.info("user sum:{},{}", users.size(), users.stream().distinct().count());
        //distinct
        for (Map<String, Object> map : users.stream().distinct().collect(Collectors.toList())) {
            //取得日程对象
            YthSchedulePlan plan = getSchedule(m, (Integer) map.get("userId"), userInfo.getUserId());
            //发布时才会保存
            if (m.getStatus() == Meeting.StatusPublish) {
                scheduleReposi.save(plan);//发布，直接更新/发布
                //发送会议消息
                noticeUtil.save(getNotice(plan, m, userInfo.getAccount()));
                //更新未发消息
                if (!updateNotPushed(start, (Integer) map.get("userId"), plan.getNoticeTime(), n -> {
                    String content = getContent(plan, "新的日程");
                    n.setPushTime(getPushTime(m.getStartTime(), plan.getNoticeTime()));
                    n.setContent(content);
                    n.setDelFlag(0);
                    noticeUtil.save(n);
                })) {
                    //更新失败，新增临近发送的消息
                    YthNoticePush no = getNotice(plan, m, userInfo.getAccount());
                    no.setMoudleType("nearschedule");
                    no.setPushTime(getPushTime(m.getStartTime(), plan.getNoticeTime()));
                    noticeUtil.save(no);
                }
            } else if (m.getStatus() == Meeting.StatusCancel) {//会议取消
                if (plan.getScheduleId() != 0) {
                    scheduleReposi.save(plan);
                    //发送会议取消消息
                    noticeUtil.save(getNotice(plan, m, userInfo.getAccount()));
                    //逻辑删除消息
                    updateNotPushed(start, (Integer) map.get("userId"), plan.getNoticeTime(), n -> {
                        n.setDelFlag(1);
                        noticeUtil.save(n);
                    });
                } else log.info("没发布的会议不能取消：{}", m.getName());
            } else if (plan.getScheduleId() != 0) {//已发布过的会议，要更新日程信息，没有的就不管了
                //edit, delete
                scheduleReposi.save(plan);
                //更新未发消息，有就更新，没有就不管
                updateNotPushed(start, (Integer) map.get("userId"), plan.getNoticeTime(), n -> {
                    //log.info("修改not pushed:{}", n.getId());
                    String content = getContent(plan, "新的日程");
                    n.setPushTime(getPushTime(m.getStartTime(), plan.getNoticeTime()));
                    n.setContent(content);
                    n.setDelFlag(1);
                    noticeUtil.save(n);
                });
            }


        }
    }

    private long getPushTime(long startTime, String noticeTime) {
        return startTime - 60 * Long.parseLong(noticeTime) * 1000;
    }

    private String getContent(YthSchedulePlan plan, String txt) {
        SimpleDateFormat sdf = new SimpleDateFormat(" yyyy年MM月dd日");
        return StrUtil.format("您有一个{}：{}:{}:{}。", txt,
                sdf.format(new Date(plan.getBeginTime() * 1000)), plan.getWorkAdress(), plan.getWorkDesp());
    }

    private interface updateNotPushed {
        void apply(YthNoticePush n);
    }

    private boolean updateNotPushed(long start, Integer userId, String noticeTime, updateNotPushed update) {
        try {
            //获得发送时间
            long time = getPushTime(start, noticeTime);
            //获取还没有发消息的notice
            List<YthNoticePush> notices = noticeUtil.getNoticeNotPush(userId);
            //从中提取会议发布时间吻合的项
            Optional<YthNoticePush> opn = notices.stream().filter(n ->
                    n.getPushTime() == time && n.getMoudleType().equals("nearschedule")).findFirst();
            opn.ifPresent(update::apply);
            //if (!opn.isPresent()) log.info("没找到消息 ");
            return opn.isPresent();
        } catch (NumberFormatException ignored) {
            return false;
        }
    }


    /**
     * 添加日程
     *
     * @param meeting 会议
     * @param target  日程目标
     * @param userId  日程创建者
     * @return 日程对象
     */
    public YthSchedulePlan getSchedule(Meeting meeting, Integer target, Integer userId) {
        long time = System.currentTimeMillis();
        Optional<YthSchedulePlan> opSchedule = scheduleReposi.querySchedule(meeting.getId(), target, "1");
        YthSchedulePlan plan = opSchedule.orElse(new YthSchedulePlan());
        plan.setBeginTime(meeting.getStartTime() / 1000).setEndTime(meeting.getEndTime() / 1000)
                .setDesignStatus(meeting.getStatus() != Meeting.StatusPublish ? 3 : 0)
                .setWorkDesp(meeting.getName()).setActiveId(meeting.getId() + "").setWorkAdress(meeting.getLocName())
                .setImportantStatus(1).setNoticeTime("30").setUserId(target)
                .setDesignNote("").setCalendarType("1");
        if (opSchedule.isPresent()) plan.setUpdateBy(userId).setUpdateTime(time);
        else plan.setCreateBy(userId).setCreateTime(time);
        return plan;
    }

    /**
     * 添加系统消息
     *
     * @param plan    日程
     * @param meeting 会议
     * @param account 创建者账号
     * @return 系统消息
     */
    public YthNoticePush getNotice(YthSchedulePlan plan, Meeting meeting, String account) {
        switch (meeting.getStatus()) {
            case Meeting.StatusPublish:
                return noticeUtil.getMeetingNotice(account, "新的日程", getContent(plan, "新的日程"),
                        plan.getUserId(), "newschedule");
            case Meeting.StatusCancel:
                return noticeUtil.getMeetingNotice(account, "日程取消", getContent(plan, "日程取消"),
                        plan.getUserId(), "deleteschedule");
            default:
                return noticeUtil.getMeetingNotice(account, "日程撤回修改", getContent(plan, "日程被撤回修改"),
                        plan.getUserId(), "changeschedule");
        }
    }
}
