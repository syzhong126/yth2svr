package com.szkj.meeting.service;

import com.szkj.meeting.entity.MeetingItem;
import com.szkj.meeting.reposi.MItemReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Slf4j
@Service
public class MItemService {
    @Autowired
    MItemReposi itemReposi;

    /**
     * 议题列表
     *
     * @param meetingId 会议Id
     */
    public List<MeetingItem> getByMeeting(Integer meetingId) {
        return itemReposi.getByMeeting(meetingId);
    }

    public void deleteById(Integer itemId) {
        itemReposi.deleteById(itemId);
    }


    public void save(MeetingItem m) {
        itemReposi.save(m);
    }

    public void updateEndTime(Integer metId) {
        itemReposi.updateEndTime(metId);
    }

    public Optional<MeetingItem> getById(Integer id) {
        return itemReposi.findById(id);
    }
}
