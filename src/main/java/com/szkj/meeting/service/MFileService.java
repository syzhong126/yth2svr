package com.szkj.meeting.service;

import cn.hutool.core.util.StrUtil;
import com.szkj.meeting.entity.Meeting;
import com.szkj.meeting.entity.MeetingFile;
import com.szkj.meeting.entity.MeetingItem;
import com.szkj.meeting.reposi.MFileReposi;
import com.szkj.meeting.reposi.MItemReposi;
import com.szkj.meeting.reposi.MeetingReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Service
public class MFileService {
    @Autowired
    MFileReposi reposi;
    @Autowired
    MItemReposi itemReposi;
    @Autowired
    MeetingReposi meetingReposi;
    @Value(value = "${upload.file.save.local.path}")
    private String uploadpath;

    /**
     * 从源查找附件
     *
     * @param type     类型0123
     * @param sourceId 会议/议题
     */
    public List<MeetingFile> getBySource(int type, int sourceId) {
        if (type >= MeetingFile.File_Type_Annex) {
            Optional<MeetingItem> opi = itemReposi.findById(sourceId);
            if (opi.isPresent()) {
                return getBySource(type, opi.get().getMeetingId(), opi.get().getId());
            } else return new ArrayList<>();
        } else return getBySource(type, sourceId, 0);
    }

    /**
     * 从源查找附件
     * 给附件对象添加下载文件路径
     *
     * @param type      类型0123
     * @param meetingId 会议Id
     * @param itemId    议题Id
     */
    public List<MeetingFile> getBySource(int type, int meetingId, int itemId) {
        //设置path
        //String path = "/meeting/file/down/" + meetingId + "/";
        //if (type >= MeetingFile.File_Type_Annex) path += itemId + "/";
        //设置sourceid
        int sourceId = type >= MeetingFile.File_Type_Annex ? itemId : meetingId;
        List<MeetingFile> list = reposi.getBySource(type, sourceId);
        //String finalPath = "/minio/file_download?filename=";// path;
        list.forEach(f -> f.setPath("/minio/file_download?filename=" + f.getStoreName()));
        return list;
    }

    public String getUrl(int type, int sourceId, String StoreName) {
        AtomicReference<String> path = new AtomicReference<>();
        if (sourceId < 0) path.set(StrUtil.format("/meeting/file/down/{}/{}", "temp", StoreName));
        else if (type < MeetingFile.File_Type_Annex)
            path.set(StrUtil.format("/meeting/file/down/{}/{}", sourceId, StoreName));
        else itemReposi.findById(sourceId).ifPresent(i ->
                    path.set(StrUtil.format("/meeting/file/down/{}/{}/{}", i.getMeetingId(), i.getId(), StoreName)));
        return path.get();
    }

    public void moveFile(MeetingFile f, Integer type, Integer id) {
        try {
            //int s = f.getSourceId();
            f.setType(type);
            f.setSourceId(id);//设置信息源id
            reposi.save(f);//可能sort已更新，所以每次必须更新文件对象
            f.setPath("/minio/file_download?filename=" + f.getStoreName());
            //if (Objects.equals(id, s)) return;//已经在议题目录下不需要搬迁
            //log.info("move file : {}", f.getFileName());
            /*String source = String.join(File.separator, getPath(f.getType(), -1), f.getStoreName());//获取源路径
            File file = new File(source);//找到源文件
            if (file.exists()) if (!file.renameTo(getFile(f)))//搬迁
                log.info("移动文件失败:{}", source);
            f.setPath(f.getPath().replace("temp", String.valueOf(f.getSourceId())));//文件路径重置*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除会议，议题的附件目录
     *
     * @param meetingId 会议ID
     * @param itemId    议题Id
     */
    public void delFile(Integer meetingId, Integer itemId) {
        if (itemId == null) reposi.deleteByMeeting(meetingId);//删除会议文件信息
        else reposi.deleteByItem(itemId);//删除议题文件信息
        //delFile(getDir(meetingId, itemId));//删除会议/议题附件文件夹
    }

    private void delFile(String path) {
        File file = new File(path);
        if (file.exists())
            if (file.isFile()) if (!file.delete())//是文件，删除文件
                log.info("删除文件异常，这都能出问题：{}", file.getPath());
            else if (file.isDirectory()) {//是目录
                File[] fs = file.listFiles();
                if (fs != null) for (File f : fs) delFile(f.getPath());//删除目录下文件
                if (!file.delete())//删除目录
                    log.info("删除目录异常，这都能出问题：{}", file.getPath());
            }
    }

    /**
     * 当会议保存，会议上传的临时文件会移入会议或者议题文件夹，其他的必须清除
     * 清理上传的临时文件
     */
    public void clearTemp() {
        reposi.getBySource(-1, -1).forEach(f -> delete(f.getId()));
    }

    /**
     * 保存文件，会议/议题下，某类型的文件，根据新的list，原来没有的搬迁过去，原来有现在没有的删除
     *
     * @param type     附件类型
     * @param sourceId 源Id
     * @param list     新的list
     */
    public void saveFiles(Integer type, Integer sourceId, List<MeetingFile> list) {
        if (list == null) return;
        //log.info("type:{},sourceId:{}", type, sourceId);
        List<MeetingFile> oList = reposi.getBySource(type, sourceId);//读取库中已有
        if (oList == null) {
            //log.info("oList null");//不在已有列表的，迁移过去
            list.forEach(f -> moveFile(f, type, sourceId));
        } else {
            list.forEach(f -> {
                Optional<MeetingFile> op = oList.stream().filter(m -> m.getId().equals(f.getId())).findFirst();
                if (!op.isPresent()) moveFile(f, type, sourceId);//不在已有列表的，迁移过去
                else oList.remove(op.get());//已有的，从已有列表移除
            });
            //剩下的就是，修改后从人员表移除的
            oList.forEach(u -> delete(u.getId()));
        }
    }


    /**
     * 上传文件
     *
     * @param mf       文件对象
     * @param type     文档类型
     * @param sourceId 所属会议或议题
     */
    public MeetingFile uploadLocal(MultipartFile mf, int type, int sourceId) {
        try {
            String path = getPath(type, sourceId);
            // 获取文件名
            String fileName = mf.getOriginalFilename();
            if (fileName == null) {
                log.error("上传文件为空");
                return null;
            }
            //设置文件名
            String storeName;
            if (fileName.indexOf(".") > 0)
                storeName = System.currentTimeMillis() + fileName.substring(fileName.lastIndexOf("."));
            else storeName = String.valueOf(System.currentTimeMillis());
            //保存数据对象
            MeetingFile f = new MeetingFile();
            f.setType(type);
            f.setSourceId(sourceId);
            f.setFileName(fileName);
            f.setStoreName(storeName);
            f.setSort(getMaxSort(type, sourceId) + 1);
            f.setDate(System.currentTimeMillis());
            save(f);
            f.setPath(getUrl(type, sourceId, storeName));
            //保存文件
            String savePath = String.join(File.separator, path, storeName);
            FileCopyUtils.copy(mf.getBytes(), new File(savePath));
            return f;
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public void down(Integer id, HttpServletResponse response) {
        Optional<MeetingFile> opf = getById(id);
        if (opf.isPresent()) {
            MeetingFile mFile = opf.get();
            try {
                String filePath = String.join(File.separator, getPath(mFile.getType(), mFile.getSourceId()), mFile.getStoreName());
                downLoad(response, mFile.getFileName(), filePath);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        } else {
            response.setStatus(404);
            log.error("文件ID[" + id + "]不存在..");
        }
    }

    /**
     * 下文件
     *
     * @param fileName 文件名
     * @param filePath 文件路径
     */
    public void downLoad(HttpServletResponse response, String fileName, String filePath) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                response.setStatus(404);
                log.error("文件[" + fileName + "]不存在..");
                return;
            }
            // 设置强制下载不打开
            response.setContentType("application/force-download");
            response.addHeader("Content-Disposition", "attachment;fileName=" + fileName);
            //new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1)
            inputStream = new BufferedInputStream(new FileInputStream(filePath));
            outputStream = response.getOutputStream();
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, len);
            }
            response.flushBuffer();
        } catch (Exception e) {
            log.error("下载文件失败：" + e.getMessage());
            response.setStatus(404);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * 获取文件保存路径
     *
     * @param type     文档类型
     * @param sourceId 所属会议或议题
     */
    public String getPath(int type, int sourceId) throws Exception {
        String path;
        if (sourceId == -1) {
            path = String.join(File.separator, uploadpath, "meeting", "temp");
        } else if (type >= MeetingFile.File_Type_Annex) {
            Optional<MeetingItem> opItem = itemReposi.findById(sourceId);
            if (!opItem.isPresent()) {
                log.error("错误的议题ID：{}", sourceId);
                throw new Exception("错误的议题ID");
            }
            path = String.join(File.separator, uploadpath, "meeting", Integer.toString(opItem.get().getMeetingId()), Integer.toString(sourceId));
            //uploadpath + File.separator + "meeting" + File.separator + item.getMeetingId() + File.separator + sourceId + File.separator;
        } else {
            Optional<Meeting> met = meetingReposi.findById(sourceId);
            if (!met.isPresent()) {
                log.error("错误的会议ID：{}", sourceId);
                throw new Exception("错误的会议ID");
            }
            path = String.join(File.separator, uploadpath, "meeting", Integer.toString(sourceId));
        }
        File file = new File(path);
        if (!file.exists()) if (!file.mkdirs())
            log.error("创建目录失败：{}", path);
        return path;
    }

    /**
     * 附件存储目录
     *
     * @param meetingId 会议Id
     * @param itemId    议题Id 可null
     */
    public String getDir(Integer meetingId, Integer itemId) {
        String path = String.join(File.separator, getBasePath(), Integer.toString(meetingId));
        if (itemId != null) path = String.join(File.separator, path, Integer.toString(itemId));
        return path;
    }

    /**
     * 读取会议文件存储的根目录
     */
    public String getBasePath() {
        return String.join(File.separator, uploadpath, "meeting");
    }

    /**
     * 某源某类附件最大sort
     *
     * @param type     欸写
     * @param sourceId 源
     */
    public int getMaxSort(Integer type, Integer sourceId) {
        return reposi.getMaxSort(type, sourceId);
    }

    public void deleteByItem(Integer itemId) {
        reposi.deleteByItem(itemId);
    }

    public void deleteByMeeting(Integer meetingId) {
        reposi.deleteByMeeting(meetingId);
    }

    public boolean delete(Integer id) {
        AtomicBoolean r = new AtomicBoolean(false);
        reposi.findById(id).ifPresent(f -> {
            /*File file = getFile(f);
            if (file.exists() && !file.delete()) {
                log.error("删除文件失败：{}", file.getName());
                r.set(false);
            }*/
            reposi.deleteById(id);
            r.set(true);
        });
        return r.get();
    }

    public File getFile(MeetingFile f) {
        try {
            return new File(String.join(File.separator, getPath(f.getType(), f.getSourceId()), f.getStoreName()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void sort(int id, int sort) {
        reposi.sort(id, sort);
    }

    public void save(MeetingFile f) {
        reposi.save(f);
    }

    public void saveAll(Iterable<MeetingFile> fs) {
        reposi.saveAll(fs);
    }

    public void update(MeetingFile f) {
        reposi.save(f);
    }

    public void removeById(Integer id) {
        reposi.deleteById(id);
    }

    public Optional<MeetingFile> getById(Integer id) {
        return reposi.findById(id);
    }
}
