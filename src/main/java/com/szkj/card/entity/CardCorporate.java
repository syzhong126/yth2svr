package com.szkj.card.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * (YthCardCorporate)实体类
 *
 * @author makejava
 * @since 2022-04-22 15:55:05
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_card_corporate")
public class CardCorporate implements Serializable {
    private static final long serialVersionUID = -27700775424334850L;
    /**
     * 主键，外键关联公司表
     */
    @Id
    @Column(name = "org_id", nullable = false)
    private String orgId;
    @Basic
    @Column(name = "org_Name")
    private String orgName;
    @Column(name = "path")
    private String path;
    /**
     * 地址
     */
    @Basic
    @Column(name = "address")
    private String address;
    /**
     * 公司简称
     */
    @Column(name = "short_name")
    private String shortName;
    /**
     * 公司简称英文名
     */
    @Column(name = "short_name_en")
    private String shortNameEn;
    /**
     * 公司主页
     */
    @Column(name = "url")
    private String url;
    @Basic
    @Column(name = "del_flag")
    private Boolean delFlag = false;
}

