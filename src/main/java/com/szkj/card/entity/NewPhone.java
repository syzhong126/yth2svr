package com.szkj.card.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * (YthUserNewPhoneNum)实体类
 *
 * @author makejava
 * @since 2022-05-13 10:16:33
 */
@Data
@Entity
@Table(name = "yth_user_new_phone_num")
public class NewPhone implements Serializable {
    private static final long serialVersionUID = 701045896760295219L;
    /**
     * 状态已处理
     */
    public final static Integer StatusNotDeal = 1;
    /**
     * 状态没有处理
     */
    public final static Integer StatusIsDeal = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sys_phone_id", nullable = false)
    private Integer sysPhoneId;
    /**
     * 用户id
     */
    @Column(name = "account")
    private String account;
    /**
     * 新手机号
     */
    @Column(name = "new_phone_num")
    private String newPhoneNum;
    /**
     * 是否已同步处理(1：未处理，2：已处理)
     */
    @Column(name = "sys_status")
    private Integer sysStatus;
    @Column(name = "create_time")
    private Long createTime;
    @Column(name = "update_by")
    private Integer updateBy;
    @Column(name = "update_time")
    private Long updateTime;
}

