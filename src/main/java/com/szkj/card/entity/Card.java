package com.szkj.card.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * (YthCard)实体类
 *
 * @author makejava
 * @since 2022-04-21 17:31:53
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_card")
public class Card implements Serializable {
    private static final long serialVersionUID = -65891480931204616L;
    /**
     * 主键，外键，关联关系表
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_id", nullable = false)
    private Integer cardId;
    @Column(name = "account")
    private String account;
    @Column(name = "org")
    private String org;
    @Column(name = "dept")
    private String dept;
    @Column(name = "post")
    private String post;
    /**是否主要名片 */
    @Basic
    @Column(name = "is_main")
    private Boolean isMain = true;
    /** base64URL串*/
    @Basic
    @Column(name = "img")
    private String img;
    /** 逻辑删除*/
    @Basic
    @Column(name = "del_flag")
    private Boolean delFlag = false;
}

