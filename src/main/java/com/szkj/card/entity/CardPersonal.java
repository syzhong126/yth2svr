package com.szkj.card.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * (YthCardPersonal)实体类
 *
 * @author makejava
 * @since 2022-04-22 15:55:10
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "yth_card_personal")
public class CardPersonal implements Serializable {
    private static final long serialVersionUID = -56077521662991691L;
    /**用户ID，外键关联用户表 */
    @Id
    @Column(name = "account", nullable = false)
    private String account;
    @Column(name = "name")
    private String name;
    /** 邮箱*/
    @Column(name = "email")
    private String email;
    @Column(name = "mobile")
    private String mobile;
    /** 办公电话 */
    @Column(name = "phone")
    private String phone;
    /** QQ*/
    @Column(name = "qq")
    private String qq;
    /**头像*/
    @Column(name = "head")
    private String head;
    @Column(name = "address")
    private String address;
    @Column(name = "url")
    private String url;
    @Column(name = "del_flag")
    private Boolean delFlag = false;
}

