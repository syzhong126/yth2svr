package com.szkj.card.controller;

import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.card.entity.Card;
import com.szkj.card.service.CardService;
import com.szkj.common.util.RespUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;


@Slf4j
@RestController
@RequestMapping("/card")
public class CardController {
    @Autowired
    private CardService service;

    @GetMapping("/img/{filename}")
    public void img(@PathVariable String filename, HttpServletResponse response) {
        int id;
        try {
            id = Integer.parseInt(filename.substring(0, filename.indexOf(".")));
        } catch (NumberFormatException e) {
            response.setStatus(404);
            return;
        }
        Card card = service.queryById(id);
        if (card == null || card.getImg() == null) {
            response.setStatus(404);
            return;
        }
        OutputStream outputStream = null;
        try {
            //设置消息头和文件名
            response.setContentType("application/force-download");
            response.addHeader("Content-Disposition", "attachment;fileName=" + filename);
            //new String(filename.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1)
            //读取img数据
            String base64Str = card.getImg().substring(card.getImg().indexOf("base64,") + 7);
            //.replaceFirst("data:image/png;base64,", "");
            BASE64Decoder decoder = new BASE64Decoder();
            // Base64解码
            byte[] bytes = decoder.decodeBuffer(base64Str);
            for (int i = 0; i < bytes.length; ++i)
                if (bytes[i] < 0) bytes[i] += 256;// 调整异常数据

            outputStream = response.getOutputStream();
            outputStream.write(bytes);
            response.flushBuffer();
        } catch (Exception e) {
            log.error("预览文件失败" + e.getMessage());
            response.setStatus(404);
        } finally {
            try {
                if (outputStream != null) outputStream.close();
            } catch (IOException ignored) {
            }
        }
    }

    @PostMapping("/save")
    public String save(@RequestBody Map<String, Object> card,
                       @ModelAttribute("authUserInfo") AuthUserInfoModel userInfo) {
        try {
            service.save(card, userInfo);
            return RespUtil.success("成功", "");
        } catch (Exception e) {
            return RespUtil.error(e.getMessage());
        }
    }
}

