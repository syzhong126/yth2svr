package com.szkj.card.controller;

import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.card.entity.NewPhone;
import com.szkj.card.service.NewPhoneService;
import com.szkj.common.util.RespUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 控制层
 *
 * @author makejava
 * @since 2022-05-13 10:16:33
 */
@RestController
@RequestMapping("/card/phone")
public class NewPhoneController {
    @Autowired
    private NewPhoneService service;


    /**
     * 提交新手机号
     *
     * @param mobile 手机号
     * @param code   验证码
     * @return 结果
     */
    @GetMapping("/newPhone")
    public String add(@RequestParam String mobile,
                      @RequestParam String code, @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        //验证，验证码
        if (!service.verify(mobile, code))
            return RespUtil.raw(2311, "手机号或者验证码有错误", null);
        // 读取信息
        Optional<NewPhone> op = service.getNotDeal(userinfo.getAccount());
        //设置信息
        NewPhone newPhone = op.orElse(new NewPhone());
        newPhone.setNewPhoneNum(mobile);
        newPhone.setSysStatus(NewPhone.StatusNotDeal);
        newPhone.setAccount(userinfo.getAccount());
        newPhone.setCreateTime(System.currentTimeMillis());
        //保存
        service.save(newPhone);
        return RespUtil.success("成功", "");
    }

    /**
     * 处理用户修改手机号
     *
     * @param account 用户账号
     * @return 处理结果
     */
    @GetMapping("/deal")
    public String deal(@RequestParam String account,
                       @ModelAttribute("authUserInfo") AuthUserInfoModel userinfo) {
        Optional<NewPhone> op = service.getNotDeal(account);
        if (op.isPresent()) {
            NewPhone phone = op.get();
            phone.setSysStatus(NewPhone.StatusIsDeal);
            phone.setUpdateBy(userinfo.getUserId());
            phone.setUpdateTime(System.currentTimeMillis());
            service.save(phone);
            return RespUtil.success("成功", null);
        } else
            return RespUtil.raw(2312, "该用户没有信息需要处理", null);
    }


    /**
     * 发送验证码
     */
    @GetMapping("/sendCode")
    public String update(@RequestParam String mobile) {
        String r = service.sendVerifyCode(mobile);
        if (r.startsWith("OK"))
            //return RespUtil.success("成功", r.replace("OK:", ""));
            return RespUtil.success("成功", null);
        else return RespUtil.raw(2313, "发送验证码失败", r);
    }

}

