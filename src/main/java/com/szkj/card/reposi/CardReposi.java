package com.szkj.card.reposi;


import com.szkj.card.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-21 17:31:58
 */
public interface CardReposi extends JpaRepository<Card, Integer> {

    @Modifying
    @Transactional
    @Query(value = "update yth_card set is_main=false where account=?1", nativeQuery = true)
    void setNotMain(@Param("account") String account);

    @Query(value = "select * from yth_card where is_main=true and account=?1", nativeQuery = true)
    Optional<Card> getMainCard(@Param("account") String account);

    @Modifying
    @Transactional
    @Query(value = "update yth_card set is_main=false,del_flag=true where account=?1", nativeQuery = true)
    void deleteAll(@Param("account") String account);
}

