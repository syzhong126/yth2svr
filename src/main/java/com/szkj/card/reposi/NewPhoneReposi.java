package com.szkj.card.reposi;


import com.szkj.card.entity.NewPhone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * 持久层
 *
 * @author makejava
 * @since 2022-05-13 10:16:34
 */
public interface NewPhoneReposi extends JpaRepository<NewPhone, Integer> {

    @Query(nativeQuery = true, value = "select * from yth_user_new_phone_num p where p.account=?1 and p.sys_status=1 ")
    Optional<NewPhone> getNotDeal(@Param("account") String account);
}

