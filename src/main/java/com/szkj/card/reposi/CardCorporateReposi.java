package com.szkj.card.reposi;


import com.szkj.card.entity.CardCorporate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 持久层
 *
 * @author makejava
 * @since 2022-04-24 12:45:11
 */
public interface CardCorporateReposi extends JpaRepository<CardCorporate, String> {

    @Transactional
    @Modifying
    @Query(value = "update yth_card_corporate t set t.del_flag=true where t.org_name in (?1)", nativeQuery = true)
    void delete(List<String> names);
}

