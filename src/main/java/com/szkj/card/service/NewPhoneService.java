package com.szkj.card.service;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import com.szkj.card.entity.NewPhone;
import com.szkj.card.reposi.NewPhoneReposi;
import com.szkj.common.util.AliSmsUtil;
import com.szkj.common.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.Optional;

/**
 * (YthUserNewPhoneNum)表服务实现类
 *
 * @author makejava
 * @since 2022-05-13 10:16:34
 */
@Slf4j
@Service
public class NewPhoneService {
    @Autowired
    private NewPhoneReposi reposi;

    @Value("${alisms.accessKeyId:LTAI5tQg6KCTVoyqCFg8Us22}")
    private String ALISMS_ACCESSKEYID;
    @Value("${alisms.secred:E7NcF9VSn31k6NrPhDYg31GBzJSlea}")
    private String ALISMS_SECRED;
    @Value("${alisms.signName:长沙城发集团}")
    private String ALISMS_SIGNNAME;
    @Value("${alisms.template.mobile:SMS_241346597}")
    private String ALISMS_TEMPLATE_CODE_MOBILE;

    private static final String redisPrefix = "yth2svr_";

    public String sendVerifyCode(String mobile) {
        // 生成验证吗
        String verifyCode = String.valueOf(RandomUtil.randomInt(1000, 9999));
        JSONObject _params = new JSONObject();
        _params.putOpt("no", verifyCode);
        // 发送短信
        String _result = sendVMessage(mobile, _params.toString(), ALISMS_TEMPLATE_CODE_MOBILE);
        //String _result = "OK";
        //保存Redis
        if ("OK".equals(_result)) {
            try (Jedis jedis = RedisUtil.getJedis()) {
                //log.info("mobile:{},verifyCode:{}", mobile, verifyCode);
                jedis.set(redisPrefix + "verifyCode_" + mobile, verifyCode);
                jedis.expire(redisPrefix + "verifyCode_" + mobile, 120);
                //_result += ":" + verifyCode;
            }
        }
        return _result;
    }

    private String sendVMessage(String mobile, String message, String tempCode) {
        return AliSmsUtil.sendSms(mobile, tempCode, message, ALISMS_SIGNNAME, ALISMS_ACCESSKEYID, ALISMS_SECRED);
    }

    public boolean verify(String mobile, String verifyCode) {
        try (Jedis jedis = RedisUtil.getJedis()) {
            return verifyCode.equals(jedis.get(redisPrefix + "verifyCode_" + mobile));
        }
    }

    public Optional<NewPhone> getNotDeal(String account) {
        return reposi.getNotDeal(account);
    }

    /**
     * 保存数据，有ID修改，无ID新增
     *
     * @param newPhone 实例对象
     */
    public void save(NewPhone newPhone) {
        reposi.save(newPhone);
    }

}
