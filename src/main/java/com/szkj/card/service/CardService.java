package com.szkj.card.service;

import com.szkj.auth.model.AuthUserInfoModel;
import com.szkj.auth.reposi.UserReposi;
import com.szkj.card.entity.Card;
import com.szkj.card.entity.CardCorporate;
import com.szkj.card.entity.CardPersonal;
import com.szkj.card.reposi.CardCorporateReposi;
import com.szkj.card.reposi.CardPersonalReposi;
import com.szkj.card.reposi.CardReposi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * (YthCard)表服务实现类
 *
 * @author makejava
 * @since 2022-04-21 17:32:04
 */
@Slf4j
@Service
public class CardService {
    @Autowired
    private CardReposi reposi;
    @Autowired
    private CardCorporateReposi creposi;
    @Autowired
    private CardPersonalReposi preposi;
    @Autowired
    UserReposi userReposi;

    /**
     * 通过ID查询单条数据
     *
     * @param cardId 主键
     * @return 实例对象
     */
    public Card queryById(Integer cardId) {
        return reposi.findById(cardId).orElse(null);
    }

    public void save(Map<String, Object> card, AuthUserInfoModel userInfo) throws Exception {
        Card c;
        if (card.get("cardId") == null) c = new Card().setAccount(userInfo.getAccount());
        else {
            Optional<Card> opc = reposi.findById((int) card.get("cardId"));
            if (opc.isPresent()) {
                c = opc.get();
                if (!c.getAccount().equals(userInfo.getAccount()) || (card.get("account") != null && !card.get("account").toString().equals(userInfo.getAccount()))) {
                    log.error("保存名片与当前账号不符，可能正遭受攻击。");
                    throw new Exception("保存名片与当前账号不符");
                }
            } else throw new Exception("cardId错误");
        }
        CardPersonal cp = preposi.findById(userInfo.getAccount()).orElse(new CardPersonal().setAccount(userInfo.getAccount()).setName(userInfo.getUserName()));
        if (card.get("phone") != null) cp.setPhone(card.get("phone").toString());
        if (card.get("email") != null) cp.setEmail(card.get("email").toString());
        if (card.get("qq") != null) cp.setQq(card.get("qq").toString());
        if (card.get("head") != null) cp.setHead(card.get("head").toString());
        if (card.get("address") != null && !card.get("address").equals("长沙市岳麓区先导路179号湘江时代广场"))
            cp.setAddress(card.get("address").toString());
        if (card.get("url") != null && !card.get("url").equals("https://www.csudgroup.com/"))
            cp.setAddress(card.get("url").toString());
        preposi.save(cp);
        if (card.get("deptName") != null) c.setDept(card.get("deptName").toString());
        if (card.get("orgName") != null) c.setOrg(card.get("orgName").toString());
        if (card.get("postName") != null) c.setPost(card.get("postName").toString());
        if (card.get("img") != null) c.setImg(card.get("img").toString());
        if (card.get("isMain") != null) c.setIsMain((boolean) card.get("isMain"));
        if (card.get("delFlag") != null) c.setDelFlag((boolean) card.get("delFlag"));
        reposi.save(c);
    }

    public void save(List<Card> cards) {
        cards.forEach(card -> {
            Optional<Card> opcp = reposi.findById(card.getCardId());
            if (opcp.isPresent()) {
                Card cp = opcp.get();
                BeanUtils.copyProperties(card, cp, "isMain", "img", "delFlag");
                reposi.save(cp);
            } else reposi.save(card);
        });
    }

    public void save(CardPersonal ythCard) {
        Optional<CardPersonal> opcp = preposi.findById(ythCard.getAccount());
        opcp.ifPresent(cp -> {
            BeanUtils.copyProperties(ythCard, cp, "email", "phone", "qq", "head");
            preposi.save(cp);
        });
    }

    public void save(CardCorporate ythCard) {
        Optional<CardCorporate> opcp = creposi.findById(ythCard.getOrgId());
        opcp.ifPresent(cp -> {
            BeanUtils.copyProperties(ythCard, cp, "address", "url");
            creposi.save(cp);
        });
    }

}
